/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.sms.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.mashape.unirest.http.JsonNode;

import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ConfigProperty;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.common.util.SmsSender;
import kt.com.im.store.common.util.Validator;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.member.web.MemberApi;
import kt.com.im.store.qna.service.QnaService;
import kt.com.im.store.qna.vo.QnaVO;
import kt.com.im.store.sms.service.SmsService;
import kt.com.im.store.sms.vo.SmsVO;

/**
 *
 * SMS 발송 관련 처리 API
 *
 * @author A2TEC
 * @since 2018.09.04
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 9. 04.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class SmsApi {

    static public final String TYPE_QNA_REGIST = "regQna";

    static public final String MSG_TITLE = "GiGA Live On";
    static public final String MSG_REGIST_QNA = "[" + MSG_TITLE + " OMS]\r\n문의사항이 등록되었습니다. OMS 고객센터>문의사항에서 확인하세요.";

    @Resource(name="SmsService")
    private SmsService smsService;

    @Resource(name="QnaService")
    private QnaService qnaService;

    @Resource(name="transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Autowired
    private ConfigProperty configProperty;

    @RequestMapping(value = "/api/sms")
    public ModelAndView smsProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("jsonView");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String type = request.getParameter("type");
            if (TYPE_QNA_REGIST.equalsIgnoreCase(type)) {
                return this.sendSmsForQna(request, rsModel, userVO);
            }

            return this.sendSms(request, rsModel, userVO);
        }

        rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView sendSms(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) {
        boolean isAdministrator = MemberApi.MEMBER_SECTION_MASTER.equals(userVO.getMbrSe()) || MemberApi.MEMBER_SECTION_CMS.equals(userVO.getMbrSe());
        if (!isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "destPhone:{" + Validator.PHONE_NO + "};" + "destName:{" + Validator.NAME + "};"
                + "msgBody:{" + Validator.MAX_LENGTH + "=1000}";

        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        SmsVO smsVo = new SmsVO();
        String destPhone = request.getParameter("destPhone");
        String[] phonList = destPhone.split(";");

        smsVo.setMsgBody(request.getParameter("msgBody"));
        smsVo.setMbrId(userVO.getMbrId());
        smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            for (int i = 0; i < phonList.length; i++) {
                destPhone = phonList[i].replaceAll("-", "").trim();
                smsVo.setDestPhone(destPhone);

                JsonNode sendResult = SmsSender.send(smsVo);

                String resultCode = (String) sendResult.getObject().get("result_code");
                smsVo.setResultCode(resultCode);
                smsService.insertSentSmsInfo(smsVo);

                transactionManager.commit(status);

                if (!"200".equals(resultCode)) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
            }
        } catch (Exception e) {
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView sendSmsForQna(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) {
        if (!usesAutoSend()) {
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            return rsModel.getModelAndView();
        }

        String validateParams = "qnaSeq:{" + Validator.NUMBER + "}";

        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        int qnaSeq = Integer.parseInt(request.getParameter("qnaSeq"));
        QnaVO searchQnaVo = new QnaVO();
        searchQnaVo.setQnaSeq(qnaSeq);
        QnaVO qnaVo = qnaService.qnaDetail(searchQnaVo);
        if (qnaVo == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(qnaSeq)");
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            SmsVO searchVO = new SmsVO();
            String[] statusList = { MemberApi.MEMBER_STATUS_ENABLE, MemberApi.MEMBER_STATUS_BLOCKED };
            searchVO.setStatusList(statusList);
            searchVO.setConnectUrl(configProperty.getProperty("sms.connect.url"));

            String[] sectionList = { MemberApi.MEMBER_SECTION_MASTER, MemberApi.MEMBER_SECTION_CMS };
            searchVO.setSectionList(sectionList);

            List<SmsVO> destList = smsService.getSmsDestnationList(searchVO);
            for (int i = 0; i < destList.size(); i++) {
                SmsVO smsVo = destList.get(i);
                smsVo.setMsgBody(MSG_REGIST_QNA);
                smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));
                smsVo.setSendDesc("[QNA] NO." + qnaSeq);
                smsVo.setMbrId(userVO.getMbrId());

                String[] phonList = smsVo.getDestPhone().split(";");

                for (int j = 0; j < phonList.length; j++) {
                    smsVo.setDestPhone(phonList[j].replaceAll("-", "").trim());
                    JsonNode sendResult = SmsSender.send(smsVo);
                    String resultCode = (String) sendResult.getObject().get("result_code");
                    smsVo.setResultCode(resultCode);
                    smsService.insertSentSmsInfo(smsVo);
                }
            }
            transactionManager.commit(status);
        } catch (Exception e) {
            transactionManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                transactionManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean usesAutoSend() {
        return "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.sms"));
    }
}
