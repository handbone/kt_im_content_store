<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<input id="memberSec" type="hidden" value="${memberSec}" />
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/mypage/MyPageDetail.js?version=201806081710"></script>

<input type="hidden" id="seq" value="${seq}">

        <!-- right contents S -->
        <div id="contents">

        <div class="boxRound">

                <!-- locate S -->
                <div id="locateGrp">
                    <div class="locate">
                        <ul>
                        </ul>
                    </div>
                </div>
                <!-- locate E -->
                
                <!-- Tab S --> 

                <div id="tabGrp">
                    <div class="tab tabOn tabEnd"><spring:message code='member.detail.modify'/></div>
                </div>
                <!-- Tab E -->


                <!-- Table Area S -->
                <div class="tableArea">
                    <table class="tableDetail" style="position:relative;">
                        <tr>
                            <th class="w150"><spring:message code='login.input.id'/></th>
                            <td><span id="mbrId"></span><c:if test="${memberSec == '06'}"><span style="margin-left:100px;"><input id="btnDelete" type="button" class="btnSmallWhite btnUserGray" value="<spring:message code='button.member.delete'/>"></span></c:if></td>
                        </tr>
                        <tr>
                            <th class="w150"><spring:message code='login.input.name'/></th>
                            <td id="mbrNm"></td>
                        </tr>
                        <tr>
                            <th><spring:message code='login.input.password'/></th>
                            <td>
                                <span><input id="btnPwd" type="button" class="btnSmallWhite btnModifyWhite btnModify" value="<spring:message code='button.change'/>"></span> 
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='login.input.section'/></th>
                            <td id="mbrSeNm"></td>
                        </tr>
                        <c:if test="${memberSec == '06'}">
                            <tr>
                                <th><spring:message code='login.input.cp'/></th>
                                <td id="cpNm"></td>
                            </tr>
                        </c:if>
                        <tr>
                            <th><spring:message code='login.input.telNo'/></th>
                            <td>
                                <input id="mbrTelNo" type="text" class="inputBox lengthL">
                                <br><span id="mbrTelNoMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='login.input.phoneNo'/></th>
                            <td id="memberPhoneNoCol">
                                <div class="margin_2">
                                    <p class="memberPhoneBox">
                                        <input type="text" class="inputBox lengthL memberPhoneNo" value="">
                                        <input type="button" class="rightIconPlus rightBtn" id="mPlus"/>
                                        <br><span class="cautionTxt"></span>
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><spring:message code='login.input.email'/></th>
                            <td><input id="mbrEmailId" type="text" class="inputBox lengthS"> @ <input id="mbrEmailDomain" type="text" class="inputBox lengthM">
                                <label style="vertical-align:top; display:inline-block">
                                    <select id="domainList" class="inputBox" style="padding:0;">
                                    </select>
                                </label>
                                <br><span id="mbrEmailIdMsg" class="cautionTxt"></span>
                            </td>
                        </tr>
                        <c:if test="${memberSec == '06'}">
                            <tr>
                                <th><spring:message code='member.detail.document.submitYn'/></th>
                                <td><input name="docSubmYn" value="Y" type="radio" disabled><label><spring:message code='button.member.document.submit'/></label>&nbsp;<span id="apvDt" style="font-size:12px;"></span>&nbsp; <input name="docSubmYn" value="N" type="radio" disabled><label><spring:message code='button.member.document.noSubmit'/></label></td>
                            </tr>
                        </c:if>
                    </table>
                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp txtCenter">
                    <input id="btnUpdate" type="button" class="btnNormal btnModify" value="<spring:message code='button.update.confirm'/>">
                    <input type="button" class="btnNormal btnCancel" value="<spring:message code='login.button.cancel'/>">
                </div>
                <!-- Button Group E -->
            
            </div>
    </div>
    <!-- Contents E -->

    <!-- Popup S -->
    <div class="center" style="display:none;">
        <div id="popupWindow">
            <div class="modifyPassword">
                <h1><center><spring:message code="login.passwordChange.msg"/></center></h1>
                <ul><br>
                    <li><input id="oldPassword" type="password" placeholder="<spring:message code='login.passwordChange.old.msg'/>" class="inputbox lengthW100 textInput"></li>
                    <li><input id="updatePwd1" type="password" placeholder="<spring:message code='login.passwordChange.new1.msg'/>" class="inputbox lengthW100 textInput"></li>
                    <li><input id="updatePwd2" type="password" placeholder="<spring:message code='login.passwordChange.new2.msg'/>" class="inputbox lengthW100 textInput"></li>
                    <li class="textGuide">* <spring:message code='login.alert.password.msg'/></li>
                </ul>
            </div>
                <!-- Button Group S -->
                <div class="btnGrp btnCenter" style="bottom:30px;">
                    <input type="button" class="btnNormal btnModify btnLeft" value="<spring:message code='button.update.confirm'/>">
                    <input type="button" class="btnNormal btnCancel btnLeft" value="<spring:message code='button.reset'/>">
                </div>
                <!-- Button Group E -->
        </div>
    </div>
    <!-- Popup E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>