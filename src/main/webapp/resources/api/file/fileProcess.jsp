<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${ resultType eq 'fileUpload' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="fileInfo">
                    <json:property name="orginlFileNm" value="${ item.orginlFileNm }"/>
                    <json:property name="fileSize" value="${ item.fileSize }"/>
                    <json:property name="fileExt" value="${ item.fileExt }"/>
                    <json:property name="fileDir" value="${ item.fileDir }"/>
                    <json:property name="streFileNm" value="${ item.streFileNm }"/>
                    <json:property name="thumbList" value="${ item.thumbList }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'filePathInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="fileInfo">
                    <json:property name="fileSeq" value="${ item.fileSeq }"/>
                    <json:property name="filePath" value="${ item.filePath }"/>
                    <json:property name="orginlFileNm" value="${ item.orginlFileNm }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'fileThumbInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="fileInfo">
                    <json:property name="fileSeq" value=""/>
                    <c:choose>
                        <c:when test="${ item.thumbSe == 'coverImgArea' }">
                            <json:property name="coverImgNm" value="${ item.orginlFileNm }"/>
                            <json:property name="coverImgPath" value=""/>
                        </c:when>
                        <c:when test="${ item.thumbSe == 'thumbnailArea' }">
                            <json:property name="thumbnailNm" value="${ item.orginlFileNm }"/>
                            <json:property name="thumbnailPath" value=""/>
                        </c:when>
                        <c:otherwise>
                            <json:property name="orginlFileNm" value="${ item.orginlFileNm }"/>
                            <json:property name="filePath" value=""/>
                        </c:otherwise>
                    </c:choose>
                    <json:property name="fileId" value="${ item.fileId }"/>
                    <json:property name="thumbList" value="${ item.thumbList }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'editThumbInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="thumbSe" value="${ thumbSe }"/>
                <json:array name="thumbInfoList" items="${thumbInfoList}" var="i">
                    <json:object>
                        <json:property name="fileSeq" value="${ i.fileSeq }"/>
                        <c:choose>
                            <c:when test="${ i.thumbSe == 'coverImgArea' }">
                                <json:property name="coverImgNm" value="${ i.orginlFileNm }"/>
                                <json:property name="coverImgPath" value="${ i.filePath }"/>
                            </c:when>
                            <c:when test="${ i.thumbSe == 'thumbnailArea' }">
                                <json:property name="thumbnailNm" value="${ i.orginlFileNm }"/>
                                <json:property name="thumbnailPath" value="${ i.filePath }"/>
                            </c:when>
                            <c:otherwise>
                                <json:property name="orginlFileNm" value="${ i.orginlFileNm }"/>
                                <json:property name="filePath" value="${ i.filePath }"/>
                            </c:otherwise>
                        </c:choose>
                        <json:property name="fileId" value="${ i.fileId }"/>
                        <json:property name="thumbList" value="${ i.thumbList }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'delFileThumbInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="delThumbInfo">
                    <json:property name="thumbSe" value="${ item.thumbSe }"/>
                    <c:if test="${ item.fileId != null and item.fileId != '' }">
                        <json:property name="fileId" value="${ item.fileId }"/>
                    </c:if>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
</json:object>