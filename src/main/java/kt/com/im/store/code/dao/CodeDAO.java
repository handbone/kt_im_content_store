/*
 * IM Platform version 1.0
 *
 *  Copyright �� 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.code.dao;

import java.util.List;

import kt.com.im.store.code.vo.CodeVO;

public interface CodeDAO {

    List<CodeVO> codeList(CodeVO data);

}
