/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.genre.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.common.util.Validator;
import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.genre.service.GenreService;
import kt.com.im.store.genre.vo.GenreVO;
import kt.com.im.store.member.vo.MemberVO;

/**
 *
 * 장르 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class GenreApi {

    private final static String viewName = "../resources/api/genre/genreProcess";

    @Resource(name = "GenreService")
    private GenreService genreService;

    /**
     * 장르 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contents/genre")
    public ModelAndView genreProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            boolean isList = request.getParameter("genreSeq") == null;
            if (isList) {
                return getGenreList(request, rsModel);
            }
            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getGenreList(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "firstCtgId:{" + Validator.MAX_LENGTH + "=1}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        GenreVO vo = new GenreVO();
        vo.setFirstCtgId(request.getParameter("firstCtgId"));

        List<GenreVO> result = genreService.getGenreList(vo);
        if (result == null || result.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setResultType("genreList");
        rsModel.setData("item", result);

        return rsModel.getModelAndView();
    }
}
