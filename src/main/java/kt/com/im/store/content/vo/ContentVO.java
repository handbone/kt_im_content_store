/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.content.vo;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 2.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class ContentVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8221104658530904385L;

    /** 콘텐츠 번호 */
    int contsSeq;

    /** 콘텐츠 제외 상태 코드 값 */
    String remoteSttus;

    /** 콘텐츠 상태 코드 값 */
    String sttusVal;

    /** 카테고리 유니크 검색 여부 */
    String unique;

    /** 콘텐츠 상태값 명 */
    String sttusNm;

    /** 콘텐츠 버전 */
    int contsVer;

    /** 콘텐츠 실행 경로 */
    String exeFilePath;

    /** 콘텐츠 이용가능 최대 인원 */
    int maxAvlNop;

    /** 콘텐츠 아이디 */
    private String cretrID;

    /** 콘텐츠 명 */
    private String contsTitle;

    /** 콘텐츠 파일 타입 */
    private String fileType;

    /** 콘텐츠 버전 수정 확인 변수 */
    private String versionModify;

    /** 메타 파일 경로 */
    private String mtdPath;

    /** 콘텐츠 설명 */
    private String contsDesc;

    /** 콘텐츠 부제 */
    private String contsSubTitle;

    /** 콘텐츠 전시기간(시작) */
    private String cntrctStDt;

    /** 콘텐츠 전시기간(종료) */
    private String cntrctFnsDt;

    /** 콘텐츠 카테고리 명(첫번째) */
    private String firstCtgNm;

    /** 콘텐츠 카테고리 명(두번째) */
    private String secondCtgNm;

    /** 콘텐츠 카테고리 아이디(첫번째) */
    private String firstCtgID;

    /** 콘텐츠 카테고리 아이디(두번째) */
    private String secondCtgID;

    /** 콘텐츠 카테고리 아이디(두번째) */
    private String GroupingYN;

    /** 콘텐츠 카테고리 번호 */
    private int contsCtgSeq;

    /** 콘텐츠 카테고리에 해당되는 콘텐츠 개수 */
    private int ctgCnt;

    /** 콘텐츠 반려 사유 */
    private String rcessWhySbst;

    /** 콘텐츠 삭제 여부 */
    private String delYn;

    /** CP사 번호 */
    int cpSeq;

    /** File 번호 */
    private int fileSeq;

    /** File 경로 */
    private String filePath;

    /** File 사이즈 */
    private BigInteger fileSize;

    /** File 명 */
    private String orginlFileNm;

    /** CP사 */
    private String cpNm;

    /** 장르 아이디 */
    private String genreID;

    /** 서비스사 번호 */
    private int svcSeq;

    /** 서비스사 명 */
    private String svcNm;

    /** 회원 아이디 */
    private String mbrID;

    /** 회원 이름 */
    private String mbrNm;

    /** 콘텐츠 카테고리 명 */
    private String contCtgNm;

    /** 콘텐츠 아이디 */
    private String contsID;

    /** 콘텐츠 생성날짜 */
    private String cretDt;

    /** 콘텐츠 수정날짜 */
    private String amdDt;

    /** 콘텐츠 수정날짜 상세(시간정보 포함) */
    private String amdDtDetail;

    /** 콘텐츠 수정자 */
    private String amdrID;

    /** 게시물 조회 페이지 */
    private int offset;

    /** 게시물 조회 개수 */
    private int limit;

    /** 검색어 */
    private String keyword;

    /** 검색 필드 */
    private String target;

    /** 정렬 필드 */
    private String sidx;

    /** 정렬 값(desc, asc) */
    private String sord; // 콘텐츠 리스트 조회시 정렬 방법

    /** 공통 코드명 */
    private String comnCdNm;

    /** 공통 코드번호 */
    private int comnCdSeq;

    private String searchConfirm;

    /** 콘텐츠 비디오 경로에 맞는 xml 파일 정보 */
    private ContentVO metadataXmlInfo;

    /** 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보 */
    private ContentVO contsXmlInfo;

    /** foreach 리스트 정보 */
    private List list;

    /** 콘텐츠 수정 번호 */
    private int contsHstSeq;

    /** 콘텐츠 변경 정보 (컬럼 또는 service) */
    private String contsChgInfo;

    /** 콘텐츠 변경 전 정보 */
    private String legacyValue;

    /** 콘텐츠 변경 후 정보 */
    private String changeValue;

    /** 검수 이력 번호 */
    private int actcSeq;

    /** 검수 등록자 이름 */
    private String verifyCretrNm;

    /* 검수 상태 */
    private String verifySttus;

    /* 검수 등록 일 */
    private String verifyRegDt;

    /** 전시 상태 여부 */
    private String contsHdn;

    /** 장르 코드 */
    private int genreSeq;

    /** 장르 명 */
    private String genreNm;

    /** 추천 콘텐츠 삭제 목록 */
    private List<Integer> delRcmdContsSeqList;

    /** 상태 변경자 아이디 */
    private String sttusChgrId;

    /** 썸네일 파일 리스트 */
    private List<String> thumbList;

    /** 썸네일 구분 */
    private String thumbSe;

    /** 저장 파일명 */
    private String streFileNm;

    /** 파일 아이디 */
    private String fileId;

    /** 파일 구분 */
    private String fileSe;

    /** 메타데이터 입력값 */
    private String metaData;

    /** 메타데이터 코드 */
    private int contsSubMetadataSeq;

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public int getContsSubMetadataSeq() {
        return contsSubMetadataSeq;
    }

    public void setContsSubMetadataSeq(int contsSubMetadataSeq) {
        this.contsSubMetadataSeq = contsSubMetadataSeq;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public String getRemoteSttus() {
        return remoteSttus;
    }

    public void setRemoteSttus(String remoteSttus) {
        this.remoteSttus = remoteSttus;
    }

    public String getSttusVal() {
        return sttusVal;
    }

    public void setSttusVal(String sttusVal) {
        this.sttusVal = sttusVal;
    }

    public String getSttusNm() {
        return sttusNm;
    }

    public void setSttusNm(String sttusNm) {
        this.sttusNm = sttusNm;
    }

    public int getContsVer() {
        return contsVer;
    }

    public void setContsVer(int contsVer) {
        this.contsVer = contsVer;
    }

    public String getExeFilePath() {
        return exeFilePath;
    }

    public void setExeFilePath(String exeFilePath) {
        this.exeFilePath = exeFilePath;
    }

    public int getMaxAvlNop() {
        return maxAvlNop;
    }

    public void setMaxAvlNop(int maxAvlNop) {
        this.maxAvlNop = maxAvlNop;
    }

    public String getCretrID() {
        return cretrID;
    }

    public void setCretrID(String cretrID) {
        this.cretrID = cretrID;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getVersionModify() {
        return versionModify;
    }

    public void setVersionModify(String versionModify) {
        this.versionModify = versionModify;
    }

    public String getMtdPath() {
        return mtdPath;
    }

    public void setMtdPath(String mtdPath) {
        this.mtdPath = mtdPath;
    }

    public String getContsDesc() {
        return contsDesc;
    }

    public void setContsDesc(String contsDesc) {
        this.contsDesc = contsDesc;
    }

    public String getContsSubTitle() {
        return contsSubTitle;
    }

    public void setContsSubTitle(String contsSubTitle) {
        this.contsSubTitle = contsSubTitle;
    }

    public String getCntrctStDt() {
        return cntrctStDt;
    }

    public void setCntrctStDt(String cntrctStDt) {
        this.cntrctStDt = cntrctStDt;
    }

    public String getCntrctFnsDt() {
        return cntrctFnsDt;
    }

    public void setCntrctFnsDt(String cntrctFnsDt) {
        this.cntrctFnsDt = cntrctFnsDt;
    }

    public String getFirstCtgNm() {
        return firstCtgNm;
    }

    public void setFirstCtgNm(String firstCtgNm) {
        this.firstCtgNm = firstCtgNm;
    }

    public String getSecondCtgNm() {
        return secondCtgNm;
    }

    public void setSecondCtgNm(String secondCtgNm) {
        this.secondCtgNm = secondCtgNm;
    }

    public String getFirstCtgID() {
        return firstCtgID;
    }

    public void setFirstCtgID(String firstCtgID) {
        this.firstCtgID = firstCtgID;
    }

    public String getSecondCtgID() {
        return secondCtgID;
    }

    public void setSecondCtgID(String secondCtgID) {
        this.secondCtgID = secondCtgID;
    }

    public String getGroupingYN() {
        return GroupingYN;
    }

    public void setGroupingYN(String GroupingYN) {
        this.GroupingYN = GroupingYN;
    }

    public int getContsCtgSeq() {
        return contsCtgSeq;
    }

    public void setContsCtgSeq(int contsCtgSeq) {
        this.contsCtgSeq = contsCtgSeq;
    }

    public int getCtgCnt() {
        return ctgCnt;
    }

    public void setCtgCnt(int ctgCnt) {
        this.ctgCnt = ctgCnt;
    }

    public int getCpSeq() {
        return cpSeq;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public int getFileSeq() {
        return fileSeq;
    }

    public void setFileSeq(int fileSeq) {
        this.fileSeq = fileSeq;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public BigInteger getFileSize() {
        return fileSize;
    }

    public void setFileSize(BigInteger fileSize) {
        this.fileSize = fileSize;
    }

    public String getOrginlFileNm() {
        return orginlFileNm;
    }

    public void setOrginlFileNm(String orginlFileNm) {
        this.orginlFileNm = orginlFileNm;
    }

    public String getCpNm() {
        return cpNm;
    }

    public void setCpNm(String cpNm) {
        this.cpNm = cpNm;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

    public String getMbrID() {
        return mbrID;
    }

    public void setMbrID(String mbrID) {
        this.mbrID = mbrID;
    }

    public String getMbrNm() {
        return mbrNm;
    }

    public void setMbrNm(String mbrNm) {
        this.mbrNm = mbrNm;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public String getAmdDtDetail() {
        return amdDtDetail;
    }

    public void setAmdDtDetail(String amdDtDetail) {
        this.amdDtDetail = amdDtDetail;
    }

    public String getAmdrID() {
        return amdrID;
    }

    public void setAmdrID(String amdrID) {
        this.amdrID = amdrID;
    }

    public int getoffset() {
        return offset;
    }

    public void setoffset(int offset) {
        this.offset = offset;
    }

    public int getlimit() {
        return limit;
    }

    public void setlimit(int limit) {
        this.limit = limit;
    }

    public String getkeyword() {
        return keyword;
    }

    public void setkeyword(String keyword) {
        this.keyword = keyword;
    }

    public String gettarget() {
        return target;
    }

    public void settarget(String target) {
        this.target = target;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public int getComnCdSeq() {
        return comnCdSeq;
    }

    public void setComnCdSeq(int comnCdSeq) {
        this.comnCdSeq = comnCdSeq;
    }

    public String getContCtgNm() {
        return contCtgNm;
    }

    public void setContCtgNm(String contCtgNm) {
        this.contCtgNm = contCtgNm;
    }

    public String getContsID() {
        return contsID;
    }

    public void setContsID(String contsID) {
        this.contsID = contsID;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public ContentVO getMetadataXmlInfo() {
        return metadataXmlInfo;
    }

    public void setMetadataXmlInfo(ContentVO metadataXmlInfo) {
        this.metadataXmlInfo = metadataXmlInfo;
    }

    public void setContsXmlInfo(ContentVO contsXmlInfo) {
        this.contsXmlInfo = contsXmlInfo;
    }

    public ContentVO getContsXmlInfo() {
        return contsXmlInfo;
    }

    public void setRcessWhySbst(String rcessWhySbst) {
        this.rcessWhySbst = rcessWhySbst;
    }

    public String getRcessWhySbst() {
        return rcessWhySbst;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getGenreID() {
        return genreID;
    }

    public void setGenreID(String genreID) {
        this.genreID = genreID;
    }

    public String getContsHdn() {
        return contsHdn;
    }

    public void setContsHdn(String contsHdn) {
        this.contsHdn = contsHdn;
    }

    public int getContsHstSeq() {
        return contsHstSeq;
    }

    public void setContsHstSeq(int contsHstSeq) {
        this.contsHstSeq = contsHstSeq;
    }

    public String getContsChgInfo() {
        return contsChgInfo;
    }

    public void setContsChgInfo(String contsChgInfo) {
        this.contsChgInfo = contsChgInfo;
    }

    public String getLegacyValue() {
        return legacyValue;
    }

    public void setLegacyValue(String legacyValue) {
        this.legacyValue = legacyValue;
    }

    public String getChangeValue() {
        return changeValue;
    }

    public void setChangeValue(String changeValue) {
        this.changeValue = changeValue;
    }

    public int getActcSeq() {
        return actcSeq;
    }

    public void setActcSeq(int actcSeq) {
        this.actcSeq = actcSeq;
    }

    public String getVerifyCretrNm() {
        return verifyCretrNm;
    }

    public void setVerifyCretrNm(String verifyCretrNm) {
        this.verifyCretrNm = verifyCretrNm;
    }

    public String getVerifySttus() {
        return verifySttus;
    }

    public void setVerifySttus(String verifySttus) {
        this.verifySttus = verifySttus;
    }

    public String getVerifyRegDt() {
        return verifyRegDt;
    }

    public void setVerifyRegDt(String verifyRegDt) {
        this.verifyRegDt = verifyRegDt;
    }

    public int getGenreSeq() {
        return genreSeq;
    }

    public void setGenreSeq(int genreSeq) {
        this.genreSeq = genreSeq;
    }

    public String getGenreNm() {
        return genreNm;
    }

    public void setGenreNm(String genreNm) {
        this.genreNm = genreNm;
    }

    public List<Integer> getDelRcmdContsSeqList() {
        return delRcmdContsSeqList;
    }

    public void setDelRcmdContsSeqList(List<Integer> delRcmdContsSeqList) {
        this.delRcmdContsSeqList = new ArrayList<Integer>(delRcmdContsSeqList.size());
        for (int i = 0; i < delRcmdContsSeqList.size(); i++) {
            this.delRcmdContsSeqList.add(delRcmdContsSeqList.get(i));
        }
    }

    public String getUnique() {
        return unique;
    }

    public void setUnique(String unique) {
        this.unique = unique;
    }

    public String getSttusChgrId() {
        return sttusChgrId;
    }

    public void setSttusChgrId(String sttusChgrId) {
        this.sttusChgrId = sttusChgrId;
    }

    public List<String> getThumbList() {
        return thumbList;
    }

    public void setThumbList(List<String> thumbList) {
        this.thumbList = thumbList;
    }

    public String getThumbSe() {
        return thumbSe;
    }

    public void setThumbSe(String thumbSe) {
        this.thumbSe = thumbSe;
    }

    public String getStreFileNm() {
        return streFileNm;
    }

    public void setStreFileNm(String streFileNm) {
        this.streFileNm = streFileNm;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileSe() {
        return fileSe;
    }

    public void setFileSe(String fileSe) {
        this.fileSe = fileSe;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}
