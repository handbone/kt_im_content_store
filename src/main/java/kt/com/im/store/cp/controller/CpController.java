/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.cp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 콘텐츠 제공사(CP) 관리에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class CpController {
    /**
     * 콘텐츠 제공사(CP)  리스트 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/cp", method = RequestMethod.GET)
    public ModelAndView contentProviderList(ModelAndView mv) {
        mv.setViewName("/views/cp/CpList");
        return mv;
    }

    /**
     * 콘텐츠 제공사(CP) 상세 정보 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @param cpSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     * 
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/cp/{cpSeq}", method = RequestMethod.GET)
    public ModelAndView contentProviderDetail(ModelAndView mv, @PathVariable(value="cpSeq") String cpSeq) {
        mv.addObject("cpSeq", cpSeq);
        mv.setViewName("/views/cp/CpDetail");
        return mv;
    }

    /**
     * 회원 등록 화면
     * @param mv - 화면 정보를 저장하는 변수
     * 
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/cp/regist", method = RequestMethod.GET)
    public ModelAndView contentProviderRegist(ModelAndView mv) {     
        mv.setViewName("/views/cp/CpRegist");
        return mv;
    }

    /**
     * 회원 수정 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @param cpSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     * 
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/cp/edit/{cpSeq}", method = RequestMethod.GET)
    public ModelAndView contentProviderEdit(ModelAndView mv, @PathVariable(value="cpSeq") String cpSeq) {
        mv.addObject("cpSeq", cpSeq);
        mv.setViewName("/views/cp/CpEdit");
        return mv;
    }
}