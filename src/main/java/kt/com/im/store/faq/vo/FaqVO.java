/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.faq.vo;

import java.io.Serializable;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class FaqVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7785530015016665535L;

    /* FAQ 번호 */
    private int faqSeq;

    /* FAQ 구분 */
    private String faqType;

    /* FAQ 구분 코드 */
    private String faqTypeCd;

    /* FAQ 카테고리 */
    private String faqCtg;

    /* FAQ 제목 (질문) */
    private String faqTitle;

    /* FAQ 내용 (답변) */
    private String faqSbst;

    /* 조회수 */
    private int retvNum;

    /* FAQ 등록 일시 */
    private String regDt;

    /* FAQ 등록자 이름 */
    private String cretrNm;

    /* FAQ 삭제 여부 */
    private String delYn;

    /* offset */
    private int offset;

    /* limit */
    private int limit;

    /* 검색 구분 */
    private String searchTarget;

    /* 검색어 */
    private String searchKeyword;

    /* 정렬 컬럼 명 */
    private String sidx;

    /* 정렬 방법 */
    private String sord;

    /* 검색 여부 확인 */
    private String searchConfirm;

    public int getFaqSeq() {
        return faqSeq;
    }

    public void setFaqSeq(int faqSeq) {
        this.faqSeq = faqSeq;
    }

    public String getFaqType() {
        return faqType;
    }

    public void setFaqType(String faqType) {
        this.faqType = faqType;
    }

    public String getFaqTypeCd() {
        return faqTypeCd;
    }

    public void setFaqTypeCd(String faqTypeCd) {
        this.faqTypeCd = faqTypeCd;
    }

    public String getFaqCtg() {
        return faqCtg;
    }

    public void setFaqCtg(String faqCtg) {
        this.faqCtg = faqCtg;
    }

    public String getFaqTitle() {
        return faqTitle;
    }

    public void setFaqTitle(String faqTitle) {
        this.faqTitle = faqTitle;
    }

    public String getFaqSbst() {
        return faqSbst;
    }

    public void setFaqSbst(String faqSbst) {
        this.faqSbst = faqSbst;
    }

    public int getRetvNum() {
        return retvNum;
    }

    public void setRetvNum(int retvNum) {
        this.retvNum = retvNum;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
