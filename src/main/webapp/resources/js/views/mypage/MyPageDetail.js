/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function() {
    $("#contents input[type='text']").blur(function() {
        if (this.id == "mbrEmailId" || this.id == "mbrEmailDomain") {
            if (!$("#mbrEmailId").val() && !$("#mbrEmailDomain").val()) {
                $("#mbrEmailIdMsg").text("");
                return;
            }

            if (this.id == "mbrEmailId" && !$("#mbrEmailDomain").val()) {
                return;
            }

            if (this.id == "mbrEmailDomain") {
                validate(document.getElementById("mbrEmailId"), false);
                return;
            }


        } else if ($(this).hasClass("memberPhoneNo")) {
            validatePhon(this, false);
        } else {
            if (!$(this).val()) {
                $("#" + this.id + "Msg").text("");
                return;
            }
        }

        validate(this, false);
    });

    $("#btnDelete").click(function(e) {
        confirmDelete();
    });

    $("#btnUpdate").click(function(e) {
        confirmModify();
    });

    $("#contents .btnCancel").click(function(e) {
        pageMove('/home');
    });

    $("#btnPwd").click(function(e) {
        showPasswordChangePopup();
    });

    $("#popupWindow .btnModify").click(function(e) {
        updatePassword();
    });

    $("#popupWindow .btnCancel").click(function(e) {
        closePasswordChangePopup();
    });

    $("#domainList").change(function(e) {
        changeEmailBox(this);
    });

    addEmailDomainList(document.getElementById("domainList"));

    $("#mbrTelNo, #mbrMphonNo").keyup(function(e) {
        var value = $(this).val();
        $(this).val(value.replace(/[^0-9\-]/gi,''));
    });

    $("#mbrEmailDomain").keyup(function() {
        var domain = $("#domainList option:selected").val();
        if (!domain) {
            return;
        }

        if ($("#domainList option:selected").val() != $("#mbrEmailDomain").val()) {
            $("#domainList option:eq(0)").prop("selected", true);
        }
    });

    $("#mPlus").click(function(){
        $("#memberPhoneNoCol").append("<div class='margin_2'><p class='memberPhoneBox'><input type='text' class='inputBox lengthL memberPhoneNo' value=''><input type='button' class='rightIconMius rightBtn' onclick='removePhonNo(this)'><br><span class='cautionTxt'></span></p></div>");
        $(".margin_2:last-child").find("input[type=text]").blur(function() {
           if ($(this).hasClass("memberPhoneNo")) {
                validatePhon(this, false);
            }
            validate(this, false);
            $("#mPlus").css("opacity","0.2").attr( 'disabled', true );
        });
        $("#mPlus").css("opacity","0.2").attr( 'disabled', true );
    });



});

function initFindUserInfo(data) {
    $("#loading").hide();
    if (data.resultCode != "1000") {
        $("#mbrTelNo").val("");
        $("#mbrMphonNo").val("");
        $("#mbrEmailId").val("");
        $("#mbrEmailDomain").val("");
        $("#domainList").val("");
        popAlertLayer(getMessage("common.bad.request.msg"), "/home");
    } else {

        getUserInfo();
    }
}

function initNotFindUserInfo(data) {
    $("#loading").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/home");
//    back
}


getUserInfo = function() {
    callByGet("/api/member?seq=" + $("#seq").val(), "userInfoSuccess", "NoneForm");
}

userInfoSuccess = function(data) {
    var item = data.result.memberInfo;
    if (data.resultCode == "1000") {
        $("#mbrNm").text(item.mbrNm);
        $("#mbrId").text(item.mbrId);
        $("#mbrSeNm").text(item.mbrSeNm);
        $("#cpNm").text(item.cpNm);
        $("#mbrTelNo").val(item.mbrTelNo);


        $(item.mbrMphonList).each(function(i, list){
            if (i != 0) {
                $("#mPlus").css("opacity","0.2").attr('disabled', true );
                $("#memberPhoneNoCol").append("<div class='margin_2'><p class='memberPhoneBox'><input type='text' class='inputBox lengthL memberPhoneNo' value="+list.mbrMphonNo+"><input type='button' class='rightIconMius rightBtn' onclick='removePhonNo(this)'><br><span class='cautionTxt'></span></p></div>");
            } else {
                $(".memberPhoneNo").eq(i).val(list.mbrMphonNo);
            }
        });

        var emailArr = item.mbrEmail.split("@");
        $("#mbrEmailId").val(emailArr[0]);
        $("#mbrEmailDomain").val(emailArr[1]);

        if ($("#memberSec").val() == "06") {
            $("#cpNm").text(item.cpNm);
            var docSubmYn = item.docSubmYn || "N";
            $("input:radio[name='docSubmYn']:input[value='"+ docSubmYn + "']").attr("checked", true);
            if (item.apvDt) {
                $("#apvDt").text("(" + item.apvDt + ")");
            }
        }
    }
}

confirmDelete = function() {
    popConfirmLayer(getMessage("member.delete.confirm.msg"), function() {
        formData("NoneForm", "seq", $("#seq").val());
        callByDelete("/api/member", "didDeleteUser", "NoneForm", "didNotDeleteUser");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("common.confirm"));
}

didDeleteUser = function(data) {
    if (data.resultCode == "1000") {
        popLayerClose();
        callByPost("/api/logout", "didLogout", "");
    } else {
        popAlertLayer(getMessage("fail.member.delete"));
    }
}

didNotDeleteUser = function(data) {
    popAlertLayer(getMessage("fail.member.delete"));
}

function didLogout(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.member.delete"), "/");

    }
}

function confirmModify() {
    if (!validateForm()) {
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        modifyMember();
    }, null, getMessage("common.confirm"));
}

function validateForm() {
    var isValid = true;
    $("#contents input[type='text']").each(function(i, item) {
        if ($(this).hasClass("memberPhoneNo")) {
            if (!validatePhon(item, true)) {
                isValid = false;
                return false;
            }
        } else {
            if (!validate(item, true)) {
                isValid = false;
                return false;
            }
        }
    });

    return isValid;
}

function validate(obj, usePopup) {
    var id = obj.id;
    var msg = "";
    var msgId = "#" + id + "Msg";

    $(msgId).hide();
    var value = $(obj).val();
    if (!usePopup) {
        if (id == "mbrEmailId") {
            value += $("#mbrEmailDomain").val();
        }
    }
    if (value.length <= 0) {
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayer(msg, '', '', '', id);
        } else {
            $(msgId).text(msg);
            $(msgId).show();
        }
        return false;
    }

    var isValid = true;
    switch (id) {
    case "mbrTelNo":
        isValid = validateTelNumber($("#mbrTelNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.telNo.msg");
        } else {
            $("#mbrTelNo").val(phoneNumberFormatter($("#mbrTelNo").val()));
        }
        break;
    case "mbrMphonNo":
        isValid = validatePhoneNumber($("#mbrMphonNo").val());
        if (!isValid) {
            msg = getMessage("login.alert.phoneNo.msg");
        } else {
            $("#mbrMphonNo").val(phoneNumberFormatter($("#mbrMphonNo").val()));
        }
        break;
    case "mbrEmailId":
        if (!usePopup) {
            isValid =  validateEmail($("#mbrEmailId").val() + "@" + $("#mbrEmailDomain").val());
        } else {
            isValid =  validateEmailId($("#mbrEmailId").val());
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    case "mbrEmailDomain":
        if (usePopup) {
            isValid =  validateEmailDomain($("#mbrEmailDomain").val());
        }
        if (!isValid) {
            msg = getMessage("login.alert.email.msg");
        }
        break;
    default:
        console.log("Nothing id = " + id);
        break;
    }

    if (!isValid) {
        if (!isValid) {
            if (usePopup) {
                popAlertLayer(msg, '', '', '', id);
            } else {
                $(msgId).text(msg);
                $(msgId).show();
            }
        }
    }
    return isValid;
}

function validatePhon(obj, usePopup) {
    var msg = "";
    var msgSpan = $(obj).parents("p").find("span");

    $(msgSpan).hide();
    var value = $(obj).val();

    if (value.length <= 0) {
        msg = getMessage("login.alert.required.msg");
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
        return false;
    }

    var isValid = true;
    isValid = validatePhoneNumber($(obj).val());
    if (!isValid) {
        msg = getMessage("login.alert.phoneNo.msg");
    } else {
        $(obj).val(phoneNumberFormatter($(obj).val()));
    }

    if (!isValid) {
        if (usePopup) {
            popAlertLayerObj(msg, '', '', '', obj);
        } else {
            $(msgSpan).text(msg);
            $(msgSpan).show();
        }
    }
    return isValid;
}



modifyMember = function() {
    var phoneNum = phoneNumberFormatter($("#mbrTelNo").val());
    var email = $("#mbrEmailId").val() + "@" + $("#mbrEmailDomain").val();
    var mbrMphonNo = "";

    formData("NoneForm", "seq", $("#seq").val());
    formData("NoneForm", "tel", phoneNum);

    for (var i=0; i<$(".memberPhoneNo").size(); i++) {
        if (mbrMphonNo != "") {
            mbrMphonNo += ";";
        }
        mbrMphonNo += phoneNumberFormatter($(".memberPhoneNo").eq(i).val());
    }

    formData("NoneForm", "phone", mbrMphonNo);
    formData("NoneForm", "email", email);

    callByPut("/api/member", "didModifyMember", "NoneForm", "didNotModifyMember");
    formDataDeleteAll("NoneForm");
}

function didModifyMember(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.common.update"), "/");
    } else if (data.resultCode == "1011") {
        popAlertLayer(getMessage("login.unavailableEmail.msg"));
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function didNotModifyMember(data) {
    popAlertLayer(getMessage("fail.common.update"));
}

function showPasswordChangePopup () {
    openPopupLayer("popupWindow");
}

function closePasswordChangePopup () {
    closePopupLayer();
}

function openPopupLayer(name) {
    popLayerDiv(name, 500, 330, true);
}

closePopupLayer = function() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();

    $("#oldPassword").val("");
    $("#updatePwd1").val("");
    $("#updatePwd2").val("");
}

updatePassword = function() {
    var oldPwd = $("#oldPassword").val();
    if (!oldPwd) {
        popAlertLayer(getMessage("login.password.msg"), '', '', '', "oldPassword");
        return;
    }

    var pwd1 = $("#updatePwd1").val();
    if (!pwd1) {
        popAlertLayer(getMessage("login.newPassword.msg"), '', '', '', "updatePwd1");
        return;
    }

    var validResult = validatePassword(pwd1, $("#mbrId").text());
    var isValid = (validResult === "valid");
    if (!isValid) {
        if (validResult === "continuousChar") {
            msg = getMessage("login.alert.continuous.character.msg");
        } else if (validResult === "containsIdPart") {
            msg = getMessage("login.alert.cotains.id.msg");
        } else {
            msg = getMessage("login.alert.password.msg");
        }
        popAlertLayer(msg, '', '', '', "updatePwd1");
        return;
    }

    var pwd2 = $("#updatePwd2").val();
    if (!pwd2) {
        popAlertLayer(getMessage("login.newPasswordConfirm.msg"), '', '', '', "updatePwd2");
        return;
    }

    if (pwd1 != pwd2) {
        $("#updatePwd2").val("");
        popAlertLayer(getMessage("login.alert.passwordConfirm.msg"), '', '', '', "updatePwd2");
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        formData("NoneForm", "seq", $("#seq").val());
        formData("NoneForm", "oldPwd", oldPwd);
        formData("NoneForm", "pwd1", pwd1);
        formData("NoneForm", "pwd2", pwd2);
        callByPost("/api/member/password", "didUpdatePassword", "NoneForm", "didNotUpdatePassword");
        formDataDeleteAll("NoneForm");
    }, null, getMessage("common.confirm"));
}

didUpdatePassword = function(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getMessage("success.login.password.update"));
        closePasswordChangePopup();
    } else {
        popAlertLayer(data.resultMsg);
    }
}

didNotUpdatePassword = function(data) {
    var result = JSON.parse(data.responseText);
    popAlertLayer(getMessage(result.resultMsg));
}

changeEmailBox = function(obj) {
    $("#mbrEmailDomain").val(obj.value);
}


function cofirmMove(mvPage) {
    pageMove(mvPage);
}

function removePhonNo(e) {
    $(e).parents("p").remove();
    $("#mPlus").css("opacity","1").attr( 'disabled', false );
}