/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var duplicationFlag  = false;
var isAvailableId  = false;
var failedLoginCount = 0;
var failedFindingIdCount = 0;
var failedFindingPwdCount = 0;

$(document).ready(function() {
    $("#joinMember input[type='text'], #joinMember input[type='password']").blur(function() {
        if (this.id == "memberEmailId" || this.id == "memberEmailDomain") {
            if (!$("#memberEmailId").val() && !$("#memberEmailDomain").val()) {
                $("#memberEmailIdMsg").text("");
                return;
            }

            if (this.id == "memberEmailId" && !$("#memberEmailDomain").val()) {
                return;
            }

            if (this.id == "memberEmailDomain") {
                validate(document.getElementById("memberEmailId"));
                return;
            }
        } else {
            if (!$(this).val()) {
                $("#" + this.id + "Msg").text("");
                return;
            }
        }

        validate(this);
    });

    $("#loginId, #loginPwd").keydown(function(e){
        var keyCodeNo = 0;
        if (typeof(e) != "undefined") {
            keyCodeNo = e.which;
        } else {
            keyCodeNo = event.keyCode;
        }
        if (keyCodeNo == 13) {
            Login();
        }
    });

    if (getCookie("idSaved")) {
        $("#loginId").val(getCookie("idSaved"));
        $("#idSaved").prop("checked", true);
    }

    $("#memberDomainList").change(function(e) {
        changeMemberEmailBox(this);
    });

    $("#findDomainList").change(function(e) {
        changeFindEmailBox(this);
    });

    $("#telNo, #mobilePhone").keyup(function(e) {
        var value = $(this).val();
        $(this).val(value.replace(/[^0-9\-]/gi,''));
    });

    $("#memberEmailDomain").keyup(function() {
        var domain = $("#memberDomainList option:selected").val();
        if (!domain) {
            return;
        }

        if ($("#memberDomainList option:selected").val() != $("#memberEmailDomain").val()) {
            $("#memberDomainList option:eq(0)").prop("selected", true);
        }
    });

    $("#findEmailDomain").keyup(function() {
        var domain = $("#findDomainList option:selected").val();
        if (!domain) {
            return;
        }

        if ($("#findDomainList option:selected").val() != $("#findEmailDomain").val()) {
            $("#findDomainList option:eq(0)").prop("selected", true);
        }
    });

    $(".btnCancel").click(function() {
        popLayerClose();
    });

    $("#checkTermsAll").click(function() {
        var checked = $("#checkTermsAll").is(":checked");
        $("#checkUseTerms").prop("checked", checked);
        $("#privacyUseTerms").prop("checked", checked);
    });

    $("#checkUseTerms").click(function() {
        var checked = $("#checkUseTerms").is(":checked") && $("#privacyUseTerms").is(":checked");
        $("#checkTermsAll").prop("checked", checked);
    });

    $("#privacyUseTerms").click(function() {
        var checked = $("#checkUseTerms").is(":checked") && $("#privacyUseTerms").is(":checked");
        $("#checkTermsAll").prop("checked", checked);
    });

    $(".btnNext").click(function() {
        if (!$("#checkTermsAll").is(":checked")) {
            popAlertLayer(getLoginMessage("login.term.alert.msg"));
            return;
        }
        insertFormShow("joinMember");
    });

    $("#joinMember").css("width", "auto");

    $("#useTermsBody").html(getTermInfo());
    $("#privacyTermsBody").html(getPolicyInfo());

    addEmailDomainList(document.getElementById("memberDomainList"));
    addEmailDomainList(document.getElementById("findDomainList"));

    contentProviderList();
});

jsLogin = function(id, pwd) {
    if (failedLoginCount >= 5) {
        popAlertLayer(getLoginMessage("fail.login.alert.block"));
        return;
    }

    formData("NoneForm", "id", id);
    formData("NoneForm", "pwd", pwd);
    callByPost("/api/login", "jsLoginSuccess", "NoneForm", "jsLoginFail");
    formDataDeleteAll("NoneForm");
}

jsLoginSuccess= function(data){
    if (data.resultCode == "1000") {
        if ($("#idSaved").prop("checked")) {
            setCookie("idSaved", $("#loginId").val());
        } else {
            removeCookie("idSaved");
        }
        var uri = makeAPIUrl("/auth");
        if ($("#requestUri").val()) {
            uri = uri + "?uri=" + $("#requestUri").val();
        }
        location.href = uri;
    } else if(data.resultCode == "1010") {
        failedLoginCount++;
        var msg = getLoginMessage("fail.login");
        popAlertLayer(msg);
    } else if(data.resultCode == "1012" || data.resultCode == "1013") {
        failedLoginCount = 0;
        popAlertLayer(getLoginMessage(data.resultMsg));
    }
}

jsLoginFail= function(data) {
    popAlertLayer(getLoginMessage("fail.login.server.error"));
}

Login = function(data) {
    if ($("#loginId").val() == "") {
        popAlertLayer(getLoginMessage("login.id.msg"), "", "loginId");
        return;
    }

    if ($("#loginPwd").val() == "") {
        popAlertLayer(getLoginMessage("login.password.msg"), "", "loginPwd");
        return;
    }

    jsLogin($("#loginId").val(), $("#loginPwd").val());
}

function openFindMemberPopup(type) {
    var popupTitle;
    var popupType;
    var callbackFunction;
    if (type == "id") {
        popupTitle = getLoginMessage("login.findIdTitle.msg");
        popupType = getLoginMessage("login.findId.name.msg");
        callbackFunction = "findId";
    } else if (type == "password") {
        popupTitle = getLoginMessage("login.findPasswordTitle.msg");
        popupType = getLoginMessage("login.input.id");
        callbackFunction = "findPassword";
    }
    $("#findTitle").text(popupTitle);
    $("#findType").text(popupType);
    $("#findMember .btnSearch").off("click");
    $("#findMember .btnSearch").on("click", function() {
        window[callbackFunction]();
    });

    popLayerDiv("findMember", 500, 300, true);
}

/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function insertFormShow(name) {
    popLayerDiv(name, 500, 300, true);
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function popLayerClose(){
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
    formCloseFnc();
}

function userRegister() {
    if (!$("#checkTermsAll").is(":checked")) {
        popAlertLayer(getLoginMessage("login.term.alert.msg"));
        return;
    }

    if (!validateJoin()) {
        return;
    }

    if (!duplicationFlag) {
        popAlertLayer(getLoginMessage("login.checkId.msg"));
        return;
    }

    if (!isAvailableId) {
        popAlertLayer(getLoginMessage("login.unavailableId.msg"));
        return;
    }

    var telNo = phoneNumberFormatter($("#telNo").val());
    var mobilePhone = phoneNumberFormatter($("#mobilePhone").val());

    formData("NoneForm" , "id", $("#memberId").val());
    formData("NoneForm" , "pwd1", $("#memberPwd").val());
    formData("NoneForm" , "pwd2", $("#memberPwdRe").val());
    formData("NoneForm" , "name", $("#memberName").val());
    formData("NoneForm" , "cpSeq", $("#memberCp option:selected").val());
    formData("NoneForm" , "tel", telNo);
    formData("NoneForm" , "phone", mobilePhone);
    formData("NoneForm" , "email", $("#memberEmailId").val() + "@" + $("#memberEmailDomain").val());

    callByPost("/api/member" , "didRegistUser", "NoneForm", "didNotRegistUser");
    formDataDeleteAll("NoneForm");
}

function didRegistUser(data) {
    if (data.resultCode == "1000") {
        popAlertLayer(getLoginMessage("success.login.regist"));
        popLayerClose();
    } else if (data.resultCode == "1011") {
        popAlertLayer(getLoginMessage(data.resultMsg));
    } else {
        popAlertLayer(getLoginMessage("fail.login.regist"));
    }
}

function didNotRegistUser(data) {
    popAlertLayer(getLoginMessage("fail.login.regist"));
}

userIdDuplication = function() {
    var isValid = validate(document.getElementById("memberId"));
    if (!isValid) {
        return;
    }

    callByGet("/api/member/check?id=" + $("#memberId").val(), "userIdDuplicationSuccess");
}

userIdDuplicationSuccess = function(data) {
    duplicationFlag = true;
    var msg = getLoginMessage("login.availableId.msg");
    if (data.resultCode == "1010") {
        isAvailableId = true;
    } else {
        msg = getLoginMessage("login.unavailableId.msg");
        $("#memberId").focus();
        isAvailableId = false;
    }

    $("#memberIdMsg").text(msg);
}

function userIdChange() {
    duplicationFlag  = false;
    isAvailableId = false;
}

function findId() {
    if (failedFindingIdCount >= 5) {
        popAlertLayer(getLoginMessage("fail.login.alert.findId.block"));
        return;
    }

    var name = $("#findId").val();
    if (!name) {
        popAlertLayer(getLoginMessage("login.name.msg"), "", "findId");
        return;
    }

    var emailId = $("#findEmailId").val();
    if (!emailId) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailId");
        return;
    }

    var emailDomain = $("#findEmailDomain").val();
    if (!emailDomain) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailDomain");
        return;
    }

    var email = emailId + "@" + emailDomain;

    formData("NoneForm" , "type", "id");
    formData("NoneForm" , "name", name);
    formData("NoneForm" , "email", email);

    $("#loading").show();
    callByGet("/api/member/find" , "didFindId", "NoneForm", "didNotFindId");
    formDataDeleteAll("NoneForm");
}

function didFindId(data) {
    $("#loading").hide();
    if (data.resultCode == "1000") {
        failedFindingIdCount = 0;
        popAlertLayer(getLoginMessage("success.login.findId") + "<br>ID : " + data.result.findInfo.id);
        popLayerClose();
    } else {
        failedFindingIdCount++;
        popAlertLayer(getLoginMessage(data.resultMsg));
    }
}

function didNotFindId(data) {
    $("#loading").hide();
    var result = JSON.parse(data.responseText);
    popAlertLayer(getLoginMessage(result.resultMsg));
}

function findPassword() {
    if (failedFindingPwdCount >= 5) {
        popAlertLayer(getLoginMessage("fail.login.findPassword"));
        return;
    }

    var id = $("#findId").val();
    if (!id) {
        popAlertLayer(getLoginMessage("login.id.msg"), "", "findId");
        return;
    }

    var emailId = $("#findEmailId").val();
    if (!emailId) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailId");
        return;
    }

    var emailDomain = $("#findEmailDomain").val();
    if (!emailDomain) {
        popAlertLayer(getLoginMessage("login.email.msg"), "", "findEmailDomain");
        return;
    }

    var email = emailId + "@" + emailDomain;

    formData("NoneForm" , "type", "password");
    formData("NoneForm" , "id", id);
    formData("NoneForm" , "email", email);

    $("#loading").show();
    callByGet("/api/member/find" , "didFindPassword", "NoneForm", "didNotFindPassword");
    formDataDeleteAll("NoneForm");
}

function didFindPassword(data) {
    $("#loading").hide();
    if (data.resultCode == "1000") {
        failedFindingPwdCount = 0;
        popAlertLayer(getLoginMessage("success.login.find"));
        popLayerClose();
    } else if (data.resultCode == "1012") {
        failedFindingPwdCount = 0;
        popAlertLayer(getLoginMessage(data.resultMsg));
    } else {
        failedFindingPwdCount++;
        popAlertLayer(getLoginMessage(data.resultMsg));
    }
}

function didNotFindPassword(data) {
    $("#loading").hide();
    var result = JSON.parse(data.responseText);
    popAlertLayer(getLoginMessage(result.resultMsg));
}

function contentProviderList(){
    callByGet("/api/codeInfo?comnCdCtg=CP&delYN=Y","contentProviderListSuccess","NoneForm");
}

function contentProviderListSuccess(data) {
    var contentProviderListHtml = "<option value=''>" + getLoginMessage("login.select.cp.title") + "</option>";
    if (data.resultCode == "1000") {
        $(data.result.codeList).each(function(i, item) {
            contentProviderListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNM + "</option>";
        });
    }
    $("#memberCp").html(contentProviderListHtml);
}

function formCloseFnc() {
    // 약관 동의 팝업
    $("#checkTermsAll").prop("checked", false);
    $("#checkUseTerms").prop("checked", false);
    $("#privacyUseTerms").prop("checked", false);

    // 회원가입 팝업
    $("#joinMember input[type='text'], #joinMember input[type='password']").val("");
    $("#joinMember ul > li > span.cautionTxt").text("");
    $("#memberCp option:eq(0)").prop("selected", true);
    $("#memberDomainList option:eq(0)").prop("selected", true);

    // 비밀번호 찾기 팝업
    $(".findForm input[type=text]").val("");
    $(".findForm select option:eq(0)").prop("selected", true);
}

function changeMemberEmailBox(obj) {
    $("#memberEmailDomain").val(obj.value);
}

function changeFindEmailBox(obj) {
    $("#findEmailDomain").val(obj.value);
}

function validate(obj) {
    var value = $(obj).val();
    var id = obj.id;
    var isValid = true;
    var msg = "";
    var msgId = "#" + id + "Msg";

    if (id == "memberEmailId") {
        value += $("#memberEmailDomain").val();
    }

    if (value.length <= 0) {
        isValid = false;
        msg = getLoginMessage("login.alert.required.msg");
    }

    if (isValid) {
        switch (id) {
        case "memberId":
            isValid = validateId($("#memberId").val());
            if (!isValid) {
                msg = getLoginMessage("login.alert.id.msg");
            }
            break;
        case "memberPwd":
            var validResult = validatePassword($("#memberPwd").val(), $("#memberId").val());
            isValid = (validResult === "valid");
            if (!isValid) {
                if (validResult === "continuousChar") {
                    msg = getLoginMessage("login.alert.continuous.character.msg");
                } else if (validResult === "containsIdPart") {
                    msg = getLoginMessage("login.alert.cotains.id.msg");
                } else {
                    msg = getLoginMessage("login.alert.password.msg");
                }
            }
            break;
        case "memberPwdRe":
            isValid = $("#memberPwd").val() == $("#memberPwdRe").val();
            if (!isValid) {
                msg = getLoginMessage("login.alert.passwordConfirm.msg");
                $("#memberPwdRe").val("");
            }
            break;
        case "memberName":
            isValid =  validateName($("#memberName").val());
            if (!isValid) {
                msg = getLoginMessage("login.alert.name.msg");
            }
            break;
        case "telNo":
            isValid = validateTelNumber($("#telNo").val());
            if (!isValid) {
                msg = getLoginMessage("login.alert.telNo.msg");
            } else {
                $("#telNo").val(phoneNumberFormatter($("#telNo").val()));
            }
            break;
        case "mobilePhone":
            isValid = validatePhoneNumber($("#mobilePhone").val());
            if (!isValid) {
                msg = getLoginMessage("login.alert.phoneNo.msg");
            } else {
                $("#mobilePhone").val(phoneNumberFormatter($("#mobilePhone").val()));
            }
            break;
        case "memberEmailId":
            isValid =  validateEmail($("#memberEmailId").val() + "@" + $("#memberEmailDomain").val());
            if (!isValid) {
                msg = getLoginMessage("login.alert.email.msg");
            }
            break;
        default:
            console.log("Nothing id = " + id);
            break;
        }
    }

    $(msgId).text(msg);
    return isValid;
}

function validateJoin() {
    var isValid = true;
    $("#joinMember input[type='text'], #joinMember input[type='password']").each(function(i, item) {
        var result = validate(item);
        isValid = result && isValid;
    });

    var cpValue = $("#memberCp option:selected").val();
    isValid = validateCpInfo(cpValue) && isValid;
    return isValid;
}

function validateCpInfo(cpValue) {
    var isValid = !( cpValue == undefined || cpValue == "");
    if (isValid) {
        $("#memberCpMsg").text("");
    } else {
        $("#memberCpMsg").text(getLoginMessage("login.alert.cp.msg"));
    }
    return isValid;
}

function changeMemberCp() {
    var cpValue = $("#memberCp option:selected").val();
    validateCpInfo(cpValue);
}

function popAlertLayer(msg, url, targetId) {
    setVisibilitySweetAlert(true);
    var content = document.createElement("div");
    content.innerHTML = msg;

    if (url) {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
            closeOnClickOutside: false,
            closeOnEsc: false,
        })
        .then(
                function(value){
                    if(value){
                        swal.close();
                        pageMove(url);
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    } else {
        swal({
            title: getLoginMessage("login.alert.msg"),
            content: content,
        })
        .then(
                function(value) {
                    if (targetId) {
                        document.getElementById(targetId).focus();
                    }
                    setVisibilitySweetAlert(false);
                }
        );
    }
}