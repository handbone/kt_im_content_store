<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swfupload/swfupload.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/swfupload/video_handlers.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/ContentEdit.js?version=6"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/swipebox/jquery.swipebox.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/libs/custorm-scrollbar/jquery.mCustomScrollbar.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/justifiedGallery/justifiedGallery.min.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/libs/justifiedGallery/jquery.justifiedGallery.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/libs/swipebox/swipebox.css">



<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="contsSeq" value="${contsSeq}">
<input type="hidden" id="memberSec" value="${memberSec}" />

<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp E -->
        <div id="tabGrp">
            <div class="tab tabOn tabEnd"><spring:message code='contents.edit.title' /></div>
        </div>
        <!-- tabGrp E -->

        <form id="fileForm">
            <input type="hidden" id="contsSeq" name="contsSeq" value="${contsSeq}">
            <input type="hidden" id="fileSe" name="fileSe">
            <input type="hidden" id="orginlFileNm" name="orginlFileNm">
            <input type="hidden" id="fileDir" name="fileDir">
            <input type="hidden" id="fileSize" name="fileSize">
            <input type="hidden" id="fileExt" name="fileExt">
            <input type="hidden" id="streFileNm" name="streFileNm">
            <input type="hidden" id="thumbList" name="thumbList">
        </form>

        <!-- Table Area S -->
        <div class="tableArea">
            <p align="left"><font color="red">*</font>&nbsp<font style="font-size: 9pt"><spring:message code='common.essential.input.msg' /></font></p>
            <table class="tableForm">
                <tbody>
                    <tr>
                        <th class="w150"><spring:message code='common.contents.name' />&nbsp<font color="red">*</font></th>
                        <td>
                            <input type="text" class="inputBox2 lengthXXL remaining" max="100" id="contsTitle" name="contsTitle">
                            <div class="boxStateBlack" id="sttusView" style="display: initial;"></div>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.category' />&nbsp<font color="red">*</font></th>
                        <td>
                            <select id="firstCtg" onchange="contsCtgList()"></select>
                            <select id="secondCtg"></select>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.contents.provider' />&nbsp<font color="red">*</font></th>
                        <td><select class="selectBox" id="cpList" onchange="$('#serviceList').html(''); ServiceList();">
                                <!-- 콘텐츠 제공사 리스트 -->
                        </select></td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.contents.genre' />&nbsp<font color="red">*</font></th>
                        <td id="genreList">
                            <!-- 장르 리스트 -->
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.service.nm' />&nbsp<font color="red">*</font></th>
                        <td id="serviceList">
                            <!-- 서비스 리스트 -->
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.contents.id' /></th>
                        <td id="contsID"></td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.contents.maxPeopl' /></th>
                        <td><select class="selectBox" name="maxAvlNop" id="maxAvlNop">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                        </select></td>
                    </tr>

                    <tr>
                        <th><spring:message code='common.sub.title' />&nbsp<font color="red">*</font></th>
                        <td><input class="lengthXL inputBox2 remaining" id="contsSubTitle" name="contsSubTitle" max="100"></td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.detail.description' />&nbsp<font color="red">*</font></th>
                        <td><textarea rows="10" class="textareaBox" id="contsDesc"
                                name="contsDesc"></textarea></td>
                    </tr>
                    <tr class="attrPart">
                        <th><spring:message code='common.file.type' /></th>
                        <td><select class="selectBox" id="exefileType">
                                <option value="ZIP">ZIP</option>
                                <option value="EXE">EXE</option>
                        </select></td>
                    </tr>
                    <tr style="display: none;" class="gamePart">
                        <th><spring:message code='common.run.file' /></th>
                        <td>
                            <div id="file_btn_placeholder"></div>
                            <div id="fileArea"></div>
                        </td>
                    </tr>
                    <tr class="exeFilePath">
                        <th><spring:message code='common.run.file.path' /></th>
                        <td><input type="text" class="inputBox lengthXXL" name="exeFilePath" id="exeFilePath"></td>
                    </tr>
                    <tr style="display: none;" class="videoPart">
                        <th><spring:message code='common.upload.video' /></th>
                        <td>
                            <div id="video_btn_placeholder"></div>
                            <div id="videoArea"></div>
                        </td>
                    </tr>
                    <tr style="display: none;" class="webtoonPart">
                        <th><spring:message code='common.upload.webtoon' /></th>
                        <td>
                            <div id="webtoon_btn_placeholder"></div>
                            <div id="webtoonArea"></div>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.upload.cover.image' />&nbsp<font color="red">*</font></th>
                        <td>
                            <div id="coverImg_btn_placeholder"></div>
                            <input type="button" id="btnCoverCretThumb" value="<spring:message code="common.create.thumbnail" />" class="btnDefaultWhite" onclick="createThumbImg('coverImgArea')">
                            <div id="coverImgArea"></div>
                        </td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.upload.thumbnail.image' />&nbsp<font color="red">*</font></th>
                        <td>
                            <div id="thumbnail_btn_placeholder"></div>
                            <input type="button" id="btnGalleryCretThumb" value="<spring:message code="common.create.thumbnail" />" class="btnDefaultWhite" onclick="createThumbImg('thumbnailArea')">
                            <div id="thumbnailArea"></div>
                        </td>
                    </tr>
                    <tr class="prevPart">
                        <th><spring:message code='common.upload.preview' /></th>
                        <td>
                            <div id="prev_btn_placeholder"></div>
                            <div id="prevArea" style="display: none;"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <spring:message code='common.contents.validity.period' />
                            <div class="infotip txtRed txtBold btnHelp">!
                                <div class="balloonGrp">
                                    <div class="balloon"><spring:message code="contents.table.infotip" /></div>
                                </div>
                            </div>
                        </th>
                        <td><input type="text" class="lengthS inputBox2 txtCenter datepicker" id="cntrctStDt" name="cntrctStDt" readonly> - <input type="text"
                            value="2018.9.1" class="lengthS inputBox2 txtCenter datepicker"
                            id="cntrctFnsDt" name="cntrctFnsDt" readonly></td>
                    </tr>
                    <tr>
                        <th><spring:message code='common.version' /></th>
                        <td>
                            <div id="contsVer"></div>
                            <input type="checkbox" id="versionModify"><spring:message code='contents.version.update.confirm.msg' />
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <!-- Table Area E -->

        <!-- Button Group S -->
        <div class="btnGrp txtCenter">
            <input type="button" class="btnNormal btnMetadata lengthM" value="<spring:message code='submeta.modify' />" onclick="showMetadataPopLayer();">
            <input type="button" class="btnNormal btnModify" onclick="validateAlreadyEdit();" value="<spring:message code='button.update.confirm' />">
            <input type="button" class="btnNormal btnCancel btnEditCancel" onclick="pageMove('/contents');" value="<spring:message code='button.reset' />">
        </div>
        <!-- Button Group E -->
    </div>
</div>
<!-- Contents E -->

<!-- popupEditSubmetadata S  -->
    <div id="popupEditSubmetadata" style="display: none;">
        <div class="popupTitle" id="popupEditSubmetadataTitle"><spring:message code='submeta.modify' /></div>
        <!-- Table Area S -->
        <div id="submetadataTableArea" class="metadataGrpSubmetadata">
            <table id="editSubmetadata">
            </table>
        </div>
        <!-- Table Area E -->

        <!-- Button Group S -->
        <div class="btnGrp txtCenter">
            <input type="button" class="btnNormal btnModify" onclick="editMetadata()" value="<spring:message code='submeta.button.modify'/>" />
            <input type="button" class="btnNormal btnCancel" onclick="popLayerClose()" value="<spring:message code='submeta.button.cancel'/>" />
        </div>
        <!-- Button Group E -->
    </div>
<!-- popupEditSubmetadata E  -->

<!-- 팝업 진행창 -->
<div id="divCenter" style="display: none;">
    <div id="popup_progressbar">
        <div class="bar">
            <img src="<c:url value='/resources/image/img/wgraph.gif'/>" width="0%" height="24"
                id="barWidth"><br> <span id="uploadPercent">0</span> <span
                id="uploadText"></span>
        </div>
    </div>
</div>

<!----------------------- 썸네일 미리보기 레이어 시작 ---------------------------->

<div id="previewThumnail" style="display:none;">
    <!-- content S -->
    <div class="content">
    <div class="popupTitle"><spring:message code='common.preview.thumbnail'/></div>
        <div id="originFile" class="thumName txtBold"></div>
        <div id="thumbView" style="display: none;"></div>
        <!-- table class="thumGrp">
            <tr>
                <td><img src="image/thum_100.jpg"><br>100 X 75 (px)</td>
                <td><img src="image/thum_160.jpg"><br>160 X 120 (px)</td>
                <td><img src="image/thum_320.jpg"><br>320 X 240 (px)</td>
            </tr>
        </table -->

        <!-- Button Group S -->
        <div class="btnGrp txtCenter">
            <input type="button" class="btnNormal btnClose" value="<spring:message code='button.close'/>" onclick="closePreviewThumbnail() ">
        </div>
        <!-- Button Group E -->
    </div>
</div>
<!-- content E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>