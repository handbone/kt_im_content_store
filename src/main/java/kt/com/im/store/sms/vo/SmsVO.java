/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.sms.vo;

import java.io.Serializable;

import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.sms.web.SmsApi;

/**
 *
 * SMS 발송 관련 VO
 *
 * @author A2TEC
 * @since 2018.09.04
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 9. 04.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class SmsVO extends MemberVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -8543784902535907695L;

    /** 발송 시간 */
    private String sendTime;

    /** 수신자 번호 */
    private String destPhone;

    /** 수신자 이름 */
    private String destName;

    /** 발신자 번호 */
    private String sendPhone = "01072017444";

    /** 발신자 이름 */
    private String sendName = SmsApi.MSG_TITLE;

    /** 발신 제목 (MMS) */
    private String subject = SmsApi.MSG_TITLE;

    /** 발신 내용 */
    private String msgBody;

    /** 결과 코드 */
    private String resultCode;

    /** 발송 상세 내역 */
    private String sendDesc;

    /** 생성자 아이디 */
    private String cretrId;

    /** 생성 일시 */
    private String cretDt;

    /** 문자 발송 SMS URL */
    String connectUrl;

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getDestPhone() {
        return destPhone;
    }

    public void setDestPhone(String destPhone) {
        this.destPhone = destPhone;
    }

    public String getDestName() {
        return destName;
    }

    public void setDestName(String destName) {
        this.destName = destName;
    }

    public String getSendPhone() {
        return sendPhone;
    }

    public void setSendPhone(String sendPhone) {
        this.sendPhone = sendPhone;
    }

    public String getSendName() {
        return sendName;
    }

    public void setSendName(String sendName) {
        this.sendName = sendName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMsgBody() {
        return msgBody;
    }

    public void setMsgBody(String msgBody) {
        this.msgBody = msgBody;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getSendDesc() {
        return sendDesc;
    }

    public void setSendDesc(String sendDesc) {
        this.sendDesc = sendDesc;
    }

    @Override
    public String getCretrId() {
        return cretrId;
    }

    @Override
    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    @Override
    public String getCretDt() {
        return cretDt;
    }

    @Override
    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getConnectUrl() {
        return connectUrl;
    }

    public void setConnectUrl(String connectUrl) {
        this.connectUrl = connectUrl;
    }

}
