/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    contractInfo();
    $("#ratTime").attr('disabled', true);
    $(".content").find("input[type=text]").attr('disabled',true);
    $(".content").find("input[type=radio]").attr('disabled',true);

    listBack($(".btnList"));
});


contractInfo = function(){
    callByGet("/api/contract?buyinSeq="+$("#buyinSeq").val(), "contractInfoSuccess", "NoneForm", "contractInfoFail");
}

contractInfoSuccess = function(data) {
    if(data.resultCode == "1000") {
        var item = data.result.contractInfo;

        $("#cpName").text(item.cpNm);
        $("#contractNo").text(item.buyinContNo);
        var titleObj = item.contsTitle.split(",");
        var titleHtml = "";
        $(titleObj).each(function(i){
            titleHtml += "<span style='display: inline-block; float: left;'>";
            titleHtml += titleObj[i];
            if(i != titleObj.length - 1) {
                titleHtml += ",&nbsp;";
            }
            titleHtml += "</span>";
        });

        $("#contractConts").html(titleHtml);
        $("#buyinContStDt").val(item.buyinContStDt);
        $("#buyinContFnsDt").val(item.buyinContFnsDt);
        $("input[name=ratYn][value="+item.ratYn+"]").attr("checked",true);
        $("#cpSeq").val(item.cpSeq);
        if (item.ratYn == "N") {
            $("input[type=radio][value=N]").attr("checked",true);
        } else {
            $("#runLmtCnt").val(item.runLmtCnt);
            $("#runPerPrc").val(item.runPerPrc);
            $("#ratTime").val(item.ratTime);
            $("#ratPrc").val(item.ratPrc);
            $("#lmsmpyPrc").val(item.lmsmpyPrc);


            switch(item.buyinContType) {
            //계약 타입 [ 횟수 ]
            case "C":
                $("input[type=radio][name=ratCntYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratCntYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            //계약 타입 [ 일시금 ]
            case "L":
                $("input[type=radio][name=ratLumpYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratLumpYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            //계약 타입 [ 시간 ]
            case "T":
                $("input[type=radio][name=ratTimeYn][value=Y]:not(input[name=ratYn])").attr("checked",true);
                $("input[type=radio][value=N]:not(input[name=ratTimeYn]):not(input[name=ratYn])").attr("checked",true);
                break;
            }
        }

        $(".remainingPrc").each(function(e) {
            $(this).val($(this).val().replace(/[^\d]+/g, ''));
            $(this).val(numberWithCommas($(this).val()));
        });
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/contents/contract");
    }
}

contractInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/contents/contract");
}

function numberWithCommas(x) {
    return x.toString().replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

