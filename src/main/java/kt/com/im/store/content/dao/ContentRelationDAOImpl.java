/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.content.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import kt.com.im.store.common.dao.StateAbstractMapper;
import kt.com.im.store.content.vo.ContentVO;

/**
 *
 * Content 이력에 관한 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.08.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 8. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("ContentRelationDAO")
@Transactional
public class ContentRelationDAOImpl extends StateAbstractMapper implements ContentRelationDAO {

    /**
     * Content 변경 내용 추가
     * 
     * @param ContentVO
     * @return 
     */
    @Override
    public int insertChangeHst(ContentVO vo) {
        return insert("ContentRelationDAO.insertChangeHst", vo);
    }

}
