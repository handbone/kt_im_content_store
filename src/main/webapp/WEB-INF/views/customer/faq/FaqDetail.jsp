<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/faq/FaqDetail.js"></script>
<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="1">
<input type="hidden" id="faqSeq" value="${faqSeq}">

<style>
#faqAns img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div id="tabGrp">
            <div class="tab tabEnd tabOn"><spring:message code="faq.detail.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S -->
        <div class="content faq">
            <div class="faqTitleQsn faqQuestion" id="faqTitleQsn"></div>
            <!-- Detail Area S -->
            <div class="faqDetail noBasic" id="faqQsn"></div>
            <!-- Detail Area E -->
            <div class="faqTitleAns faqAnswer"><span class="txtBlue"><spring:message code="faq.answer"/></span></div>
            <!-- Detail Area S -->
            <div class="faqDetail noBasic" id="faqAns"></div>
            <!-- Detail Area E -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/faq')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>