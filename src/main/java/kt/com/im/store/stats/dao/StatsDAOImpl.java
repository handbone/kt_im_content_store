/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.stats.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import kt.com.im.store.common.dao.mysqlAbstractMapper;
import kt.com.im.store.stats.vo.StatsVO;

/**
 *
 * 통계 관리에 관한 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.21
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 21.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("StatsDAO")
public class StatsDAOImpl extends mysqlAbstractMapper implements StatsDAO {

    /**
     * 콘텐츠 사용 정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getContentUsageList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentUsageList", vo);
    }

    /**
     * 콘텐츠 사용 정보 목록 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentUsageListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectContentUsageListTotalCount", vo);
    }

    /**
     * 콘텐츠 사용 상세정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getContentUsageDetailList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentUsageDetailList", vo);
    }

    /**
     * 콘텐츠 사용 정보 상세목록 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentUsageDetailListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectContentUsageDetailListTotalCount", vo);
    }

    /**
     * 콘텐츠 다운로드 정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<Map> getContentDownloadList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentDownloadList", vo);
    }

    /**
     * 콘텐츠 다운로드 정보 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentDownloadListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectContentDownloadListTotalCount", vo);
    }

    /**
     * 콘텐츠 다운로드 상세정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getContentDownloadDetailList(StatsVO vo) throws Exception {
        return selectList("StatsDAO.selectContentDownloadDetailList", vo);
    }

    /**
     * 콘텐츠 다운로드 정보 상세목록 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentDownloadDetailListTotalCount(StatsVO vo) throws Exception {
        return selectOne("StatsDAO.selectContentDownloadDetailListTotalCount", vo);
    }

    /**
     * 콘텐츠 검수현황 통계
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<StatsVO> contsStusStat(StatsVO vo) {
        return selectList("StatsDAO.contsStusStat", vo);
    }

    /**
     * 콘텐츠 사용현황 (TOP 5) 통계
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<StatsVO> contsUseStus(StatsVO vo) {
        return selectList("StatsDAO.contsUseStus", vo);
    }

    /**
     * 콘텐츠 다운현황 (TOP 5) 통계
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<StatsVO> contsDownTopStus(StatsVO vo) {
        return selectList("StatsDAO.contsDownTopStus", vo);
    }
}
