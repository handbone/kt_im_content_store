/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.notice.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.store.notice.dao.NoticeDAO;
import kt.com.im.store.notice.vo.NoticeVO;

/**
 *
 * 공지사항 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("NoticeService")
public class NoticeServiceImpl implements NoticeService {

    @Resource(name = "NoticeDAO")
    private NoticeDAO noticeDAO;

    /**
     * 공지사항 리스트 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 공지사항 리스트
     */
    @Override
    public List<NoticeVO> noticeList(NoticeVO vo) {
        return noticeDAO.noticeList(vo);
    }

    /**
     * 공지사항 리스트 합계 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 공지사항 리스트 합계
     */
    @Override
    public int noticeListTotalCount(NoticeVO vo) {
        int res = noticeDAO.noticeListTotalCount(vo);
        return res;
    }

    /**
     * 공지사항 상세 정보 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 공지사항 상세 정보
     */
    @Override
    public NoticeVO noticeDetail(NoticeVO vo) {
        return noticeDAO.noticeDetail(vo);
    }

    /**
     * 공지사항 조회수 업데이트
     * @param NoticeVO
     * @return
     */
    @Override
    public void updateNoticeRetvNum(NoticeVO vo) {
        noticeDAO.updateNoticeRetvNum(vo);
    }

    /**
     * 중요 공지사항 리스트 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 중요 공지사항 리스트
     */
    @Override
    public List<NoticeVO> noticeImptList(NoticeVO vo) {
        return noticeDAO.noticeImptList(vo);
    }

    /**
     * 중요 공지사항 리스트 합계 조회
     * @param NoticeVO
     * @return 검색 조건에 부합하는 중요 공지사항 리스트 합계
     */
    @Override
    public int noticeImptListTotalCount(NoticeVO vo) {
        int res = noticeDAO.noticeImptListTotalCount(vo);
        return res;
    }

}
