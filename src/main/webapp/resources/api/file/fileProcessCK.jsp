<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<json:object>
    <c:set var="req" value="${pageContext.request}" />
    <c:set var="baseURL" value="${fn:replace(req.requestURL, req.requestURI, '')}" />
    <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
    <c:if test="${ resultType eq 'fileUploadCKSuccess' }">
        <json:property name="filename" value="${ fileName }"/>
        <json:property name="uploaded" value="${ 1 }"/>
        <json:property name="url" value="${baseURL}${contextPath}/api/imgUrl?oriName=${ fileName }&filePath=${ file_path }" escapeXml="false" />
    </c:if>

    <c:if test="${ resultType eq 'fileUploadCKFail' }">
        <json:property name="uploaded" value="${ 0 }"/>
        <json:object name="error">
            <json:property name="message" value="${ errorMsg }"/>
        </json:object>
    </c:if>

</json:object>