/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.content.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.store.content.dao.ContentDAO;
import kt.com.im.store.content.dao.ContentRelationDAO;
import kt.com.im.store.content.vo.ContentVO;

/**
 *
 * 클래스에 대한 상세 설명
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 7.   A2TEC      최초생성
* 2018. 5. 10.  A2TEC      /api/contents (POST,GET,PUT)추가
* 2018. 5. 10.  A2TEC      /api/contentsCategory (POST,GET,PUT)추가
* 2018. 5. 14.  A2TEC      콘텐츠 수정관련 추가
* 2018. 5. 15.  A2TEC      콘텐츠 삭제관련 추가
* 2018. 5. 21.  A2TEC      콘텐츠 미리보기 파일 리스트 메소드 추가
* 2018. 5. 30.  A2TEC      콘텐츠 검수, 수정 이력 추가
 *
 *
 *      </pre>
 */


@Service("ContentService")
public class ContentServiceImpl implements ContentService {
    @Resource(name = "ContentDAO")
    private ContentDAO ContentDAO;

    @Resource(name = "ContentRelationDAO")
    private ContentRelationDAO ContentRelationDAO;

    /**
     * 검색조건에 해당되는 콘텐츠 리스트 정보
     *
     * @param ContentVO
     * @return 조회 목록 결과
     */
    @Override
    public List<ContentVO> contentsList(ContentVO data) {
        return ContentDAO.contentsList(data);
    }

    /**
     * 검색조건에 해당되는 콘텐츠 리스트의 총 개수
     *
     * @param ContentVO
     * @return 총 콘텐츠 수 /
     */
    @Override
    public int contentsListTotalCount(ContentVO data) {
        int res = ContentDAO.contentsListTotalCount(data);
        return res;
    }

    /**
     * 콘텐츠 상세 정보
     *
     * @param ContentVO
     * @return 콘텐츠 정보 (파일 / 장르 제외)
     */
    @Override
    public ContentVO contentInfo(ContentVO item) {
        return ContentDAO.contentInfo(item);
    }

    /**
     * 콘텐츠 장르 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 장르 정보
     */
    @Override
    public List<ContentVO> contentsGenreList(ContentVO data) {
        return ContentDAO.contentsGenreList(data);
    }

    /**
     * 콘텐츠 서비스 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 서비스 리스트 정보
     */
    @Override
    public List<ContentVO> contentServiceList(ContentVO data) {
        return ContentDAO.contentServiceList(data);
    }

    /**
     * 콘텐츠 첫번째 카테고리 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 첫번째 카테고리 리스트 정보
     */
    @Override
    public List<ContentVO> firstCtgList(ContentVO data) {
        return ContentDAO.firstCtgList(data);
    }

    /**
     * 콘텐츠 카테고리 리스트
     *
     * @param ContentVO
     * @return 콘텐츠 카테고리 리스트 정보
     */
    @Override
    public List<ContentVO> contsCtgList(ContentVO data) {
        return ContentDAO.contsCtgList(data);
    }

    /**
     * 콘텐츠 등록
     *
     * @param ContentVO
     * @return 콘텐츠 등록 성공 여부
     */
    @Override
    public int contentsInsert(ContentVO data) {
        int res = ContentDAO.contentsInsert(data);
        return res;
    }

    /**
     * 카테고리 값 (개수) 업데이트
     *
     * @param ContentVO
     * @return 카테고리 값 (개수) 업데이트 성공 여부
     */
    @Override
    public int contsCtgValueUpdate(ContentVO data) {
        int res = ContentDAO.contsCtgValueUpdate(data);
        return res;

    }

    /**
     * 콘텐츠 장르 정보 등록
     *
     * @param ContentVO
     * @return 콘텐츠 장르 정보 등록 성공 여부
     */
    @Override
    public int contentsGenreInsert(ContentVO data) {
        return ContentDAO.contentsGenreInsert(data);
    }

    /**
     * 콘텐츠 서비스 값 등록
     *
     * @param ContentVO
     * @return 콘텐츠 서비스 값 등록 성공 여부
     */
    @Override
    public int contentsServiceInsert(ContentVO data) {
        return ContentDAO.contentsServiceInsert(data);
    }

    /**
     * 콘텐츠 상태 값 변경
     *
     * @param ContentVO
     * @return 콘텐츠 상태 값 변경 성공 여부
     */
    @Override
    public int contentsSttusUpdate(ContentVO data) {
        return ContentDAO.contentsSttusUpdate(data);
    }

    /**
     * 콘텐츠 갤러리 이미지 목록
     *
     * @param ContentVO
     * @return 콘텐츠 갤러리 이미지 리스트
     */
    @Override
    public List<ContentVO> contentsThumbnailList(ContentVO data) {
        return ContentDAO.contentsThumbnailList(data);
    }

    /**
     * 콘텐츠 비디오 파일 목록
     *
     * @param ContentVO
     * @return 콘텐츠 비디오 파일 리스트
     */
    @Override
    public List<ContentVO> contentsVideoList(ContentVO data) {
        return ContentDAO.contentsVideoList(data);
    }

    /**
     * 콘텐츠 실행 파일 목록
     *
     * @param ContentVO
     * @return 콘텐츠 실행 파일 리스트
     */
    @Override
    public List<ContentVO> fileDataInfo(ContentVO data) {
        return ContentDAO.fileDataInfo(data);
    }

    /**
     * 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
     *
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 콘텐츠 xml 파일 정보
     */
    @Override
    public ContentVO contsMetaInfo(ContentVO data) {
        return ContentDAO.contsMetaInfo(data);
    }

    /**
     * 콘텐츠 비디오 경로에 맞는 xml 파일 정보
     *
     * @param ContentVO
     * @return 콘텐츠 비디오 경로에 맞는 xml 파일 정보
     */
    @Override
    public ContentVO metadataXmlInfo(ContentVO data) {
        return ContentDAO.metadataXmlInfo(data);
    }

    /**
     * 콘텐츠 커버 이미지 파일 정보
     *
     * @param ContentVO
     * @return 콘텐츠 커버 이미지 파일 정보
     */
    @Override
    public ContentVO contentCoverImageInfo(ContentVO data) {
        return ContentDAO.contentCoverImageInfo(data);
    }

    /**
     * 카테고리 값 변경
     * @param ContentVO
     * @return 카테고리 값 변경 성공 여부
     */
    @Override
    public int categoryValueUpdate(ContentVO data) {
        return ContentDAO.categoryValueUpdate(data);
    }

    /**
     * 파일 SEQ로 파일 정보 조회
     * @param ContentsVO
     * @return 파일 정보 출력
     */
    @Override
    public ContentVO fileInfo(ContentVO data) {
        return ContentDAO.fileInfo(data);
    }

    /**
     * fileSeq를 이용한 파일 삭제
     * @param ContentsVO
     * @return 파일 삭제 성공 여부
     */
    @Override
    public int thumbnailDelete(ContentVO data) {
        return ContentDAO.thumbnailDelete(data);
    }

    /**
     * 컨텐츠 정보 수정
     * @param ContentsVO
     * @return 컨텐츠 정보 수정 성공 여부
     */
    @Override
    public int contentsUpdate(ContentVO data) {
        return ContentDAO.contentsUpdate(data);
    }

    /**
     * 컨텐츠 정보 삭제
     * @param ContentsVO
     * @return 컨텐츠 정보 삭제 성공 여부
     */
    @Override
    public int contentsDelete(ContentVO data) {
        return ContentDAO.contentsDelete(data);
    }

    /**
     * 콘텐츠 장르 정보 삭제(exist)
     * @param ContentsVO
     * @return 콘텐츠 장르 정보 삭제(exist) 성공 여부
     */
    @Override
    public int contentsGenreDelete(ContentVO data) {
        return ContentDAO.contentsGenreDelete(data);
    }

    /**
     * 콘텐츠 서비스 정보 삭제(exist)
     * @param ContentsVO
     * @return 콘텐츠 서비스 정보 삭제(exist) 성공 여부
     */
    @Override
    public int contentsServiceDelete(ContentVO data) {
        return ContentDAO.contentsServiceDelete(data);
    }

    /**
     * 콘텐츠 미리보기 파일 목록
     *
     * @param ContentVO
     * @return 콘텐츠 미리보기 파일 리스트
     */
    @Override
    public List<ContentVO> contentsPrevList(ContentVO data) {
        return ContentDAO.contentsPrevList(data);
    }

    /**
     * 콘텐츠 검수 이력 목록
     *
     * @param ContentVO
     * @return 검수 이력 목록
     */
    @Override
    public List<ContentVO> verifyHstList(ContentVO data) {
        return ContentDAO.verifyHstList(data);
    }

    /**
     * 콘텐츠 검수 이력 목록의 총 개수
     *
     * @param ContentVO
     * @return 총 검수 이력 수
     */
    @Override
    public int verifyHstListTotalCount(ContentVO data) {
        return ContentDAO.verifyHstListTotalCount(data);
    }

    /**
     * 콘텐츠 수정 이력 목록
     *
     * @param ContentVO
     * @return 수정 이력 목록
     */
    @Override
    public List<ContentVO> modifyHstList(ContentVO data) {
        return ContentDAO.modifyHstList(data);
    }

    /**
     * 콘텐츠 수정 이력 목록의 총 개수
     *
     * @param ContentVO
     * @return 총 수정 이력 수
     */
    @Override
    public int modifyHstListTotalCount(ContentVO data) {
        return ContentDAO.modifyHstListTotalCount(data);
    }

    /**
     * 콘텐츠 제목 조회 (통계 사용 용도)
     *
     * @param ContentVO
     * @return 결과
     */
    @Override
    public ContentVO getContentTitle(ContentVO data) {
        return ContentDAO.getContentTitle(data);
    }

    /**
     * 추천 콘텐츠 목록
     *
     * @param ContentVO
     * @return 추천 콘텐츠 목록
     */
    @Override
    public List<ContentVO> recommendContentList(ContentVO data) {
        return ContentDAO.recommendContentList(data);
    }

    /**
     * 추천 콘텐츠 삭제(DEL_YN 컬럼 값 업데이트로 삭제 유무 판단)
     *
     * @param ContentVO
     * @return
     */
    @Override
    public int deleteRecommendContents(ContentVO data) {
        return ContentDAO.deleteRecommendContents(data);
    }

    /**
     * 콘텐츠 수정 이력 등록
     *
     * @param ContentVO
     * @return 
     */
    @Override
    public int insertChangeHst(ContentVO data) {
        return ContentRelationDAO.insertChangeHst(data);
    }

    /**
     * 대표 이미지 / 갤러리 이미지의 썸네일 리스트 조회
     * 
     * @param ContentVO
     * @return 썸네일 이미지 리스트
     */
    @Override
    public List<ContentVO> contentThumbList(ContentVO data) {
        return ContentDAO.contentThumbList(data);
    }

    /**
     * 대표이미지, 갤러리 이미지의 썸네일 정보 업데이트
     *
     * @param ContentVO
     * @return 업데이트 결과
     */
    @Override
    public int updateThumbFileInfo(ContentVO data) {
        int res = ContentDAO.updateThumbFileInfo(data);
        return res;
    }

    /**
     * 대표이미지, 갤러리 이미지 썸네일 파일 등록
     *
     * @param ContentVO
     * @return 결과
     */
    @Override
    public int thumbFileInsert(ContentVO data) {
        int res = ContentDAO.thumbFileInsert(data);
        return res;
    }

    /**
     * 콘텐츠 서브 메타데이터 정보 조회
     *
     * @param ContentVO
     * @return 조회 결과
     */
    @Override
    public ContentVO getMetadata(ContentVO data) {
        return ContentDAO.getMetadata(data);
    }

    /**
     * 메타데이터 등록
     *
     * @param ContentVO
     * @return 메타데이터 등록 성공 여부
     */
    @Override
    public int metadataInsert(ContentVO data) {
        int res = ContentDAO.metadataInsert(data);
        return res;
    }

    /**
     * 메타데이터 정보 수정
     * @param ContentsVO
     * @return 메타데이터 정보 수정 성공 여부
     */
    @Override
    public int metadataUpdate(ContentVO data) {
        return ContentDAO.metadataUpdate(data);
    }

    /**
     * 메타데이터 정보 삭제
     * @param ContentsVO
     * @return 메타데이터 정보 삭제 성공 여부
     */
    @Override
    public int metadataDelete(ContentVO data) {
        return ContentDAO.metadataDelete(data);
    }

    /**
     * 콘텐츠 정보 조회 (조건&조인 없이)
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public ContentVO contentCdtNotInfo(ContentVO data) {
        return ContentDAO.contentCdtNotInfo(data);
    }

    /**
     * 콘텐츠 정보 리스트 조회 (조건&조인 없이)
     *
     * @param ContentVO
     * @return 결과 값 (int)
     */
    @Override
    public List<ContentVO> contentCdtNotInfoList(ContentVO data) {
        return ContentDAO.contentCdtNotInfoList(data);
    }

}
