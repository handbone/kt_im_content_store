/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.common.util;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.member.service.MemberService;
import kt.com.im.store.member.vo.MemberVO;

public class ApiInterceptor extends HandlerInterceptorAdapter {

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            HttpSession session = request.getSession(true);
            if (session.getAttribute("authenticatedUser") == null) {
                MemberVO userVO = this.getAuthenticatedUser(request);
                session.setAttribute("authenticatedUser", userVO);
            }
        } catch (Exception e) {

        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        // 클라이언트의 요청을 처리한 뒤에 호출됨. 컨트롤러에서 예외가 발생되면 실행하지 않는다.
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
        // 클라이언트 요청 처리뒤 클리이언트에 뷰를 통해 응답을 전송한뒤 실행 됨. 뷰를 생설항때 예외가 발생해도 실행된다
        if (request.getSession().isNew()) {
            request.getSession().invalidate();
        }
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 동시에 핸들링
    }

    private MemberVO getAuthenticatedUser(HttpServletRequest request) throws Exception {
        MemberVO userVO = null;

        boolean hasSession = (request.getSession().getAttribute("seq") != null);
        boolean hasToken = (request.getHeader("x-token") != null);
        if (hasSession) {
            userVO = new MemberVO();
            userVO.setMbrSeq(Integer.parseInt((String) request.getSession().getAttribute("seq")));
            userVO.setMbrId((String) request.getSession().getAttribute("id"));
            userVO.setMbrNm((String) request.getSession().getAttribute("name"));
            userVO.setMbrSe((String) request.getSession().getAttribute("memberSec"));
            userVO.setMbrLevel(Integer.parseInt((String) request.getSession().getAttribute("level")));
            userVO.setCpSeq(Integer.parseInt((String) request.getSession().getAttribute("cpSeq")));
        } else if (hasToken) {
            MemberVO vo = new MemberVO();
            vo.setMbrTokn(request.getHeader("x-token"));
            userVO = memberService.getMember(vo);
        }

        if (userVO != null) {
            String clientIp = CommonFnc.getClientIp(request);
            userVO.setIpadr(clientIp);
        }
        return userVO;
    }
}
