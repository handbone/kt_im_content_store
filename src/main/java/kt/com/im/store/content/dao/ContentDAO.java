/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.content.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.store.content.vo.ContentVO;

/**
*
* 클래스에 대한 상세 설명
*
* @author A2TEC
* @since 2018
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 2.   A2TEC      최초생성
* 2018. 5. 10.  A2TEC      /api/contents (POST,GET,PUT)추가
* 2018. 5. 10.  A2TEC      /api/contentsCategory (POST,GET,PUT)추가
* 2018. 5. 14.  A2TEC      콘텐츠 수정관련 추가
* 2018. 5. 15.  A2TEC      콘텐츠 삭제관련 추가
* 2018. 5. 30.  A2TEC      콘텐츠 검수, 수정 이력 추가
*
* </pre>
*/

@Repository("ContentDAO")
public interface ContentDAO {

    List<ContentVO> contentsList(ContentVO data);

    int contentsListTotalCount(ContentVO seq);

    ContentVO contentInfo(ContentVO item);

    List<ContentVO> contentsGenreList(ContentVO data);

    List<ContentVO> contentServiceList(ContentVO data);

    List<ContentVO> firstCtgList(ContentVO data);

    List<ContentVO> contsCtgList(ContentVO data);

    int contentsInsert(ContentVO data);

    int contsCtgValueUpdate(ContentVO data);

    int contentsGenreInsert(ContentVO data);

    int contentsServiceInsert(ContentVO data);

    int contentsSttusUpdate(ContentVO data);
    
    List<ContentVO> contentsThumbnailList(ContentVO item);

    List<ContentVO> fileDataInfo(ContentVO item);

    List<ContentVO> contentsVideoList(ContentVO data);

    ContentVO metadataXmlInfo(ContentVO data);

    ContentVO contentCoverImageInfo(ContentVO data);

    int categoryValueUpdate(ContentVO data);

    ContentVO fileInfo(ContentVO data);

    int thumbnailDelete(ContentVO data);

    int contentsUpdate(ContentVO data);

    int contentsDelete(ContentVO data);

    int contentsGenreDelete(ContentVO data);

    int contentsServiceDelete(ContentVO data);

    List<ContentVO> contentsPrevList(ContentVO data);

    ContentVO contsMetaInfo(ContentVO data);

    List<ContentVO> verifyHstList(ContentVO data);

    int verifyHstListTotalCount(ContentVO data);

    List<ContentVO> modifyHstList(ContentVO data);

    int modifyHstListTotalCount(ContentVO data);

    ContentVO getContentTitle(ContentVO data);

    List<ContentVO> recommendContentList(ContentVO data);

    int deleteRecommendContents(ContentVO data);

    List<ContentVO> contentThumbList(ContentVO data);

    int updateThumbFileInfo(ContentVO data);

    int thumbFileInsert(ContentVO data);

    ContentVO getMetadata(ContentVO data);

    int metadataInsert(ContentVO data);

    int metadataUpdate(ContentVO data);

    int metadataDelete(ContentVO data);

    ContentVO contentCdtNotInfo(ContentVO data);

    List<ContentVO> contentCdtNotInfoList(ContentVO data);

}