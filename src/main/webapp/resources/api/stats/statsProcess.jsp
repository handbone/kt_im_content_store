<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'contentUsageInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentUsageList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * 10 + count) }" />
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="ctgNm" value="${ i.ctgNm }"/>
                        <json:property name="contsTotalPlayCount" value="${ i.contsTotalPlayCount }"/>
                        <json:property name="contsTotalPlayTime" value="${ i.contsTotalPlayTime }"/>
                        <json:property name="contsAvgPlayTime" value="${ i.contsAvgPlayTime }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentUsageDetailInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="contsTitle" value="${ contsTitle }" />
                <json:array name="contentUsageDetailList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * 10 + count) }" />
                        <json:property name="svcSeq" value="${ i.svcSeq }"/>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="svcNm" value="${ i.svcNm }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="contsTotalPlayCount" value="${ i.contsTotalPlayCount }"/>
                        <json:property name="contsTotalPlayTime" value="${ i.contsTotalPlayTime }"/>
                        <json:property name="contsAvgPlayTime" value="${ i.contsAvgPlayTime }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
        <c:if test="${ resultCode eq '1010' }">
            <json:object name="result">
                <json:property name="contsTitle" value="${ contsTitle }" />
             </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentDownloadDetailInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="storNm" value="${ storNm }" />
                <json:array name="contentDownloadDetailList" items="${item}" var="i">
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage - 1) * 10 + count) }" />
                        <json:property name="ctgNm" value="${ i.ctgNm }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="contsDownloadCount" value="${ i.contsDownloadCount }"/>
                        <json:property name="contsDownloadDate" value="${ i.contsDownloadDate }"/>
                        <json:property name="frmtnNm" value="${ i.frmtnNm }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage }" />
            </json:object>
        </c:if>
        <c:if test="${ resultCode eq '1010' }">
            <json:object name="result">
                <json:property name="storNm" value="${ storNm }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsStusStat' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contsStusStat" items="${item}" var="i">
                    <json:object>
                        <json:property name="sttus" value="${ i.sttus }"/>
                        <json:property name="sttusNm" value="${ i.comnCdNm }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsUseStus' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contsUseStus" items="${item}" var="i">
                    <json:object>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }"/>
                        <json:property name="second" value="${ i.second }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsDownTopStus' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contsDownTopStus" items="${item}" var="i">
                    <json:object>
                        <json:property name="storSeq" value="${ i.storSeq }"/>
                        <json:property name="storNm" value="${ i.storNm }"/>
                        <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                        <json:property name="count" value="${ i.count }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>



</json:object>