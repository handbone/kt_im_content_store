/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.store.stats.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kt.com.im.store.content.vo.ContentVO;

/**
 *
 * 통계 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.05.21
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 21.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class StatsVO extends DefaultStatsVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -7724043286083841011L;

    /** CP사 고유 번호 */
    int cpSeq;

    /** 첫번째 카테고리명 */
    String firstCategoryName;

    /** 두번째 카테고리명 */
    String secondCategoryName;

    /** 장르명 */
    String genreName;

    /** 카테고리 분류 명 */
    String firstCtgNm;

    /** 콘텐츠 상태 값 */
    String sttus;

    /** 콘텐츠 상태 값 명칭 */
    String comnCdNm;

    /** 카운트 */
    int count;

    /** 날짜 타입 */
    String dateType;

    /** 콘텐츠 고유 번호 */
    int contsSeq;

    /** 콘텐츠 명 */
    String contsTitle;

    /** 재생 일시 */
    String playDate;

    /** 재생 회수 */
    String playCount;

    /** 콘텐츠 총 사용 회수 */
    String contsTotalPlayCount;

    /** 콘텐츠 총 사용 시간 */
    String contsTotalPlayTime;

    /** 콘텐츠 평균 사용 시간 */
    String contsAvgPlayTime;

    /** 매장 고유 번호 */
    int storSeq;

    /** 시간 초 */
    int second;

    /** 매장 명 */
    String storNm;

    /** 첫번째 카테고리 정보 목록 */
    List<ContentVO> firstCtgList;

    /** 카테고리 고유 번호 */
    String ctgCtgSeq;

    /** 카테고리 명 (첫번째 + 두번째) */
    String ctgNm;

    /** 콘텐츠 다운로드 수 */
    String contsDownloadCount;

    /** 콘텐츠 다운로드 날짜 */
    String contsDownloadDate;

    /** 편성명 */
    String frmtnNm;

    /** 서비스 고유 번호 */
    int svcSeq;

    /** 서비스 명 */
    String svcNm;

    public void setContsAvgPlayTime(String contsAvgPlayTime) {
        this.contsAvgPlayTime = contsAvgPlayTime;
    }

    public int getCpSeq() {
        return cpSeq;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public String getFirstCategoryName() {
        return firstCategoryName;
    }

    public void setFirstCategoryName(String firstCategoryName) {
        this.firstCategoryName = firstCategoryName;
    }

    public String getSecondCategoryName() {
        return secondCategoryName;
    }

    public void setSecondCategoryName(String secondCategoryName) {
        this.secondCategoryName = secondCategoryName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public int getContsSeq() {
        return contsSeq;
    }

    public void setContsSeq(int contsSeq) {
        this.contsSeq = contsSeq;
    }

    public String getContsTitle() {
        return contsTitle;
    }

    public void setContsTitle(String contsTitle) {
        this.contsTitle = contsTitle;
    }

    public String getPlayDate() {
        return playDate;
    }

    public void setPlayDate(String playDate) {
        this.playDate = playDate;
    }

    public String getPlayCount() {
        return playCount;
    }

    public void setPlayCount(String playCount) {
        this.playCount = playCount;
    }

    public String getContsTotalPlayCount() {
        return contsTotalPlayCount;
    }

    public void setContsTotalPlayCount(String contsTotalPlayCount) {
        this.contsTotalPlayCount = contsTotalPlayCount;
    }

    public String getContsTotalPlayTime() {
        return contsTotalPlayTime;
    }

    public void setContsTotalPlayTime(String contsTotalPlayTime) {
        this.contsTotalPlayTime = contsTotalPlayTime;
    }

    public String getContsAvgPlayTime() {
        return contsAvgPlayTime;
    }

    public int getStorSeq() {
        return storSeq;
    }

    public void setStorSeq(int storSeq) {
        this.storSeq = storSeq;
    }

    public String getStorNm() {
        return storNm;
    }

    public void setStorNm(String storNm) {
        this.storNm = storNm;
    }

    public List<ContentVO> getFirstCtgList() {
        return firstCtgList;
    }

    public void setFirstCtgList(List<ContentVO> firstCtgList) {
        this.firstCtgList = firstCtgList;
    }

    public String getCtgCtgSeq() {
        return ctgCtgSeq;
    }

    public void setCtgCtgSeq(String ctgCtgSeq) {
        this.ctgCtgSeq = ctgCtgSeq;
    }

    public String getCtgNm() {
        return ctgNm;
    }

    public void setCtgNm(String ctgNm) {
        this.ctgNm = ctgNm;
    }

    public String getContsDownloadCount() {
        return contsDownloadCount;
    }

    public void setContsDownloadCount(String contsDownloadCount) {
        this.contsDownloadCount = contsDownloadCount;
    }

    public String getContsDownloadDate() {
        return contsDownloadDate;
    }

    public void setContsDownloadDate(String contsDownloadDate) {
        this.contsDownloadDate = contsDownloadDate;
    }

    public String getFrmtnNm() {
        return frmtnNm;
    }

    public void setFrmtnNm(String frmtnNm) {
        this.frmtnNm = frmtnNm;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getSttus() {
        return sttus;
    }

    public void setSttus(String sttus) {
        this.sttus = sttus;
    }

    public String getComnCdNm() {
        return comnCdNm;
    }

    public void setComnCdNm(String comnCdNm) {
        this.comnCdNm = comnCdNm;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public String getFirstCtgNm() {
        return firstCtgNm;
    }

    public void setFirstCtgNm(String firstCtgNm) {
        this.firstCtgNm = firstCtgNm;
    }

    public int getSvcSeq() {
        return svcSeq;
    }

    public void setSvcSeq(int svcSeq) {
        this.svcSeq = svcSeq;
    }

    public String getSvcNm() {
        return svcNm;
    }

    public void setSvcNm(String svcNm) {
        this.svcNm = svcNm;
    }

}
