/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.file.web;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.imgscalr.Scalr;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import kt.com.im.store.file.web.FileApi;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.CompressionUtil;
import kt.com.im.store.common.util.FormatCheckUtil;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.file.service.FileService;
import kt.com.im.store.file.vo.FileVO;

@Controller
public class FileApi {

    @Resource(name = "FileService")
    private FileService fileService;

    public void matrixTime(int delayTime) {
        long saveTime = System.currentTimeMillis();
        long currTime = 0;
        while (currTime - saveTime < delayTime) {
            currTime = System.currentTimeMillis();
        }
    }

    /**
     * Build Facility Process
     */
    @Inject
    private FileSystemResource fsResource;

    @RequestMapping(value = "/api/file")
    public ModelAndView videoProcess(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fileupload") MultipartFile file, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/file/fileProcess");
            FileVO item = new FileVO();
            if (request.getMethod().equals("POST")) {
                if (request.getParameter("_method") == "PUT") { // put
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else if (request.getParameter("_method") == "DELETE") { // delete
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else { // post
                    if (!file.isEmpty()) {
                        String checkString = CommonFnc.requiredChecking(request, "imgMode");
                        if (!checkString.equals("")) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                            return rsModel.getModelAndView();
                        }

                        String imgMode = request.getParameter("imgMode");
                        String contsSeq = request.getParameter("contsSeq") == null ? ""
                                : request.getParameter("contsSeq");
                        String filename = file.getOriginalFilename();
                        filename = new String(filename.getBytes("UTF-8"), "UTF-8");
                        if (!isValidFileName(filename)) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                            return rsModel.getModelAndView();
                        }

                        String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                        String filePath = request.getParameter("filePath") == null ? "" : request.getParameter("filePath");
                        String dir = "";
                        String root = "";

                        matrixTime(100);
                        long time;

                        if (filePath == "") {
                            time = System.currentTimeMillis();
                            // Creating the directory to store file
                            if (imgMode.equals("qna")) {
                                dir = "/" + imgMode + "/" + contsSeq + "/" + time + "." + ext;
                                root = fsResource.getPath() + "/" + imgMode + "/" + contsSeq + "/" + time + "." + ext;
                            } else if (contsSeq.equals("")) {
                                dir = "/" + imgMode + "/" + time + "." + ext;
                                root = fsResource.getPath() + "/" + imgMode + "/" + time + "." + ext;
                            } else {
                                dir = "/" + contsSeq + "/" + imgMode + "/" + time + "." + ext;
                                root = fsResource.getPath() + "/" + contsSeq + "/" + imgMode + "/" + time + "." + ext;
                            }

                            item.setStreFileNm(time + "." + ext);
                        } else {
                            dir = filePath;
                            root = fsResource.getPath() + filePath;
                            String pathTemp = filePath.split("/")[2];
                            item.setStreFileNm(pathTemp);
                        }

                        if (!isValidFilePath(root)) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                            return rsModel.getModelAndView();
                        }

                        File lOutFile = new File(root);
                        if (!lOutFile.exists())
                            lOutFile.mkdirs();

                        file.transferTo(lOutFile);

                        item.setOrginlFileNm(filename);
                        item.setFileDir(dir);
                        item.setFileExt(ext);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setResultType("fileUpload");
                        rsModel.setData("item", item);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                    }

                }
            } else { // get
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        }
        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/fileProcess")
    public ModelAndView fileProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/file/fileProcess");
            FileVO item = new FileVO();
            item.setCretrId((String) session.getAttribute("id"));
            if (request.getMethod().equals("POST")) {
                if (request.getParameter("_method") == "PUT") { // put
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else if (request.getParameter("_method") == "DELETE") { // delete
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else { // post
                    int contsSeq = request.getParameter("contsSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("contsSeq"));
                    int qnaSeq = request.getParameter("qnaSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("qnaSeq"));

                    String checkString = "";
                    if (contsSeq > 0) {
                        checkString = CommonFnc.requiredChecking(request,
                                "contsSeq,fileExt,fileDir,orginlFileNm,fileSize,streFileNm");
                        item.setFileContsSeq(contsSeq);
                    } else if (qnaSeq > 0) {
                        checkString = CommonFnc.requiredChecking(request,
                                "qnaSeq,fileExt,fileDir,orginlFileNm,fileSize,streFileNm");
                        item.setFileContsSeq(qnaSeq);
                    }

                    if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }

                    String ext = request.getParameter("fileExt");
                    String fileSe = request.getParameter("fileSe") == null ? "" : request.getParameter("fileSe");
                    if (fileSe.equals("qna")) {
                        item.setFileSe("QNA");
                    } else {
                        if (ext.toUpperCase().equalsIgnoreCase("JPG") || ext.toUpperCase().equalsIgnoreCase("JPEG")
                                || ext.toUpperCase().equalsIgnoreCase("GIF")
                                || ext.toUpperCase().equalsIgnoreCase("PNG")) { // upload

                            if (fileSe.equals("img")) {
                                item.setFileSe("IMAGE");
                            } else {
                                item.setFileSe("COVERIMG");
                                FileVO resData = fileService.fileInfo(item);
                                String filePath = "";
                                if (resData != null) {
                                    filePath = resData.getFilePath();
                                }
                                if (fileService.fileDelete(item) == 1) {
                                    File file = new File(fsResource.getPath() + filePath);
                                    file.delete();
                                }
                            }
                        } else if (ext.toUpperCase().equalsIgnoreCase("XML")) {
                            if (fileSe.equals("prevmeta")) {
                                item.setFileSe("PREVMETA");
                            } else if (fileSe.equals("contents")) {
                                item.setFileSe("CONTENTS");
                            } else {
                                item.setFileSe("METADATA");
                            }
                            FileVO resData = fileService.fileInfo(item);
                            String filePath = "";
                            if (resData != null) {
                                filePath = resData.getFilePath();
                            }
                            if (fileService.fileDelete(item) == 1) {
                                File file = new File(fsResource.getPath() + filePath);
                                file.delete();
                            }

                            /*
                             * if(fileService.filePathDelete(item) == 1){
                             * File f = new File(fsResource.getPath()+filePath);
                             * f.delete();
                             * }
                             * item.setfileDir(request.getParameter("fileDir"));
                             * if(fileService.filePathDelete(item) == 1){
                             * File f = new File(fsResource.getPath()+request.getParameter("fileDir"));
                             * f.delete();
                             * }
                             */
                            item.setFileSe("METADATA");
                        } else if (ext.toUpperCase().equalsIgnoreCase("MP4")) {
                            if (fileSe.equals("prev")) {
                                item.setFileSe("PREV");
                            } else {
                                item.setFileSe("VIDEO");
                            }

                            FileVO resData = fileService.fileInfo(item);
                            String filePath = "";
                            if (resData != null) {
                                filePath = resData.getFilePath();
                            }
                            if (fileService.fileDelete(item) == 1) {
                                File file = new File(fsResource.getPath() + filePath);
                                file.delete();
                            }
                        } else {
                            item.setFileSe("FILE");
                            /*
                             * FileVO resData = fileService.fileInfo(item);
                             * String filePath = "";
                             * if(resData != null){
                             * filePath = resData.getfilePath();
                             * }
                             * if(fileService.fileDelete(item) == 1){
                             * File f = new File(fsResource.getPath()+filePath);
                             * f.delete();
                             * }
                             */
                        }
                    }
                    item.setOrginlFileNm(request.getParameter("orginlFileNm"));
                    item.setFileDir(request.getParameter("fileDir"));
                    String fileSizeStr = request.getParameter("fileSize");
                    BigInteger fileSize = BigInteger.valueOf(0);
                    fileSize = fileSize.add(BigInteger.valueOf(Long.parseLong(fileSizeStr)));
                    item.setFileSize(fileSize);

                    item.setFileExt(ext);
                    item.setStreFileNm(request.getParameter("streFileNm"));
                    if (fileService.fileInsert(item) == 1) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    }
                }
            } else { // get
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        }
        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/contentsXml")
    public ModelAndView contentsXmlCreate(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcess");

        String checkString = CommonFnc.requiredChecking(request, "contsSeq");
        if (!checkString.equals("")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
            rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
            rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        FileVO item = new FileVO();
        String contsSeq = request.getParameter("contsSeq") == null ? "" : request.getParameter("contsSeq");
        //String contentsId = request.getParameter("contsID") == null ? "" : request.getParameter("contsID");
        String arrayParams = request.getParameter("videoFormData") == null ? "" : request.getParameter("videoFormData");
        String[] videoResult = arrayParams.split("GNB");

        // DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        // DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        // root elements
        // Document doc = docBuilder.newDocument();
        // Element rootElement = doc.createElement("contents");
        // doc.appendChild(rootElement);

        item.setFileContsSeq(Integer.parseInt(contsSeq));
        fileService.fileMatchDelete(item);

        if (getActualSize(videoResult) == 0) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            return rsModel.getModelAndView();
        }

        for (int i = 0; i < videoResult.length; i++) {
            String[] data = videoResult[i].split(",");
            // staff elements
            // Element staff = doc.createElement("Content");

            // rootElement.appendChild(staff);
            // set attribute to staff element
            // Attr attr1 = doc.createAttribute("media");
            // attr1.setValue(data[0].replace("/", "\\"));
            // staff.setAttributeNode(attr1);
            item.setvideoPath(data[0]);
            try {
                if (!data[1].equals("")) {
                    // Attr attr2 = doc.createAttribute("metadata");
                    // attr2.setValue(data[1].replace("/", "\\"));
                    // staff.setAttributeNode(attr2);

                    item.setMetadatas(data[1]);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                item.setMetadataPath("");
            }
            fileService.fileMatch(item);

        }

        item.setFileSe("CONTENTS");
        FileVO resData = fileService.fileInfo(item);
        // String filePath = "";
        // if (resData != null) {
        // filePath = resData.getFilePath();
        // }
        /*
         * if(fileDaoImpl.fileDelete(item) == 1){
         * File f = new File(fsResource.getPath()+filePath);
         * f.delete();
         * }
         */

        List<FileVO> metadataDeletList = fileService.metadataDeletList(item);
        for (FileVO itemList : metadataDeletList) {
            FileVO metaItem = new FileVO();
            String filePath1 = itemList.getFilePath();
            metaItem.setFileSeq(itemList.getFileSeq());
            if (fileService.fileSeqDelete(metaItem) == 1) {
                File file = new File(fsResource.getPath() + filePath1);
                file.delete();
            }
        }
        try {
            // TransformerFactory transformerFactory = TransformerFactory.newInstance();
            // Transformer transformer = transformerFactory.newTransformer();
            // DOMSource source = new DOMSource(doc);
            // long time = System.currentTimeMillis();
            // // Creating the directory to store file
            // // Creating the directory to store file
            // String dir = "";
            // String root = "";
            // if (resData == null) {
            // dir = "/" + contsSeq + "/contents/" + time + ".xml";
            // root = fsResource.getPath() + "/" + contsSeq + "/contents/" + time + ".xml";
            // } else {
            // dir = filePath;
            // root = fsResource.getPath() + filePath;
            // }
            // File lOutFile = new File(fsResource.getPath() + "/" + contsSeq + "/contents");
            // if (!lOutFile.exists()) {
            // lOutFile.mkdirs();
            // }
            // StreamResult resultXml = new StreamResult(root);
            // // Output to console for testing
            // // StreamResult result = new StreamResult(System.out);
            // transformer.transform(source, resultXml);

            if (resData == null) {
                // item.setOrginlFileNm("Contents.xml");
                // item.setFileDir(dir);
                // item.setFileExt("xml");
                // item.setStreFileNm(time + ".xml");
                // if (fileService.fileInsert(item) == 1) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                // } else {
                // rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                // rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT);
                // rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                // }
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new IOException();
        } finally {
        }

        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/package/{contsSeq}")
    public void packagedownload(HttpServletResponse response, @PathVariable(value = "contsSeq") String contsSeq)
            throws Exception {
        CompressionUtil compressutil = new CompressionUtil();
        FileVO item = new FileVO();
        item.setFileContsSeq(Integer.parseInt(contsSeq));
        List<FileVO> fileList = fileService.fileList(item);
        List<File> ziplist = new ArrayList<File>();
        List<String> nameList = new ArrayList<String>();
        String contentsName = "";
        for (FileVO fileInfo : fileList) {
            ziplist.add(new File(fsResource.getPath() + fileInfo.getFilePath()));
            nameList.add(fileInfo.getOrginlFileNm());
            contentsName = fileInfo.getTitle();
        }
        // Setting Resqponse Header
        File zippedFile = new File(fsResource.getPath() + "/zip/" + contentsName + "_xml.zip");
        compressutil.zip(ziplist, new FileOutputStream(zippedFile), nameList);

        response.setContentType("application/x-msdownload");

        String fileName = zippedFile.getName().toString(); // 리퀘스트로 넘어온 파일명
        String docName = URLEncoder.encode(fileName, "UTF-8"); // UTF-8로 인코딩

        response.setHeader("Content-Disposition", "attachment;filename=" + docName.replace("+", " ") + ";");

        int nRead = 0;
        byte btReadByte[] = new byte[(int) zippedFile.length()];

        if (zippedFile.length() > 0 && zippedFile.isFile()) {
            BufferedInputStream objBIS = new BufferedInputStream(new FileInputStream(zippedFile));
            BufferedOutputStream objBOS = new BufferedOutputStream(response.getOutputStream());

            while ((nRead = objBIS.read(btReadByte)) != -1) {
                objBOS.write(btReadByte, 0, nRead);
            }

            objBOS.flush();
            objBOS.close();
            objBIS.close();
            return;
        }

        zippedFile.delete();
    }

    /* String 배열 길이 */
    public static int getActualSize(String[] items) {
        int size = 0;
        for (int i = 0; i < items.length; i++) {

            if (items[i] != null) {
                size = size + 1;
            }
        }
        return size;

    }

    @RequestMapping(value = "/api/fileDownload/{fileSeq}")
    public void fileDownload(HttpServletRequest request, HttpServletResponse response,
            @PathVariable(value = "fileSeq") String fileSeq) throws Exception {
        FileVO item = new FileVO();
        if (!FormatCheckUtil.checkNumber(fileSeq)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        item.setFileSeq(Integer.parseInt(fileSeq));
        item.setFilePath("");
        FileVO metadataRes = fileService.MedatadataInfo(item);
        if (metadataRes == null) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }

        String filePath = fsResource.getPath() + metadataRes.getFilePath();
        if (!isValidFilePath(filePath)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        File downfile = new File(filePath);
        //long L = downfile.length();
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }
        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            // byte[] outByte = new byte[(int) L];
            // while (inputStream.read(outByte, 0, (int) L) != -1) {
            // outStream.write(outByte, 0, (int) L);
            // }

            byte[] buf = new byte[4096];
            int bytesread = 0, bytesBuffered = 0;
            System.out.println("[/api/fileDownload][filePath]:"+filePath + "[fileSeq]:"+fileSeq +" (START)");
            while ((bytesread = inputStream.read(buf)) > -1) {
                outStream.write(buf, 0, bytesread);
                bytesBuffered += bytesread;
                if (bytesBuffered > 1024 * 1024) { // flush after 1MB
                    bytesBuffered = 0;
                    outStream.flush();
                }
            }
            System.out.println("[/api/fileDownload][filePath]:"+filePath + "[fileSeq]:"+fileSeq +" (END)" + "[bytesBuffered]:"+bytesread);
        } catch (Exception e) {
            System.out.println("[_______FILE ERROR LOG_____API : fileDownload]");
            e.printStackTrace();
            throw new IOException();
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    @RequestMapping(value = "/api/imgUrl")
    public void fileDownload(HttpServletRequest request, HttpServletResponse response) throws Exception {
        //ResultModel rsModel = new ResultModel(response);
        FileVO item = new FileVO();

        String filePath = request.getParameter("filePath");
        String oriName = CommonFnc.emptyCheckString("oriName", request);

        String widthStr = CommonFnc.emptyCheckString("width", request);
        String heightStr = CommonFnc.emptyCheckString("height", request);

        if (oriName != null && !oriName.isEmpty()) {
            if (!isValidFileName(oriName)) {
                response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return;
            }
        }

        if (!widthStr.equals("") && !heightStr.equals("")) {
            String[] pathStrArr = filePath.split("/");
            String fName = pathStrArr[pathStrArr.length -1];
            String name = fName.substring(0, fName.lastIndexOf("."));
            String ext = fName.substring(fName.lastIndexOf(".") + 1, fName.length());

            String filePathWithSize = "";
            for (int i = 0; i < pathStrArr.length - 1; i++) { // 마지막 인자는 파일 명으로 제외
                if (i == 0) {
                    filePathWithSize += "/";
                }

                if (!pathStrArr[i].equals("")) {
                    filePathWithSize += pathStrArr[i] + "/";
                }
            }
            if (!filePathWithSize.equals("")) {
                filePathWithSize += name + "_" + widthStr + "x" + heightStr + "." + ext;
            }
            item.setFilePath(filePathWithSize);
        } else {
            item.setFilePath(filePath);
        }

        FileVO metadataRes = fileService.MedatadataInfo(item);

        if (metadataRes == null) {
            // 썸네일 이미지가 존재하지 않을 경우 원본 이미지 가져옴.
            if (!widthStr.equals("") && !heightStr.equals("")) {
                item.setFilePath(filePath);
                metadataRes = fileService.MedatadataInfo(item);

                if (metadataRes == null) {
                    metadataRes = new FileVO();
                    metadataRes.setFilePath(filePath);
                    metadataRes.setOrginlFileNm(oriName);
                }
            } else {
                metadataRes = new FileVO();
                metadataRes.setFilePath(filePath);
                metadataRes.setOrginlFileNm(oriName);
            }
        }

        String fullFilePath = fsResource.getPath() + metadataRes.getFilePath();
        if (!isValidFilePath(fullFilePath)) {
            response.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return;
        }

        File downfile = new File(fullFilePath);
        long L = downfile.length();
        if (!downfile.exists()) {
            response.setStatus(ResultModel.HTTP_STATUS_NOT_FOUND);
            return;
        }
        ServletOutputStream outStream = null;
        FileInputStream inputStream = null;
        try {
            outStream = response.getOutputStream();
            inputStream = new FileInputStream(downfile);

            String downName = null;
            String browser = request.getHeader("User-Agent");

            // 파일 인코딩
            if (browser.contains("MSIE") || browser.contains("Trident")) {
                downName = URLEncoder.encode(metadataRes.getOrginlFileNm(), "UTF-8").replaceAll("\\+", "%20");
            } else if (browser.contains("Chrome")) {
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < metadataRes.getOrginlFileNm().length(); i++) {
                    char c = metadataRes.getOrginlFileNm().charAt(i);
                    if (c > '~') {
                        sb.append(URLEncoder.encode("" + c, "UTF-8"));
                    } else {
                        sb.append(c);
                    }
                }
                downName = sb.toString();

            } else {
                downName = new String(metadataRes.getOrginlFileNm().getBytes("UTF-8"), "ISO-8859-1");
            }

            // Setting Resqponse Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=\"" + downName + "\"");
            response.setHeader("Content-Transfer-Encoding", "binary;");

            byte[] outByte = new byte[(int) L];
            while (inputStream.read(outByte, 0, (int) L) != -1) {
                outStream.write(outByte, 0, (int) L);
            }
        } catch (Exception e) {
            throw new IOException();
        } finally {
            inputStream.close();
            outStream.flush();
            outStream.close();
        }
        return;
    }

    @RequestMapping(value = "/api/fileCK")
    public ModelAndView ckProcess(HttpServletRequest request, HttpServletResponse response, FileVO item,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcessCK");
        MultipartFile file = item.getUpload();

        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        int CKEditorFuncNum = 0;

        if (item.getCKEditorFuncNum() != "" && item.getCKEditorFuncNum() != null) {
            CKEditorFuncNum = Integer.parseInt(item.getCKEditorFuncNum());
        }

        if (request.getMethod().equals("POST")) {
            if (!file.isEmpty()) {
                String filename = file.getOriginalFilename();
                filename = new String(filename.getBytes("ISO-8859-1"), "utf-8");
                if (!isValidFileName(filename)) {
                    rsModel.setData("errorMsg", "Not allowed file name.");
                    rsModel.setResultType("fileUploadCKFail");
                    return rsModel.getModelAndView();
                }

                String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());

                if (!isAllowFileType(ext)) {
                    rsModel.setData("errorMsg", "Not allowed file type.");
                    rsModel.setResultType("fileUploadCKFail");
                    return rsModel.getModelAndView();
                }

                String dir = "";
                String root = "";
                String filePath = "";

                matrixTime(100);
                long time;
                if (filePath == "") {
                    time = System.currentTimeMillis();
                    // Creating the directory to store file
                    dir = "/editor/" + time + "." + ext;
                    root = fsResource.getPath() + "/editor" + "/" + time + "." + ext;
                    item.setStreFileNm(time + "." + ext);
                }

                if (!isValidFilePath(root)) {
                    rsModel.setData("errorMsg", "Not allowed file.");
                    rsModel.setResultType("fileUploadCKFail");
                    return rsModel.getModelAndView();
                }

                File lOutFile = new File(root);
                if (!lOutFile.exists())
                    lOutFile.mkdirs();

                file.transferTo(lOutFile);

                item.setOrginlFileNm(filename);
                item.setFileDir(dir);
                item.setFileExt(ext);

                rsModel.setData("filename", filename);
                rsModel.setData("file_path", dir);
                rsModel.setResultType("fileUploadCKSuccess");
            } else {
                rsModel.setData("errorMsg", "File is empty!");
                rsModel.setResultType("fileUploadCKFail");
            }
        }

        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/thumbFileUpload")
    public ModelAndView thumbUploadProcess(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fileupload") MultipartFile file, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        FileVO item = new FileVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            if (!file.isEmpty()) {
                String checkString = CommonFnc.requiredChecking(request, "imgMode");
                if (!checkString.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                String imgMode = request.getParameter("imgMode");
                String contsSeq = request.getParameter("contsSeq") == null ? ""
                        : request.getParameter("contsSeq");
                String filename = file.getOriginalFilename();
                filename = new String(filename.getBytes("UTF-8"), "UTF-8");
                if (!isValidFileName(filename)) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                String filePath = request.getParameter("filePath") == null ? "" : request.getParameter("filePath");

                String thumbList = request.getParameter("thumbList") == null ? "" : request.getParameter("thumbList");
                String[] thumbArr = thumbList.split(", ");
                String[] thumbNmArr = thumbArr[0].split("/"); // 0번 이미지는 썸네일 생성 시의 원본이미지
                String name = thumbNmArr[thumbNmArr.length - 1].substring(0, thumbNmArr[thumbNmArr.length - 1].lastIndexOf("."));

                String dir = "/" + contsSeq + "/" + imgMode + "/";
                String root = fsResource.getPath() + "/" + contsSeq + "/" + imgMode + "/";

                if (!isValidDirPath(root)) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                // 생성된 썸네일 이동을 위해 썸네일 리스트의 파일들에 대한 유효성 검사
                boolean isValid = true;
                for (int i = 0; i < thumbArr.length; i++) {
                    String[] thumbNm = thumbArr[i].split("/");
                    String thumbPath = thumbNm[thumbNm.length - 1];
                    String fileExt = thumbPath.substring(thumbPath.lastIndexOf('.' ) + 1);
                    if (fileExt.equals("")) {
                        isValid = false;
                    }

                    if (!isAllowUploadFileType(fileExt)) {
                        isValid = false;
                    }
                }

                if (!isValid) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                File lOutFile = new File(root);
                if (!lOutFile.exists())
                    lOutFile.mkdirs();

                List<String> newThumbList = new ArrayList<String>();

                for (int i = 0; i < thumbArr.length; i++) {
                    String[] thumb = thumbArr[i].split("/");
                    String thumbNm = thumb[thumb.length - 1];
                    if (i == 0) { // thumbArr[0] 원본 이미지
                        item.setStreFileNm(thumbNm);
                    }
                    String moveFilePath = root + thumbNm;

                    File thumbFile =new File(fsResource.getPath() + thumbArr[i]);
                    if (thumbFile.renameTo(new File(moveFilePath))) {
                        newThumbList.add(dir + thumbNm);
                    }
                }

                item.setOrginlFileNm(filename);
                item.setFileDir(dir);
                item.setFileExt(ext);
                item.setThumbList(newThumbList);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setResultType("fileUpload");
                rsModel.setData("item", item);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    @RequestMapping(value = "/api/thumbFileProcess")
    public ModelAndView thumbFileProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        FileVO item = new FileVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);

            String checkString = "";
            if (contsSeq > 0) {
                checkString = CommonFnc.requiredChecking(request, "contsSeq,fileExt,fileDir,orginlFileNm,fileSize,streFileNm,fileSe");
            }

            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            item.setFileContsSeq(contsSeq);
            item.setCretrId(userVO.getMbrId());

            String ext = request.getParameter("fileExt");
            String fileSe = request.getParameter("fileSe");
            String thumbListStr = request.getParameter("thumbList");
            String[] thumbArr = thumbListStr.split(", ");

            if (thumbArr.length == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(empty thumbList)");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            if (fileSe.equals("cover")) {
                for (int i = 0; i < thumbArr.length; i++) { // 대표이미지
                    if (i == 0) { // 0번은 원본 이미지로 fileSe : COVERIMG, 나머지는 썸네일 이미지로 fileSe : COVERIMG_THUMB
                        item.setFileSe("COVERIMG");

                        String fileSizeStr = request.getParameter("fileSize");
                        BigInteger fileSize = BigInteger.valueOf(0);
                        fileSize = fileSize.add(BigInteger.valueOf(Long.parseLong(fileSizeStr)));
                        item.setFileSize(fileSize);
                    } else {
                        item.setFileSe("COVERIMG_T");
                        File file = new File(fsResource.getPath() + thumbArr[i]);
                        BigInteger fileSize = BigInteger.valueOf(0);
                        fileSize = fileSize.add(BigInteger.valueOf(file.length()));
                        item.setFileSize(fileSize);
                    }
                    String[] filePathArr = thumbArr[i].split("/");
                    String streFileNm = filePathArr[filePathArr.length - 1];
                    item.setStreFileNm(streFileNm);
                    item.setOrginlFileNm(request.getParameter("orginlFileNm"));
                    item.setFileDir(thumbArr[i]); // file_path
                    item.setFileExt(ext);

                    fileService.fileInsert(item);
                }
            } else { // 갤러리 이미지
                for (int i = 0; i < thumbArr.length; i++) {
                    if (i == 0) { // 0번은 원본 이미지로 fileSe : COVERIMG, 나머지는 썸네일 이미지로 fileSe : COVERIMG_THUMB
                        item.setFileSe("IMAGE");

                        String fileSizeStr = request.getParameter("fileSize");
                        BigInteger fileSize = BigInteger.valueOf(0);
                        fileSize = fileSize.add(BigInteger.valueOf(Long.parseLong(fileSizeStr)));
                        item.setFileSize(fileSize);
                    } else {
                        item.setFileSe("IMAGE_T");
                        File file = new File(fsResource.getPath() + thumbArr[i]);
                        BigInteger fileSize = BigInteger.valueOf(0);
                        fileSize = fileSize.add(BigInteger.valueOf(file.length()));
                        item.setFileSize(fileSize);
                    }
                    String[] filePathArr = thumbArr[i].split("/");
                    String streFileNm = filePathArr[filePathArr.length - 1];
                    item.setStreFileNm(streFileNm);
                    item.setOrginlFileNm(request.getParameter("orginlFileNm"));
                    item.setFileDir(thumbArr[i]); // file_path
                    item.setFileExt(ext);

                    fileService.fileInsert(item);
                }
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    /**
     * 썸네일 파일 생성 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/cretThumb")
    public ModelAndView cretThumbProcess(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fileupload") MultipartFile file, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        FileVO item = new FileVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            if (!file.isEmpty()) {
                String imgMode = request.getParameter("imgMode");
                String filename = file.getOriginalFilename();
                String thumbSizes = request.getParameter("thumbSizes");
                String fileId = request.getParameter("fileId");
                filename = new String(filename.getBytes("UTF-8"), "UTF-8");

                if (!isValidFileName(filename) || thumbSizes.equals("")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                String ext = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                String filePath = request.getParameter("filePath") == null ? "" : request.getParameter("filePath");
                String dir = "";
                String root = "";
                matrixTime(100);
                long time = 0;

                if (filePath == "") {
                    time = System.currentTimeMillis();
                    dir = "/temp/" + time + "." + ext;
                    root = fsResource.getPath() + "/temp/" + time + "." + ext;
                    item.setStreFileNm(time + "." + ext);
                } else {
                    dir = filePath;
                    root = fsResource.getPath() + filePath;
                    String pathTemp = filePath.split("/")[2];
                    item.setStreFileNm(pathTemp);
                }

                if (!isValidFilePath(root)) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                File lOutFile = new File(root);
                if (!lOutFile.exists())
                    lOutFile.mkdirs();

                file.transferTo(lOutFile);

                String streFileName = item.getStreFileNm();
                List<String> thumbList = new ArrayList<String>();
                // 썸네일 생성을 위한 원본 이미지
                thumbList.add(dir);
                String[] thumbSizeArr = thumbSizes.split(",");
                for (int i = 0; i < thumbSizeArr.length; i++) {
                    String[] sizeArr = thumbSizeArr[i].split("x");
                    thumbList.add(makeThumbnail(root, streFileName.substring(0, streFileName.lastIndexOf(".")), ext, Integer.parseInt(sizeArr[0]), Integer.parseInt(sizeArr[1])));
                }

                item.setThumbSe(CommonFnc.emptyCheckString("thumbSe", request));
                item.setOrginlFileNm(filename);
                item.setFileDir(dir);
                item.setFileExt(ext);
                item.setThumbList(thumbList);
                item.setFileId(fileId);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setResultType("fileThumbInfo");
                rsModel.setData("item", item);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
        } else {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    /**
     * 썸네일 파일 수정 API - 콘텐츠 수정화면에서 기 등록된 콘텐츠 이미지 파일의 썸네일을 재성성할 경우 호출 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/editThumb")
    public ModelAndView editThumbProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcess");
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        FileVO item = new FileVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            String editThumbListStr = request.getParameter("chgFileSeqs");

            if (editThumbListStr.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            String thumbSe = CommonFnc.emptyCheckString("thumbSe", request);
            String thumbSizes = request.getParameter("thumbSizes");
            String[] editThumbListArr = editThumbListStr.split(",");

            List<FileVO> list = new ArrayList<FileVO>();

            for (int i = 0; i < editThumbListArr.length; i++) {
                item.setFileSeq(Integer.parseInt(editThumbListArr[i]));
                FileVO vo = fileService.getFileInfoByFileSeq(item);

                String streFileNm = vo.getStreFileNm();
                String fileId = streFileNm.substring(0, streFileNm.lastIndexOf("."));
                vo.setFileId(fileId);
                vo.setThumbSe(thumbSe);

                String ext = vo.getStreFileNm().substring(vo.getStreFileNm().lastIndexOf(".") + 1, vo.getStreFileNm().length());
                String dir = "";
                String root = "";
                matrixTime(100);
                long time = System.currentTimeMillis();

                dir = "/temp/" + time + "." + ext;
                root = fsResource.getPath() + "/temp/" + time + "." + ext;

                if (!isValidFilePath(root)) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }

                FileInputStream input = null;
                FileOutputStream output = null;

                try{
                    File file = new File(fsResource.getPath() + vo.getFilePath());

                    // 원본 파일이 존재하지 않을 경우
                    if (!file.exists()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FILE_NOT_EXIST + "(" + vo.getOrginlFileNm() + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        return rsModel.getModelAndView();
                    }

                    input = new FileInputStream(file);
                    output = new FileOutputStream(new File(root));

                    int readBuffer = 0;
                    byte [] buffer = new byte[512];
                    while((readBuffer = input.read(buffer)) != -1) {
                        output.write(buffer, 0, readBuffer);
                    }

                    input.close();
                    output.close();

                    List<String> thumbList = new ArrayList<String>();
                    // 썸네일 생성을 위한 원본 이미지
                    thumbList.add(dir);
                    String[] thumbSizeArr = thumbSizes.split(",");
                    for (int j = 0; j < thumbSizeArr.length; j++) {
                        String[] sizeArr = thumbSizeArr[j].split("x");
                        thumbList.add(makeThumbnail(root, String.valueOf(time), ext, Integer.parseInt(sizeArr[0]), Integer.parseInt(sizeArr[1])));
                    }

                    vo.setThumbList(thumbList);

                    list.add(vo);
                } catch (Exception e) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                    rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                }
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setResultType("editThumbInfo");
            rsModel.setData("thumbSe", thumbSe);
            rsModel.setData("thumbInfoList", list);
        } else { // GET
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    /**
     * 썸네일 파일 삭제 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/delThumb")
    public ModelAndView delThumbProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/file/fileProcess");
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        FileVO item = new FileVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            String delThumbListStr = request.getParameter("delThumbList");

            if (delThumbListStr.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM);
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }

            String[] delThumbListArr = delThumbListStr.split(", ");
            String thumbSe = CommonFnc.emptyCheckString("thumbSe", request);
            String fileId = CommonFnc.emptyCheckString("fileId", request);

            for (int i = 0; i < delThumbListArr.length; i++) {
                File delFile = new File(fsResource.getPath() + delThumbListArr[i]);
                if (delFile.exists()) {
                    delFile.delete();
                }
            }

            item.setThumbSe(thumbSe);
            item.setFileId(fileId);
            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setResultType("delFileThumbInfo");
            rsModel.setData("item", item);
        } else { // GET
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        }

        return rsModel.getModelAndView();
    }

    protected boolean isAllowFileType(String fileType) {
        boolean isAllow = false;
        String[] allowFileTypeArr = { "jpg", "jpeg", "gif", "bmp", "png" };

        for (String allowFileType : allowFileTypeArr) {
            if (allowFileType.equals(fileType.toLowerCase())) {
                isAllow = true;
                break;
            }
        }

        return isAllow;
    }

    static public boolean isAllowUploadFileType(String fileType) {
        boolean isAllow = false;
        String[] allowFileTypeArr = { "pdf", "hwp", "txt", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "png", "jpg", "jpeg", "bmp", "gif", "mp4", "xml", "zip", "exe" };

        for (String allowFileType : allowFileTypeArr) {
            if (allowFileType.equalsIgnoreCase(fileType)) {
                isAllow = true;
                break;
            }
        }

        return isAllow;
    }

    static public boolean isValidFileName(String fileName) {
        if (fileName == null || fileName.isEmpty() || fileName.length() > 50) {
            return false;
        }

        if ((fileName.indexOf("\0")) != -1 || (fileName.indexOf(";")) != -1 || (fileName.indexOf("./")) != -1 || (fileName.indexOf(".\\")) != -1) {
            return false;
        }

        String fileExt = fileName.substring(fileName.lastIndexOf('.') + 1);
        if (fileExt.equals("")) {
            return false;
        }

        if (!isAllowUploadFileType(fileExt)) {
            return false;
        }

        return true;
    }

    static public boolean isValidFilePath(String filePath) {
        if (filePath == null || filePath.isEmpty()) {
            return false;
        }

        if ((filePath.indexOf("\0")) != -1 || (filePath.indexOf(";")) != -1 || (filePath.indexOf("..")) != -1
                || (filePath.indexOf("./")) != -1 || (filePath.indexOf(".\\")) != -1 || (filePath.indexOf(":")) != -1) {
            return false;
        }

        String fileExt = filePath.substring(filePath.lastIndexOf('.') + 1);
        if (fileExt.equals("")) {
            return false;
        }

        if (!isAllowUploadFileType(fileExt)) {
            return false;
        }

        return true;
    }

    static public boolean isValidDirPath(String path) {
        if (path == null || path.isEmpty()) {
            return false;
        }

        if ((path.indexOf("\0")) != -1 || (path.indexOf(";")) != -1 || (path.indexOf("..")) != -1
                || (path.indexOf("./")) != -1 || (path.indexOf(".\\")) != -1 || (path.indexOf(":")) != -1) {
            return false;
        }

        return true;
    }

    /**
     * 썸네일 생성
     */
    private String makeThumbnail(String filePath, String fileName, String fileExt, int thumbW, int thumbH) throws Exception {
        // 저장된 원본파일로부터 BufferedImage 객체를 생성합니다.
        BufferedImage srcImg = ImageIO.read(new File(filePath));

        // 원본 이미지의 너비와 높이 입니다.
        int originW = srcImg.getWidth();
        int originH = srcImg.getHeight();

        // 원본 너비를 기준으로 하여 썸네일의 비율로 높이를 계산합니다.
        int newW = originW;
        int newH = (originW * thumbH) / thumbW;

        // 계산된 높이가 원본보다 높다면 crop이 안되므로
        // 원본 높이를 기준으로 썸네일의 비율로 너비를 계산합니다.
        if(newH > originH) {
            newW = (originH * thumbW) / thumbH;
            newH = originH;
        }

        // 계산된 크기로 원본이미지를 가운데에서 crop 합니다.
        BufferedImage cropImg = Scalr.crop(srcImg, (originW - newW) / 2, (originH - newH) / 2, newW, newH);

        // crop된 이미지로 썸네일을 생성합니다.
        BufferedImage destImg = Scalr.resize(cropImg, thumbW, thumbH);

        // 썸네일을 저장합니다.
        String thumbName = "/temp/" + fileName + "_" + thumbW + "x" + thumbH + "." + fileExt;
        File thumbFile = new File(fsResource.getPath() + thumbName);

        ImageIO.write(destImg, fileExt, thumbFile);

        return thumbName;
    }

}
