/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.content.web;

import java.io.File;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.content.service.ContentService;
import kt.com.im.store.content.vo.ContentVO;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.member.web.MemberApi;

/**
 *
 * 콘텐츠에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 7.   A2TEC      최초생성
* 2018. 5. 10.  A2TEC      /api/contents (POST,GET,PUT)추가
* 2018. 5. 10.  A2TEC      /api/contentsCategory (POST,GET,PUT)추가
* 2018. 5. 14.  A2TEC      콘텐츠 수정관련 추가
* 2018. 5. 15.  A2TEC      콘텐츠 삭제관련 추가
* 2018. 5. 21.  A2TEC      콘텐츠 미리보기 파일 리스트 메소드 추가
 *
 *
 *      </pre>
 */

@Controller
public class ContentApi {
    @Resource(name = "ContentService")
    private ContentService contentService;

    @Inject
    private FileSystemResource fsResource;

    @Resource(name = "transactionManager")
    private DataSourceTransactionManager transactionManager;

    @Resource(name="stateTransactionManager")
    private DataSourceTransactionManager stateTransactionManager;

    /**
     * 콘텐츠 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contents")
    public ModelAndView contentsProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/contents/contentsProcess");
            ContentVO item = new ContentVO();
            if (request.getMethod().equals("POST")) {
                String Method = request.getParameter("_method");
                if (Method == null) {
                    Method = "POST";
                }
                if (Method.equals("PUT")) { // put
                    String sttus = request.getParameter("sttus") == null ? "" : request.getParameter("sttus");

                    String checkString = CommonFnc.requiredChecking(request,
                            "contsSeq,contsCtgSeq,contsTitle,contsSubTitle,contsDesc,maxAvlNop");
                    if (!sttus.equals("")) {
                        item.setContsSeq(Integer.parseInt(request.getParameter("contsSeq")));
                        item.setSttusVal(sttus);
                        item.setSttusChgrId((String) session.getAttribute("id"));
                        int result = contentService.contentsSttusUpdate(item);
                        if (result == 1) {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        } else {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_UPDATE);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        }
                        return rsModel.getModelAndView();
                    } else if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }
                    item.setContsSeq(Integer.parseInt(request.getParameter("contsSeq")));
                    item.setMbrID((String) session.getAttribute("id"));
                    item.setCretrID((String) session.getAttribute("id"));
                    item.setContsCtgSeq(Integer.parseInt(request.getParameter("contsCtgSeq")));
                    item.setContsTitle(request.getParameter("contsTitle"));
                    item.setContsSubTitle(request.getParameter("contsSubTitle"));
                    item.setContsDesc(request.getParameter("contsDesc"));
                    item.setMaxAvlNop(Integer.parseInt(request.getParameter("maxAvlNop")));
                    item.setCntrctStDt(request.getParameter("cntrctStDt"));
                    item.setCntrctFnsDt(request.getParameter("cntrctFnsDt"));

                    String fileType = request.getParameter("fileType") == null ? "" : request.getParameter("fileType");
                    String exeFilePath = request.getParameter("exeFilePath") == null ? ""
                            : request.getParameter("exeFilePath");
                    String versionModify = request.getParameter("versionModify") == null ? ""
                            : request.getParameter("versionModify");

                    int cpSeq = request.getParameter("cpSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("cpSeq"));

                    item.setFileType(fileType);
                    item.setExeFilePath(exeFilePath);
                    item.setFileType(fileType);
                    item.setCpSeq(cpSeq);
                    item.setVersionModify(versionModify);

                    // 공통 코드 예외 처리
                    item.setSttusVal("");

                    //String sttusSeq = request.getParameter("sttus") == null ? "" : request.getParameter("sttus");

                    String genres = request.getParameter("genre") == null ? "" : request.getParameter("genre");
                    String services = request.getParameter("service") == null ? "" : request.getParameter("service");
                    String[] genre = genres.split(",");
                    String[] service = services.split(",");

                    int result = 0;
                    List<String> deleteFilePath = null;
                    try {
                        String dThumbnailSeqs = CommonFnc.emptyCheckString("dThumbnailSeqs", request);
                        deleteFilePath = new ArrayList<String>();

                        if (dThumbnailSeqs != "") {
                            String[] dThumbnailSeq = dThumbnailSeqs.split(",");

                            for (int i = 0; i < dThumbnailSeq.length; i++) {
                                item.setFileSeq(Integer.parseInt(dThumbnailSeq[i]));
                                ContentVO resData = contentService.fileInfo(item);
                                String filePath = resData.getFilePath();
                                if (contentService.thumbnailDelete(item) == 1) {
                                    deleteFilePath.add(filePath);
                                    // video에 속한 metadata파일 삭제
                                    if (filePath.contains("video")) {
                                        ContentVO metaItem = new ContentVO();
                                        metaItem.setFilePath(filePath);
                                        ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                                        if (metadataInfo != null) {
                                            if (contentService.thumbnailDelete(metadataInfo) == 1) {
                                                deleteFilePath.add(metadataInfo.getFilePath());
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // 대표 이미지, 갤러리 이미지는 삭제 시 썸네일 정보가 있으면 함께 삭제되어야 하므로 dThumbnailSeqs와 분리하여 처리
                        String deleteThumbListSeqs = CommonFnc.emptyCheckString("deleteThumbListSeqs", request);
                        if (!deleteThumbListSeqs.equals("")) {
                            String[] delThumbListSeq = deleteThumbListSeqs.split(",");

                            for (int i = 0; i < delThumbListSeq.length; i++) {
                                item.setFileSeq(Integer.parseInt(delThumbListSeq[i]));
                                ContentVO vo = contentService.fileInfo(item);
                                vo.setContsSeq(item.getContsSeq());

                                String fileId = vo.getStreFileNm().substring(0, vo.getStreFileNm().lastIndexOf("."));
                                vo.setFileId(fileId);

                                // 썸네일 파일 리스트 조회
                                List<ContentVO> thumbFileList = contentService.contentThumbList(vo);
                                if (thumbFileList != null) {
                                    for (int j = 0; j < thumbFileList.size(); j++) {
                                        String filePath = thumbFileList.get(j).getFilePath();
                                        if (contentService.thumbnailDelete(thumbFileList.get(j)) == 1) {
                                            deleteFilePath.add(filePath);
                                        }
                                    }
                                }
                            }
                        }

                        // 대표 이미지, 갤러리 이미지에서 썸네일 재생성 버튼 클릭으로 썸네일 정보가 변경되었을 때 해당 정보 반영,
                        // 변경되었지만 삭제되었다면 deleteThumbListSeqs로 값을 전달 받으므로 해당 코드는 미 수행
                        String editThumbInfoStr = CommonFnc.emptyCheckString("editThumbInfo", request).replaceAll("&quot;", "\"");
                        if (!editThumbInfoStr.equals("")) {
                            JSONParser parser = new JSONParser();
                            JSONObject obj = (JSONObject) parser.parse(editThumbInfoStr);
                            JSONArray editThumbInfoArr = (JSONArray) obj.get("thumbInfo");
                            for (int i = 0; i < editThumbInfoArr.size(); i++) {
                                JSONObject thumbInfoObj = (JSONObject) editThumbInfoArr.get(i);
                                String thumbSe = thumbInfoObj.get("thumbSe").toString();
                                String[] thumbListArr = thumbInfoObj.get("thumbList").toString().split(", ");

                                ContentVO vo = new ContentVO();
                                vo.setContsSeq(item.getContsSeq());
                                vo.setFileId(thumbInfoObj.get("fileId").toString());

                                List<ContentVO> thumbInfoList = contentService.contentThumbList(vo);

                                String dir = "";
                                if (thumbInfoList != null) {
                                    // 기존 파일 삭제하도록 deleteFilePath에 추가하고 새로 생성한 썸네일을 가져옴.
                                    for (int j = 0; j < thumbInfoList.size(); j++) {
                                        if (vo.getOrginlFileNm() == null || vo.getOrginlFileNm().equals("")) {
                                            vo.setOrginlFileNm(thumbInfoList.get(j).getOrginlFileNm());
                                        }
                                        String filePath = thumbInfoList.get(j).getFilePath();

                                        if (dir.equals("")) {
                                            String[] path = filePath.split("/");
                                            for (int pIndex = 0; pIndex < path.length - 1; pIndex++) { // path의 마지막 값은 파일 명 이므로 제외
                                                if (pIndex > 0) {
                                                    dir += "/";
                                                }
                                                dir += path[pIndex];
                                            }
                                            if (!dir.equals("")) {
                                                dir += "/";
                                            }
                                        }

                                        if (j > thumbListArr.length - 1) {
                                            // 재 생성 썸네일 갯수보다 기존 파일이 많은 경우 썸네일 생성 갯수가 변경된 것이므로 이전 파일들은 삭제함.
                                            if (contentService.thumbnailDelete(thumbInfoList.get(j)) == 1) {
                                                deleteFilePath.add(thumbInfoList.get(j).getFilePath());
                                            }
                                        } else {
                                            String newThumbPath = thumbListArr[j];
                                            String[] newThumbPathArr = newThumbPath.split("/");
                                            String newThumbName = newThumbPathArr[newThumbPathArr.length -1];
    
                                            if (newThumbName != null && !newThumbName.equals("")) {
                                                thumbInfoList.get(j).setStreFileNm(newThumbName);
                                                String moveFilePath = dir + newThumbName;
                                                thumbInfoList.get(j).setFilePath(moveFilePath);

                                                File thumbFile =new File(fsResource.getPath() + newThumbPath);
                                                if (thumbFile.renameTo(new File(fsResource.getPath() + moveFilePath))) {
                                                    File nFile = new File(fsResource.getPath() + moveFilePath);
                                                    BigInteger fileSize = BigInteger.valueOf(0);
                                                    fileSize = fileSize.add(BigInteger.valueOf(nFile.length()));
                                                    thumbInfoList.get(j).setFileSize(fileSize);
    
                                                    if (contentService.updateThumbFileInfo(thumbInfoList.get(j)) == 1) {
                                                        deleteFilePath.add(filePath);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (thumbInfoList.size() < thumbListArr.length) {
                                        // 재 생성한 썸네일이 기존 저장된 썸네일 보다 갯수가 많을 경우 기존 정보들을 업데이트 하고 남은 정보들은 insert
                                        for (int addIndex = thumbInfoList.size(); addIndex < thumbListArr.length; addIndex++) {
                                            vo.setCretrID(userVO.getMbrId());
                                            String newThumbPath = thumbListArr[addIndex];
                                            String[] newThumbPathArr = newThumbPath.split("/");
                                            String newThumbName = newThumbPathArr[newThumbPathArr.length -1];

                                            if (newThumbName != null && !newThumbName.equals("")) {
                                                vo.setStreFileNm(newThumbName);
                                                String moveFilePath = dir + newThumbName;
                                                vo.setFilePath(moveFilePath);

                                                File thumbFile =new File(fsResource.getPath() + newThumbPath);
                                                if (thumbFile.renameTo(new File(fsResource.getPath() + moveFilePath))) {
                                                    File nFile = new File(fsResource.getPath() + moveFilePath);
                                                    BigInteger fileSize = BigInteger.valueOf(0);
                                                    fileSize = fileSize.add(BigInteger.valueOf(nFile.length()));
                                                    vo.setFileSize(fileSize);
                                                    if (thumbSe.equals("coverImg")) {
                                                        vo.setFileSe("COVERIMG_T");
                                                    } else if (thumbSe.equals("thumbnailImg")) {
                                                        vo.setFileSe("IMAGE_T");
                                                    }

                                                    contentService.thumbFileInsert(vo);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        result = contentService.contentsUpdate(item);

                        if (result == 1) {
                            List<Integer> list = new ArrayList<Integer>();
                            for (int i = 0; i < genre.length; i++) {
                                list.add(Integer.parseInt(genre[i]));
                            }
                            item.setList(list);

                            contentService.contentsGenreInsert(item);
                            contentService.contentsGenreDelete(item);

                            list = new ArrayList<Integer>();

                            for (int i = 0; i < service.length; i++) {
                                list.add(Integer.parseInt(service[i]));
                            }
                            item.setList(list);

                            contentService.contentsServiceInsert(item);
                            contentService.contentsServiceDelete(item);

                            for (int i = 0; i < deleteFilePath.size(); i++) {
                                File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                                f.delete();
                            }

                            int contsSubMetadataSeq = CommonFnc.emptyCheckInt("contsSubMetadataSeq", request);
                            if (contsSubMetadataSeq != 0) {
                                item.setContsSubMetadataSeq(contsSubMetadataSeq);
                                contentService.metadataDelete(item);
                            }

                            // 콘텐츠 공개기간이 현재보다 이전으로 변경되었을 경우 해당 콘텐츠가 추천 콘텐츠에 포함되어 있다면 추천 콘텐츠 비활성화 처리함.
                            // START
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            Calendar c1 = Calendar.getInstance();
                            String strToday = sdf.format(c1.getTime());
                            Date today = sdf.parse(strToday);
                            Date contFnsdate = sdf.parse(request.getParameter("cntrctFnsDt"));

                            if (today.compareTo(contFnsdate) > 0) {
                                ContentVO contVO = new ContentVO();
                                List<ContentVO> resultItem = contentService.recommendContentList(contVO);
                                List<Integer> delRcmdContsSeqList = new ArrayList<Integer>();
                                if (!resultItem.isEmpty()) {
                                    int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));
                                    for (int j = 0; j < resultItem.size(); j++) {
                                        int rcmdContsSeq = resultItem.get(j).getContsSeq();
                                        if (contsSeq == rcmdContsSeq) {
                                            delRcmdContsSeqList.add(contsSeq);
                                        }
                                    }
                                }

                                if (delRcmdContsSeqList.size() > 0) {
                                    contVO = new ContentVO();
                                    contVO.setAmdrID(userVO.getMbrId());
                                    contVO.setDelRcmdContsSeqList(delRcmdContsSeqList);
                                    contentService.deleteRecommendContents(contVO);
                                }
                            }
                            // END

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        } else {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                            rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        }
                        rsModel.setResultType("contentsUpdate");
                    } catch (Exception e) {

                    } finally {
                    }

                    /* - 불필요 코드로 삭제 필요.
                    if (result == 1) {
                        for (int i = 0; i < deleteFilePath.size(); i++) {
                            File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                            f.delete();
                        }
                    }
                    rsModel.setResultType("contentsUpdate");
                    */

                    // 콘텐츠장르 변경 시 이력 저장
                    String genreCol = CommonFnc.emptyCheckString("genreCol", request);
                    String befGenres = CommonFnc.emptyCheckString("beforeGenres", request);
                    String editGenres = CommonFnc.emptyCheckString("editGenres", request);

                    if (!genreCol.equals("")) {
                        item.setContsChgInfo(genreCol);
                        item.setLegacyValue(befGenres);
                        item.setChangeValue(editGenres);
                        item.setAmdrID(userVO.getMbrId());

                        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                        TransactionStatus status = stateTransactionManager.getTransaction(def);

                        try {
                            contentService.insertChangeHst(item);
                            stateTransactionManager.commit(status);
                        } catch (Exception e) {
                            stateTransactionManager.rollback(status);
                        } finally {
                            if (!status.isCompleted()) {
                                stateTransactionManager.rollback(status);
                            }
                        }
                    }

                    // 대상서비스 변경 시 이력 저장
                    String serviceCol = CommonFnc.emptyCheckString("serviceCol", request);
                    String befServices = CommonFnc.emptyCheckString("beforeServices", request);
                    String editServices = CommonFnc.emptyCheckString("editServices", request);

                    if (!serviceCol.equals("")) {
                        item.setContsChgInfo(serviceCol);
                        item.setLegacyValue(befServices);
                        item.setChangeValue(editServices);
                        item.setAmdrID(userVO.getMbrId());

                        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                        TransactionStatus status = stateTransactionManager.getTransaction(def);

                        try {
                            contentService.insertChangeHst(item);
                            stateTransactionManager.commit(status);
                        } catch (Exception e) {
                            stateTransactionManager.rollback(status);
                        } finally {
                            if (!status.isCompleted()) {
                                stateTransactionManager.rollback(status);
                            }
                        }
                    }
                } else if (Method.equals("DELETE")) { // delete
                    int contsSeq = request.getParameter("contsSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("contsSeq"));
                    String delYn = request.getParameter("delYn") == null ? "Y" : request.getParameter("delYn");

                    /* Checking Mendatory S */
                    String checkString = CommonFnc.requiredChecking(request, "contsSeq");
                    if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }
                    /* Checking Mendatory E */

                    DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                    def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                    TransactionStatus status = transactionManager.getTransaction(def);

                    try {
                        item.setContsSeq(contsSeq);
                        item.setDelYn(delYn);
                        item.setMbrID((String) session.getAttribute("id"));
                        contentService.contentsDelete(item);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        transactionManager.commit(status);
                    } catch (Exception e) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    } finally {
                        if (!status.isCompleted()) {
                            transactionManager.rollback(status);
                        }
                    }

                } else { // post
                    String checkString = CommonFnc.requiredChecking(request,
                            "contsCtgSeq,contsTitle,contsSubTitle,contsDesc,contsID,maxAvlNop,sttus");
                    if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }

                    DefaultTransactionDefinition def = new DefaultTransactionDefinition();
                    def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
                    TransactionStatus status = transactionManager.getTransaction(def);

                    item.setMbrID((String) session.getAttribute("id"));
                    item.setCretrID((String) session.getAttribute("id"));

                    item.setContsCtgSeq(Integer.parseInt(request.getParameter("contsCtgSeq")));
                    item.setContsTitle(request.getParameter("contsTitle"));
                    item.setContsSubTitle(request.getParameter("contsSubTitle"));
                    item.setContsDesc(request.getParameter("contsDesc"));
                    item.setMaxAvlNop(Integer.parseInt(request.getParameter("maxAvlNop")));
                    item.setCntrctStDt(request.getParameter("cntrctStDt"));
                    item.setCntrctFnsDt(request.getParameter("cntrctFnsDt"));
                    item.setSttusVal(request.getParameter("sttus"));

                    int contsCtgSeq = CommonFnc.emptyCheckInt("contsCtgSeq", request);
                    item.setUnique("off");
                    item.setGroupingYN("N");
                    item.setContsCtgSeq(contsCtgSeq);
                    List<ContentVO> resultCtgList = contentService.contsCtgList(item);
                    int ctgCnt = 0;
                    for (int i = 0; i < resultCtgList.size(); i++) {
                        ContentVO CtgItem = resultCtgList.get(i);
                        if (CtgItem.getContsCtgSeq() == contsCtgSeq) {
                            ctgCnt = CtgItem.getCtgCnt();
                            break;
                        }

                    }

                    String ctgStr = String.format("%05d", ctgCnt); // 0009

                    item.setSttusChgrId((String) session.getAttribute("id"));

                    String fileType = request.getParameter("fileType") == null ? "" : request.getParameter("fileType");
                    String exeFilePath = request.getParameter("exeFilePath") == null ? ""
                            : request.getParameter("exeFilePath");
                    String contsID = request.getParameter("contsID") == null ? "" : request.getParameter("contsID");

                    int cpSeq = request.getParameter("cpSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("cpSeq"));

                    // contsId 작업

                    item.setFileType(fileType);
                    item.setExeFilePath(exeFilePath);
                    item.setContsID(contsID + ctgStr);
                    item.setFileType(fileType);
                    item.setCpSeq(cpSeq);

                    // String sttusSeq = request.getParameter("sttus") == null ? "" : request.getParameter("sttus");

                    String genres = request.getParameter("genre") == null ? "" : request.getParameter("genre");
                    String services = request.getParameter("service") == null ? "" : request.getParameter("service");
                    String[] genre = genres.split(",");
                    String[] service = services.split(",");
                    final Logger log = LogManager.getLogger();
                    try {
                        log.warn("contents INSERT_Param :{}", item.toString());
                        int result = contentService.contentsInsert(item);
                        log.warn("contents INSERT Success");
                        if (result == 1) {
                            log.warn("contsCtgValue UPDATE");
                            // if (contentService.contsCtgValueUpdate(item) == 1) {
                            // log.warn("contsCtgValue UPDATE Success");
                            List<Integer> list = new ArrayList<Integer>();
                            log.warn("contsGenre INSERT");
                            for (int i = 0; i < genre.length; i++) {
                                list.add(Integer.parseInt(genre[i]));
                            }
                            item.setList(list);
                            log.warn("contsGenre_Param :{}", list.toString());
                            contentService.contentsGenreInsert(item);
                            log.warn("contsGenre INSERT Success");
                            list = new ArrayList<Integer>();

                            for (int i = 0; i < service.length; i++) {
                                list.add(Integer.parseInt(service[i]));
                            }
                            item.setList(list);
                            log.warn("contsService INSERT :{}", list.toString());

                            contentService.contentsServiceInsert(item);
                            log.warn("contsService Success");

                            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            rsModel.setData("contsSeq", item.getContsSeq());
                            // } else {
                            // rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
                            // rsModel.setResultMessage(ResultModel.MESSAGE_UNAVAILABLE_CAT_VAL);
                            // rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                            // }

                        } else {
                            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                            rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_INSERT_CONTS);
                            rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        }
                        transactionManager.commit(status);
                    } catch (Exception e) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                        rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                        transactionManager.rollback(status);
                    } finally {
                        if (!status.isCompleted()) {
                            transactionManager.rollback(status);
                        }
                    }

                    rsModel.setResultType("contentsInsert");
                }
            } else { // get
                int contsSeq = request.getParameter("contsSeq") == null ? 0
                        : Integer.parseInt(request.getParameter("contsSeq"));

                /** 리스트 검색 콘텐츠 상태 값 번호 */
                String sttusSeq = CommonFnc.emptyCheckString("sttus", request);

                /** 리스트 CP사 번호 */
                int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

                // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
                if (userVO.getMbrSe().equals("06")) {
                    cpSeq = userVO.getCpSeq();
                }

                int svcSeq = CommonFnc.emptyCheckInt("svcSeq", request);

                /** 제외 코드값 */
                String remoteCodes = CommonFnc.emptyCheckString("remoteCode", request);

                String[] remoteCode = remoteCodes.split(",");

                List<String> list = new ArrayList<String>();
                for (int i = 0; i < remoteCode.length; i++) {
                    list.add(remoteCode[i]);
                }
                item.setList(list);

                item.setCpSeq(cpSeq);
                item.setSvcSeq(svcSeq);
                item.setSttusVal(sttusSeq);

                if (contsSeq != 0) {
                    item.setContsSeq(contsSeq);
                    /** 해당 콘텐츠 장르 리스트 */
                    List<ContentVO> genreList = contentService.contentsGenreList(item);

                    /** 해당 콘텐츠 상세정보 */
                    ContentVO resultItem = contentService.contentInfo(item);

                    /** 해당 콘텐츠 대표 이미지 */
                    ContentVO coverImg = contentService.contentCoverImageInfo(item);

                    List<String> thumbList = null;
                    if (coverImg != null) {
                        /** 해당 콘텐츠 대표 이미지 썸네일 리스트*/
                        // 저장 파일명(파일 확장자 제외)으로 해당 이미지의 썸네일 리스트 조회
                        coverImg.setContsSeq(item.getContsSeq());
                        coverImg.setThumbSe("COVERIMG_T");
                        String fileId = coverImg.getStreFileNm().substring(0, coverImg.getStreFileNm().lastIndexOf("."));
                        coverImg.setFileId(fileId);
                        thumbList = new ArrayList<String>();
                        thumbList.add(coverImg.getFilePath());
                        List<ContentVO> coverThumbList = contentService.contentThumbList(coverImg);

                        if (coverThumbList != null) {
                            for (int i = 0; i < coverThumbList.size(); i++) {
                                thumbList.add(coverThumbList.get(i).getFilePath());
                            }

                            coverImg.setThumbList(thumbList);
                        }
                    }

                    /** 해당 콘텐츠 서비스 리스트 */
                    List<ContentVO> serviceList = contentService.contentServiceList(item);

                    /** 해당 콘텐츠 갤러리 이미지 리스트 */
                    List<ContentVO> thumbnailList = contentService.contentsThumbnailList(item);

                    /** 해당 콘텐츠 갤러리 이미지 썸네일 리스트*/
                    if (thumbnailList != null) {
                        for (int i = 0; i < thumbnailList.size(); i++) {
                            ContentVO vo = thumbnailList.get(i);
                            vo.setContsSeq(item.getContsSeq());
                            vo.setThumbSe("IMAGE_T");
                            String streFileNm = thumbnailList.get(i).getStreFileNm();
                            String fId = streFileNm.substring(0, streFileNm.lastIndexOf("."));
                            vo.setFileId(fId);
                            thumbnailList.get(i).setFileId(fId);
                            List<ContentVO> galleryThumbList = contentService.contentThumbList(vo);
                            thumbList = new ArrayList<String>();
                            thumbList.add(vo.getFilePath());

                            if (galleryThumbList != null) {
                                for (int j = 0; j < galleryThumbList.size(); j++) {
                                    thumbList.add(galleryThumbList.get(j).getFilePath());
                                }

                                thumbnailList.get(i).setThumbList(thumbList);
                            }
                        }
                    }

                    /** 해당 콘텐츠 비디오 파일 리스트 */
                    List<ContentVO> videoList = contentService.contentsVideoList(item);

                    /** 해당 콘텐츠 실행 파일 리스트 */
                    List<ContentVO> fileList = contentService.fileDataInfo(item);

                    /** 미리보기 영상 파일 리스트 */
                    List<ContentVO> prevList = contentService.contentsPrevList(item);

                    for (ContentVO videoItem : videoList) {
                        ContentVO metaItem = new ContentVO();
                        metaItem.setContsSeq(contsSeq);
                        metaItem.setFilePath(videoItem.getFilePath());
                        ContentVO metadataInfo = contentService.metadataXmlInfo(metaItem);
                        ContentVO contsMetaInfo = contentService.contsMetaInfo(metaItem);
                        videoItem.setMetadataXmlInfo(metadataInfo);
                        videoItem.setContsXmlInfo(contsMetaInfo);

                    }
                    if (resultItem == null) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                        resultItem.setMbrNm(CommonFnc.getNameMask(resultItem.getMbrNm()));
                        resultItem.setCretrID(CommonFnc.getIdMask(resultItem.getCretrID()));
                        resultItem.setAmdrID(CommonFnc.getIdMask(resultItem.getAmdrID()));

                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("genreList", genreList);
                        rsModel.setData("serviceList", serviceList);
                        rsModel.setData("videoList", videoList);
                        rsModel.setData("thumbnailList", thumbnailList);
                        rsModel.setData("fileList", fileList);
                        rsModel.setData("prevList", prevList);
                        rsModel.setData("coverImg", coverImg);
                    }
                    rsModel.setResultType("contentsInfo");
                } else {
                    // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                    if (contsSeq == 0 && CommonFnc.checkReqParameter(request, "contsSeq")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        return rsModel.getModelAndView();
                    }

                    /** 검색여부 */
                    String searchConfirm = request.getParameter("_search") == null ? "false"
                            : request.getParameter("_search");

                    item.setSearchConfirm(searchConfirm);
                    if (searchConfirm.equals("true")) {
                        /** 검색 카테고리 */
                        String searchField = request.getParameter("searchField") == null ? ""
                                : request.getParameter("searchField");

                        /** 검색어 */
                        String searchString = request.getParameter("searchString") == null ? ""
                                : request.getParameter("searchString");

                        if (searchField == "CRETR_ID" || searchField.equals("CRETR_ID")) {
                            searchField = "A." + searchField;
                        }

                        item.settarget(searchField);
                        item.setkeyword(searchString);

                    }

                    /** 리스트 콘텐츠 카테고리 번호 */
                    int contsCtgSeq = request.getParameter("contsCtgSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("contsCtgSeq"));
                    item.setContsCtgSeq(contsCtgSeq);

                    /** 장르 번호 */
                    int genreSeq = request.getParameter("genreSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("genreSeq"));
                    item.setGenreSeq(genreSeq);

                    /** 현재 페이지 */
                    int currentPage = request.getParameter("page") == null ? 1
                            : Integer.parseInt(request.getParameter("page"));

                    /** 검색에 출력될 개수 */
                    int limit = request.getParameter("rows") == null ? 10
                            : Integer.parseInt(request.getParameter("rows"));

                    /** 정렬할 필드 */
                    String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");

                    /** 정렬 방법 */
                    String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

                    /** 페이지 & 출력개수 계산 변수 (페이지 개수) */
                    int page = (currentPage - 1) * limit + 1;

                    /** 페이지 & 출력개수 계산 변수 (마지막페이지값) */
                    int pageEnd = (currentPage - 1) * limit + limit;

                    // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                    if (sidx.equals("CONTS_ID")) {
                    } else if (sidx.equals("CONTS_TITLE")) {
                    } else if (sidx.equals("CTG_NM")) {
                    } else if (sidx.equals("CRETR_ID")) {
                    } else if (sidx.equals("STTUS")) {
                        sidx = "COMN_CD_VALUE";
                    } else if (sidx.equals("CRET_DT")) {
                    } else if (sidx.equals("VRF_RQT_DT")) {
                    } else {
                        sidx = "CONTS_SEQ";
                    }

                    item.setSidx(sidx);
                    item.setSord(sord);
                    item.setoffset(page);
                    item.setlimit(pageEnd);
                    item.setSttusVal(sttusSeq);

                    /** 로그인 중인 계정 레벨 값 */
                    // int user_level = session.getAttribute("level") == null ? 0
                    // : Integer.parseInt((String) session.getAttribute("level"));

                    // CP사 정보(사용자 로그인 일 경우 해당 멤버가 작성한 콘텐츠만 출력)
                    // if (user_level < 5) {
                    // cpSeq = session.getAttribute("cpSeq") == null ? 0
                    // : Integer.parseInt((String) session.getAttribute("cpSeq"));
                    //
                    // item.setCpSeq(cpSeq);
                    // }
                    List<ContentVO> resultItem = contentService.contentsList(item);

                    if (resultItem.isEmpty()) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    } else {
                        /* 개인정보 마스킹 (등록자 정보(이름, 아이디)) */
                        for (int i = 0; i < resultItem.size(); i++) {
                            resultItem.get(i).setMbrNm(CommonFnc.getNameMask(resultItem.get(i).getMbrNm()));
                            resultItem.get(i).setMbrID(CommonFnc.getIdMask(resultItem.get(i).getMbrID()));
                        }

                        /** 가져온 리스트 총 개수 */
                        int totalCount = contentService.contentsListTotalCount(item);
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                        rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                        rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                        rsModel.setData("item", resultItem);
                        rsModel.setData("totalCount", totalCount);
                        rsModel.setData("currentPage", currentPage);
                        rsModel.setData("totalPage", (totalCount - 1) / limit);
                        rsModel.setResultType("contentsList");
                    }
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 정보 수정 전 원본 유효성 검사 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/validateOrganContsInfo")
    public ModelAndView validateOrganContentsProcess(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ContentVO vo = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String organAmdDt = CommonFnc.emptyCheckString("organAmdDt", request);
            boolean isValid = false;

            vo.setContsSeq(contsSeq);
            ContentVO resultItem = contentService.contentInfo(vo);

            if (resultItem.getAmdDtDetail() == null) {
                resultItem.setAmdDtDetail("");
            }

            if (organAmdDt.equals(resultItem.getAmdDtDetail())) {
                isValid = true;
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setData("isValid", isValid);
            rsModel.setResultType("validateContsInfo");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 카테고리 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contentsCategory")
    public ModelAndView contentsCategoryProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);

        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            rsModel.setViewName("../resources/api/contents/contentsProcess");
            ContentVO item = new ContentVO();
            if (request.getMethod().equals("POST")) {
                String Method = request.getParameter("_method");
                if (Method == null) {
                    Method = "POST";
                }
                if (Method.equals("PUT")) { // put
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else if (Method.equals("DELETE")) { // delete
                    String checkString = CommonFnc.requiredChecking(request, "firstCateogoryId,firstCateogoryName");
                    if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }
                } else { // post
                    String checkString = CommonFnc.requiredChecking(request, "firstCateogoryId,firstCateogoryName");
                    if (!checkString.equals("")) {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                        rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                        rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                        return rsModel.getModelAndView();
                    }

                    item.setMbrID((String) session.getAttribute("id"));
                    String firstCtgID = request.getParameter("firstCtgID") == null ? ""
                            : request.getParameter("firstCtgID");
                    String firstCtgNm = request.getParameter("firstCtgNm") == null ? ""
                            : request.getParameter("firstCtgNm");
                    String secondCtgID = request.getParameter("secondCtgID") == null ? "1"
                            : request.getParameter("secondCtgID");
                    String secondCtgNm = request.getParameter("secondCtgNm") == null ? "basic"
                            : request.getParameter("secondCtgNm");

                    item.setFirstCtgID(firstCtgID);
                    item.setFirstCtgNm(firstCtgNm);
                    item.setSecondCtgID(secondCtgID);
                    item.setSecondCtgNm(secondCtgNm);

                }
            } else { // get
                // 콘텐츠 등록, 수정 시에 콘텐츠 카테고리를 그룹화 할 경우
                /*
                 * 첫번째 카테고리 리스트를 가져오지 않으면 해당 하는 첫번째 카테고리명을 정적 문자열로 호출해야만 하기 때문에
                 * 동적으로 처리하기 위해 첫 번째 리스트를 가져온 다음 해당하는 두 번째 리스트를 가져오게 구성되어 있음
                 */

                /** 카테고리 그룹핑 검색 여부 */
                String GroupingYn = request.getParameter("GroupingYN") == null ? ""
                        : request.getParameter("GroupingYN");

                String unique = CommonFnc.emptyCheckString("unique", request);
                item.setGroupingYN(GroupingYn);
                item.setUnique(unique);
                if (GroupingYn.equals("Y") || GroupingYn == "Y") {
                    /** 첫번째 카테고리 명 */
                    String firstCtgId = request.getParameter("firstCtgID") == null ? ""
                            : request.getParameter("firstCtgID");

                    /** 콘텐츠 카테고리 번호 */
                    int contsCtgSeq = request.getParameter("contsCtgSeq") == null ? 0
                            : Integer.parseInt(request.getParameter("contsCtgSeq"));

                    List<ContentVO> firstCtgList = contentService.firstCtgList(item);
                    rsModel.setData("firstCtgList", firstCtgList);

                    if (contsCtgSeq != 0) {
                        item.setGroupingYN("N");
                        item.setContsCtgSeq(contsCtgSeq);
                    } else {
                        if (firstCtgId == "") {
                            firstCtgId = firstCtgList.get(0).getFirstCtgID();
                        }
                        item.setFirstCtgID(firstCtgId);
                    }
                }
                List<ContentVO> resultItem = contentService.contsCtgList(item);

                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }

                if (unique.equals("on")) {
                    rsModel.setResultType("contsCtgListNm");
                } else {
                    rsModel.setResultType("contsCtgList");
                }

            }

        }
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 수정 이력 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/modifyHistory")
    public ModelAndView modifyRecord(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        if (session.getAttribute("id") == null) {
            response.setStatus(401);
            return rsModel.getModelAndView();
        }
        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int contsSeq = request.getParameter("contsSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("contsSeq"));
            item.setContsSeq(contsSeq);

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("contsChgInfo")) {
                sidx = "CONTS_CHG_INFO";
            } else if (sidx.equals("legacyValue")) {
                sidx = "LEGACY_VALUE";
            } else if (sidx.equals("changeValue")) {
                sidx = "CHANGE_VALUE";
            } else if (sidx.equals("amdrId")) {
                sidx = "AMDR_ID";
            } else if (sidx.equals("amdDt")) {
                sidx = "AMD_DT";
            } else {
                sidx = "CONTS_HST_SEQ";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setoffset(page);
            item.setlimit(pageEnd);

            List<ContentVO> resultItem = contentService.modifyHstList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (수정자 아이디) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setAmdrID(CommonFnc.getIdMask(resultItem.get(i).getAmdrID()));
                }

                int totalCount = contentService.modifyHstListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("modifyHstList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 검수 이력 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/verifyHistory")
    public ModelAndView verifyRecord(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        if (session.getAttribute("id") == null) {
            response.setStatus(401);
            return rsModel.getModelAndView();
        }
        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int contsSeq = request.getParameter("contsSeq") == null ? 0
                    : Integer.parseInt(request.getParameter("contsSeq"));
            item.setContsSeq(contsSeq);

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = request.getParameter("page") == null ? 1 : Integer.parseInt(request.getParameter("page"));
            int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows"));
            String sidx = request.getParameter("sidx") == null ? "" : request.getParameter("sidx");
            String sord = request.getParameter("sord") == null ? "" : request.getParameter("sord");

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;

            // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
            if (sidx.equals("verifyCretrNm")) {
                sidx = "VERIFY_CRETR_NM";
            } else if (sidx.equals("verifySttus")) {
                sidx = "VERIFY_STTUS";
            } else if (sidx.equals("verifyRegDt")) {
                sidx = "VERIFY_REG_DT";
            } else {
                sidx = "ACTC_SEQ";
            }

            item.setSidx(sidx);
            item.setSord(sord);
            item.setoffset(page);
            item.setlimit(pageEnd);

            List<ContentVO> resultItem = contentService.verifyHstList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                /* 개인정보 마스킹 (검수 등록자 이름) */
                for (int i = 0; i < resultItem.size(); i++) {
                    resultItem.get(i).setVerifyCretrNm(CommonFnc.getNameMask(resultItem.get(i).getVerifyCretrNm()));
                }

                int totalCount = contentService.verifyHstListTotalCount(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("verifyHstList");
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 메타데이터 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/metadata")
    public ModelAndView metadataProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contents/contentsProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ContentVO item = new ContentVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            String checkString = CommonFnc.requiredChecking(request, "contsSeq");
            if (!checkString.equals("")) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                return rsModel.getModelAndView();
            }
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String metaData = CommonFnc.emptyCheckString("metaData", request).replaceAll("&quot;", "\"");

            if (!metaData.equals("")) {
                item.setContsSeq(contsSeq);
                item.setCretrID(userVO.getMbrId());
                item.setMetaData(metaData);

                contentService.metadataInsert(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID_PARAM);
                rsModel.setResultMessage(ResultModel.MESSAGE_INVALID_PARAM + "(" + checkString + ")");
                rsModel.setStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
        } else if (method.equals("PUT")) {
            String invalidParams = CommonFnc.requiredChecking(request,"contsSeq");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            int contsSubMetadataSeq = CommonFnc.emptyCheckInt("contsSubMetadataSeq", request);
            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            String metaData = CommonFnc.emptyCheckString("metaData", request).replaceAll("&quot;", "\"");

            item.setMetaData(metaData);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = 0;
                if (contsSubMetadataSeq == 0) {
                    // 수정화면을 통한 등록
                    item.setContsSeq(contsSeq);
                    item.setCretrID(userVO.getMbrId());
                    contentService.metadataInsert(item);
                } else {
                    // 수정
                    item.setContsSubMetadataSeq(contsSubMetadataSeq);
                    item.setAmdrID(userVO.getMbrId());
                    contentService.metadataUpdate(item);
                }
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                transactionManager.commit(status);
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if(!status.isCompleted()){
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // Get
            String invalidParams = CommonFnc.requiredChecking(request,"contsSeq");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            int contsSeq = CommonFnc.emptyCheckInt("contsSeq", request);
            item.setContsSeq(contsSeq);

            ContentVO resultVO = contentService.getMetadata(item);

            if (resultVO == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                return rsModel.getModelAndView();
            }

            rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
            rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
            rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            rsModel.setData("item", resultVO);
            rsModel.setResultType("resultSubMetadata");
        }

        return rsModel.getModelAndView(); 
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }
}
