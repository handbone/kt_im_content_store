<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>

<%@include file="/WEB-INF/views/frame/header.jsp"%>

<script>
$(document).ready(function(){
    var name = "<c:out value='${name}' />" + " " + getMessage("common.man");
    $("#loginUserName").html(name);
});
</script>
        <!-- left group S -->
        <div id="leftGrp">
            <ul>
                <li class="logo">
                    <a href="javascript:;" onclick="pageMove('/home')">
                        <img src="<c:url value='/resources/image/logo_w.png' />" alt="<spring:message code='menu.title'/>">
                    </a>
                </li>
                <li>
                    <div class="loginUserGrp">
                        <ul>
                            <li class="loginUser">
                                <span class="loginUserName">
                                    <a href="javascript:;" onclick="cofirmMove('/mypage')" id="loginUserName"></a>
                                </span>
                            <li class="loginUserLogout">
                                <input type="button" class="btnLogout cursorPointer" value="<spring:message code='button.logout'/>" onclick="logout()">
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="leftMenuGrp">
                    <ul>
                        <li class="leftMenu leftIconHome cursorPointer">
                            <a href="javascript:;"><spring:message code='menu.home.title'/></a>
                        </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/home')"><spring:message code='menu.home.dashboard'/></a>
                            </li>
                        <li class="leftMenu leftIconContent cursorPointer">
                            <a href="javascript:;"><spring:message code='menu.contents.title'/></a>
                        </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/contents')"><spring:message code='menu.contents.info'/></a>
                            </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/contents/contract')"><spring:message code='menu.contents.contract'/></a>
                            </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/contents/usage')"><spring:message code='menu.contents.statistics'/></a>
                            </li>
                            <c:if test="${memberSec eq '01' or memberSec eq '02'}">
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/contents/blockStats')"><spring:message code='menu.contents.blockStats'/></a>
                            </li>
                            </c:if>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/contents/category')"><spring:message code='menu.contents.category'/></a>
                            </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/contents/genre')"><spring:message code='menu.contents.genre'/></a>
                            </li>
                        <li class="leftMenu leftIconCustom cursorPointer">
                            <a href="javascript:;"><spring:message code='menu.customer.title'/></a>
                        </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/notice')"><spring:message code='menu.customer.notice'/></a>
                            </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/faq')"><spring:message code='menu.customer.faq'/></a>
                            </li>
                            <li class="leftSubMenu">
                                <a href="javascript:;" onclick="pageMove('/qna')"><spring:message code='menu.customer.qna'/></a>
                            </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- left group E -->

        <!-- right group S -->
        <div id="rightGrp">