/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

// 콘텐츠 장르 또는 대상 서비스 변경 시 저장을 위한 용도

var cpListData = null;

var beforeGenres = "";
var beforeServices = "";

var fileUploadQueue = new Array();
var curFileUpload;

var coverThumbSizes = coverThumbSizes();
var galleryThumbSizes = galleryThumbSizes();

var isCoverThumb = false;
var isGalleryThumb = false;

var coverThumbInfo = [];
var galleryThumbInfo = [];

var chgCoverThumbFileSeq = "";
var chgGalleryThumbFileSeq = "";

//대표 이미지와 갤러리 이미지 삭제 시 사용 변수, 기존 thumbnaildelSeq와 분리
var deleteThumbListSeq = "";

// 콘텐츠 원본의 수정 일시를 저장하기 위한 용도, 수정완료 시 수정일시를 재확인하여 다를 경우 원본이 변경된 것으로 간주함.
var organAmdDt = "";

var FILE_VIDEO;
var FILE_THUMBNAIL;
var FILE_METADATA;
var FILE_COVERIMG;
var FILE_CONTENTS;
var FILE_FILE1;
var FILE_PREV;
var FILE_PREVMETA;
var FILE_PREVCONT;
var FILE_WEBTOON;
var FILE_SETTINGS   = {
        // Backend Settings
        //upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
    };

//swfupload - cover
coverImgUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'coverImg', isThumb : 'false', thumbSizes : coverThumbSizes};
    var file_settings   ={};
    file_settings.button_placeholder_id = "coverImg_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_COVERIMG   = new SWFUpload(file_settings);
}

//swfupload - thumbnail
thumbnailUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'thumbnail', isThumb : 'false', thumbSizes : galleryThumbSizes};
    var file_settings   ={};
    file_settings.button_placeholder_id = "thumbnail_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "0";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_THUMBNAIL  = new SWFUpload(file_settings);
}

//swfupload - video
videoUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'video'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "video_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_VIDEO  = new SWFUpload(file_settings);
}

//swfupload - PREV
prevUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'prev'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prev_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREV  = new SWFUpload(file_settings);
}

//swfupload - metadata
prevmetaUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'prevmeta', fileSeq : seq};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prevmeta_btn_placeholder"+seq;
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREVMETA   = new SWFUpload(file_settings);
}

//swfupload - metadata
metadataUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'metadata', fileSeq : seq};
    var file_settings   ={};
    file_settings.button_placeholder_id = "metadata_btn_placeholder"+seq;
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_METADATA   = new SWFUpload(file_settings);
}

//swfupload - file1
file1UploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'file'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.zip;*.exe;",
    file_settings.file_queue_limit =  "1";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_FILE1  = new SWFUpload(file_settings);
}

//swfupload - webtoon
webtoonUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'webtoon'};
    var file_settings    ={};
    file_settings.button_placeholder_id    = "webtoon_btn_placeholder";
    file_settings.button_width    = 62;
    file_settings.button_height    = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.zip;",
    file_settings.file_queue_limit =  "1";
    //file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url    = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,    file_settings,    FILE_SETTINGS);
    FILE_WEBTOON = new SWFUpload(file_settings);
}

var contsSubMetadataSeq;
var metaData = ""; // 서브메타데이터 정보

var categorySeq;

/** 장르 리스트 번호들 */
var genreList="";
var firstCtg = "";
var webState = true;
$(document).ready(function(){
    var now=new Date();
    $("#popupEditSubmetadata").draggable();
    $("#cntrctStDt").val(dateString((new Date(now.getTime()))));
    $("#cntrctFnsDt").val(nextYearDateString((new Date(now.getTime()))));

    $("#cntrctStDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#cntrctFnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#cntrctFnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#cntrctStDt").datepicker("option","maxDate", selected)
        }
    });

    $("#contsTitle").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$("#contsTitle").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#contsTitle").val(temp.replace(re,""));
        }
    });

    $("#contsSubTitle").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$("#contsSubTitle").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#contsSubTitle").val(temp.replace(re,""));
        }
    });

    $("#exeFilePath").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$("#exeFilePath").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#exeFilePath").val(temp.replace(re,""));
        }
    });

    Array.prototype.last = function() {return this[this.length-1];}

    setkeyup();

    // 초기 로딩 시 썸네일 생성 버튼 hide
    $("#btnGalleryCretThumb").hide();
    $("#btnCoverCretThumb").hide();

    coverImgUploadBtn();
    videoUploadBtn();
    thumbnailUploadBtn();
    file1UploadBtn();
    CPList();
    prevUploadBtn();
    contsCtgList();
    webtoonUploadBtn();
});

var ctgVal = "none";
contsCtgList = function(ctgSeq){
    var searchKey = "?GroupingYN=Y";

    ctgSeq = typeof ctgSeq !== 'undefined' ? ctgSeq : "none";

    if(ctgSeq != "none") {
        ctgVal = ctgSeq;
        searchKey += "&contsCtgSeq="+ctgSeq;
    } else {
        ctgVal = "none";
        if($("#firstCtg option:selected").val()){
            searchKey +="&firstCtgID="+$("#firstCtg option:selected").val();
        }
    }

    callByGet("/api/contentsCategory"+searchKey, "contsCtgListSuccess", "NoneForm");
}

var _firstLoading = true;
var curYear = new Date().getFullYear(); // 현재 날짜의 년도(ContentsID에서 사용)
contsCtgListSuccess = function(data){
    if(data.resultCode == "1000"){
        var contsCtgListHtml = "";
        var firstCtgNmListHtml= "";

        $(data.result.firstCtgList).each(function(i,item) {
            firstCtgNmListHtml += "<option value='"+item.firstCtgID+"' data-id='"+item.firstCtgID+"'>"+item.firstCtgNm+"</option>";
        });

        if(_firstLoading){
            $("#firstCtg").html(firstCtgNmListHtml);
            _firstLoading = false;
        }
        var firstId = "";
        $(data.result.contsCtgList).each(function(i,item) {
            var seleted = "";
            if(ctgVal != "none" && item.contsCtgSeq) {
                seleted = "seleted";
                firstId = item.firstCtgID;

            }
            contsCtgListHtml += "<option value='"+item.contsCtgSeq+"' name=\"" + item.firstCtgID + item.secondCtgID + item.ctgCnt +" "+seleted+"\">"+item.secondCtgNm+"</option>";
        });
        if (firstId != "") {
            $("#firstCtg").val(firstId);
        }

        // 첫번째 카테고리 명이 '영상'이 아닐때
        if ($("#firstCtg option:selected").text().match(getMessage("common.video")) == null) {
            $(".attrPart").show();
            $(".gamePart").show();
            $(".videoPart").hide();
            $(".exeFilePath").show();
            $(".exeFilePath th").text(getMessage("common.run.file.path"));
            $(".webtoonPart").hide();
        } else if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) {
            $(".attrPart").hide();
            $(".gamePart").hide();
            $(".exeFilePath").hide();
            if(!_isHaveComicsValue){
                $(".videoPart").show();
            }
            if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){ // 첫번째 카테고리 명이 '영상_LiveOn'일때
                $(".exeFilePath").show();
                $(".exeFilePath th").text(getMessage("common.run.streaming.url"));
            }
        }
        $("#secondCtg").html(contsCtgListHtml);
        $("#secondCtg").val(categorySeq);

        GenreList();
    }
}

//카테고리 변경시 콘텐츠 ID 최신화
contentsIDRefresh = function(){
    var categoryInfo = $("#secondCtg option:selected").attr("name");
    $("#contsID").text(categoryInfo.slice(0,2) + curYear + prependZero(categoryInfo.slice(2), 5));
}

// 콘텐츠 ID 년도 뒷부분 숫자 앞 빈칸 0으로 채우기
function prependZero(num, len) {
    while(num.toString().length < len) {
        num = "0" + num;
    }
    return num;
}

function GenreList(){
    var searchKey = $("#firstCtg").find(':selected').data("id");

    if (typeof searchKey != "undefined") {
        searchKey = "?firstCtgId="+searchKey;
    } else {
        searchKey = "";
    }

    callByGet("/api/contents/genre"+searchKey, "GenreListSuccess");
}

function GenreListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $("#genreList").html("");
        $(data.result.genreList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.genreSeq+"\" value=\""+item.genreNm+"\" class=\"ContentSelectBox\"/ onclick=\"buttonBoxToggle(this);\"> ";
        });
        $("#genreList").append(menuHtml);
    } else {
        $("#genreList").html("");
    }

    if (webState) {
        contentInfo();
        webState = false;
    } else {
        genreCheck();
    }
}

var service="";
var oriCp = 0;
function ServiceList(){
    var searchCp = "";
    if ($("#cpList").val() != "" && $("#cpList").val() != null) {
        searchCp = "?cpSeq="+$("#cpList").val();
    }

    callByGet("/api/service"+searchCp, "ServiceListSuccess");
}

function ServiceListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.serviceList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.svcSeq+"\" value=\""+item.svcNm+"\" class=\"ContentSelectBox\"/ onclick=\"buttonBoxToggle(this);\"> ";
        });
        $("#serviceList").append(menuHtml);
        if (service != "" && $("#cpList").val() == oriCp) {
            $(service).each(function(i,item){
                beforeServices += item.svcNm + ",";
                $("#serviceList").find("input[data-seq="+item.svcSeq+"]").addClass('ContentSelectBoxOn');
            });
        }
    }
}

function CPList(){
    callByGet("/api/cp?rows=10000", "CPListSuccess");
}

function CPListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";

        //if ($("#memberSec").val() == "06") {
        //    $("#cpList").attr("disabled","disabled");
        //}
        // 콘텐츠 수정 시 CP 사 변경 불가능하도록 변경
        $("#cpList").attr("disabled","disabled");

        cpListData = data.result.cpList;
        $(data.result.cpList).each(function(i, item){
            menuHtml = "<option value=\""+item.cpSeq+"\">"+item.cpNm+"</option>";
            $("#cpList").append(menuHtml);
        });
    }
}

var _isHaveComicsValue = false;
function buttonBoxToggle(e){

    $(e).toggleClass("ContentSelectBoxOn");
    if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){ // 첫번째 카테고리가 LiveOn일 경우
        _isHaveComicsValue = false;
        $("#genreList .ContentSelectBoxOn").each(function(i, item){
            if(item.value == "__COMICS__"){
                _isHaveComicsValue = true;
                return false;
            } else {
                _isHaveComicsValue = false;
            }
        });
        if(_isHaveComicsValue){ // 장르에 코믹이 있을 경우
            $(".webtoonPart").show();
            $(".videoPart").hide();
        } else {
            $(".webtoonPart").hide();
            $(".videoPart").show();
        }
    } else {
        $(".webtoonPart").hide();
    }
}

contentInfo = function(){
    callByGet("/api/contents?contsSeq="+$("#contsSeq").val(), "contentsInfoSuccess", "NoneForm", "contentsInfoFail");
}
var firstCtgNm = "";
contentsInfoSuccess = function(data){
    if(data.resultCode == "1000"){
        var item = data.result.contentsInfo;
        categorySeq = item.contsCtgSeq;
        contsSubMetadataSeq = item.contsSubMetadataSeq;
        metaData = item.metaData.replace(/&#034;/gi, "\"");
        $("#contsTitle, #headTitle").val(xssChk(item.contsTitle));
        $("#contsID").text(item.contsID);
        $("#contsSubTitle").val(item.contsSubTitle);
        $("#contsDesc").val(xssChk(item.contsDesc));
        $("#contCtgNm").val(item.contCtgNm);
        $("#cretrID").val(item.cretrID);
        $("#mbrNm").val(item.mbrNm);
        $("#sttus").text(item.sttus);
        var str = item.rcessWhySbst;
        var changeNbsp = replaceAll(str,"\u0020", "&nbsp;");
        var rcessWhySbst = "";
        firstCtgNm = item.contCtgNm;
        if (item.sttusVal == "04") {
            rcessWhySbst = 'popAlertLayer("'+changeNbsp+"\",'','" + getMessage("common.refuse") + "&nbsp;" + getMessage("common.reason") + "',true)";
            $("#sttusView").after("<span id='refuseConfirm' onclick="+rcessWhySbst+"  >("+getMessage("contents.table.refuse.verify")+")</span>");
            $("#sttusView").text(item.sttus);
            $("#sttusView").addClass("boxStateRed").removeClass("boxStateBlack");
        } else {
            rcessWhySbst = "";
            if (item.sttusVal != "01") {
                if (item.sttusVal == "06") {
                    $("#sttusView").text(getMessage("contents.table.state.verify.success"));
                } else {
                    $("#sttusView").text(item.sttus);
                }

                $("#sttusBtn").hide();
            } else {
                $("#sttusView").text(item.sttus);
                $("#sttusView").addClass("boxStateBlue").removeClass("boxStateBlack");
                $("#sttusBtn").attr("disabled",true);
            }
        }

        oriCp = item.cpSeq;

        $("#cpList").val(item.cpSeq);

        // 콘텐츠 유효기간이 콘텐츠 제공사의 계약 기간을 초과할 수 없도록 종료일시 max 값 설정
        if (cpListData != undefined && cpListData != null) {
            $(cpListData).each(function(i, item) {
                if ($("#cpList").val() == item.cpSeq) {
                    console.log(">> cpNm : " + item.cpNm + ", contFnsDt : " + item.cpContFnsDt);
                    $("#cntrctStDt").datepicker("option", "minDate", item.cpContStDt);
                    $("#cntrctStDt").datepicker("option", "maxDate", item.cpContFnsDt);
                    $("#cntrctFnsDt").datepicker("option", "minDate", item.cpContStDt);
                    $("#cntrctFnsDt").datepicker("option", "maxDate", item.cpContFnsDt);
                }
            });
        }

        $("#cntrctDt").html(item.cntrctStDt+" ~ "+item.cntrctFnsDt);
        $("#cretDt").html(item.cretDt);
        $("#amdDt").html(item.amdDt);
        $("#amdrID").html(item.amdrID);
        $("#exeFilePath").val(item.exeFilePath);

        // 콘텐츠 정보 로드 시 수정일시 상세정보 저장, 수정완료 진행 전 수정일시 재확인하여 다를 경우 원본이 변경된 것으로 새로고침 후 수정 요청
        organAmdDt = item.amdDtDetail;

        /*
        if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) {
            $("#exeFilePath").val("");
        } else {
            $("#exeFilePath").val(item.exeFilePath);
        }
         */

        $("#contsVer").text(item.contsVer);

        genreList=data.result.contentsInfo.genreList;
        firstCtg = item.firstCtgID;

        $("#firstCtg option[value="+item.firstCtgID+"]").attr('selected','selected');
        contsCtgList();

        $(data.result.contentsInfo.genreList).each(function(i,item){
            beforeGenres += item.genreNm + ",";
            $("#genreList").find("input[data-seq="+item.genreSeq+"]").addClass('ContentSelectBoxOn');
            if(item.genreNm == "__COMICS__"){
                _isHaveComicsValue = true;
            }
        });
        if(_isHaveComicsValue){ // 장르에 코믹이 있을 경우
            $(".webtoonPart").show();
            $(".videoPart").hide();
        } else {
            $(".webtoonPart").hide();
            $(".videoPart").show();
        }

        /** 서비스 리스트 번호들 */
        service = data.result.contentsInfo.serviceList;

        ServiceList();

        $("#exefileType").val(item.fileType);
        $("#maxAvlNop").val(item.maxAvlNop);

        var thumbnailHtml = "";
        $(item.thumbnailList).each(function(i,item){
            var splitImg = item.thumbList.split(",").last().replace("]","").trim();
            // response 받은 데이터에서 갤러리 이미지 정보를 따로 저장
            galleryThumbInfo.push(item);
            thumbnailHtml += "<table class=\"fileTable\"><tr id=\"thumbnail" + item.fileSeq + "\"><td><div class='tableBox'><span class='article' class='thumImgPv'><a href=\""+makeAPIUrl(item.thumbnailPath,"img")+"\" class=\"swipebox\"><img class='prevVwImg' src="+makeAPIUrl(splitImg,"img")+" title="+item.thumbnailNm+" /></a></span><p class=\"fileName tableCell\" title=\"" + item.thumbnailNm + "\">"
                    + getMessage("common.filename") + " : "+item.thumbnailNm+"</p></div></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\""
                    + " onclick=\"thumbnailDelete('thumbnailArea', '" + item.fileSeq + "', '" + item.fileId + "')\" value=\"" + getMessage("common.delete") + "\"><input type=\"button\""
                    + " class=\"btnPrevWhite prevGalleryNone\" onclick=\"previewThumbnail('thumbnailArea', '" + item.fileId + "')\"" + " value=\""
                    + getMessage("common.preview.thumbnail") + "\"/></td></tr></table>";
        });

        $("#thumbnailArea").html(thumbnailHtml);
        $(".swipebox").swipebox();


        // 갤러리 이미지들의 썸네일 리스트가 존재할 경우 썸네일 미리보기 버튼 활성화
        var hasThumbCnt = 0;
        $("#thumbnailArea").find(".fileTable tr").each(function(i) {
            var fileSeq = $(this).attr('id').replace("thumbnail","").trim();

            var hasThumbList = false;
            for (var index = 0; index < galleryThumbInfo.length; index++) {
                if (galleryThumbInfo[i].fileSeq == fileSeq) {
                    // thumbList에는 원본 이미지도 포함, 썸네일 생성 기능이 들어가기 전에 만들어진 콘텐츠의 경우에는 원본 이미지만 들어가 있으므로 length > 1 일 경우 썸네일 이미지가 있는 것으로 간주
                    if (galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1).split(", ").length > 1) {
                        hasThumbList = true;
                    }
                }
            }
            if (hasThumbList) {
                $(this).find(".btnPrevWhite").toggleClass("prevGalleryNone");
                hasThumbCnt++;
            }
        });

        if (thumbnailHtml != "") {
            if ($("#thumbnailArea").find(".fileTable tr").length == hasThumbCnt) {
                $("#btnGalleryCretThumb").val(getMessage("common.recreate.thumbnail"));
                isGalleryThumb = true;
            } else {
                $("#btnGalleryCretThumb").val(getMessage("common.create.thumbnail"));
                isGalleryThumb = false;
            }

            $("#btnGalleryCretThumb").show();
        }

//            $("#videoName").html(item.videoName);
//        if(item.contentsXMLName){
//            $("#contentsXmlName").html(""+item.contentsXMLName+"<a href='/api/fileDownload/"+item.contentsXMLSeq+"' class='fontblack'>다운로드</a>");
//        }

        // response 받은 데이터에서 대표 이미지 정보를 따로 저장
        var coverImgHtml = "";
        if (item.coverImg.fileSeq != "") {
            var splitImg = item.coverImg.thumbList.split(",").last().replace("]","").trim();
            coverThumbInfo.push(item.coverImg);

            coverImgHtml += "<table class=\"fileTable\"><tr id=\"cover" + item.coverImg.fileSeq + "\"><td><div class='tableBox'>"
                    + "<span class='article' id='coverImgPv'><a href=\""+makeAPIUrl(item.coverImg.coverImgPath,"img")+"\" class=\"swipebox\"><img class='prevVwImg' src="+makeAPIUrl(splitImg,"img")+" title="+item.coverImg.coverImgNm+" /></a></span><p class=\"fileName tableCell\" title=\"" + item.coverImg.coverImgNm + "\">"
                    + getMessage("common.filename") + " : "+item.coverImg.coverImgNm+"</p>"
                    + "</div>"
                    + "</td><td><input type=\"button\" class=\"btnNormal"
                    + " btnDelete btnDownSmall\" onclick=\"savedfileDelete('coverImgArea', '" + item.coverImg.fileSeq + "', '" + item.coverImg.fileId + "')\" value=\""
                    + getMessage("common.delete") + "\"><input type=\"button\" class=\"btnPrevWhite prevCoverNone\" onclick=\"previewThumbnail('coverImgArea', '"
                    + item.coverImg.fileId + "')\" value=\"" + getMessage("common.preview.thumbnail") + "\"/></td></tr></table>";
            $("#coverImgArea").html(coverImgHtml);
        }

        // 대표 이미지의 썸네일 리스트가 존재할 경우 썸네일 미리보기 버튼 활성화
        hasThumbCnt = 0;
        $("#coverImgArea").find(".fileTable tr").each(function(i) {
            var fileSeq = $(this).attr('id').replace("cover","").trim();

            var hasThumbList = false;
            for (var index = 0; index < coverThumbInfo.length; index++) {
                if (coverThumbInfo[i].fileSeq == fileSeq) {
                    // thumbList에는 원본 이미지도 포함, 썸네일 생성 기능이 들어가기 전에 만들어진 콘텐츠의 경우에는 원본 이미지만 들어가 있으므로 length > 1 일 경우 썸네일 이미지가 있는 것으로 간주
                    if (coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1).split(", ").length > 1) {
                        hasThumbList = true;
                    }
                }
            }
            if (hasThumbList) {
                $(this).find(".btnPrevWhite").toggleClass("prevCoverNone");
                hasThumbCnt++;
            }
        });

        if (coverImgHtml != "") {
            if ($("#coverImgArea").find(".fileTable tr").length == hasThumbCnt) {
                $("#btnCoverCretThumb").val(getMessage("common.recreate.thumbnail"));
                isCoverThumb = true;
            } else {
                $("#btnCoverCretThumb").val(getMessage("common.create.thumbnail"));
                isCoverThumb = false;
            }

            $("#btnCoverCretThumb").show();
        }

        var metadataHtml = "";
        $(data.result.contentsInfo.videoList).each(function(i,item){
            var videoHtml = "";
            metadataSeq++;
            videoHtml += "<table class=\"videoForm\" id=\"videoItem"+item.fileSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">" + getMessage("common.name") +" : " + item.fileNm + "&nbsp;&nbsp;"+ "</td><td class=\"content\" colspan=\"3\">"
                            + "<input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"videoDelete('" + item.fileSeq
                            + "')\" value=\"" + getMessage("common.delete") + "\"></td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" value=\""+item.filePath+"\" >";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"metadata"+metadataSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">" + getMessage("common.metadata") + "</td><td class=\"content\" colspan=\"3\"><div style=\"display:inline-block; float:left;\""
                            + " id=\"metadataName" + metadataSeq + "\"></div>&nbsp;&nbsp;<div id=\"metadata_btn_placeholder" + metadataSeq + "\"></div></td></tr>";
            videoHtml += "</table>";
            $("#videoArea").append(videoHtml);
            $("table[id=videoItem"+item.fileSeq+"]").find("input[name=metadataPath]").val(item.metadataInfo.filePath);
            $("#metadataName"+metadataSeq).html(getMessage("common.filename") + " : " + item.metadataInfo.fileNm);
            metadataUploadBtn(metadataSeq,item.metadataInfo.filePath);
            $("#metadataName"+metadataSeq).next().css({"position": "absolute","top": "-250px"});
        });

        $(data.result.contentsInfo.prevList).each(function(i,item){
            prevmetadataSeq++;
            var videoHtml = "";
            videoHtml += "<table class=\"prevForm\" id=\"prevItem"+item.fileSeq+"\">";
            videoHtml += "  <tr><td class=\"sub\">" + getMessage("common.filename") +" : " + item.fileNm  + "</td><td class=\"content\" colspan=\"3\">"
                            + "<input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"prevDelete('" + item.fileSeq + "')\" value=\""
                            + getMessage("common.delete") + "\"></td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" id=\"prev"+item.filePath+"\">";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"prevmeta"+prevmetadataSeq+"\">";
            videoHtml += "  <tr style='display:none'><td class=\"sub\">" + getMessage("common.metadata") + "</td><td class=\"content\" colspan=\"3\">&nbsp;&nbsp;"
                            + "<div style=\"display:inline-block;\" id=\"prevmetaName" + prevmetadataSeq+"\"></div>&nbsp;&nbsp;<div id=\"prevmeta_btn_placeholder"
                            + prevmetadataSeq+"\"></div></td></tr>";
            videoHtml += "</table>";

            $("#prevArea").append(videoHtml);
            $("#prevArea").show();
            prevmetaUploadBtn(prevmetadataSeq);
/*            prevUploadTemp = this;*/
        });

        var fileHtml = "";
        $(data.result.contentsInfo.fileList).each(function(i,item){
            fileHtml = "<table class=\"fileTable\"><tr><td><p class=\"fileName\" title=\"" + item.fileNm + "\">" + getMessage("common.filename") + " : "
                            + item.fileNm + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileDelete('"+ (data.result.contentsInfo.contCtgNm.match(getMessage('common.liveon')) != null ? 'webtoonArea' : 'fileArea') +"', "
                            + item.fileSeq + ")\" value=\"" + getMessage("common.delete") + "\"></td></tr></table>";
        });
        if(item.contCtgNm.match(getMessage("common.liveon")) != null){
            if(_isHaveComicsValue){
                $("#webtoonArea").html(fileHtml);
            } else {
                $("#fileArea").html(fileHtml);
            }
        } else {
            $("#fileArea").html(fileHtml);
        }
        listBack($(".btnList"));

        $('#cntrctStDt').datepicker().datepicker('setDate',item.cntrctStDt);
        $('#cntrctFnsDt').datepicker().datepicker('setDate',item.cntrctFnsDt);

        genreCheck();
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
    }

    listBack($(".btnEditCancel"));
}

contentsInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
}

function contentsSttusChange(){
    formData("NoneForm" , "contsSeq", $("#contsSeq").val());
    formData("NoneForm" , "sttus", "02");
    callByPut("/api/contents" , "contentsSttusChangeSuccess", "NoneForm");
}

contentsSttusChangeSuccess = function(data){
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("success.common.request"), "/contents/"+$("#contsSeq").val());
    }else if(data.resultCode == "1011"){
        formDataDeleteAll("NoneForm");
        popAlertLayer(getMessage("fail.request.msg"));
    }
}

var thumbnaildelSeq="";
thumbnailDelete = function(thumbSe, seq, fileId) {
    $("#thumbnail"+seq).parents(".fileTable").remove();
    if ($("#thumbnailArea").find(".fileTable tr").length == 0) {
        isGalleryThumb = false;
        $("#btnGalleryCretThumb").hide();
        $("#btnGalleryCretThumb").val(getMessage("common.create.thumbnail"));
    }
    deleteThumbListSeq += seq+",";

    if (chgGalleryThumbFileSeq.indexOf(seq + ",") != -1) {
        chgGalleryThumbFileSeq = chgGalleryThumbFileSeq.replace(seq + ",", "");
    }
    var delThumbList = deleteThumbList(thumbSe, fileId);
    // 콘텐츠 번호로 등록된 파일은 최종 수정완료 버튼 클릭 시 삭제 처리, 여기서는 coverThumbInfo에서만 해당 내용 삭제
    if (delThumbList == "") {
        deleteThumbInfo(thumbSe, fileId, galleryThumbInfo);
    } else {
        delThumbImg(thumbSe, fileId, delThumbList);
    }
}

savedfileDelete = function(thumbSe, seq, fileId) {
    $("#"+thumbSe).html("");
    $("#"+thumbSe).hide("");
    isCoverThumb = false;
    $("#btnCoverCretThumb").hide();
    $("#btnCoverCretThumb").val(getMessage("common.create.thumbnail"));
    deleteThumbListSeq += seq+",";

    if (chgCoverThumbFileSeq.indexOf(String(seq + ",")) != -1) {
        chgCoverThumbFileSeq = chgCoverThumbFileSeq.replace(seq + ",", "");
    }

    var delThumbList = deleteThumbList(thumbSe, fileId);
    // 콘텐츠 번호로 등록된 파일은 최종 수정완료 버튼 클릭 시 삭제 처리, 여기서는 coverThumbInfo에서만 해당 내용 삭제
    if (delThumbList == "") {
        deleteThumbInfo(thumbSe, fileId, coverThumbInfo);
    } else {
        delThumbImg(thumbSe, fileId, delThumbList);
    }
}

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

videoDelete = function(seq){
    document.getElementById("videoItem"+seq).remove();
    thumbnaildelSeq += seq+",";
}

prevDelete = function(seq){
    document.getElementById("prevItem"+seq).remove();
    thumbnaildelSeq += seq+",";
}

fileDelete = function(id, seq){
    $("#"+id).html("");
    $("#"+id).hide("");
    thumbnaildelSeq += seq+",";
}

function uploadS(){
    // 파일 업로드 초기화 - 썸네일 생성 시 동일 코드 사용으로 썸네일 생성 후 등록 시 이전 업로드 객체를 초기화
    if (curFileUpload) {
        curFileUpload = null;
    }

    if(prevUploadTemp == undefined && prevmetaUploadTemp == undefined && thumbnailUploadTemp == undefined && videoUploadTemp == undefined  && coverImgUploadTemp == undefined && metadataUploadTemp  == undefined && fileTemp == undefined && webtoonUploadTemp == undefined){
        contentsXmlCreate();
        return;
    }

    if(prevUploadTemp != undefined){
        fileUploadQueue.push(prevUploadTemp);
    }

    $(prevmetaUploadTemp).each(function(){
        fileUploadQueue.push(this);
    });

    if(coverImgUploadTemp != undefined){
        coverImgUploadTemp.setUploadURL(makeAPIUrl("/api/thumbFileUpload"));
        fileUploadQueue.push(coverImgUploadTemp);
    }

    if(thumbnailUploadTemp != undefined){
        thumbnailUploadTemp.setUploadURL(makeAPIUrl("/api/thumbFileUpload"));
        fileUploadQueue.push(thumbnailUploadTemp);
    }

    if(videoUploadTemp != undefined){
        fileUploadQueue.push(videoUploadTemp);
    }

    $(metadataUploadTemp).each(function(){
        fileUploadQueue.push(this);
    });

    if(fileTemp != undefined){
        fileUploadQueue.push(fileTemp);
    }

    if(webtoonUploadTemp != undefined){
        fileUploadQueue.push(webtoonUploadTemp);
    }

    startUploadIfNeeded();
}

function startUploadIfNeeded() {
    if (curFileUpload && curFileUpload.getStats().files_queued > 0) {
        return;
    }

    if (!fileUploadQueue || fileUploadQueue.length <= 0) {
        curFileUpload = null;
        contentsXmlCreate();
        return;
    }

    curFileUpload = fileUploadQueue.shift();
    curFileUpload.startUpload();
}

contentsXmlCreate = function(){
    if ($("#firstCtg option:selected").text().match(getMessage("common.video")) == null) {
        fileInsertSuccessPopup();
    } else {
        if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){
            if(!_isHaveComicsValue){ // 영상_LiveOn 카테고리 장르에 코믹이 없을 경우
                var videoFormData = "";
                $("table[id ^= videoItem]").each(function(i,item){
                    videoFormData += $(this).find("input[name=videoPath]").val()+",";
                    videoFormData += $(this).find("input[name=metadataPath]").val()+"GNB";
                });
                formData("NoneForm" , "contsSeq", $("#contsSeq").val());
                formData("NoneForm" , "#contsID", $("#contsID").text());
                formData("NoneForm" , "videoFormData", videoFormData.slice(0,-3));
                callByPost("/api/contentsXml", "contentsXmlSuccess", "NoneForm" );
            } else {
                fileInsertSuccessPopup();
            }
        } else {
            var videoFormData = "";
            $("table[id ^= videoItem]").each(function(i,item){
                videoFormData += $(this).find("input[name=videoPath]").val()+",";
                videoFormData += $(this).find("input[name=metadataPath]").val()+"GNB";
            });
            formData("NoneForm" , "contsSeq", $("#contsSeq").val());
            formData("NoneForm" , "#contsID", $("#contsID").text());
            formData("NoneForm" , "videoFormData", videoFormData.slice(0,-3));
            callByPost("/api/contentsXml", "contentsXmlSuccess", "NoneForm" );
        }
    }
}

contentsXmlSuccess = function(data){
    if(data.resultCode == "1000"){
        fileInsertSuccessPopup();
    }
}
fileInsertSuccess = function(data){
    startUploadIfNeeded();
}

fileInsertFail = function() {
    startUploadIfNeeded();
}

fileInsertSuccessPopup = function() {
    $("#divCenter").hide();

    var msg = getMessage("success.common.update");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }
    popAlertLayer(msg, "/contents/"+$("#contsSeq").val());
}

//콘텐츠 ID 년도 뒷부분 숫자 앞 빈칸 0으로 채우기
function prependZero(num, len) {
    while(num.toString().length < len) {
        num = "0" + num;
    }
    return num;
}

fileQueueDelete = function(id, e, fileId){
    if (id == "thumbnailArea") {
        $(e).parents(".fileTable").remove();
    } else {
        $("#"+id).html("");
        $("#"+id).hide("");
    }

    if(id == "thumbnailArea"){
        thumbnailUploadTemp.cancelUpload(fileId);

        // 썸네일 정보 및 파일 삭제
        if (galleryThumbInfo.length > 0) {
            delThumbImg(id, fileId, deleteThumbList(id, fileId));
        }

        // 썸네일 생성이 완료된 모든 항목이 삭제되었을 경우 썸네일 생성 아이콘 값 초기화
        if (thumbnailUploadTemp.getStats().files_queued == 0) {
            $("#btnGalleryCretThumb").hide();
            $("#btnGalleryCretThumb").val(getMessage("common.create.thumbnail"));
            thumbnailUploadTemp.customSettings.isThumb = "false";
        }
    } else if(id == "coverImgArea"){
        coverImgUploadTemp.cancelUpload(fileId);

        // 썸네일 정보 및 파일 삭제
        if (coverThumbInfo.length > 0) {
            delThumbImg(id, fileId, deleteThumbList(id, fileId));
        }

        // 썸네일 생성이 완료된 모든 항목이 삭제되었을 경우 썸네일 생성 아이콘 값 초기화
        if (coverImgUploadTemp.getStats().files_queued == 0) {
            $("#btnCoverCretThumb").hide();
            $("#btnCoverCretThumb").val(getMessage("common.create.thumbnail"));
            coverImgUploadTemp.customSettings.isThumb = "false";
        }
    } else if(id == "fileArea"){
        fileTemp.cancelUpload(fileId);
    } else if(id == "prevArea"){
        prevUploadTemp.cancelUpload(fileId);
    } else if(id == "videoArea"){
        videoUploadTemp.cancelUpload();
        metadataUploadTemp.cancelUpload();
    } else if(id == "webtoonArea"){
        webtoonUploadTemp.cancelUpload(fileId);
    }
}

// 수정내용 반영 전 원본 데이터의 변경 여부를 검증
function validateAlreadyEdit() {
    formData("NoneForm", "contsSeq", $("#contsSeq").val());
    formData("NoneForm", "organAmdDt", organAmdDt);
    callByGet("/api/validateOrganContsInfo", "validateContsInfoSuccess", "NoneForm");
}

validateContsInfoSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if(data.resultCode == "1000") {
        if (data.result.isValid) {
            contentsEdit();
        } else {
            popAlertLayer(getMessage("contents.info.already.edit.msg"));
            return;
        }
    }
}

function contentsEdit() {
    if($("#contsTitle").val().trim() == ""){
        popAlertLayer(getMessage("contents.empty.contents.name.msg"));
        return;
    }

    if($("#coverImgArea").children().length <= 0){
        popAlertLayer(getMessage("contents.empty.cover.image.msg"));
        return;
    }

    if($("#thumbnailArea").children().length <= 0){
        popAlertLayer(getMessage("contents.empty.thumbnail.image.msg"));
        return;
    }

    // isCoverThumb : 기 등록된 대표 이미지의 썸네일이 존재하는지 여부 판단
    // coverImgUploadTemp.customSettings.isThumb : 추가로 업로드 할 파일의 썸네일 생성 여부 판단
    if (isCoverThumb) {
        if (coverImgUploadTemp != undefined && coverImgUploadTemp.customSettings.isThumb == "false") {
            popAlertLayer(getMessage("contents.empty.thumbnail.msg"));
            return;
        }
    } else {
        if (coverImgUploadTemp == undefined || coverImgUploadTemp.customSettings.isThumb == "false") {
            popAlertLayer(getMessage("contents.empty.thumbnail.msg"));
            return;
        }
    }

    if (isGalleryThumb) {
        if (thumbnailUploadTemp != undefined && thumbnailUploadTemp.customSettings.isThumb == "false") {
            popAlertLayer(getMessage("contents.empty.thumbnail.msg"));
            return;
        }
    } else {
        if (thumbnailUploadTemp == undefined || thumbnailUploadTemp.customSettings.isThumb == "false") {
            popAlertLayer(getMessage("contents.empty.thumbnail.msg"));
            return;
        }
    }

    if($("#contsSubTitle").val().trim() == ""){
        popAlertLayer(getMessage("contents.empty.sub.title.msg"));
        return;
    }
    if($("#contsDesc").val().trim() == ""){
        popAlertLayer(getMessage("contents.empty.detail.description.msg"));
        return;
    }

    if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) { // 첫번째 카테고리 명이 '영상'일때
        if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){ // 첫번째 카테고리가 LiveOn일 경우
            if($("#exeFilePath").val() == ""){ // 스트리밍 URL정보가 없을 경우
                if(_isHaveComicsValue){ // 영상_LiveOn 카테고리 장르에 코믹이 있을 경우
                    if($("#webtoonArea").children().length <= 0){ // 웹툰 콘텐츠 파일이 없을 경우
                        popAlertLayer(getMessage("contents.empty.webtoon.msg"));
                        return;
                    }
                } else {
                    if($("#videoArea").children().length <= 0){ // 영상 파일이 없을 경우
                        popAlertLayer(getMessage("contents.empty.video.msg"));
                        return;
                    }
                }
            }
        } else {
            if($("#videoArea").children().length <= 0){
                popAlertLayer(getMessage("contents.empty.video.msg"));
                return;
            }
        }
    }

    var genre="";
    var editGenres = "";

    $("#genreList .ContentSelectBoxOn").each(function(i, item){
        if (genre == "") {
            genre += $(this).attr("data-seq");
            editGenres += $(this).val();
        } else {
            genre += "," + $(this).attr("data-seq");
            editGenres += "," + $(this).val();
        }
    });

    var service="";
    var editServices = "";
    $("#serviceList .ContentSelectBoxOn").each(function(i, item){
        if (service == "") {
            service += $(this).attr("data-seq");
            editServices += $(this).val();
        } else {
            service += "," + $(this).attr("data-seq");
            editServices += "," + $(this).val();
        }
    });

    if(!genre){
        popAlertLayer(getMessage("contents.genre.error.msg"));
        editGenres = "";
        return;
    }

    if(!service){
        popAlertLayer(getMessage("contents.service.error.msg"));
        editServices = "";
        return;
    }

    var isChgGenre = chkChange(beforeGenres, editGenres);
    var isChgService = chkChange(beforeServices, editServices);


    if($("#versionModify").is(":checked")){
        formData("NoneForm" , "versionModify", "Y");
    }

    if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) { // 첫번째 카테고리 명이 '영상'일때
        if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){ // 첫번째 카테고리가 LiveOn일 경우
            if($("#exeFilePath").val() == ""){ // 스트리밍 URL정보가 없을 경우
                if(_isHaveComicsValue){ // 영상_LiveOn 카테고리 장르에 코믹이 있을 경우
                    if($("#webtoonArea").children().length <= 0){ // 웹툰 콘텐츠 파일이 없을 경우
                        popAlertLayer(getMessage("contents.empty.webtoon.msg"));
                        return;
                    }
                } else {
                    if($("#videoArea").children().length <= 0){ // 영상 파일이 없을 경우
                        popAlertLayer(getMessage("contents.empty.video.msg"));
                        return;
                    }
                }
            }
        } else {
            if($("#videoArea").children().length <= 0){
                popAlertLayer(getMessage("contents.empty.video.msg"));
                return;
            }
        }
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) { // 첫번째 카테고리 명이 '영상'일때
            if(_isHaveComicsValue){ // 영상_LiveOn 카테고리 장르에 코믹이 있을 경우
                formData("NoneForm" , "fileType", "ZIP");
            } else {
                formData("NoneForm" , "fileType", "VIDEO");
            }
        } else {
            formData("NoneForm" , "fileType", $("#exefileType option:selected").val());
        }

        // 카테고리가 달라졌을 경우에만 contentsID 수정
        if(categorySeq != $("#secondCtg option:selected").val()){
           formData("NoneForm" , "contsID", $("#contsID").text());
        }

        formData("NoneForm", "contsSeq", $("#contsSeq").val());
        formData("NoneForm", "contsTitle", $("#contsTitle").val().trim());
        formData("NoneForm", "contsCtgSeq", $("#secondCtg option:selected").val());
        formData("NoneForm", "contsID", $("#contsID").text());
        formData("NoneForm", "exeFilePath", $("#exeFilePath").val());
        formData("NoneForm", "contsSubTitle", $("#contsSubTitle").val());
        formData("NoneForm", "contsDesc", $("#contsDesc").val());
        formData("NoneForm", "maxAvlNop", $("#maxAvlNop").val());
        formData("NoneForm", "genre", genre);
        formData("NoneForm", "service", service);
        formData("NoneForm", "cpSeq", $("#cpList option:selected").val());
        formData("NoneForm", "cntrctStDt", $("#cntrctStDt").val() + " 00:00:00");
        formData("NoneForm", "cntrctFnsDt", $("#cntrctFnsDt").val() + " 23:59:59");
        formData("NoneForm", "dThumbnailSeqs", thumbnaildelSeq.slice(0,-1));
        formData("NoneForm", "deleteThumbListSeqs", deleteThumbListSeq.slice(0, -1));
        var editThumbInfoData = editThumbInfo();
        if (editThumbInfoData != "") {
            formData("NoneForm", "editThumbInfo", editThumbInfoData);
        }

        if (isChgGenre) {
            formData("NoneForm" , "genreCol", getMessage("contents.table.genre"));
            formData("NoneForm" , "beforeGenres", beforeGenres.slice(0,-1));
            formData("NoneForm" , "editGenres", editGenres);
        }

        if (isChgService) {
            formData("NoneForm" , "serviceCol", getMessage("contents.table.service"));
            formData("NoneForm" , "beforeServices", beforeServices.slice(0,-1));
            formData("NoneForm" , "editServices", editServices);
        }

        callByPut("/api/contents", "contentsUpdateSuccess", "NoneForm");
    }, null, getMessage("common.confirm"));
}

editThumbInfo = function() {
    if (chgCoverThumbFileSeq == "" && chgGalleryThumbFileSeq == "") {
        return "";
    }

    var thumbInfoArr = new Array();
    var thumbInfoObj = new Object();

    if (chgCoverThumbFileSeq != "") {
        var chgThumbFileSeq = chgCoverThumbFileSeq.slice(0, -1);
        if (coverThumbInfo.length == 1) {
            if (chgThumbFileSeq == coverThumbInfo[0].fileSeq) {
                thumbInfoObj.fileSeq = coverThumbInfo[0].fileSeq;
                thumbInfoObj.fileId = coverThumbInfo[0].fileId;
                thumbInfoObj.thumbSe = "coverImg";
                thumbInfoObj.thumbList = coverThumbInfo[0].thumbList.substring(1, coverThumbInfo[0].thumbList.length - 1);

                thumbInfoArr.push(thumbInfoObj);
                thumbInfoObj = new Object();
            }
        } else {
            for (var cIndex = 0; cIndex < coverThumbInfo.length; cIndex++) {
                if (chgCoverThumbFileSeq == coverThumbInfo[cIndex].fileSeq) {
                    thumbInfoObj.fileSeq = coverThumbInfo[cIndex].fileSeq;
                    thumbInfoObj.fileId = coverThumbInfo[cIndex].fileId;
                    thumbInfoObj.thumbSe = "coverImg";
                    thumbInfoObj.thumbList = coverThumbInfo[cIndex].thumbList.substring(1, coverThumbInfo[cIndex].thumbList.length - 1);

                    thumbInfoArr.push(thumbInfoObj);
                    thumbInfoObj = new Object();
                }
            }
        }
    }

    if (chgGalleryThumbFileSeq != "") {
        var chgThumbInfoFileSeqArr = chgGalleryThumbFileSeq.slice(0, -1).split(",");
        for (var i = 0; i < chgThumbInfoFileSeqArr.length; i++) {
            for (var gIndex = 0; gIndex < galleryThumbInfo.length; gIndex++) {
                if (chgThumbInfoFileSeqArr[i] == galleryThumbInfo[gIndex].fileSeq) {
                    thumbInfoObj.fileSeq = galleryThumbInfo[gIndex].fileSeq;
                    thumbInfoObj.fileId = galleryThumbInfo[gIndex].fileId;
                    thumbInfoObj.thumbSe = "thumbnailImg";
                    thumbInfoObj.thumbList = galleryThumbInfo[gIndex].thumbList.substring(1, galleryThumbInfo[gIndex].thumbList.length - 1);

                    thumbInfoArr.push(thumbInfoObj);
                    thumbInfoObj = new Object();
                    break;
                }
            }
        }
    }

    var editThumbInfo = new Object();
    editThumbInfo.thumbInfo = thumbInfoArr;

    return JSON.stringify(editThumbInfo);
}

contentsUpdateSuccess = function(data){
    formDataDeleteAll("NoneForm");
    initData();
    if(data.resultCode == "1000"){
        uploadS();
    }
}

/*서브메타데이터 수정 함수 시작*/

genreCheck = function(){
    if ($("#firstCtg").val() == firstCtg) {
        $(genreList).each(function(i,item){
            $("#genreList").find("input[data-seq="+item.genreSeq+"]").addClass('ContentSelectBoxOn');
        });
    }
}



/* blockUI 닫기
 * parameter: x
 * return: x
 */
function popLayerClose() {
    $("body").css("overflow-y" , "visible");
    $("#submetadataTableArea").mCustomScrollbar('destroy');
    $.unblockUI();
    $("#submetadataTableArea input").val('');
}

/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function showMetadataPopLayer() {
    callByGet("/api/codeInfo?comnCdCtg=CONTS_SUB_META", "showMetadataConts", "NoneForm");
}

showMetadataConts = function(data) {
    if(data.resultCode == "1000"){
        var tableHtml = "";
        $("#editSubmetadata").html("");
        $(data.result.codeList).each(function(i, item) {
            if(item.comnCdNM == getMessage("submeta.time")){ // '영상상영시간'이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthM onlynum remaining keyupEq\" max=\"200\"> "
                    + getMessage("common.minute") + "</td></tr>";
            } else if(item.comnCdNM == getMessage("submeta.rating")){ // '심의등급'이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\"></th>"
                    + "<td><select class='selectBoxUnselect'><option>" +getMessage("submeta.rating.basic")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.general")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.twelve")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.fifteen")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.RatedR")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.limited")+ "</option></select></td></tr>";
            } else if(item.comnCdNM.indexOf(getMessage("submeta.rt")) != -1){ // 'RT(0'00")'이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthL remaining onlyNumNQuotation\" max=\"200\"> "
                    + "</td></tr>";
            } else if(item.comnCdNM.indexOf(getMessage("submeta.fps")) != -1 || item.comnCdNM.indexOf(getMessage("submeta.gb")) != -1
                    || item.comnCdNM.indexOf(getMessage("submeta.bitrate")) != -1){ // 'FPS' 또는 '용량(GB)' 또는  'Bitrate(mbps)' 이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthL remaining onlyNumNSpot\" max=\"200\"> "
                    + "</td></tr>";
            } else {
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthL remaining keyupEq\" max=\"200\">"
                    + "</td></tr>";
            }
        });

        $("#editSubmetadata").append(tableHtml);

        if (metaData != "") {
            var metaObj = JSON.parse(metaData);
            for(key in metaObj) {
                $("#editSubmetadata tr").each(function(i) {
                    if (key == $(this).find("input[type=hidden]").val()) {
                        if ($(this).find("th").text() == getMessage("submeta.rating")) {
                            $(this).find("option").each(function() {
                                if (this.value == metaObj[key]) {
                                    $(this).attr("selected",true);
                                }
                            });
                        } else if($(this).find("th").text().indexOf(getMessage("submeta.rt")) != -1){
                            // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                            if(metaObj[key].match("#a8484") != null || metaObj[key].match("#q0808") != null){
                                metaObj[key] = metaObj[key].replace("#a8484", "\'");
                                metaObj[key] = metaObj[key].replace("#q0808", "\"");
                            }
                            $(this).find("input[type=text]").val(metaObj[key]);
                        } else {
                            $(this).find("input[type=text]").val(metaObj[key]);
                        }
                    }
                });
            }
        }
    } else {
        $("#editSubmetadata").html("");
    }

    keyup();

    $(".keyupEq").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$(this).val();
        if (re.test(temp)) { // 특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $(this).val(temp.replace(re,""));
        }
    });

    $(".onlyNumNQuotation").bind("keyup",function(){ // 숫자, ', " 만 입력가능
        re = /[^\d\'\"]/gi;
        var temp=$(this).val();
        if (re.test(temp)) { // 특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $(this).val(temp.replace(re,""));
        }
    });

    $(".onlyNumNSpot").bind("keyup",function(){ // 숫자, . 만 입력가능
        re = /[^\d\.]/gi; //숫자와 온점 말고는 다 삭제
        var temp=$(this).val();
        if (re.test(temp)) { // 특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $(this).val(temp.replace(re,""));
        }
    });

    setkeyup();

    $("#popupEditSubmetadata").css({"left":"-1000px","top":"-1000px"}).show();
    var popupHeight = $("#popupEditSubmetadata").height();
    var popupWidth = $("#popupEditSubmetadata").width();
    $("#popupEditSubmetadata").hide();
    popLayerDiv("popupEditSubmetadata", popupWidth, popupHeight, true);
    $("#submetadataTableArea").mCustomScrollbar({ axis : "y", theme:"inset-3" });
}

function editMetadata() {
    var metaStr = "";
    var changeMetaData = "";//수정된 메타데이트 값을 저장하기 위한 변수
    var hasVal = false;

    $("#editSubmetadata tr").each(function(i){
        metaStr += "\"";
        metaStr += $(this).find("input[value]").val();
        metaStr += "\":\"";
        if ($(this).find("select").length == 1) {
            var optionVal = $(this).find("select option:selected").val();
            if(optionVal.indexOf(getMessage("submeta.rating.basic")) == -1){
                metaStr += $(this).find("select option:selected").val();
            }
        } else if($(this).find("th").text().indexOf(getMessage("submeta.rt")) != -1){
            var rtValue = $(".onlyNumNQuotation").val();
            if(rtValue.match("\'") != null || rtValue.match("\"") != null){
                // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                rtValue = rtValue.replace("\'", "#a8484"); // 작은 따옴표 입력시 #a8484로 변환하여 DB 저장
                rtValue = rtValue.replace("\"", "#q0808"); // 큰 따옴표 입력시 #q0808로 변환하여 DB 저장
            }
            $(".onlyNumNQuotation").val(rtValue);
            metaStr += $(this).find("input[type=text]").val();
        } else {
            metaStr += $(this).find("input[type=text]").val();
        }

        if (metaStr != "") {
            hasVal = true;
        }

        metaStr += "\"";
        var trLength = $("#editSubmetadata tr").length-1;
        if (trLength != i) {
            metaStr += ",";
        }
    });
    changeMetaData = "{" + metaStr + "}";
    if (hasVal == false && metaData == "") { // 입력된 값이 없고, 이전 metaData 정보도 없을 경우 저장이 필요하지 않으므로 그냥 팝업 close
        popLayerClose();
    } else {
        popConfirmLayer(getMessage("common.update.submeta.msg"), function() {
            formData("NonePopup", "contsSeq", $("#contsSeq").val());
            formData("NonePopup", "contsSubMetadataSeq", contsSubMetadataSeq);
            formData("NonePopup" , "metaData", changeMetaData);
            callByPut("/api/metadata" , "metadataUpdateSuccess", "NonePopup", "metadataUpdateFail");
        }, null, getMessage("common.confirm"));
    }
}

metadataUpdateSuccess = function(data) {
    formDataDeleteAll("NonePopup");
    popAlertLayer(getMessage("success.common.update"));
    $(".swal-button").click(function() {
        reloadSubMetadata();
    });
}

metadataUpdateFail = function(data) {
    formDataDeleteAll("NonePopup");
    popAlertLayer(getMessage("fail.common.update"));
}

function reloadSubMetadata() {
    formData("NonePopup", "contsSeq", $("#contsSeq").val());
    callByGet("/api/metadata" , "metadataInfoSuccess", "NonePopup", "metadataInfoFail");

    $("body").css("overflow-y" , "visible");
    $("#submetadataTableArea").mCustomScrollbar('destroy');
    $.unblockUI();
}

metadataInfoSuccess = function(data) {
    formDataDeleteAll("NonePopup");
    if(data.resultCode == "1000"){
        var resultData = data.result.subMetadataInfo;
        contsSubMetadataSeq = resultData.contsSubMetadataSeq;
        metaData = resultData.metaData.replace(/&#034;/gi, "\"");
    }
}

metadataInfoFail = function(data) {
    formDataDeleteAll("NonePopup");
    popAlertLayer(getMessage("fail.common.select"));
}

initData = function() {
    beforeGenres = "";
    beforeServices = "";
}

chkChange = function(beforeVal, editVal) {
    var isChg = false;
    var arrEditVal = editVal.split(',');
    var arrBefVal = beforeVal.slice(0,-1).split(',');
    if (arrEditVal.length == arrBefVal.length) {
        arrEditVal.forEach(function(item) {
            if (arrBefVal.indexOf(item) == -1) {
                isChg = true;
                return false;
            }
        });
    } else {
        isChg = true;
    }

    return isChg;
}

stopLoading = function() {
    $("#loading").hide().css("background", "initial");
    $(".thumbCretLoading").remove();
    $(".thumbDelLoading").remove();
}

chgThumbFileSeq = function(thumbSe) {
    var chgThumbFileSeqs = "";

    if (thumbSe == "coverImgArea") {
        for (var i = 0; i < coverThumbInfo.length; i++) {
            if (coverThumbInfo[i].fileSeq != undefined && coverThumbInfo[i].fileSeq != "") {
                var chgCoverThumbFileSeqArr = chgCoverThumbFileSeq.split(",");
                var isExist = false;

                if (chgCoverThumbFileSeqArr.length > 0) {
                    for (j = 0; j < chgCoverThumbFileSeqArr.length; j++) {
                        if (chgCoverThumbFileSeqArr[j] == coverThumbInfo[i].fileSeq) {
                            isExist = true;
                            break;
                        }
                    }
                }
                if (!isExist) {
                    chgCoverThumbFileSeq += coverThumbInfo[i].fileSeq + ",";
                }
            }
        }
        chgThumbFileSeqs = chgCoverThumbFileSeq;
    } else if (thumbSe == "thumbnailArea") {
        for (var i = 0; i < galleryThumbInfo.length; i++) {
            if (galleryThumbInfo[i].fileSeq != undefined && galleryThumbInfo[i].fileSeq != "") {
                var chgGalleryThumbFileSeqArr = chgGalleryThumbFileSeq.split(",");
                var isExist = false;
                if (chgGalleryThumbFileSeqArr.length > 0) {
                    for (j = 0; j < chgGalleryThumbFileSeqArr.length; j++) {
                        if (chgGalleryThumbFileSeqArr[j] == galleryThumbInfo[i].fileSeq) {
                            isExist = true;
                            break;
                        }
                    }
                }
                if (!isExist) {
                    chgGalleryThumbFileSeq += galleryThumbInfo[i].fileSeq + ",";
                }
            }
        }

        chgThumbFileSeqs = chgGalleryThumbFileSeq;
    }

    return chgThumbFileSeqs;
}

deleteThumbList = function(thumbSe, fileId) {
    var delList = "";

    if (thumbSe == "coverImgArea") {
        var delCoverThumbList = "";
        if (fileId == "") {
            for (var i = 0; i < coverThumbInfo.length; i++) {
                if (coverThumbInfo[i].fileSeq == "") {
                    delCoverThumbList += coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1) + ", ";
                } else {
                    // 재 생성된 파일일 경우 기존의 fileId는 유지하고 thumbList만 변경되었으므로 thumbList 내 각 값이 fileId를 포함하지 않으면 새로 생성된 것으로 간주
                    var thumbListArr = coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1).split(", ");
                    for (var j = 0; j < thumbListArr.length; j++) {
                        if (thumbListArr[j].indexOf(coverThumbInfo[i].fileId) == -1) {
                            delCoverThumbList += thumbListArr[j] + ", ";
                        }
                    }
                }
            }
        } else {
            for (var i = 0; i < coverThumbInfo.length; i++) {
                if (coverThumbInfo[i].fileId == fileId) {
                    if (coverThumbInfo[i].fileSeq == "") {
                        delCoverThumbList += coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1) + ", ";
                    } else {
                        var thumbListArr = coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1).split(", ");
                        for (var j = 0; j < thumbListArr.length; j++) {
                            if (thumbListArr[j].indexOf(coverThumbInfo[i].fileId) == -1) {
                                delCoverThumbList += thumbListArr[j] + ", ";
                            }
                        }
                    }
                }
            }
        }
        delList = delCoverThumbList;
    } else if (thumbSe == "thumbnailArea") {
        var delGalleryThumbList = "";
        if (fileId == "") {
            for (var i = 0; i < galleryThumbInfo.length; i++) {
                if (galleryThumbInfo[i].fileSeq == "") {
                    delGalleryThumbList += galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1) + ", ";
                } else {
                    // 재 생성된 파일일 경우 기존의 fileId는 유지하고 thumbList만 변경되었으므로 thumbList 내 각 값이 fileId를 포함하지 않으면 새로 생성된 것으로 간주
                    var thumbListArr = galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1).split(", ");
                    for (var j = 0; j < thumbListArr.length; j++) {
                        if (thumbListArr[j].indexOf(galleryThumbInfo[i].fileId) == -1) {
                            delGalleryThumbList += thumbListArr[j] + ", ";
                        }
                    }
                }
            }
        } else {
            for (var i = 0; i < galleryThumbInfo.length; i++) {
                if (galleryThumbInfo[i].fileId == fileId) {
                    if (galleryThumbInfo[i].fileSeq == "") {
                        delGalleryThumbList += galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1) + ", ";
                    } else {
                        var thumbListArr = galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1).split(", ");
                        for (var j = 0; j < thumbListArr.length; j++) {
                            if (thumbListArr[j].indexOf(galleryThumbInfo[i].fileId) == -1) {
                                delGalleryThumbList += thumbListArr[j] + ", ";
                            }
                        }
                    }
                }
            }
        }
        delList = delGalleryThumbList;
    }

    return delList;
}

//대표 이미지 또는 갤러리 이미지 저장 배열 값에서 해당 fileId에 해당되는 값을 삭제
deleteThumbInfo = function(thumbSe, fileId, thumbInfo) {
    var delIndexStr = "";

    for (var i = 0; i < thumbInfo.length; i++) {
        if (thumbInfo[i].fileId == fileId) {
            delIndexStr = String(i);
            break;
        }
    }

    if (delIndexStr != "") {
        thumbInfo.splice(parseInt(delIndexStr), 1);
    }
}

function createThumbImg(thumbSe) {
    // 기 업로드 된 파일들은 이미 서버에 저장된 상태로 swupload를 사용하지 않으므로 내부 api를 통해 먼서 썸네일 이미지 생성,
    // 새로 추가되거나 기존 파일에서 삭제 버튼 클릭 후 변경한 파일에 대해서는 기존 파일 생성이 완료된 후 swupload를 이용하여 썸네일 생성
    $("#loading").show().css("background", "#f9f9f9");
    $("#loading").append("<p class='thumbCretLoading'>" + getMessage("common.creating.thumbnail") + "</p>");

    // 이전 생성되었던 파일들을 삭제, 기 등록된 파일들은 수정완료 버튼 클릭 시 삭제되며 여기서는 temp 폴더에 생성하였던 썸네일 파일들을 삭제
    // 삭제 api 실행 완료 후 reCretThumbImg 호출
    var delThumbList = deleteThumbList(thumbSe, "");

    if (delThumbList == "") {
        reCretThumbImg(thumbSe);
    } else {
        delThumbImg(thumbSe, "", delThumbList);
    }
}

delThumbImg = function(thumbSe, fileId, delList) {
    if (fileId != "") {
        $("#loading").show().css("background", "#f9f9f9");
        $("#loading").append("<p class='thumbDelLoading'>" + getMessage("common.delete.prev.thumbnail") + "</p>");
        formData("NoneForm", "fileId", fileId);
        formData("NoneForm", "delThumbList", delList);
    } else {
        formData("NoneForm", "delThumbList", delList.slice(0, -2));
    }
    formData("NoneForm", "thumbSe", thumbSe);
    callByPost("/api/delThumb", "delThumbSuccess", "NoneForm", "delThumbFail");
}

delThumbSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        if (data.result.delThumbInfo.fileId == undefined) {
            reCretThumbImg(data.result.delThumbInfo.thumbSe);
        } else {
            if (data.result.delThumbInfo.thumbSe == "coverImgArea") {
                deleteThumbInfo("coverImgArea", data.result.delThumbInfo.fileId, coverThumbInfo);
            } else if (data.result.delThumbInfo.thumbSe == "thumbnailArea") {
                deleteThumbInfo("thumbnailArea", data.result.delThumbInfo.fileId, galleryThumbInfo);
            }
            stopLoading();
        }
    } else {
        stopLoading();
        popAlertLayer(getMessage("common.fail.delete.thumbnail"));
    }
}

delThumbFail = function(data) {
    stopLoading();
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("common.fail.delete.thumbnail"));
}

reCretThumbImg = function(thumbSe) {
    var chgFileSeqs = chgThumbFileSeq(thumbSe);

    if (chgFileSeqs == "") { // 기 등록된 파일들 정보를 모두 삭제 후 파일 추가하여 썸네일 생성 시
        cretThumbNewFile(thumbSe);
    } else {
        if (thumbSe == "coverImgArea") {
            formData("NoneForm", "thumbSizes", coverThumbSizes);
        } else {
            formData("NoneForm", "thumbSizes", galleryThumbSizes);
        }
        formData("NoneForm", "thumbSe", thumbSe);
        formData("NoneForm", "chgFileSeqs", chgFileSeqs.slice(0, -1));
        callByPost("/api/editThumb", "editThumbSuccess", "NoneForm", "editThumbFail");
    }
}

editThumbSuccess = function(data) {
    formDataDeleteAll("NoneForm");

    if (data.resultCode == "1000") {
        var thumbSe = data.result.thumbSe;
        if (thumbSe == "coverImgArea") {
            $(data.result.thumbInfoList).each(function(i,item) {
                for (var index = 0; index < coverThumbInfo.length; index++) {
                    if (item.fileSeq == coverThumbInfo[index].fileSeq) {
                        coverThumbInfo[index].thumbList = item.thumbList;
                        $("#coverImgArea").find(".fileTable tr").each(function(i) {
                            var name = $(this).find(".fileName").text().replace(getMessage("common.filename") + " : ","").trim();
                            if (coverThumbInfo[index].coverImgNm == name && $(this).find(".btnPrevWhite").hasClass("prevCoverNone")) {
                                $(this).find(".btnPrevWhite").toggleClass("prevCoverNone");
                            }
                        });
                        continue;
                    }
                }
            });

            $("#btnCoverCretThumb").val(getMessage("common.recreate.thumbnail"));
            isCoverThumb = true;
        } else if (thumbSe == "thumbnailArea") {
            $(data.result.thumbInfoList).each(function(i,item) {
                for (var index = 0; index < galleryThumbInfo.length; index++) {
                    if (item.fileSeq == galleryThumbInfo[index].fileSeq) {
                        galleryThumbInfo[index].thumbList = item.thumbList;
                        $("#thumbnailArea").find(".fileTable tr").each(function(i) {
                            var fileSeq = "";
                            if ($(this).attr('id') != undefined) {
                                fileSeq = $(this).attr('id').replace("thumbnail","").trim();
                            }
                            if (galleryThumbInfo[index].fileSeq == fileSeq && $(this).find(".btnPrevWhite").hasClass("prevGalleryNone")) {
                                $(this).find(".btnPrevWhite").toggleClass("prevGalleryNone");
                            }
                        });
                        continue;
                    }
                }
            });

            $("#btnGalleryCretThumb").val(getMessage("common.recreate.thumbnail"));
            isGalleryThumb = true;
        }

        cretThumbNewFile(thumbSe);
    } else {
        stopLoading();
        if (data.resultCode == "1010") {
            popAlertLayer(getMessage("common.fail.file.not.exist"));
        } else {
            popAlertLayer(getMessage("common.fail.creating.thumbnail"));
        }
    }
}

var queueLen = 0;
var queueTotLen = 0;
cretThumbNewFile = function(thumbSe) {
    if (curFileUpload) {
        curFileUpload = null;
    }

    if (thumbSe == "coverImgArea") {
        if (coverImgUploadTemp == undefined) { // 새로이 추가된 파일이 없는 것으로 간주
            stopLoading();
            return;
        }

        if (coverImgUploadTemp.customSettings.isThumb = "true") {
            coverImgUploadTemp.customSettings.isThumb = "false";
        }
        queueLen = coverImgUploadTemp.getStats().files_queued;
        queueTotLen = coverImgUploadTemp.getStats().files_queued;
        coverImgUploadTemp.setUploadURL(makeAPIUrl("/api/cretThumb"));
        fileUploadQueue.push(coverImgUploadTemp);
    } else if (thumbSe == "thumbnailArea") {
        if (thumbnailUploadTemp == undefined) { // 새로이 추가된 파일이 없는 것으로 간주
            stopLoading();
            return;
        }

        if (thumbnailUploadTemp.customSettings.isThumb = "true") {
            thumbnailUploadTemp.customSettings.isThumb = "false";
        }
        queueLen = thumbnailUploadTemp.getStats().files_queued;
        queueTotLen = thumbnailUploadTemp.getStats().files_queued;
        thumbnailUploadTemp.setUploadURL(makeAPIUrl("/api/cretThumb"));
        fileUploadQueue.push(thumbnailUploadTemp);
    }

    startUploadIfNeeded();
}

createThumbImgFail = function(data) {
    stopLoading();

    popAlertLayer(getMessage("common.fail.creating.thumbnail"));
}

editThumbFail = function(data) {
    formDataDeleteAll("NoneForm");

    stopLoading();
    popAlertLayer(getMessage("common.fail.creating.thumbnail"));
}

var preThumMaxW = 0;
var preThumMaxH = 0;
function previewThumbnail(thumbSe, fileId) {
    $("#thumbView").html("");
    var tableHtml = "<table class='thumGrp'><tr>";
    if (thumbSe == "coverImgArea") {
        for (var i = 0; i < coverThumbInfo.length; i++) {
            if (coverThumbInfo[i].fileId == fileId) {
                $("#originFile").html(getMessage("common.origin.file.name") + " : " + coverThumbInfo[i].coverImgNm);
                var thumbList = coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1).split(", ");

                for (var j = thumbList.length - 1; j > 0; j--) { // 0번 이미지는 원본 이미지로 제외
                    var fileNmArr = thumbList[j].split(".");
                    var fileInfoArr = fileNmArr[0].split("_");
                    var fileSizeArr = fileInfoArr[fileInfoArr.length - 1].split("x");

                    tableHtml += "<td><img src='" + makeAPIUrl(thumbList[j], "img") + "'><br>" + fileSizeArr[0] + " x " + fileSizeArr[1] + " (px)" + "</td>";

                    if(preThumMaxW <= parseInt(fileSizeArr[0])) {
                        preThumMaxW = parseInt(fileSizeArr[0]);
                    }
                    if(preThumMaxH <= parseInt(fileSizeArr[1])) {
                        preThumMaxH = parseInt(fileSizeArr[1]);
                    }
                }
            }
        }
    } else {
        for (var i = 0; i < galleryThumbInfo.length; i++) {
            if (galleryThumbInfo[i].fileId == fileId) {
                $("#originFile").html(getMessage("common.origin.file.name") + " : " + galleryThumbInfo[i].thumbnailNm);
                var thumbList = galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1).split(", ");

                for (var j = thumbList.length - 1; j > 0; j--) { // 0번 이미지는 원본 이미지로 제외
                    var fileNmArr = thumbList[j].split(".");
                    var fileInfoArr = fileNmArr[0].split("_");
                    var fileSizeArr = fileInfoArr[fileInfoArr.length - 1].split("x");

                    tableHtml += "<td><img src='" + makeAPIUrl(thumbList[j], "img") + "'><br>" + fileSizeArr[0] + " x " + fileSizeArr[1] + " (px)" + "</td>";

                    if(preThumMaxW <= parseInt(fileSizeArr[0])) {
                        preThumMaxW = parseInt(fileSizeArr[0]);
                    }
                    if(preThumMaxH <= parseInt(fileSizeArr[1])) {
                        preThumMaxH = parseInt(fileSizeArr[1]);
                    }
                }
            }
        }
    }

    tableHtml += "</tr></table>";
    $("#thumbView").append(tableHtml);
    $("#thumbView").show();
    popLayerDiv("previewThumnail", 500, 300, true);
}

function closePreviewThumbnail() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}
