/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

/*
 * 전역 변수 목록
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;

$(document).ready(function() {

    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateContentsList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateContentsList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateContentsList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateContentsList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateContentsList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateContentsList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateContentsList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateContentsList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $(".btnSearch").click(function() {
        updateContentsList();
    });

    $("#keyword").keydown(function(event) {
        if (event.keyCode == 13) {
            updateContentsList();
        }
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    if ($("#dateType").val() && $("#startDate").val() && $("#endDate").val()) {
        restoreDateType($("#dateType").val(), $("#startDate").val(), $("#endDate").val())
    } else {
        setDateType();
    }

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    var options = {
        "obj" : $(".btnList"),
        "defaultUri" : "/contents/usage"
    }
    setListButton(options);

    initContentUsageDetailList();
});

initContentUsageDetailList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("contents.table.service.name"),
        getMessage("contents.table.store"),
        getMessage("contents.table.playCount"),
        getMessage("contents.table.playTime"),
        getMessage("contents.table.avgPlayTime"),
        "svcSeq",
        "seq"
    ];
    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentUsageDetailList",
            records: "result.totalCount",
            repeatitems: false,
            id: "storSeq"
        },
        colNames: columnNames,
        colModel:[
            {name:"num", index:"num", align:"center", width:40, hidden:true},
            {name:"svcNm", index:"svcNm", align:"center"},
            {name:"storNm", index:"storNm", align:"center"},
            {name:"contsTotalPlayCount", index:"contsTotalPlayCount", align:"center"},
            {name:"contsTotalPlayTime", index:"contsTotalPlayTime", align:"center"},
            {name:"contsAvgPlayTime", index:"contsAvgPlayTime", align:"center"},
            {name:"svcSeq", index:"svcSeq", hidden:true},
            {name:"storSeq", index:"storSeq", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: 10, // 페이지에 출력될 칼럼 개수
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "svcNm",
            sortorder: "asc",
            height: "auto",
            multiselect: false,
            postData: {
                type : "contentUsageDetail",
                contsSeq: $("#contsSeq").val(),
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val()
            },

            beforeRequest:function() {
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                if (hasChangedHash) {
                    var strHash = document.location.hash.replace("#", "");
                    var page = parseInt(findGetParameter(strHash, "page"));
                    var dateType = findGetParameter(strHash, "dateType");
                    var startDate = findGetParameter(strHash, "startDate");
                    var endDate = findGetParameter(strHash, "endDate");
                    var target = findGetParameter(strHash, "target");
                    var keyword = findGetParameter(strHash, "keyword");

                    $("#dateType").val(dateType);
                    $("#startDate").val(startDate);
                    $("#endDate").val(endDate);
                    restoreDateType(dateType, startDate, endDate);

                    $("#target").val(target);
                    $("#keyword").val(keyword);

                    myPostData.page = page;
                    myPostData.startDate = startDate;
                    myPostData.endDate = endDate;
                    if (keyword != null) {
                        myPostData.target = target;
                        myPostData.keyword = keyword;
                    } else {
                        delete myPostData.target;
                        delete myPostData.keyword;
                    }
                }

                $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
            },
            /*
             * parameter: x
             * jqGrid 그리기가 끝나면 함수 실행
             */
            loadComplete: function(data) {
                var contsTitle = "";
                if (data.result != undefined && data.result.contsTitle != undefined) {
                    contsTitle = data.result.contsTitle;
                }
                $("#contentTitle").text(contsTitle);
                didCompleteLoad(this);
            },
            loadError: function (jqXHR, textStatus, errorThrown) {
                $("#contentTitle").text("");
                didCompleteLoad(this);
                $("#jqgridData").clearGridData();
                $("#sp_1_pageDiv").text(1);
            }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

function updateContentsList() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            search: true,
            postData: {
                type : "contentUsageDetail",
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                target: $("#target").val(),
                keyword: $("#keyword").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

function setSearchField() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        var searchHtml = "";

        for (var i = 0; i < colModel.length; i++) {
            var name = colModel[i].name;
            if (name == "storNm") {
                searchHtml += "<option value=\"" + name + "\">" + colNames[i] + "</option>";
            }
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
}

function downloadExcel() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "contentUsageDetail",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "contsSeq" : $("#contsSeq").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
            "target" : $("#target").val(),
            "keyword" : $("#keyword").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    setSearchField();

    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        var searchTarget = $("#target").val();
        var searchKeyword = $("#keyword").val();
        if ($(obj).context.p.search && searchKeyword) {
            hashlocation += "&target=" + searchTarget + "&keyword=" + searchKeyword;
        }
        document.location.hash = hashlocation;
    }
}