/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */


package kt.com.im.store.code.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.code.service.CodeService;
import kt.com.im.store.code.vo.CodeVO;
import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.member.vo.MemberVO;

@Controller
public class CodeApi {

    @Resource(name = "CodeService")
    private CodeService codeService;

    /**
    * 공통 코드 관련 처리 API (GET-코드 목록)
    * @param mv - 화면 정보를 저장하는 변수
    * @param comnCdCtg - 조건값 : 코드 분류
    * @param comnCdValue - 조건값 : 코드 값
    * @return modelAndView  & API 메세지값 & WebStatus 
    *     + GET - 검색조건에 따른 코드 리스트 
    */
    @RequestMapping(value = "/api/codeInfo")
    public ModelAndView CodeInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/code/codeProcess");

        if (request.getMethod().equals("POST")) {
            MemberVO userVO = (MemberVO)session.getAttribute("authenticatedUser");
            if (userVO == null) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }

            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
            }
        } else { // get
                 // 코드 분류
            String comnCdCtg = request.getParameter("comnCdCtg") == null ? "" : request.getParameter("comnCdCtg");

            // 코드 값
            String comnCdValue = request.getParameter("comnCdValue") == null ? "" : request.getParameter("comnCdValue");

            // 코드 값
            String delYN = request.getParameter("delYN") == null ? "N" : request.getParameter("delYN");

            // 코드 값
            String remoteCodes = request.getParameter("remoteCode") == null ? "" : request.getParameter("remoteCode"); // 제외코드

            CodeVO item = new CodeVO();
            item.setComnCdCtg(comnCdCtg);
            item.setComnCdValue(comnCdValue);
            item.setDelYn(delYN);

            // 회원 가입 화면에서 CP 목록이 필요하기 때문에 권한 체크하지 않도록 예외처리
            if (!"CP".equals(comnCdCtg.toUpperCase())) {
                MemberVO userVO = (MemberVO)session.getAttribute("authenticatedUser");
                if (userVO == null) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }
            }

            String[] remoteCode = remoteCodes.split(",");

            List<String> list = new ArrayList<String>();
            for (int i = 0; i < remoteCode.length; i++) {
                list.add(remoteCode[i]);
            }
            item.setList(list);

            List<CodeVO> resultItem = codeService.codeList(item);
            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
            }

            rsModel.setResultType("codeList");
        }
        return rsModel.getModelAndView();
    }

}
