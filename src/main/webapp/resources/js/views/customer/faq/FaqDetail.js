/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    faqInfo();
});

function faqInfo(){
    formData("NoneForm", "faqSeq", $("#faqSeq").val());
    callByGet("/api/faq", "faqInfoSuccess", "NoneForm", "faqInfoFail");
}

faqInfoSuccess = function(data) {
    formDataDelete("NoneForm", "faqSeq");
    if(data.resultCode == "1000"){
        var faqDetail = data.result.faqDetail;
        $("#faqTitleQsn").html("<span class='txtRed'>" + getMessage("faq.question") + "</span><span class='faqDate'>"
                + getMessage("common.type") + " : " + faqDetail.faqType + "<span class='div'>|</span>"
                + getMessage("common.category") + " : " + faqDetail.faqCtg + "<span class='div'> | </span>"
                + getMessage("common.reger") + " : " + faqDetail.cretrNm + "<span class='div'> | </span>"
                + getMessage("common.regdate") + " : " + faqDetail.regDt + "</span>");
        $("#faqQsn").html(faqDetail.faqTitle);
        $("#faqAns").html(xssChk(faqDetail.faqSbst));
        $("#faqAns").html($("#faqAns").text());

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/faq");
    }
}

faqInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/faq");
}