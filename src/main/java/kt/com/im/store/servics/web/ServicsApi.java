/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.servics.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.servics.service.ServicsService;
import kt.com.im.store.servics.vo.ServicsVO;

@Controller
public class ServicsApi {

    @Resource(name = "ServicsService")
    private ServicsService servicsService;

    /**
     * 서비스 관련 처리 API (GET-서비스 목록)
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return modelAndView & API 메세지값 & WebStatus
     *         + GET - 검색조건에 따른 코드 리스트
     */
    @RequestMapping(value = "/api/service")
    public ModelAndView serviceInfo(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        if (session.getAttribute("id") == null) {
            response.setStatus(401);
        } else {
            if (request.getMethod().equals("POST")) {
                String Method = request.getParameter("_method");
                if (Method == null) {
                    Method = "POST";
                }
                if (Method.equals("PUT")) { // put
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else if (Method.equals("DELETE")) { // delete
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                } else { // post
                    rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
                }
            } else { // get
                // 코드 분류
                // String comnCdCtg = request.getParameter("comnCdCtg") == null ? "" :
                // request.getParameter("comnCdCtg");

                ServicsVO item = new ServicsVO();
                // item.setComnCdCtg(comnCdCtg);

                int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);
                item.setCpSeq(cpSeq);

                List<ServicsVO> resultItem = servicsService.serviceList(item);
                if (resultItem.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                }

                rsModel.setResultType("serviceList");
            }
        }
        rsModel.setViewName("../resources/api/service/serviceProcess");
        return rsModel.getModelAndView();
    }
}
