<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/customer/notice/NoticeDetail.js"></script>

<style>
#noticeSbst img {
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
</style>

<input type="hidden" id="leftNum" value="2">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" id="noticeSeq" value="${noticeSeq}">

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div id="tabGrp">
            <div class="tab tabEnd tabOn"><spring:message code="notice.detail.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- content S -->
        <div class="content notice">
            <div class="noticeTitle" id="noticeTitle"></div>
            <!-- Detail Area S -->
            <div class="viewDetail noBasic" id="noticeSbst"></div>
            <!-- Detail Area E -->

            <!-- Attatch S -->
            <div class="attatchGrp" id="fileGrp">
            </div>
            <!-- Attatch S -->

            <!-- Button Group S -->
            <div class="btnGrp">
                <input type="button" class="btnNormal btnList btnRight cursorPointer" value="<spring:message code="button.list"/>" onclick="pageMove('/notice')">
            </div>
            <!-- Button Group E -->
        </div>
        <!-- content E -->

    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>