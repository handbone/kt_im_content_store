/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.content.service;

import java.util.List;

import kt.com.im.store.content.vo.ContentVO;

/**
*
* 클래스에 대한 상세 설명
*
* @author A2TEC
* @since 2018
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 2.   A2TEC      최초생성
*
*
* </pre>
*/

public interface ContentService {

    List<ContentVO> contentsList(ContentVO item);

    int contentsListTotalCount(ContentVO item);

    ContentVO contentInfo(ContentVO item);

    List<ContentVO> contentsGenreList(ContentVO item);

    List<ContentVO> contentServiceList(ContentVO item);

    List<ContentVO> firstCtgList(ContentVO item);

    List<ContentVO> contsCtgList(ContentVO item);

    int contentsInsert(ContentVO item);

    int contsCtgValueUpdate(ContentVO item);

    int contentsGenreInsert(ContentVO item);

    int contentsServiceInsert(ContentVO item);

    int contentsSttusUpdate(ContentVO item);

    List<ContentVO> contentsThumbnailList(ContentVO item);

    List<ContentVO> contentsVideoList(ContentVO item);

    List<ContentVO> fileDataInfo(ContentVO item);

    ContentVO metadataXmlInfo(ContentVO metaItem);

    ContentVO contentCoverImageInfo(ContentVO item);

    int categoryValueUpdate(ContentVO item);

    ContentVO fileInfo(ContentVO item);

    int thumbnailDelete(ContentVO item);

    int contentsUpdate(ContentVO item);

    int contentsDelete(ContentVO item);

    int contentsGenreDelete(ContentVO item);

    int contentsServiceDelete(ContentVO item);

    List<ContentVO> contentsPrevList(ContentVO item);

    ContentVO contsMetaInfo(ContentVO metaItem);

    List<ContentVO> verifyHstList(ContentVO data);

    int verifyHstListTotalCount(ContentVO data);

    List<ContentVO> modifyHstList(ContentVO data);

    int modifyHstListTotalCount(ContentVO data);

    public ContentVO getContentTitle(ContentVO data);

    List<ContentVO> recommendContentList(ContentVO data);

    int deleteRecommendContents(ContentVO data);

    int insertChangeHst(ContentVO data);

    List<ContentVO> contentThumbList(ContentVO data);

    int updateThumbFileInfo(ContentVO data);

    int thumbFileInsert(ContentVO data);

    ContentVO getMetadata(ContentVO data);

    int metadataInsert(ContentVO data);

    int metadataUpdate(ContentVO data);

    int metadataDelete(ContentVO data);

    ContentVO contentCdtNotInfo(ContentVO searchConts);

    List<ContentVO> contentCdtNotInfoList(ContentVO searchConts);

}
