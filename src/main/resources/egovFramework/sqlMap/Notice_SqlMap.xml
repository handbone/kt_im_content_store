<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<mapper namespace="NoticeDAO">

<!--
    SQL File Name : Notice_SqlMap.xml
    Description : 공지사항 데이터 관리
    Modification Information
       수정일 수정자 Version Query Id
    ──── ──── ─────── ────────────
    2018.05.09 허용수 1.0 최초 생성
    2018.05.21 허용수 중요 공지사항 리스트 조회 쿼리 추가
-->

    <!-- 쿼리명 : 중요 공지사항 리스트  
                설명 : 중요 공지사항 리스트(노출 우선 순위 존재) 정보를 조회하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2018.05.21 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2018.05.09
     -->
    <select id="noticeImptList" parameterType="NoticeVO" resultType="NoticeVO">
        SELECT A.NOTICE_SEQ,
               C.COMN_CD_NM AS NOTICE_TYPE,
               A.NOTICE_TITLE,
               A.RETV_NUM,
               B.MBR_NM AS CRETR_NM,
               COALESCE(TO_CHAR(A.AMD_DT, 'YYYY-MM-DD HH24:MI'), TO_CHAR(A.CRET_DT, 'YYYY-MM-DD HH24:MI')) AS REG_DT,
               (SELECT COUNT(FILE_SEQ) FROM IM_FILE WHERE FILE_CONTS_SEQ = A.NOTICE_SEQ AND FILE_SE = 'NOTICE') AS FILE_COUNT,
               A.NOTICE_PREF_RANK
        FROM IM_NOTICE A
            INNER JOIN IM_MEMBER B ON A.CRETR_ID = B.MBR_ID
            INNER JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'POST_SE' AND A.NOTICE_TYPE = C.COMN_CD_VALUE AND (C.DEL_YN IS NULL OR C.DEL_YN = 'N')
        WHERE (A.DEL_YN IS NULL OR A.DEL_YN = 'N')
            AND A.NOTICE_TYPE IN ('01', '05') /* NOTICE_TYPE 01: 전체, 05: CP */
            AND A.NOTICE_PREF_RANK != '0' /* 1: 가장 중요, 2: 중요, 3: 보통, 0: 노출 우선 순위 없음 */
        ORDER BY A.NOTICE_PREF_RANK
    </select>

    <!-- 쿼리명 : 중요 공지사항 리스트 합계
                설명 : 중요 공지사항 리스트(노출 우선 순위 존재) 합계를 조회하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2018.05.21 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2018.05.09
     -->
    <select id="noticeImptListTotalCount" parameterType="NoticeVO" resultType="Integer">
        SELECT COUNT(*)
        FROM IM_NOTICE A
            INNER JOIN IM_MEMBER B ON A.CRETR_ID = B.MBR_ID
            INNER JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'POST_SE' AND A.NOTICE_TYPE = C.COMN_CD_VALUE AND (C.DEL_YN IS NULL OR C.DEL_YN = 'N')
        WHERE (A.DEL_YN IS NULL OR A.DEL_YN = 'N')
            AND A.NOTICE_TYPE IN ('01', '05') /* NOTICE_TYPE 01: 전체, 05: CP */
            AND A.NOTICE_PREF_RANK != '0' /* 1: 가장 중요, 2: 중요, 3: 보통, 0: 노출 우선 순위 없음 */
    </select>

    <!-- 쿼리명 : 공지사항 리스트  
                설명 : 공지사항 리스트 정보를 조회하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2018.05.09 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2018.05.09
     -->
    <select id="noticeList" parameterType="NoticeVO" resultType="NoticeVO">
        SELECT *
        FROM (
            SELECT ROW_NUMBER() OVER () as NUM,
                   NOTICE_SEQ,
                   NOTICE_TYPE,
                   NOTICE_TITLE,
                   RETV_NUM,
                   CRETR_NM,
                   FILE_COUNT,
                   TO_CHAR(REG_DT, 'YYYY-MM-DD HH24:MI') AS REG_DT,
                   NOTICE_PREF_RANK
            FROM (
                SELECT NOTICE_SEQ,
                       NOTICE_TYPE,
                       NOTICE_TITLE,
                       RETV_NUM,
                       CRETR_NM,
                       FILE_COUNT,
                       REG_DT,
                       NOTICE_PREF_RANK
                FROM (
                    SELECT A.NOTICE_SEQ,
                           C.COMN_CD_NM AS NOTICE_TYPE,
                           A.NOTICE_TITLE,
                           A.RETV_NUM,
                           COALESCE(B.MBR_NM, A.CRETR_ID) AS CRETR_NM,
                           A.CRET_DT AS REG_DT,
                           (SELECT COUNT(FILE_SEQ) FROM IM_FILE WHERE FILE_CONTS_SEQ = A.NOTICE_SEQ AND FILE_SE = 'NOTICE') AS FILE_COUNT,
                           A.NOTICE_PREF_RANK
                    FROM IM_NOTICE A
                        INNER JOIN IM_MEMBER B ON A.CRETR_ID = B.MBR_ID
                        INNER JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'POST_SE' AND A.NOTICE_TYPE = C.COMN_CD_VALUE AND (C.DEL_YN IS NULL OR C.DEL_YN = 'N')
                    WHERE (A.DEL_YN IS NULL OR A.DEL_YN = 'N')
                        AND A.NOTICE_TYPE IN ('01', '05') /* NOTICE_TYPE 01: 전체, 05: CP */
                        AND A.NOTICE_PREF_RANK = '0' /* 3: 가장 중요, 2: 중요, 01: 보통, 0: 노출 우선 순위 없음 */
                ) N1
                WHERE 1=1
                    <if test="searchConfirm == 'true'">
                        AND
                        <choose>
                            <when test="searchTarget == 'NOTICE_TITLE'">
                                <![CDATA[UPPER(NOTICE_TITLE) like CONCAT('%' || UPPER(#{searchKeyword}),'%')]]>
                            </when>
                            <otherwise>
                                <![CDATA[UPPER(CRETR_NM) like CONCAT('%' || UPPER(#{searchKeyword}),'%')]]>
                            </otherwise>
                        </choose>
                    </if>
                <choose>
                    <when test="sidx == 'NOTICE_TITLE'">
                        ORDER BY NOTICE_TITLE <if test="sord == 'desc'">DESC</if>, REG_DT DESC
                    </when>
                    <when test="sidx == 'CRETR_NM'">
                        ORDER BY CRETR_NM <if test="sord == 'desc'">DESC</if>, REG_DT DESC
                    </when>
                    <when test="sidx == 'NOTICE_TYPE'">
                        ORDER BY NOTICE_TYPE <if test="sord == 'desc'">DESC</if>, REG_DT DESC
                    </when>
                    <when test="sidx == 'RETV_NUM'">
                        ORDER BY RETV_NUM <if test="sord == 'desc'">DESC</if>, REG_DT DESC
                    </when>
                    <when test="sidx == 'FILE'">
                        ORDER BY FILE_COUNT <if test="sord == 'desc'">DESC</if>, REG_DT DESC
                    </when>
                    <otherwise>
                        ORDER BY REG_DT <if test="sord == 'desc'">DESC</if>
                    </otherwise>
                </choose>
            ) NT1
        ) NTT1
        WHERE NUM BETWEEN #{offset} and #{limit}
    </select>

    <!-- 쿼리명 : 공지사항 리스트 합계 조회
                설명 : 공지사항 리스트 합계 정보를 조회하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2018.05.09 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2018.05.09
     -->
    <select id="noticeListTotalCount" parameterType="NoticeVO"  resultType="Integer">
        SELECT COUNT(*)
        FROM (
            SELECT NOTICE_SEQ,
                   NOTICE_TYPE,
                   NOTICE_TITLE,
                   RETV_NUM,
                   CRETR_NM,
                   FILE_COUNT,
                   TO_CHAR(REG_DT, 'YYYY-MM-DD') AS REG_DT,
                   NOTICE_PREF_RANK
            FROM (
                SELECT NOTICE_SEQ,
                       NOTICE_TYPE,
                       NOTICE_TITLE,
                       RETV_NUM,
                       CRETR_NM,
                       FILE_COUNT,
                       REG_DT,
                       NOTICE_PREF_RANK
                FROM (
                    SELECT A.NOTICE_SEQ,
                           C.COMN_CD_NM AS NOTICE_TYPE,
                           A.NOTICE_TITLE,
                           A.RETV_NUM,
                           COALESCE(B.MBR_NM, A.CRETR_ID) AS CRETR_NM,
                           A.CRET_DT AS REG_DT,
                           (SELECT COUNT(FILE_SEQ) FROM IM_FILE WHERE FILE_CONTS_SEQ = A.NOTICE_SEQ AND FILE_SE = 'NOTICE') AS FILE_COUNT,
                           A.NOTICE_PREF_RANK
                    FROM IM_NOTICE A
                        INNER JOIN IM_MEMBER B ON A.CRETR_ID = B.MBR_ID
                        INNER JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'POST_SE' AND A.NOTICE_TYPE = C.COMN_CD_VALUE AND (C.DEL_YN IS NULL OR C.DEL_YN = 'N')
                    WHERE (A.DEL_YN IS NULL OR A.DEL_YN = 'N')
                        AND A.NOTICE_TYPE IN ('01', '05') /* NOTICE_TYPE 01: 전체, 05: CP */
                        AND A.NOTICE_PREF_RANK = '0' /* 3: 가장 중요, 2: 중요, 1: 보통, 0: 노출 우선 순위 없음 */
                ) N1
                WHERE 1=1
                    <if test="searchConfirm == 'true'">
                        AND
                        <choose>
                            <when test="searchTarget == 'NOTICE_TITLE'">
                                <![CDATA[UPPER(NOTICE_TITLE) like CONCAT('%' || UPPER(#{searchKeyword}),'%')]]>
                            </when>
                            <otherwise>
                                <![CDATA[UPPER(CRETR_NM) like CONCAT('%' || UPPER(#{searchKeyword}),'%')]]>
                            </otherwise>
                        </choose>
                    </if>
            ) NT1
        ) NTT1
    </select>

    <!-- 쿼리명 : 공지사항 상세 정보 조회
                설명 : 공지사항 상세 정보를 조회하기 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2018.05.09 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2018.05.09
     -->
    <select id="noticeDetail" parameterType="NoticeVO" resultType="NoticeVO">
        SELECT A.NOTICE_SEQ,
               A.NOTICE_TYPE AS NOTICE_TYPE_CD,
               C.COMN_CD_NM AS NOTICE_TYPE,
               COALESCE(B.MBR_NM, A.CRETR_ID) AS CRETR_NM,
               A.NOTICE_TITLE,
               A.NOTICE_SBST,
               TO_CHAR(A.CRET_DT, 'YYYY-MM-DD HH24:MI') AS REG_DT
        FROM IM_NOTICE A
            INNER JOIN IM_MEMBER B ON A.CRETR_ID = B.MBR_ID
            INNER JOIN IM_COMMON_CODE C ON C.COMN_CD_CTG = 'POST_SE' AND C.COMN_CD_VALUE = A.NOTICE_TYPE AND (C.DEL_YN IS NULL OR C.DEL_YN = 'N')
        WHERE A.NOTICE_SEQ = #{noticeSeq}
            AND (A.DEL_YN IS NULL OR A.DEL_YN = 'N')
    </select>

    <!-- 쿼리명 : 공지사항 조회 수 업데이트
                설명 : 공지사항 상세 정보 확인 시 조회 수 업데이트를 위한 쿼리
                수정일 수정자 수정내용
         ──── ──── ───────
         2018.05.09 허용수 최초생성
                작성자: 허용수
                최초작성일 : 2018.05.09
     -->
    <update id="updateNoticeRetvNum" parameterType="NoticeVO">
        UPDATE IM_NOTICE
        SET RETV_NUM = RETV_NUM + 1
        WHERE NOTICE_SEQ = #{noticeSeq}
    </update>
</mapper>