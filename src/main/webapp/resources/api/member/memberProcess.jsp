<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<json:object>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultCode eq '1000' }">
        <c:if test="${!empty result}">
            <json:object name="result">
                <json:property name="mbrSeq" value="${ result.mbrSeq }" />
                <json:property name="mbrId" value="${ result.mbrId }" />
                <json:property name="mbrNm" value="${ result.mbrNm }" />
                <json:property name="mbrSe" value="${ result.mbrSe }" />
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'memberTokenInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="id" value="${ id }" />
                <json:property name="token" value="${ token }" />
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'findInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="findInfo">
                    <json:property name="id" value="${ item }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>
    <c:if test="${ resultType eq 'memberInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="mbrSeq" value="${ item.mbrSeq }" />
                    <json:property name="mbrId" value="${ item.mbrId }" />
                    <json:property name="mbrNm" value="${ item.mbrNm }" />
                    <json:property name="mbrTelNo" value="${ item.mbrTelNo }" />
                    <c:set var="tel" value="${fn:split(item.mbrMphonNo,';')}" />
                    <json:array name="mbrMphonList">
                        <c:forEach var="telNum" items="${tel}" varStatus="g">
                            <json:object>
                                <json:property name="mbrMphonNo" value="${ telNum }" />
                            </json:object>
                        </c:forEach>
                    </json:array>
                    <json:property name="mbrEmail" value="${ item.mbrEmail }" />
                    <json:property name="mbrSe" value="${ item.mbrSe }" />
                    <json:property name="mbrSeNm" value="${ item.mbrSeNm }" />
                    <json:property name="mbrSttus" value="${ item.mbrSttus }" />
                    <json:property name="mbrSttusNm" value="${ item.mbrSttusNm }" />
                    <c:if test="${ item.svcSeq != 0 }">
                        <json:property name="svcSeq" value="${ item.svcSeq }" />
                        <json:property name="svcNm" value="${ item.svcNm }" />
                    </c:if>
                    <c:if test="${ item.storSeq != 0 }">
                        <json:property name="storSeq" value="${ item.storSeq }" />
                        <json:property name="storNm" value="${ item.storNm }" />
                    </c:if>
                    <c:if test="${ item.cpSeq != 0 }">
                        <json:property name="cpSeq" value="${ item.cpSeq }" />
                        <json:property name="cpNm" value="${ item.cpNm }" />
                        <json:property name="docSubmYn" value="${ item.docSubmYn }" />
                    </c:if>
                    <json:property name="cretDt" value="${ item.cretDt }" />
                    <json:property name="cretrId" value="${ item.cretrId }" />
                    <json:property name="amdDt" value="${ item.amdDt }" />
                    <json:property name="amdrId" value="${ item.amdrId }" />
                    <json:property name="apvDt" value="${ item.apvDt }" />
                    <json:property name="apvrId" value="${ item.apvrId }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'cofirmPasswd' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="mbrId" value="${ item.mbrId }" />
                    <json:property name="mbrPwd" value="${ item.mbrPwd }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>


    <c:if test="${ resultType eq 'memberUpdateHistoryList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="memberUpdateHistoryList"
                    items="${item}" var="i">
                    <json:object>
                        <json:property name="num"
                            value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="seq" value="${ i.seq }" />
                        <json:property name="memberSeq"
                            value="${ i.memberSeq }" />
                        <c:choose>
                            <c:when
                                test="${ i.columnName eq 'MB_NAME' }">
                                <json:property name="columnName"
                                    value="멤버명" />
                            </c:when>
                            <c:when test="${ i.columnName eq 'MB_SEC' }">
                                <json:property name="columnName"
                                    value="멤버 구분" />
                            </c:when>
                            <c:when
                                test="${ i.columnName eq 'MB_OFFICE' }">
                                <json:property name="columnName"
                                    value="지점명" />
                            </c:when>
                            <c:when
                                test="${ i.columnName eq 'MB_USAGE_YN' }">
                                <json:property name="columnName"
                                    value="사용 상태" />
                            </c:when>
                            <c:when
                                test="${ i.columnName eq 'MB_PHONE' }">
                                <json:property name="columnName"
                                    value="전화번호" />
                            </c:when>
                            <c:when
                                test="${ i.columnName eq 'MB_MOBILE_PHONE' }">
                                <json:property name="columnName"
                                    value="핸드폰" />
                            </c:when>
                            <c:when
                                test="${ i.columnName eq 'MB_EMAIL' }">
                                <json:property name="columnName"
                                    value="이메일" />
                            </c:when>
                            <c:otherwise></c:otherwise>
                        </c:choose>
                        <json:property name="oldValue"
                            value="${ i.oldValue }" />
                        <json:property name="newValue"
                            value="${ i.newValue }" />
                        <json:property name="updateId"
                            value="${ i.updateId }" />
                        <json:property name="regDate"
                            value="${ i.regDate }" />
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage"
                    value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'posMemberInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="memberInfo">
                    <json:property name="seq" value="${ item.seq }" />
                    <json:property name="memberId"
                        value="${ item.memberId }" />
                    <json:property name="memberName"
                        value="${ item.memberName }" />
                    <c:choose>
                        <c:when
                            test="${ item.memberPhone1 != '' && item.memberPhone2 != '' && item.memberPhone3 != '' }">
                            <json:property name="memberPhone"
                                value="${ item.memberPhone1 }-${ item.memberPhone2 }-${ item.memberPhone3 }" />
                        </c:when>
                        <c:otherwise>
                            <json:property name="memberPhone" value="" />
                        </c:otherwise>
                    </c:choose>
                    <json:property name="mobilePhone"
                        value="${ item.mobilePhone1 }-${ item.mobilePhone2 }-${ item.mobilePhone3 }" />
                    <json:property name="memberEmail"
                        value="${ item.memberEmail1 }@${item.memberEmail2}" />
                    <json:property name="memberSec"
                        value="${ item.memberSecName }" />
                    <json:property name="memberOffice"
                        value="${ item.memberOffice }" />
                    <json:property name="storeCode"
                        value="${ item.storeCode }" />
                    <c:if test="${ item.memberSec != null }">
                        <json:property name="memberSecSeq"
                            value="${ item.memberSec }" />
                    </c:if>
                    <c:if test="${ clientSeq != null }">
                        <json:property name="clientSeq"
                            value="${ clientSeq }" />
                    </c:if>
                    <c:if test="${ item.memberOffice != null }">
                        <json:property name="officeSeq"
                            value="${ item.memberOfficeSeq }" />
                    </c:if>
                    <c:if test="${ token != null }">
                        <json:property name="token" value="${ token }" />
                    </c:if>
                    <json:property name="regDate"
                        value="${ item.regDate }" />
                </json:object>
            </json:object>
        </c:if>
    </c:if>
</json:object>