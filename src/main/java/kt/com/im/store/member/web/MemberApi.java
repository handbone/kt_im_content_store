/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.member.web;

import java.security.SecureRandom;
import java.util.Date;
import java.util.Random;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mashape.unirest.http.JsonNode;

import kt.com.im.store.common.util.AesAlgorithm;
import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ConfigProperty;
import kt.com.im.store.common.util.FormatCheckUtil;
import kt.com.im.store.common.util.OTP;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.common.util.SHA256;
import kt.com.im.store.common.util.SmsSender;
import kt.com.im.store.common.util.Validator;
import kt.com.im.store.member.service.MemberService;
import kt.com.im.store.member.vo.LoginVO;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.sms.service.SmsService;
import kt.com.im.store.sms.vo.SmsVO;
import kt.com.im.store.sms.web.SmsApi;

/**
 *
 * 회원 관리에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 8.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class MemberApi {

    public final static String TEMPORARY_ID = "tempLoginId";

    public final static String SHOULD_CHANGE_PWD = "shouldChangePassword";

    // 멤버 상태 코드
    public final static String MEMBER_STATUS_WAITING = "01";
    public final static String MEMBER_STATUS_ENABLE = "02";
    public final static String MEMBER_STATUS_DISABLE = "03";
    public final static String MEMBER_STATUS_BLOCKED = "04";
    
    // 멤버 권한 코드
    public final static String MEMBER_SECTION_MASTER = "01";
    public final static String MEMBER_SECTION_CMS = "02";
    public final static String MEMBER_SECTION_ACCEPTOR = "03";
    public final static String MEMBER_SECTION_SERVICE = "04";
    public final static String MEMBER_SECTION_STORE = "05";
    public final static String MEMBER_SECTION_CP = "06";

    private final static String viewName = "../resources/api/member/memberProcess";

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Resource(name = "memberTransactionManager")
    private DataSourceTransactionManager txManager;

    @Resource(name = "SmsService")
    private SmsService smsService;

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 로그인 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/login")
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // 파라미터 유효성 여부 확인
            String invalidParams = Validator.validate(request, "id;pwd");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            LoginVO vo = new LoginVO();
            vo.setMbrId(request.getParameter("id"));

            String memberPwd = this.makeEncryptPassword(request.getParameter("pwd"), memberService.getSalt(vo));
            vo.setMbrPwd(memberPwd);

            String clientIp = CommonFnc.getClientIp(request);
            vo.setIpadr(clientIp);

            String loginYn = "N";
            LoginVO result = memberService.getLoginInfo(vo);
            if (result == null) {
                // 로그인 실패 회수 증가
                memberService.increaseLoginFailCount(vo);

                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "아이디 비밀번호 오류");

                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }

            // 로그인 실패 회수 초기화
            memberService.resetLoginFailCount(vo);

            // 계정 비활성화 여부 확인 (대기:01, 활성:02, 비활성:03, 잠금:04)
            boolean isUseable = MEMBER_STATUS_ENABLE.equals(result.getMbrSttus()) ? true : false;
            if (!isUseable) {
                String messageId = "";
                String loginDesc = "";
                if (MEMBER_STATUS_WAITING.equals(result.getMbrSttus())) {
                    messageId = "fail.login.waiting";
                    loginDesc = "계정 대기 상태";
                } else if (MEMBER_STATUS_DISABLE.equals(result.getMbrSttus())) {
                    messageId = "fail.login";
                    loginDesc = "계정 비활성화 상태";
                } else if (MEMBER_STATUS_BLOCKED.equals(result.getMbrSttus())) {
                    messageId = "fail.login.alert.block";
                    loginDesc = "계정 잠금 상태";
                }

                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, loginDesc);

                rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
                rsModel.setResultMessage(messageId);
                return rsModel.getModelAndView();
            }

            if (MEMBER_SECTION_CP.equals(result.getMbrSe())) {
                // CP 계약기간 종료 여부 확인
                String cpExpireYn = result.getCpExpireYn();
                if ("Y".equals(cpExpireYn)) {
                    // 로그인 이력 추가
                    this.insertLoginHistory(vo, loginYn, "CP사 계약기간 종료");
                    rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                    rsModel.setResultMessage("fail.login.expire.cp");
                    return rsModel.getModelAndView();
                }

                // CP 활성화 여부 확인
                String cpUseYn = result.getCpUseYn();
                if (cpUseYn == null || cpUseYn.isEmpty() || cpUseYn.equals("N")) {
                    // 로그인 이력 추가
                    this.insertLoginHistory(vo, loginYn, "CP사 비활성화");
                    rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                    rsModel.setResultMessage("fail.login.unavailable.cp");
                    return rsModel.getModelAndView();
                }
                session.setAttribute("cpName", result.getCpNm());
            }

            // 로그인 이력 추가
            loginYn = "Y";
            this.insertLoginHistory(vo, loginYn, "성공");

            session.setAttribute(TEMPORARY_ID, result.getMbrId());

            session.setAttribute(SHOULD_CHANGE_PWD, memberService.shouldChangePassword(result));

            rsModel.setData("result", result);
            return rsModel.getModelAndView();
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 로그아웃 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/logout", method = RequestMethod.POST)
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        if (session.getAttribute(TEMPORARY_ID) != null) {
            session.removeAttribute(TEMPORARY_ID);
            if (session.getAttribute(SHOULD_CHANGE_PWD) != null) {
                session.removeAttribute(SHOULD_CHANGE_PWD);
            }
            return rsModel.getModelAndView();
        }

        // 로그아웃 이력 추가
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO != null) {
            LoginVO vo = new LoginVO();
            vo.setMbrId(userVO.getMbrId());
            vo.setIpadr(userVO.getIpadr());
            this.insertLoginHistory(vo, "", "로그아웃");
        }

        session.invalidate();
        return rsModel.getModelAndView();
    }

    /**
     * 로그인 추가 인증 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/login/auth")
    public ModelAndView auth(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return this.verifyOtpCode(request, rsModel, session);
        }

        if (method.equals("GET")) {
            return this.getOtpCode(request, rsModel, session);
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView verifyOtpCode(HttpServletRequest request, ResultModel rsModel, HttpSession session)
            throws Exception {
        String loginYn = "N";

        // 1차 로그인 시 세션에 회원 아이디를 저장, 해당 값이 없는 경우 추가 인증 API에 접근 방지
        String id = (String) session.getAttribute(TEMPORARY_ID);
        if (id == null || id.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("session is disconnected");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(id);

        String clientIp = CommonFnc.getClientIp(request);
        vo.setIpadr(clientIp);

        String validateParams = "otpCode";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            return rsModel.getModelAndView();
        }

        long otpCreateTime = memberService.getOtpCreateTime(vo);
        String secretKey = result.getMbrId() + result.getSalt();

        boolean didVerify = OTP.vertify(secretKey, otpCreateTime, request.getParameter("otpCode"));
        if (!didVerify) {
            // 로그인 이력 추가
            this.insertLoginHistory(vo, loginYn, "OTP 인증번호 오류");
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            return rsModel.getModelAndView();
        }

        if (MEMBER_SECTION_SERVICE.equals(result.getMbrSe())) {
            // 서비스 활성화 여부 확인
            String svcUseYn = result.getSvcUseYn();
            if (svcUseYn == null || svcUseYn.isEmpty() || svcUseYn.equals("N")) {
                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "서비스 비활성화");
                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                rsModel.setResultMessage("fail.login.unavailable.service");
                return rsModel.getModelAndView();
            }
            session.setAttribute("svcName", result.getSvcNm());
        } else if (MEMBER_SECTION_STORE.equals(result.getMbrSe())) {
            // 매장 활성화 여부 확인
            String storeUseYn = result.getStorUseYn();
            if (storeUseYn == null || storeUseYn.isEmpty() || storeUseYn.equals("N")) {
                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "매장 비활성화");
                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                rsModel.setResultMessage("fail.login.unavailable.store");
                return rsModel.getModelAndView();
            }
            session.setAttribute("storeName", result.getStorNm());
        } else if (MEMBER_SECTION_CP.equals(result.getMbrSe())) {
            // CP 활성화 여부 확인
            String cpUseYn = result.getCpUseYn();
            if (cpUseYn == null || cpUseYn.isEmpty() || cpUseYn.equals("N")) {
                // 로그인 이력 추가
                this.insertLoginHistory(vo, loginYn, "CP사 비활성화");
                rsModel.setResultCode(ResultModel.RESULT_CODE_CANCEL_REQUEST);
                rsModel.setResultMessage("fail.login.unavailable.cp");
                return rsModel.getModelAndView();
            }
            session.setAttribute("cpName", result.getCpNm());
        }

        // 로그인 이력 추가
        loginYn = "Y";
        this.insertLoginHistory(vo, loginYn, "성공");

        session.setAttribute("seq", String.valueOf(result.getMbrSeq()));
        session.setAttribute("id", result.getMbrId());
        // 멤버 이름 마스킹 처리
        session.setAttribute("name", CommonFnc.getNameMask(result.getMbrNm()));
        // 멤버 구분(01:MASTER, 02:CMS 관리자, 03:검수자, 04:서비스 관리자, 05:ㅣ매장 관리자, 06:CP사 관계자)
        session.setAttribute("memberSec", result.getMbrSe());
        // 멤버 레벨(8:최고 관리자, 5:일반 관리자, 2:사용자)
        session.setAttribute("level", String.valueOf(result.getMbrLevel()));
        session.setAttribute("svcSeq", String.valueOf(result.getSvcSeq()));
        session.setAttribute("storSeq", String.valueOf(result.getStorSeq()));
        session.setAttribute("cpSeq", String.valueOf(result.getCpSeq()));

        rsModel.setData("result", result);
        return rsModel.getModelAndView();
    }

    private ModelAndView getOtpCode(HttpServletRequest request, ResultModel rsModel, HttpSession session)
            throws Exception {
        rsModel.setViewName("jsonView");

        // 1차 로그인 시 세션에 회원 아이디를 저장, 해당 값이 없는 경우 추가 인증 API에 접근 방지
        String id = (String) session.getAttribute(TEMPORARY_ID);
        if (id == null || id.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("session is disconnected");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(id);

        LoginVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            return rsModel.getModelAndView();
        }

        long otpCreateTime = new Date().getTime();
        vo.setOtpCretTime(otpCreateTime);
        memberService.updateOtpCreateTime(vo);

        String secretKey = result.getMbrId() + result.getSalt();
        String otpCode = OTP.create(secretKey, otpCreateTime);

        // TODO : jbwook 운영 서버 반영 시 usesOtpSms 변수 Y로 변경 필요
        boolean usesOtpSms = "Y".equalsIgnoreCase(configProperty.getProperty("use.otp.sms"));
        if (usesOtpSms) {
            SmsVO smsVo = new SmsVO();
            String[] phonList = result.getMbrMphonNo().split(";");
            smsVo.setConnectUrl(configProperty.getProperty("sms.connect.url"));
            smsVo.setDestName(result.getMbrNm());
            smsVo.setMsgBody("[" + SmsApi.MSG_TITLE + " STORE]\r\n본인인증번호는[" + otpCode + "]입니다. 정확히 입력해주세요.");

            for (int i = 0; i < phonList.length; i++) {
                SmsVO smsVoTemp = new SmsVO();
                smsVoTemp.setConnectUrl(smsVo.getConnectUrl());
                smsVoTemp.setDestName(smsVo.getDestName());
                smsVoTemp.setMsgBody(smsVo.getMsgBody());

                smsVoTemp.setDestPhone(phonList[i].replaceAll("-", "").trim());
                JsonNode sendResult = SmsSender.send(smsVoTemp);
                String resultCode = (String) sendResult.getObject().get("result_code");

                // SMS 발송 이력은 2차 개발에 포함되어 있어 신규 기능 Flag Enable시 동작되도록 처리
                boolean usesAutoSend = "Y".equalsIgnoreCase(configProperty.getProperty("use.auto.send.sms"));
                if (usesAutoSend) {
                    smsVoTemp.setMsgBody("[" + SmsApi.MSG_TITLE + " STORE]\r\n본인인증번호는[" + otpCode.substring(0, 3)
                            + "***]입니다. 정확히 입력해주세요.");
                    smsVoTemp.setSendDesc("[OTP] NO." + result.getMbrSeq());
                    smsVoTemp.setCretrId(result.getMbrId());
                    smsVoTemp.setMbrId(result.getMbrId());

                    smsVoTemp.setResultCode(resultCode);
                    smsService.insertSentSmsInfo(smsVoTemp);

                }

                if (!"200".equals(resultCode)) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
                    return rsModel.getModelAndView();
                }
            }

        } else {
            rsModel.setData("otpCode", otpCode);
        }
        return rsModel.getModelAndView();
    }

    /**
     * 아이디 중복 확인 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/check")
    public ModelAndView checkId(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            return checkId(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView checkId(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String invalidParams = Validator.validate(request, "id");
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            rsModel.setResultMessage("invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        vo.setMbrId(request.getParameter("id"));

        MemberVO result = memberService.getMember(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
        }
        return rsModel.getModelAndView();
    }

    /**
     * 비밀번호 찾기 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/find")
    public ModelAndView find(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String invalidParams = Validator.validate(request, "type");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            String type = request.getParameter("type");
            if ("id".equals(type)) {
                return this.findId(request, rsModel);
            }
            if ("password".equals(type)) {
                return this.findPassword(request, rsModel);
            }

            if ("comfirmPassword".equals(type) || "comfirmEdit".equals(type)) {
                return this.comfirmPassword(request, rsModel, session);
            }

        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView findId(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "name;email";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrNm(request.getParameter("name"));
        vo.setMbrEmail(request.getParameter("email"));

        try {
            MemberVO result = memberService.getLoginInfo(vo);
            if (result == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage("fail.login.findId");
                return rsModel.getModelAndView();
            }

            // 아이디 뒷자리 별표 처리
            int asteriskLength = 3;
            if (result.getMbrId().length() <= asteriskLength) {
                asteriskLength = result.getMbrId().length() - 1;
            }

            String id = result.getMbrId().substring(0, result.getMbrId().length() - asteriskLength);
            for (int i = 0; i < asteriskLength; i++) {
                id += "*";
            }

            rsModel.setData("item", id);
            rsModel.setResultType("findInfo");

        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findId.serverError");
        }
        return rsModel.getModelAndView();
    }

    private ModelAndView findPassword(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "id;email";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(request.getParameter("id"));

        MemberVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("fail.login.alert.id");
            return rsModel.getModelAndView();
        }

        String mbrEmail = request.getParameter("email");
        if (!mbrEmail.equals(result.getMbrEmail())) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("fail.login.alert.email");
            return rsModel.getModelAndView();
        }
        vo.setMbrEmail(mbrEmail);

        boolean isBlocked = MEMBER_STATUS_BLOCKED.equals(result.getMbrSttus());
        if (isBlocked) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
            rsModel.setResultMessage("fail.login.alert.block");
            return rsModel.getModelAndView();
        }

        String changedPassword = this.makeInitPassword();
        vo.setTempPwd(changedPassword);
        vo.setMbrPwd(this.makeEncryptPassword(changedPassword, result.getSalt()));
        vo.setAmdrId(request.getParameter("id"));

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.initPassword(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_PROCESS);
            }
            txManager.commit(status);
        } catch (MessagingException e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findPassword.sendMail");
        } catch(Exception e) {

            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, "fail.login.findPassword.serverError");
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView comfirmPassword(HttpServletRequest request, ResultModel rsModel, HttpSession session)
            throws Exception {
        String validateParams = "pwd";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        vo.setMbrId(userVO.getMbrId());

        String memberPwd;
        if (request.getParameter("type").equals("comfirmPassword")) {
            memberPwd = this.makeEncryptPassword(request.getParameter("pwd"), memberService.getSalt(vo));
        } else {
            memberPwd = request.getParameter("pwd");
        }
        vo.setMbrPwd(memberPwd);

        MemberVO result = memberService.getLoginInfo(vo);
        if (result == null) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
            rsModel.setResultMessage("fail.login.alert.id");
            return rsModel.getModelAndView();
        }

        boolean isBlocked = MEMBER_STATUS_BLOCKED.equals(result.getMbrSttus());
        if (isBlocked) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_PARTIAL_SUCCESS);
            rsModel.setResultMessage("fail.login.alert.block");
            return rsModel.getModelAndView();
        }
        rsModel.setResultType("cofirmPasswd");
        rsModel.setData("item", result);
        session.setAttribute("comfirmPwd", true);
        return rsModel.getModelAndView();
    }

    /**
     * 회원 토큰 정보 조회 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/token")
    public ModelAndView token(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return getMemberToken(request, rsModel);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView getMemberToken(HttpServletRequest request, ResultModel rsModel) throws Exception {
        String validateParams = "id;pwd";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        LoginVO vo = new LoginVO();
        vo.setMbrId(request.getParameter("id"));

        String memberPwd = this.makeEncryptPassword(request.getParameter("pwd"), memberService.getSalt(vo));
        vo.setMbrPwd(memberPwd);
        LoginVO memberResult = memberService.getLoginInfo(vo);
        if (memberResult == null) {
            rsModel.setNoData();
            rsModel.setResultMessage("Please check your id and password");
            return rsModel.getModelAndView();
        }

        if (!MemberApi.MEMBER_STATUS_ENABLE.equals(memberResult.getMbrSttus())) {
            rsModel.setNoData();
            rsModel.setResultMessage(ResultModel.MESSAGE_UNAVAILABLE_MEMBER);
            return rsModel.getModelAndView();
        }

        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(memberResult.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(memberResult.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(memberResult.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String token = memberResult.getMbrTokn();
        if (token == null) {
            token = this.generateToken(memberResult.getMbrId());
        }

        vo.setMbrTokn(token);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            // 토큰 유효기간 만료된 토큰에 대해서 갱신 처리
            memberService.updateMemberToken(vo);

            rsModel.setResultType("memberTokenInfo");
            rsModel.setData("id", request.getParameter("id"));
            rsModel.setData("token", token);
            txManager.commit(status);
        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_PROCESS);
            txManager.rollback(status);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 회원 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member")
    public ModelAndView memberProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            return addMember(request, rsModel, userVO);
        }

        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        if (method.equals("GET")) {
            return getMember(request, rsModel, userVO);
        }

        if (method.equals("PUT")) {
            return updateMember(request, rsModel, userVO);
        }

        if (method.equals("DELETE")) {
            return deleteMember(request, rsModel, userVO);
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView addMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "id:{" + Validator.ID + "};" + "pwd1:{" + Validator.PWD + "};"
                + "pwd2:{" + Validator.PWD + "};" + "name:{" + Validator.NAME + "};" + "tel:{" + Validator.TEL_NO + "};"
                + "phone:{" + Validator.PHONE_NO + "};" + "email:{" + Validator.EMAIL + "};" + "cpSeq:{" + Validator.NUMBER + "}";

        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (FormatCheckUtil.containsIdInPassword(request.getParameter("id"), request.getParameter("pwd1"))) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(pwd1)");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        vo.setMbrSe(MEMBER_SECTION_CP);

        // 아이디 중복 여부 체크
        MemberVO searchVO = new MemberVO();
        searchVO.setMbrId(request.getParameter("id"));
        MemberVO result = memberService.getMember(searchVO);
        boolean isExisted = (result != null);
        if (isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage("login.unavailableId.msg");
            return rsModel.getModelAndView();
        }

        String pwd1 = request.getParameter("pwd1");
        String pwd2 = request.getParameter("pwd2");
        if (!pwd1.equals(pwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        // 이메일 중복 여부 체크
        vo.setMbrEmail(request.getParameter("email"));
        result = memberService.getMember(vo);
        isExisted = (result != null);
        if (isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            rsModel.setResultMessage("login.unavailableEmail.msg");
            return rsModel.getModelAndView();
        }

        vo.setMbrId(request.getParameter("id"));
        vo.setMbrNm(request.getParameter("name"));
        vo.setMbrTelNo(request.getParameter("tel"));
        vo.setMbrMphonNo(request.getParameter("phone"));

        String salt = this.getGeneratedSalt();
        vo.setSalt(salt);
        vo.setMbrPwd(this.makeEncryptPassword(pwd1, salt));

        String cretrId = request.getParameter("id");
        vo.setCretrId(cretrId);
        vo.setMbrLevel(2);
        vo.setMbrSttus(MEMBER_STATUS_WAITING);
        vo.setCpSeq(Integer.parseInt(request.getParameter("cpSeq")));

        try {
            int resultCode = memberService.addMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            }
        } catch (Exception e) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView getMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt(request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean prevented = vo.getMbrSeq() != userVO.getMbrSeq();
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        MemberVO result = memberService.getMember(vo);
        if (result == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }
        rsModel.setData("item", result);
        rsModel.setResultType("memberInfo");
        return rsModel.getModelAndView();
    }

    private ModelAndView updateMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt(request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean prevented = vo.getMbrSeq() != userVO.getMbrSeq();
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        validateParams = "tel:{" + Validator.TEL_NO + "};" + "phone:{" + Validator.PHONE_NO + "};" + "email:{" + Validator.EMAIL + "}";
        invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        vo = memberService.getMember(vo);
        if (vo == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_NO_DATA);
            return rsModel.getModelAndView();
        }

        // 이메일 중복 여부 체크
        MemberVO searchVO = new MemberVO();
        searchVO.setMbrEmail(request.getParameter("email"));
        MemberVO result = memberService.getMember(searchVO);
        boolean isExisted = (result != null) && !vo.getMbrId().equals(result.getMbrId());
        if (isExisted) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_EXIST_RESULT);
            return rsModel.getModelAndView();
        }

        vo.setMbrTelNo(request.getParameter("tel"));
        vo.setMbrMphonNo(request.getParameter("phone"));
        vo.setMbrEmail(request.getParameter("email"));
        vo.setAmdrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updateMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView deleteMember(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt((String)request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean prevented = vo.getMbrSeq() != userVO.getMbrSeq();
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        vo.setAmdrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.deleteMember(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_DELETE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_DELETE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 비밀번호 변경 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/member/password")
    public ModelAndView password(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null && !this.mustChangePassword(session)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            if (this.mustChangePassword(session)) {
                return updateOldestPassword(request, rsModel, session);
            } else {
                String type = request.getParameter("type");
                if ("delayToChangePassword".equals(type)) {
                    return this.delayToChangePassword(request, rsModel, userVO);
                }
                return updatePassword(request, rsModel, userVO);
            }

        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    private ModelAndView updatePassword(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        String validateParams = "seq:{" + Validator.NUMBER + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + " )");
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        int memberSeq = Integer.parseInt((String)request.getParameter("seq"));
        vo.setMbrSeq(memberSeq);

        boolean prevented = vo.getMbrSeq() != userVO.getMbrSeq();
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        validateParams = "oldPwd;" + "pwd1:{" + Validator.PWD + "};" + "pwd2:{" + Validator.PWD + "}";
        invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String salt = memberService.getSalt(vo);
        String oldPwd1 = this.makeEncryptPassword(request.getParameter("oldPwd"), salt);
        String oldPwd2 = memberService.getPassword(vo);
        if (!oldPwd1.equals(oldPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.member.notMatched.oldPassword");
            return rsModel.getModelAndView();
        }

        String newPwd1 = request.getParameter("pwd1");
        String newPwd2 = request.getParameter("pwd2");
        if (!newPwd1.equals(newPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.member.notMatched.newPassword");
            return rsModel.getModelAndView();
        }

        newPwd1 = this.makeEncryptPassword(newPwd1, salt);
        boolean notChanged = (oldPwd2.equals(newPwd1));
        if (notChanged) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.member.matched.oldPassword");
            return rsModel.getModelAndView();
        }

        MemberVO memberVO = memberService.getMember(vo);
        if (memberVO != null) {
            if (FormatCheckUtil.containsIdInPassword(memberVO.getMbrId(), request.getParameter("pwd1"))) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(pwd1)");
                return rsModel.getModelAndView();
            }
        }

        vo.setMbrPwd(newPwd1);
        vo.setAmdrId(userVO.getMbrId());

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updatePassword(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView updateOldestPassword(HttpServletRequest request, ResultModel rsModel, HttpSession session)
            throws Exception {
        if (session.getAttribute(TEMPORARY_ID) == null || session.getAttribute(SHOULD_CHANGE_PWD) == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        String mbrId = (String) session.getAttribute(TEMPORARY_ID);
        vo.setMbrId(mbrId);

        String validateParams = "oldPwd;" + "pwd1:{" + Validator.PWD + "};" + "pwd2:{" + Validator.PWD + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        if (FormatCheckUtil.containsIdInPassword(mbrId, request.getParameter("pwd1"))) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(pwd1)");
            return rsModel.getModelAndView();
        }

        String salt = memberService.getSalt(vo);
        String oldPwd1 = makeEncryptPassword(request.getParameter("oldPwd"), salt);
        String oldPwd2 = memberService.getPassword(vo);
        if (!oldPwd1.equals(oldPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.login.change.notMatched.oldPassword");
            return rsModel.getModelAndView();
        }

        String newPwd1 = request.getParameter("pwd1");
        String newPwd2 = request.getParameter("pwd2");
        if (!newPwd1.equals(newPwd2)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.login.change.notMatched.newPassword");
            return rsModel.getModelAndView();
        }

        newPwd1 = makeEncryptPassword(newPwd1, salt);
        boolean notChanged = (oldPwd2.equals(newPwd1));
        if (notChanged) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "fail.login.change.matched.oldPassword");
            return rsModel.getModelAndView();
        }

        vo.setMbrPwd(newPwd1);
        vo.setAmdrId(mbrId);

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.updatePassword(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_UPDATE);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_UPDATE);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    private ModelAndView delayToChangePassword(HttpServletRequest request, ResultModel rsModel, MemberVO userVO)
            throws Exception {
        boolean prevented = !MEMBER_SECTION_CP.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        MemberVO vo = new MemberVO();
        vo.setMbrSeq(userVO.getMbrSeq());
        vo.setMbrId(userVO.getMbrId());

        if (!memberService.shouldChangePassword(vo)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST);
            return rsModel.getModelAndView();
        }

        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);

        TransactionStatus status = txManager.getTransaction(def);
        try {
            int resultCode = memberService.insertDelayToChangePassword(vo);
            if (resultCode != 1) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, ResultModel.MESSAGE_FAILED_INSERT);
            }
            txManager.commit(status);
        } catch (Exception e) {
            txManager.rollback(status);
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR, ResultModel.MESSAGE_FAILED_INSERT);
        } finally {
            if (!status.isCompleted()) {
                txManager.rollback(status);
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 임시 비밀번호 생성
     *
     * @param String salt(고유값)
     * @return String
     * @exception Exception
     */
    private String makeInitPassword() throws Exception {
        Random rnd = new Random();
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < 15; i++) {
            if (rnd.nextBoolean()) {
                buf.append((char)((int)(rnd.nextInt(26))+97));
            } else {
                buf.append(rnd.nextInt(10));
            }
        }
        return buf.toString();
    }

    /**
     * 비밀번호 문자열 암호화
     * 
     * @param String plainPwd(암호화할 문자열), salt(고유값)
     * @return String
     * @exception Exception
     */
    private String makeEncryptPassword(String plainPwd, String salt) throws Exception {
        if (plainPwd == null || plainPwd.isEmpty())
            return plainPwd;

        String combinedPwd = this.getCombinedPassword(plainPwd, salt);

        byte[] pszMessage = combinedPwd.getBytes();
        int uPlainTextLen = pszMessage.length;
        byte[] pszDigest = new byte[32];

        SHA256.SHA256_Encrpyt(pszMessage, uPlainTextLen, pszDigest);

        return this.bytesToHex(pszDigest);
    }

    /**
     * 비밀번호 암호화 대상 문자열 생성
     *
     * @param String text(암호화할 문자열), salt(고유값)
     * @return String
     * @exception Exception
     */
    private String getCombinedPassword(String plainPwd, String salt) throws Exception {
        if (plainPwd == null || plainPwd.isEmpty() || salt == null || salt.isEmpty())
            return plainPwd;

        StringBuilder builder = new StringBuilder();
        builder.append(salt);
        builder.append(plainPwd);
        builder.append(salt);

        return builder.toString();
    }

    /**
     * 비밀번호 고유 문자열 생성
     *
     * @param
     * @return String salt(고유값)
     * @exception Exception
     */
    private String getGeneratedSalt() throws Exception {
        byte[] saltBytes = new byte[16];
        try {
            SecureRandom secureRandom = SecureRandom.getInstance(("SHA1PRNG"));
            secureRandom.nextBytes(saltBytes);
        } catch (Exception e) {

        }

        return this.bytesToHex(saltBytes);
    }

    /**
     * byte를 hex로 변경 함수
     * 
     * @param byte[] bytes
     * @return String
     * @exception Exception
     */
    private String bytesToHex(byte[] bytes) {
        StringBuffer result = new StringBuffer();
        for (byte b : bytes) {
            result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

    /**
     * token 생성 함수
     * 
     * @param id
     * @return String
     * @exception Exception
     */
    private String generateToken(String id) throws Exception {
        return AesAlgorithm.encrypt(id + new Date().toString(), configProperty.getPropertyAfterDecrypt("aes.key"));
    }

    /**
     * 로그인 이력 생성
     * 
     * @param vo, loginYn, msg
     * @return void
     * @exception Exception
     */
    private void insertLoginHistory(LoginVO vo, String loginYn, String msg) throws Exception {
        vo.setLoginYn(loginYn);
        vo.setLoginDesc("[STORE] " + msg);
        // 로그인 이력 추가
        memberService.insertLoginHistory(vo);
    }

    private boolean mustChangePassword(HttpSession session) {
        if (session.getAttribute(TEMPORARY_ID) == null || session.getAttribute(SHOULD_CHANGE_PWD) == null) {
            return false;
        }

        return (boolean) session.getAttribute(MemberApi.SHOULD_CHANGE_PWD);
    }
}
