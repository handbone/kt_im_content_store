/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.contract.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ConfigProperty;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.content.service.ContentService;
import kt.com.im.store.content.vo.ContentVO;
import kt.com.im.store.contract.service.ContractService;
import kt.com.im.store.contract.vo.ContractVO;
import kt.com.im.store.cp.service.CpService;
import kt.com.im.store.cp.vo.CpVO;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.member.web.MemberApi;

/**
 *
 * 과금관리 관련 처리 API
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 10. 18.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class ContractApi {

    @Resource(name = "ContractService")
    private ContractService contractService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "CpService")
    private CpService cpService;

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 과금 관리 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contract")
    public ModelAndView contractProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contract/contractProcess");

        // Master 관리자, CMS 관리자, CP 관리자만 접근 가능
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        ContractVO item = new ContractVO();
        String method = CommonFnc.getMethod(request);

        if (method.equals("POST")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("PUT")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else if (method.equals("DELETE")) {
            rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
            rsModel.setResultMessage(ResultModel.MESSAGE_NOT_IMPLEMENTED);
            rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        } else { // GET
            int buyinSeq = CommonFnc.emptyCheckInt("buyinSeq", request);

            if (buyinSeq != 0) {
                item.setBuyinSeq(buyinSeq);
                ContractVO resultVO = contractService.contractDetail(item);

                if (resultVO == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 아이디, 수정자 아이디) */
                    resultVO.setCretrId(CommonFnc.getIdMask(resultVO.getCretrId()));
                    resultVO.setAmdrId(CommonFnc.getIdMask(resultVO.getAmdrId()));

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);

                    /*
                     * String[] contsSeqs = resultVO.getContsSeqs().split(",");
                     * List<ContentVO> resultContListItem = new ArrayList<ContentVO>();
                     * for (int i = 0; i < contsSeqs.length; i++) {
                     * ContentVO contVO = new ContentVO();
                     * contVO.setList(null);
                     * contVO.setDelYn("N");
                     * contVO.setCpSeq(0);
                     * contVO.setSvcSeq(0);
                     * contVO.setCntrctFnsDt("");
                     * contVO.setContsSeq(Integer.parseInt(contsSeqs[i]));
                     * contVO.setSttusVal("05");
                     * ContentVO resultContItem = contentService.contentInfo(contVO);
                     * if (resultContItem != null) {
                     * resultContListItem.add(resultContItem);
                     * }
                     * }
                     */

                    rsModel.setData("item", resultVO);
                    // rsModel.setData("cntItem", resultContListItem);
                    rsModel.setResultType("contractInfo");
                }
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (buyinSeq == 0 && CommonFnc.checkReqParameter(request, "buyinSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);
                String searchConfirm = request.getParameter("_search") == null ? "false"
                        : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                if (searchConfirm.equals("true")) {
                    String searchField = CommonFnc.emptyCheckString("searchField", request); // 검색 카테고리
                    String searchString = CommonFnc.emptyCheckString("searchString", request); // 검색어

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }

                int currentPage = request.getParameter("page") == null ? 1
                        : Integer.parseInt(request.getParameter("page")); // 현재 페이지
                int limit = request.getParameter("rows") == null ? 10 : Integer.parseInt(request.getParameter("rows")); // 검색에
                                                                                                                        // 출력될
                                                                                                                        // 개수
                String sidx = CommonFnc.emptyCheckString("sidx", request); // 정렬할 필드
                String sord = CommonFnc.emptyCheckString("sord", request); // 정렬 방법
                int page = (currentPage - 1) * limit + 1; // 페이지 & 출력개수 계산 변수 (페이지 개수)
                int pageEnd = (currentPage - 1) * limit + limit; // 페이지 & 출력개수 계산 변수 (마지막페이지값)

                // 정렬 칼럼에 따라 해당하는 테이블 지정 및 칼럼 지정
                if (!sidx.equals("BUYIN_SEQ") && !sidx.equals("BUYIN_CONT_NO") && !sidx.equals("CONTS_TITLE")
                        && !sidx.equals("BUYIN_CONT_TYPE") && !sidx.equals("REG_DT") && !sidx.equals("SET_VAL")
                        && !sidx.equals("TOT_PRICE") && !sidx.equals("USE_PRICE") && !sidx.equals("CRETR_NM")
                        && !sidx.equals("AMD_DT") && !sidx.equals("CONTS_COUNT")) {
                    sidx = "BUYIN_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);
                item.setCpSeq(cpSeq);

                List<ContractVO> resultList = contractService.contractList(item);

                if (resultList.isEmpty()) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디) */
                    for (int i = 0; i < resultList.size(); i++) {
                        resultList.get(i).setCretrNm(CommonFnc.getNameMask(resultList.get(i).getCretrNm()));
                        resultList.get(i).setCretrId(CommonFnc.getIdMask(resultList.get(i).getCretrId()));
                    }

                    // 가져온 리스트 총 개수
                    int totalCount = contractService.contractListTotalCount(item);

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultList);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", (totalCount - 1) / limit);
                    rsModel.setResultType("contractList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    /**
     * 검수 이력 관련 API
     *
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     *
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cpContStat")
    public ModelAndView cpContentStat(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/contract/contractProcess");

        String Url_Block = configProperty.getProperty("chain.url");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAdministrator = this.isAdministrator(userVO);
        if (userVO == null || !isAdministrator) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        ContractVO item = new ContractVO();
        ContentVO Citem = new ContentVO();
        if (request.getMethod().equals("POST")) {
            String Method = request.getParameter("_method");
            if (Method == null) {
                Method = "POST";
            }
            if (Method.equals("PUT")) { // put
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else if (Method.equals("DELETE")) { // delete
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            } else { // post
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_FUNCTION);
                rsModel.setStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
            }
        } else { // get
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
            String searchField = CommonFnc.emptyCheckString("searchField", request);
            String searchCp = "";
            if (cpSeq != 0) {
                searchCp = "&cpSeq=" + cpSeq;
            }

            // 검색 여부가 true일 경우
            ContentVO searchConts = new ContentVO();
            CpVO searchCps = new CpVO();

            item.setSearchConfirm(searchConfirm);
            searchConts.setSearchConfirm(searchConfirm);
            searchCps.setSearchConfirm(searchConfirm);

            String searchContsSeq = "";
            String searchCpSeq = "";

            if (searchConfirm.equals("true")) {
                searchConts.setSearchConfirm(searchConfirm);
                if (searchField.equals("CONTS_TITLE")) {
                    String searchString = CommonFnc.emptyCheckString("searchString", request);
                    searchConts.setkeyword(searchString);

                    List<ContentVO> cntInfoList = contentService.contentCdtNotInfoList(searchConts);
                    if (!cntInfoList.isEmpty()) {
                        for (int z = 0; z < cntInfoList.size(); z++) {
                            if (searchContsSeq != "") {
                                searchContsSeq += ",";
                            }
                            searchContsSeq += cntInfoList.get(z).getContsSeq();
                        }
                    }
                } else if (searchField.equals("CP_NM")) {
                    String searchString = CommonFnc.emptyCheckString("searchString", request);
                    searchCps.setKeyword(searchString);

                    List<CpVO> cpInfoList = cpService.getContentProviderList(searchCps);
                    if (!cpInfoList.isEmpty()) {
                        for (int z = 0; z < cpInfoList.size(); z++) {
                            if (searchCpSeq != "") {
                                searchCpSeq += ",";
                            }
                            searchCpSeq += cpInfoList.get(z).getCpSeq();
                        }
                    } else {
                        rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                        rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                        return rsModel.getModelAndView();

                    }
                }
            }

            /*
             * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
             */
            int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
            int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
            String sidx = CommonFnc.emptyCheckString("sidx", request);
            String type = CommonFnc.emptyCheckString("type", request);
            String year = CommonFnc.emptyCheckString("year", request);
            String month = CommonFnc.emptyCheckString("month", request);

            int page = (currentPage - 1) * limit + 1;
            int pageEnd = page + limit - 1;
            item.setSidx(sidx);
            item.setOffset(1);
            item.setLimit(10000);
            JSONParser jsonParser = new JSONParser();
            // search

            String getUrl = Url_Block + "/api/v1/statistics/cp?date=" + year + "-" + month + "&type=" + type;

            if (searchConfirm.equals("true")) {
                if (searchField.equals("CONTS_TITLE")) {
                    getUrl += "&contSeq=" + searchContsSeq;
                    if (searchCp.equals("")) {
                        searchCps = new CpVO();

                        List<CpVO> cpAllList = cpService.cpList(searchCps);

                        for (int j = 0; j < cpAllList.size(); j++) {
                            if (!searchCp.equals("")) {
                                searchCp += ",";
                            } else {
                                searchCp = "&cpSeq=";
                            }
                            searchCp += cpAllList.get(j).getCpSeq();
                        }

                    }
                } else if (searchField.equals("CP_NM")) {
                    searchCp = "&cpSeq=" + searchCpSeq;
                }
            }
            getUrl += searchCp;

            String jsonString = getJson(getUrl, "127.0.0.1", "", "GET");

            if (jsonString == null) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                return rsModel.getModelAndView();
            }
            JSONObject jsonObj = (JSONObject) jsonParser.parse(jsonString);
            JSONArray ja = (JSONArray) jsonObj.get("data");

            if (ja == null || ja.size() == 0) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                // 가공
                List<ContractVO> resultItem = new ArrayList<ContractVO>();
                for (int i = page - 1; i < pageEnd; i++) {
                    ContractVO citem = new ContractVO();
                    if (i >= ja.size() || page < 0) {
                        break;
                    }

                    JSONObject jsonObjchild = (JSONObject) ja.get(i);
                    JSONObject jsonVal = (JSONObject) jsonObjchild.get("value");
                    // 공통

                    citem.setCpSeq(Integer.parseInt((String) jsonVal.get("cpSeq")));
                    citem.setContsSeq(Integer.parseInt((String) jsonVal.get("contsSeq")));

                    ContentVO Contitem = new ContentVO();
                    Contitem.setContsSeq(citem.getContsSeq());
                    Contitem.setCpSeq(0);
                    Contitem.setContsCtgSeq(0);
                    Contitem.setSttusVal("");
                    Contitem.setGenreSeq(0);
                    Contitem.setList(null);
                    Contitem.setSvcSeq(0);

                    /** 해당 콘텐츠 상세정보 */
                    ContentVO cntInfo = contentService.contentCdtNotInfo(Contitem);

                    CpVO cpItem = new CpVO();
                    cpItem.setCpSeq(Integer.parseInt((String) jsonVal.get("cpSeq")));

                    /** 해당 CP 상세정보 */
                    CpVO cpInfo = cpService.getContentProvider(cpItem);

                    if (cntInfo != null) {
                        citem.setContsTitle(cntInfo.getContsTitle());
                    }

                    if (cpInfo != null) {
                        citem.setCpNm(cpInfo.getCpNm());
                    }

                    String contractType = (String) jsonVal.get("contractType");

                    citem.setContractType(contractType);

                    if (contractType.equals("N")) {
                        citem.setContract("");
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setPricePerSec(0);
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                        citem.setPrice(0);
                    } else if (contractType.equals("L")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setPricePerSec(0);
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                        citem.setPrice(0);

                    } else if (contractType.equals("T")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPricePerSec(((Long) jsonVal.get("pricePerSec")).intValue());
                        citem.setPrice(((Long) jsonVal.get("price")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setTotalPlayCnts(0);
                        citem.setRemainPlayCnts(0);
                    } else if (contractType.equals("C")) {
                        citem.setContract((String) jsonVal.get("contract"));
                        citem.setPlayTime(((Long) jsonVal.get("playTime")).intValue());
                        citem.setPricePerSec(((Long) jsonVal.get("pricePerSec")).intValue());
                        citem.setPlayCnts(((Long) jsonVal.get("playCnts")).intValue());
                        citem.setTotalPlayCnts(((Long) jsonVal.get("totalPlayCnts")).intValue());
                        citem.setRemainPlayCnts(((Long) jsonVal.get("remainPlayCnts")).intValue());
                        citem.setPrice(0);
                    }

                    resultItem.add(citem);
                }

                /* 가공 */

                int totalCount = ja.size();
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setData("item", resultItem);
                rsModel.setData("totalCount", totalCount);
                rsModel.setData("row", limit);
                rsModel.setData("currentPage", currentPage);
                rsModel.setData("totalPage", totalCount / limit);
                rsModel.setResultType("cpContStat");
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

    private boolean isAdministrator(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe);
    }

    /* 외부 URL 정보 GET 함수_ 현재 이용: [블록체인 데이터 전송] */
    public static String getJson(String serverUrl, String host, String jsonobject, String Method) throws Exception {

        StringBuilder sb = new StringBuilder();

        String http = serverUrl;

        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(http);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod(Method);
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(100000);
            urlConnection.setReadTimeout(10000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Host", host);
            urlConnection.connect();
            // You Can also Create JSONObject here
            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "utf-8"));
                StringBuilder result = new StringBuilder();
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                return sb.toString();

            } else {
                System.out.println(urlConnection.getResponseMessage());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }

}
