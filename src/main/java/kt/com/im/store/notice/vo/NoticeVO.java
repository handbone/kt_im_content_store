/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.notice.vo;

import java.io.Serializable;

/**
*
* NoticeVO
*
* @author A2TEC
* @since 2018
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 3.   A2TEC      최초생성
*
*
* </pre>
*/

public class NoticeVO  implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9113903728834778303L;

    /** 공지사항 번호 */
    private int noticeSeq;

    /** 공지사항 구분 */
    private String noticeType;

    /** 공지사항 구분 코드 */
    private String noticeTypeCd;

    /** 공지사항 제목 */
    private String noticeTitle;

    /** 공지사항 내용 (noticeSbst: notice Substance) */
    private String noticeSbst;

    /** 조회수 */
    private int retvNum;

    /** 첨부파일 수 */
    private int fileCount;

    /** 공지사항 게시 시작 일자 */
    private String stDt;

    /** 공지사항 게시 종료 일자 */
    private String fnsDt;

    /** 공지사항 등록 일시 */
    private String regDt;

    /** 등록자 이름 */
    private String cretrNm;

    /** 공지사항 팝업 노출 여부 */
    private String popupViewYn;

    /** 공지사항 삭제 여부 */
    private String delYn;

    /** offset */
    private int offset;

    /** limit */
    private int limit;

    /** 검색 구분 */
    private String searchTarget;

    /** 검색어 */
    private String searchKeyword;

    /** 정렬 컬럼 명 */
    private String sidx;

    /** 정렬 방법 */
    private String sord;

    /** 검색 여부 확인 */
    private String searchConfirm;

    /** 공지사항 노출 우선 순위 */
    private String noticePrefRank;

    public int getNoticeSeq() {
        return noticeSeq;
    }

    public void setNoticeSeq(int noticeSeq) {
        this.noticeSeq = noticeSeq;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public String getNoticeTypeCd() {
        return noticeTypeCd;
    }

    public void setNoticeTypeCd(String noticeTypeCd) {
        this.noticeTypeCd = noticeTypeCd;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeSbst() {
        return noticeSbst;
    }

    public void setNoticeSbst(String noticeSbst) {
        this.noticeSbst = noticeSbst;
    }

    public int getRetvNum() {
        return retvNum;
    }

    public void setRetvNum(int retvNum) {
        this.retvNum = retvNum;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int fileCount) {
        this.fileCount = fileCount;
    }

    public String getStDt() {
        return stDt;
    }

    public void setStDt(String stDt) {
        this.stDt = stDt;
    }

    public String getFnsDt() {
        return fnsDt;
    }

    public void setFnsDt(String fnsDt) {
        this.fnsDt = fnsDt;
    }

    public String getRegDt() {
        return regDt;
    }

    public void setRegDt(String regDt) {
        this.regDt = regDt;
    }

    public String getCretrNm() {
        return cretrNm;
    }

    public void setCretrNm(String cretrNm) {
        this.cretrNm = cretrNm;
    }

    public String getPopupViewYn() {
        return popupViewYn;
    }

    public void setPopupViewYn(String popupViewYn) {
        this.popupViewYn = popupViewYn;
    }

    public String getDelYn() {
        return delYn;
    }

    public void setDelYn(String delYn) {
        this.delYn = delYn;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getSearchTarget() {
        return searchTarget;
    }

    public void setSearchTarget(String searchTarget) {
        this.searchTarget = searchTarget;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getNoticePrefRank() {
        return noticePrefRank;
    }

    public void setNoticePrefRank(String noticePrefRank) {
        this.noticePrefRank = noticePrefRank;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
