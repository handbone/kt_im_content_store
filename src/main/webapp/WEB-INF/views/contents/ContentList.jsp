<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>
<script type="text/javascript"
    src="<%=request.getContextPath()%>/resources/js/views/contents/ContentList.js?version=1"></script>

<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" name="mbrSe" id="mbrSe" value="${memberSec}" />
<input type="hidden" name="cpSeq" id="cpSeq" value="${cpSeq}" />
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">


<!-- Contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- tabGrp E -->
        <div id="tabGrp">
            <div class="tab tabOn tabEnd"><spring:message code='contents.table.list' /></div>
        </div>
        <!-- tabGrp E -->

        <div class="rsvGrp rsvGrpPos" style="width: 360px">
            <div class="rsvSelect" style="float: left;">
                <span><spring:message code='title.cp' /></span> <span> <select id="cpList" onchange="keywordSearch()" style="min-width:136px"></select>
                </span>
            </div>

            <div class="rsvSelect">
                <span><spring:message code='table.progressStatus' /></span> <span> <select id="sttusList"
                    onchange="keywordSearch()"></select>
                </span>
            </div>
        </div>


        <!-- Table Area S -->
        <div class="content">
            <table id="jqgridData"></table>
            <div id="pageDiv"></div>
        </div>
        <!-- Table Area E -->

        <!-- Button Group S -->
        <div class="CenterGrp absol_btn">
            <!-- Search Group S -->
            <div class="searchBox">
                <div class="searchGrp">
                    <div class="selectBox">
                        <select class="selectBox" id="target"></select>
                    </div>
                    <div class="searchInput">
                        <input type="text" id="keyword">
                    </div>
                    <div class="searchBtn">
                        <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code='button.search' />">
                    </div>
                </div>
            </div>
            <!-- Search Group E -->
            <div class="cellTable">
                <input type="button" class="btnNormal btnWrite btnRight2 cursorPointer" onclick="pageMove('/contents/regist')" value="<spring:message code='button.create' />">
            </div>
        </div>
    </div>
    <!-- Button Group E -->
</div>
<!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>