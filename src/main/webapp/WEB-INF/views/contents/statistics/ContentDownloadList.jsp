<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%> 
<!--  Left Menu S -->
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<!-- Left Menu E -->
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/statistics/CommonStatistics.js?version=201806261512"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/statistics/ContentDownloadList.js?version=201806221512"></script>
<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="2">
<input type="hidden" id="cpSeq" value="${cpSeq}">
<input type="hidden" id="startDate" />
<input type="hidden" id="endDate" />

        <!-- right contents S -->
        <div id="contents">

        <div class="boxRound">
                <!-- Tab S --> 

                <div id="tabGrp" style="margin-bottom:10px;">
                    <div class="tab"><a id="btnContentUsage" href="javascript:;"><spring:message code='contents.usage.title'/></a></div>
                    <div class="tab tabOn tabEnd"><spring:message code='contents.download.title'/></div>
                </div>
                <!-- Tab E -->

                <!-- select S -->
                <div class="selectGrp" style="text-align:right; width:100%; margin-bottom:10px;">
                    <span id="cpListGrp"  style="display:none;">
                        <span><spring:message code='title.cp'/></span>
                        <span>
                            <select id="cpList" class="selectBox">
                            </select>
                        </span>
                    </span>
                    <span><spring:message code='contents.table.store'/></span>
                    <span>
                        <select id="storeList" class="selectBox">
                        </select>
                    </span>
                </div>
                <!-- select E -->

                <!-- Select Option S -->
                <div class="subLeftGrp">
                    <!-- Tab Button Grp S  -->
                    <div class="tabButtonGrp">
                        <span class="itemName" style="display:none;padding-right:0px !important"><spring:message code='common.period'/></span>
                        <span class="buttons" style="display:none;"><a id="btnPrveDate" href="javascript:;"><img src="<c:url value='/resources/image/icon_left.png'/>"></a></span>
                        <span id="datePicker" style="display:none;padding:0px !important;">
                            <input id="inputDatePicker1" type="text" class="inputBox2 lengthM txtCenter datepicker" readonly>
                            <span id="periodTxt">~&nbsp;</span><input id="inputDatePicker2" type="text" class="inputBox2 lengthS txtCenter datepicker" readonly>
                        </span>
                        <span class="buttons" style="display:none;"><a id="btnNextDate" href="javascript:;"><img src="<c:url value='/resources/image/icon_right.png'/>"></a></span>
                        <span id="dateSelector" style="display:none;padding:0px !important;">
                            <span class="inputArea">
                                <select id="yearSelector">
                                </select>&nbsp;<spring:message code='common.year'/>
                            </span>
                            <span class="inputArea">
                                <select id="monthSelector">
                                </select>&nbsp;<spring:message code='common.month'/>
                            </span>
                        </span>

                        <div class="tabButton" style="margin-left:15px">
                            <div id="btnCustom" class="tabBtn" style="cursor:pointer"><spring:message code='button.custom'/></div>
                            <div id="btnDay" class="tabBtn" style="cursor:pointer"><spring:message code='button.day'/></div>
                            <div id="btnWeek" class="tabBtn tabBtnOn" style="cursor:pointer"><spring:message code='button.week'/></div>
                            <div id="btnMonth" class="tabBtn tabBtnEnd" style="cursor:pointer"><spring:message code='button.month'/></div>
                        </div>
                    </div>
                    <!-- Tab Button Grp E  -->
                </div>
                <!-- Select Option E -->

                <!-- Button down S  -->
                <div class="btnGrp2">
                    <div class="btnNormal btnXls btnRight" style="cursor:pointer"><spring:message code='button.download.excel'/></div>
                </div>
                <!-- Button down E  -->

                <!-- Table Area S -->
                <div id="gridArea" class="">
                    <table id="jqgridData"></table>
                    <div id="pageDiv"></div>
                </div>
                <!-- Table Area E -->

                <!-- Button Group S -->
                <div class="btnGrp">
                    <!-- Search Group S -->
                    <div class="searchBox">
                        <div class="searchGrp">
                            <div class="selectBox">
                                <select id="target">
                                </select>
                            </div>
                            <div class="searchInput"><input id="keyword" type="text"></div>
                            <div class="searchBtn"><input type="button" class="btnNormal btnSearch" value="<spring:message code='button.search'/>"></div>
                        </div>
                    </div>
                    <!-- Search Group E -->
               </div>
               <!-- Button Group E -->

        </div>
    </div>
    <!-- Contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>