/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.qna.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.store.common.dao.mysqlAbstractMapper;
import kt.com.im.store.qna.vo.QnaVO;

/**
 *
 * 문의사항 관련 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("QnaDAO")
public class QnaDAOImpl extends mysqlAbstractMapper implements QnaDAO {

    /**
     * 문의사항 리스트 조회
     * @param QnaVO
     * @return 검색 조건에 부합하는 문의사항 리스트
     */
    @Override
    public List<QnaVO> qnaList(QnaVO vo) {
        return selectList("QnaDAO.qnaList", vo);
    }

    /**
     * 문의사항 리스트 합계 조회
     * @param QnaVO
     * @return 검색 조건에 부합하는 문의사항 리스트 합계
     */
    public int qnaListTotalCount(QnaVO vo){
         int res = (Integer) selectOne("QnaDAO.qnaListTotalCount", vo);
         return res;
    }

    /**
     * 문의사항 상세 정보 조회
     * @param QnaVO
     * @return 검색 조건에 부합하는 문의사항 상세 정보
     */
    @Override
    public QnaVO qnaDetail(QnaVO vo) {
        return selectOne("QnaDAO.qnaDetail", vo);
    }

    /**
     * 문의사항 조회수 업데이트
     * @param QnaVO
     * @return
     */
    @Override
    public void updateQnaRetvNum(QnaVO vo) {
        update("QnaDAO.updateQnaRetvNum", vo);
    }

    /**
     * 문의사항 등록
     * @param QnaVO
     * @return
     */
    @Override
    public void insertQna(QnaVO vo) {
        insert("QnaDAO.insertQna", vo);
    }

    /**
     * 첨부파일 정보
     * @param QnaVO
     * @return 첨부파일 정보
     */
    @Override
    public QnaVO fileInfo(QnaVO vo) {
        return selectOne("QnaDAO.fileInfo", vo);
    }

    /**
     * 첨부파일 삭제
     * @param QnaVO
     * @return 삭제 결과
     */
    @Override
    public int attachFileDelete(QnaVO vo) {
        return delete("QnaDAO.attachFileDelete", vo);
    }

    /**
     * 문의사항 수정
     * @param QnaVO
     * @return
     */
    @Override
    public int updateQna(QnaVO vo) {
        return update("QnaDAO.updateQna", vo);
    }

    /**
     * 문의사항 삭제 (DB 상에서 DEL_YN = 'N'로 변경)
     * @param QnaVO
     * @return
     */
    @Override
    public int deleteQna(QnaVO vo) {
        return update("QnaDAO.deleteQna", vo);
    }

}
