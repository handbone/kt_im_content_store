<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><spring:message code="login.title"/></title>
<script src="<%=request.getContextPath()%>/resources/js/message.js?category=login&time=<%=session.getLastAccessedTime()%>"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/apiFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/baseFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/commonFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/cookie.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/views/login.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery.blockUI.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/libs/sweetalert/sweetalert.min.js"></script>

<link href="<%=request.getContextPath()%>/resources/css/common.css?version=201805081745" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/resources/css/imcs.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/resources/css/custom.css?version=201805081746" rel="stylesheet" type="text/css">
<style>
#loading {
    width: 100%;
    height: 100%;
    top: 0px;
    left: 0px;
    position: fixed;
    display: block;
    opacity: 0.7;
    background-color: #fff;
    z-index: 99;
    text-align: center;
}

#loading-image {
    position: absolute;
    top: 50%;
    left: 50%;
    z-index: 100;
}

.swal-title {
    margin: 0px;
    font-size: 16px;
    box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.21);
    margin-bottom: 28px;
}

.swal-button {
    background-color: #000000;
}

.swal-button[not :disabled]:hover {
    background-color: #A6A6A6;
}

.swal-button:active {
    background-color: #A6A6A6;
}

.swal-button:focus {
    box-shadow: 0 0 0 0;
}
</style>
</head>

<body class="loginBg">
    <input type="hidden" id="contextPath" value="<%=request.getContextPath()%>">
    <input type="hidden" id="requestUri" value="${requestUri}">
    <div id="frameSet">
    <div class="loginTopFrame"><img src="<c:url value='/resources/image/logo.png'/>" alt="<spring:message code='login.title'/>"></div>

    <!-- Middle S -->
    <div id="loginFrame">
        <div class="loginGrp">
            <div class="loginTitle"><spring:message code="login.title.msg"/></div>
                <div class="loginBox">
                    <div class="inputID"><input id="loginId" type="text" placeholder="<spring:message code='login.id.msg'/>"></div>
                    <div class="inputPW"><input id="loginPwd" type="password" placeholder="<spring:message code='login.password.msg'/>"></div>
                    <div class="buttonLogin"><input type="button" value="<spring:message code='login.msg'/>" onclick="Login()" style="cursor:pointer"></div>
                </div>
            <ul>
                <li class="loginBtnGrp">
                    <input id="idSaved" type="checkbox" class="chkbox" style="margin-top:7px;"><label><spring:message code='login.checkbox.saveId'/></label>
                    <input type="button" value="<spring:message code='login.button.joinId'/>" class="regID" onclick="insertFormShow('termsPopup')">
                </li>
                <li class="findIDPW">
                    <a href="javascript:;" onclick="openFindMemberPopup('id')"><spring:message code='login.findIdTitle.msg'/></a>
                    <span class="div"> | </span>
                    <a href="javascript:;" onclick="openFindMemberPopup('password')"><spring:message code='login.findPasswordTitle.msg'/></a>
                </li>
            </ul>
        </div>
    </div>

    <!-- Popup S -->
    <div id="center" style="display: none;">
        <div id="termsPopup" class="agreeMember">
            <div class="agreeTitle"><spring:message code="login.joinTitle.msg"/><div class="btnClose"><a href="javascript:;" onclick="popLayerClose()"><img src="<c:url value='/resources/image/icon_close.png'/>"></a></div></div>
            <div class="agreeGrp">
                <div class="column"><input id="checkTermsAll" type="checkbox"><spring:message code="login.term.all.agree"/></div>
                <div class="agreeForm">
                    <p class="h4 txtBold titleTerms"><spring:message code="login.term.use.title"/>&nbsp;<span class="txtRed"><spring:message code="login.required.msg"/></span></p>
                    <div id="useTermsBody" class="agreeTerms">
                    </div>
                    <div class="column txtRight"><input id="checkUseTerms" type="checkbox"><spring:message code="login.term.use.agree"/></div>
                </div>

                <div class="agreeForm">
                    <p class="h4 txtBold titleTerms"><spring:message code="login.term.privacy.title"/>&nbsp;<span class="txtRed"><spring:message code="login.required.msg"/></span></p>
                    <div id="privacyTermsBody" class="agreeTerms">
                    </div>
                    <div class="column txtRight"><input id="privacyUseTerms" type="checkbox"><label><spring:message code="login.term.privacy.agree"/></label></div>
                </div>

            </div>
            <div class="btnGrp btnGrpagree">
                <span><input type="button" class="btnNormal btnNext" value="<spring:message code='login.button.next'/>"></span>
                <span><input type="button" class="btnNormal btnCancel" value="<spring:message code='login.button.cancel'/>"></span>
            </div>
        </div>

        <div id="joinMember" style="max-height:665px;overflow-y:auto">
            <div class="joinTitle"><spring:message code="login.joinTitle.msg"/><div class="btnClose"><a href="javascript:;" onclick="popLayerClose()"><img src="<c:url value='/resources/image/icon_close.png'/>"></a></div></div>

            <div class="joinForm">
                <ul>
                    <li>
                        <input id="memberId" type="text" class="joinInput lengthXL" style="width:365px" placeholder="<spring:message code='login.input.id'/>" onchange="userIdChange()">&nbsp;<input type="button" value="<spring:message code='login.button.checkId'/>" class="btnNormal btnDouble" style="padding:5px 5px 5px 35px;" onclick="userIdDuplication()">
                        <br><span id="memberIdMsg" class="cautionTxt"></span>
                    </li>
                    <li>
                        <input id="memberPwd" type="password" class="joinInput lengthXXL" placeholder="<spring:message code='login.input.password'/>">
                        <br><span id="memberPwdMsg" class="cautionTxt"></span>
                    </li>
                    <li>
                        <input id="memberPwdRe" type="password" class="joinInput lengthXXL" placeholder="<spring:message code='login.input.passwordConfirm'/>">
                        <br><span id="memberPwdReMsg" class="cautionTxt"></span>
                    </li>
                    <li>
                        <input id="memberName" type="text" class="joinInput lengthXXL" placeholder="<spring:message code='login.input.name'/>">
                        <br><span id="memberNameMsg" class="cautionTxt"></span>
                    </li>
                    <li>
                        <select id="memberCp" class="lengthXXL" onchange="changeMemberCp()"></select>
                        <br><span id="memberCpMsg" class="cautionTxt"></span>
                    </li>
                    <li>
                        <input id="telNo" type="text" class="joinInput lengthXXL" placeholder="<spring:message code='login.input.telNo'/>">
                        <br><span id="telNoMsg" class="cautionTxt"></span>
                    </li>
                    <li>
                        <input id="mobilePhone" type="text" class="joinInput lengthXXL" placeholder="<spring:message code='login.input.phoneNo'/>">
                        <br><span id="mobilePhoneMsg" class="cautionTxt"></span>
                    </li>
                    <li>
                        <input id="memberEmailId" type="text" class="joinInput emailInput" placeholder="<spring:message code='login.input.email'/>"><span class="emailAt">@</span><input id="memberEmailDomain" type="text" class="joinInput emailInput">
                        <select id="memberDomainList" class="joinInput emailSelect">
                        </select>
                        <br><span id="memberEmailIdMsg" class="cautionTxt"></span>
                    </li>
                    <!-- <li><input type="text" class="joinInput lengthXXL" value="Mac Address"></li> -->
                    <li>
                        <div class="btnGrp btnGrpJoin">
                        <span><input type="button" class="btnNormal btnOk btnJoin" value="<spring:message code='login.button.join'/>" onclick="userRegister()"></span>
                        <span><input type="button" class="btnNormal btnCancel btnJoin" value="<spring:message code='login.button.cancel'/>"></span>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div id="findMember">
            <div id="loading" style="display:none;">
                <img id="loading-image" src="<c:url value='/resources/image/img/loading.gif' />" alt="Loading..." />
            </div>
            <div class="findTitle"><span id="findTitle"><spring:message code='login.findPasswordTitle.msg'/></span><div class="btnClose"><a href="javascript:;" onclick="popLayerClose()"><img src="<c:url value='/resources/image/icon_close.png'/>"></a></div></div>

            <div class="findForm">
                <ul>
                    <li>
                        <span id="findType" class="item"><spring:message code='login.input.id'/></span><input id="findId" type="text" class="joinInput lengthL">
                    </li>
                    <li>
                        <span class="item"><spring:message code='login.input.email'/></span><input id="findEmailId" type="text" class="joinInput lengthS"> @ <input id="findEmailDomain" type="text" class="joinInput lengthS">&nbsp;
                        <select id="findDomainList">
                        </select>
                    </li>
                </ul>
                <div class="btnGrp btnGrpJoin">
                    <span><input type="button" class="btnNormal btnSearch btnJoin" value="<spring:message code='login.button.find'/>"></span>
                    <span><input type="button" class="btnNormal btnCancel btnJoin" value="<spring:message code='button.reset'/>"></span>
                </div>
            </div>
        </div>
    </div>
    <!-- Popup E -->

    <!-- Middle E -->
    <div class="bottomLine">Copyright © <spring:message code='login.title'/> Co.,Ltd.  All Rights Reserved.</div>
    </div>
    <form id="NoneForm"></form>
</body>
</html>