/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.member.service;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kt.com.im.store.common.util.ConfigProperty;
import kt.com.im.store.common.util.MailSender;
import kt.com.im.store.member.dao.MemberDAO;
import kt.com.im.store.member.vo.LoginVO;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.member.web.MemberApi;
import kt.com.im.store.sms.web.SmsApi;

/**
 *
 * 회원 관리에 관한 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 8.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("MemberService")
public class MemberServiceImpl implements MemberService {

    @Resource(name = "MemberDAO")
    private MemberDAO memberDAO;

    @Autowired
    private ConfigProperty configProperty;

    /**
     * 로그인  정보 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public LoginVO getLoginInfo(LoginVO vo) throws Exception {
        return memberDAO.getLoginInfo(vo);
    }

    /**
     * 로그인 이력  추가
     * 
     * @param LoginVO
     * @return 처리 결과
     */
    @Override
    public int insertLoginHistory(LoginVO vo) throws Exception {
        return memberDAO.insertLoginHistory(vo);
    }

    /**
     * 로그인 실패 회수 증가
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int increaseLoginFailCount(MemberVO vo) throws Exception {
        return memberDAO.increaseLoginFailCount(vo);
    }

    /**
     * 로그인 실패 회수 초기화
     * 
     * @param LoginVO
     * @return 처리 결과
     */
    @Override
    public int resetLoginFailCount(MemberVO vo) throws Exception {
        return memberDAO.resetLoginFailCount(vo);
    }

    /**
     * 멤버  정보 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public MemberVO getMember(MemberVO vo) throws Exception {
        return memberDAO.getMember(vo);
    }

    /**
     * 멤버 정보 추가
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int addMember(MemberVO vo) throws Exception {
        return memberDAO.addMember(vo);
    }

    /**
     * 멤버 정보 수정
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updateMember(MemberVO vo) throws Exception {
        return memberDAO.updateMember(vo);
    }

    /**
     * 멤버 정보 삭제
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int deleteMember(MemberVO vo) throws Exception {
        return memberDAO.deleteMember(vo);
    }

    /**
     * 멤버  비밀번호 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public String getPassword(MemberVO vo) throws Exception {
        return memberDAO.getPassword(vo);
    }

    /**
     * 멤버 비밀번호 수정
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updatePassword(MemberVO vo) throws Exception {
        return memberDAO.updatePassword(vo);
    }

    /**
     * 멤버 비밀번호 초기화
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int initPassword(MemberVO vo) throws Exception {
        int resultCode = memberDAO.updatePassword(vo);

        if (resultCode == 1) {
            MailSender mailSender = new MailSender(configProperty);
            String subject = SmsApi.MSG_TITLE + " Store 비밀번호 찾기";
            mailSender.addRecipient(vo.getMbrEmail());
            mailSender.setSubject(subject);
            mailSender.setContent(mailSender.getPasswordContent(vo));
            boolean sendResult = mailSender.send();
            if (!sendResult) {
                throw new MessagingException();
            }
        }

        return resultCode;
    }

    /**
     * 멤버 비밀번호 생성용 고유값 조회
     * 
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public String getSalt(MemberVO vo) throws Exception {
        return memberDAO.getSalt(vo);
    }

    /**
     * 멤버 토큰 수정
     * 
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updateMemberToken(MemberVO vo) throws Exception {
        return memberDAO.updateMemberToken(vo);
    }

    /**
     * 비밀번호 변경 기한 연장
     * 
     * @param MemberVO
     * @return 조회 목록 결과
     */
    @Override
    public int insertDelayToChangePassword(MemberVO vo) throws Exception {
        return memberDAO.insertDelayToChangePassword(vo);
    }

    /**
     * 비밀번호 변경 알림 여부
     * 
     * @param MemberVO
     * @return 조회 목록 결과
     */
    @Override
    public boolean shouldChangePassword(MemberVO vo) throws Exception {
        return memberDAO.shouldChangePassword(vo);
    }

    /**
     * OTP 발급 시간 조회
     *
     * @param MemberVO
     * @return 조회 결과
     */
    @Override
    public long getOtpCreateTime(LoginVO vo) throws Exception {
        return memberDAO.getOtpCreateTime(vo);
    }

    /**
     * OTP 발급 시간 저장
     *
     * @param MemberVO
     * @return 처리 결과
     */
    @Override
    public int updateOtpCreateTime(LoginVO vo) throws Exception {
        return memberDAO.updateOtpCreateTime(vo);
    }

}
