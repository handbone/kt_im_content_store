/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;

$(document).ready(function() {

    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateContentsList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateContentsList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateContentsList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateContentsList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateContentsList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateContentsList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateContentsList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateContentsList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $("#btnContentDownload").click(function() {
        pageMove("/contents/download");
    });

    $("#cpList").change(function() {
        updateContentsList();
    });

    $(".btnSearch").click(function() {
        updateContentsList();
    });

    $("#keyword").keydown(function(event) {
        if (event.keyCode == 13) {
            updateContentsList();
        }
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    cpList();
});

cpList = function() {
    callByGet("/api/codeInfo?comnCdCtg=CP","cpListSuccess","NoneForm");
}

cpListSuccess = function(data) {
    if (data.resultCode != "1000") {
        popAlertLayer(getMessage("info.nodata.msg"));
        return
    }

    var cpListHtml = "";
    cpListHtml = "<option value='0'>" + getMessage("select.all") + "</option>";
    $(data.result.codeList).each(function(i, item) {
        cpListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNM + "</option>";
    });

    $("#cpList").html(cpListHtml);

    var cpSeq = $("#cpSeq").val();
    if (cpSeq != "" && cpSeq != 0) {
        $("#cpList > option[value=" + cpSeq + "]").attr("selected", true);
    } else {
        $(".selectGrp").show();
    }

    if (!didFirstLayout) {
        didFirstLayout = true;
        initContentUsageList();
    }
}

initContentUsageList = function() {
    var columnNames = [
        getMessage("table.num"),
        getMessage("table.category"),
        getMessage("contents.table.name"),
        getMessage("contents.table.playCount"),
        getMessage("contents.table.playTime"),
        getMessage("contents.table.avgPlayTime"),
        "seq"
    ];
    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentUsageList",
            records: "result.totalCount",
            repeatitems: false,
            id: "contsSeq"
        },
        colNames: columnNames,
        colModel:[
            {name:"num", index: "num", align:"center", width:40, hidden:true},
            {name:"ctgNm", index:"ctgNm", align:"center"},
            {name:"contsTitle", index: "contsTitle", align:"center", classes:"pointer", formatter:pointercursor},
            {name:"contsTotalPlayCount", index:"contsTotalPlayCount", align:"center"},
            {name:"contsTotalPlayTime", index:"contsTotalPlayTime", align:"center"},
            {name:"contsAvgPlayTime", index:"contsAvgPlayTime", align:"center"},
            {name:"contsSeq", index:"contsSeq", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: 10, // 페이지에 출력될 칼럼 개수
            pager: "#pageDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "ctgNm",
            sortorder: "asc",
            height: "auto",
            multiselect: false,
            postData: {
                type : "contentUsage",
                cpSeq: $("#cpList option:selected").val(),
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val()
            },

            beforeRequest:function() {
                var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
                if (hasChangedHash) {
                    var strHash = document.location.hash.replace("#", "");
                    var page = parseInt(findGetParameter(strHash, "page"));
                    var dateType = findGetParameter(strHash, "dateType");
                    var cpSeq = findGetParameter(strHash, "cpSeq");
                    var startDate = findGetParameter(strHash, "startDate");
                    var endDate = findGetParameter(strHash, "endDate");
                    var target = findGetParameter(strHash, "target");
                    var keyword = findGetParameter(strHash, "keyword");

                    $("#dateType").val(dateType);
                    $("#startDate").val(startDate);
                    $("#endDate").val(endDate);
                    restoreDateType(dateType, startDate, endDate);

                    $("#cpList").val(cpSeq);
                    $("#target").val(target);
                    $("#keyword").val(keyword);

                    myPostData.page = page;
                    myPostData.cpSeq = $("#cpList option:selected").val();
                    myPostData.startDate = startDate;
                    myPostData.endDate = endDate;
                    if (keyword != null) {
                        myPostData.target = target;
                        myPostData.keyword = keyword;
                    } else {
                        delete myPostData.target;
                        delete myPostData.keyword;
                    }
                }

                $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
            },
            /*
             * parameter: x
             * jqGrid 그리기가 끝나면 함수 실행
             */
            loadComplete: function() {
                didCompleteLoad(this);
            },
            loadError: function (jqXHR, textStatus, errorThrown) {
                didCompleteLoad(this);
                $("#jqgridData").clearGridData();
                $("#sp_1_pageDiv").text(1);
            },
            /*
             * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
             * 해당 로우의 각 셀마다 이벤트 생성
             */
            onCellSelect: function(rowId, columnId, cellValue, event) {
                // 콘텐츠명 셀 클릭시
                if (columnId == 2) {
                    sessionStorage.setItem("last-url", location);
                    sessionStorage.setItem("state", "view");

                    var list = $("#jqgridData").jqGrid('getRowData', rowId);

                    pageMove("/contents/usage/" + list.contsSeq + "?dateType=" + dateType + "&startDate=" + $("#startDate").val() + "&endDate=" + $("#endDate").val());
                }
            }
    });
    jQuery("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

function updateContentsList() {
    $("#jqgridData").jqGrid("setGridParam",
        {
            search: true,
            postData: {
                type : "contentUsage",
                cpSeq: $("#cpList option:selected").val(),
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                target: $("#target").val(),
                keyword: $("#keyword").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

function setSearchField() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        var searchHtml = "";

        for (var i = 0; i < colModel.length; i++) {
            var name = colModel[i].name;
            if (name == "contsTitle") {
                searchHtml += "<option value=\"" + name + "\">" + colNames[i] + "</option>";
            }
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
}

function downloadExcel() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "contentUsage",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "cpSeq" : $("#cpList option:selected").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
            "target" : $("#target").val(),
            "keyword" : $("#keyword").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    setSearchField();

    //session
    if (sessionStorage.getItem("last-url") != null) {
        sessionStorage.removeItem('state');
        sessionStorage.removeItem('last-url');
    }

    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        var cpSeqValue = $("#cpList option:selected").val();
        if (cpSeqValue) {
            hashlocation += "&cpSeq=" + cpSeqValue;
        }
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        var searchTarget = $("#target").val();
        var searchKeyword = $("#keyword").val();
        if ($(obj).context.p.search && searchKeyword) {
            hashlocation += "&target=" + searchTarget + "&keyword=" + searchKeyword;
        }
        document.location.hash = hashlocation;
    }
}
