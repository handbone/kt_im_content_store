/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.stats.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * 회원 관리에 관한 컨트롤러 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.21
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 21.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class StatsController {
    /**
     * 콘텐츠 사용 통계 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/usage", method = RequestMethod.GET)
    public ModelAndView contentUsageList(ModelAndView mv) {
        mv.setViewName("/views/contents/statistics/ContentUsageList");
        return mv;
    }

    /**
     * 콘텐츠 사용 통계 상세 화면
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환  
     */
    @RequestMapping(value = "/contents/usage/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView contentUsageDetail(ModelAndView mv, HttpServletRequest request, @PathVariable(value="contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.addObject("dateType", request.getParameter("dateType"));
        mv.addObject("startDate", request.getParameter("startDate"));
        mv.addObject("endDate", request.getParameter("endDate"));
        mv.setViewName("/views/contents/statistics/ContentUsageDetailList");
        return mv;
    }

    /**
    * 콘텐츠 다운로드 통계 화면
    * @param mv - 화면 정보를 저장하는 변수
    * @return 모델 정보와 뷰 정보를 반환  
    */
   @RequestMapping(value = "/contents/download", method = RequestMethod.GET)
   public ModelAndView contentDownloadList(ModelAndView mv) {
       mv.setViewName("/views/contents/statistics/ContentDownloadList");
       return mv;
   }

   /**
    * 콘텐츠 다운로드 통계 상세 화면
    * @param mv - 화면 정보를 저장하는 변수
    * @return 모델 정보와 뷰 정보를 반환  
    */
   @RequestMapping(value = "/contents/download/{storSeq}", method = RequestMethod.GET)
   public ModelAndView contentDownloadDetail(ModelAndView mv, HttpServletRequest request, @PathVariable(value="storSeq") String storSeq) {
       mv.addObject("storSeq", storSeq);
       mv.addObject("cpSeq", request.getParameter("cpSeq"));
       mv.addObject("dateType", request.getParameter("dateType"));
       mv.addObject("startDate", request.getParameter("startDate"));
       mv.addObject("endDate", request.getParameter("endDate"));
       mv.setViewName("/views/contents/statistics/ContentDownloadDetailList");
       return mv;
   }
}