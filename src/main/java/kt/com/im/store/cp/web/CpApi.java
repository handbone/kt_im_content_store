/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.cp.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.cp.service.CpService;
import kt.com.im.store.cp.vo.CpVO;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.member.web.MemberApi;

/**
 *
 * 콘텐츠 제공사(CP)에 관한 API 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Controller
public class CpApi {

    @Resource(name = "CpService")
    private CpService cpService;

    /**
     * 콘텐츠 제공사(CP) 관련 API
     * 
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/cp")
    public ModelAndView cpProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/cp/cpProcess");

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");

        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // Not implemented
        } else if (method.equals("GET")) {
            // 전체 리스트 조회

            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            boolean needsAllList = (cpSeq == 0);
            if (needsAllList) {
                CpVO vo = new CpVO();

                if (cpSeq == 0) {
                    if (userVO.getMbrSe().equals(MemberApi.MEMBER_SECTION_CP)) {
                        vo.setCpSeq(userVO.getCpSeq());
                    } else {
                        vo.setCpSeq(cpSeq);
                    }
                } else {
                    vo.setCpSeq(cpSeq);
                }

                List<CpVO> resultList = cpService.listContentProvider(vo);
                rsModel.setResultType("cpList");
                rsModel.setData("item", resultList);
                return rsModel.getModelAndView();
            }

        } else if (method.equals("PUT")) {
            // Not implemented
        } else if (method.equals("DELETE")) {
            // Not implemented
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe) || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
                || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }
}
