/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.store.cp.vo;

import java.io.Serializable;

/**
 *
 * 콘텐츠 제공사(CP) 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

public class CpVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -1332195297897723706L;

    /** 콘텐츠 제공사 번호 */
    int cpSeq;

    /** 콘텐츠 제공사명 */
    String cpNm;

    /** 콘텐츠 제공사 전화 번호 */
    String cpTelNo;

    /** 우편번호 */
    String zipcd;

    /** 기본 주소 */
    String cpBasAddr;

    /** 상세 주소 */
    String cpDtlAddr;

    /** 사업자등록번호 */
    String bizno;

    /** 사용 여부 */
    String useYn;

    /** 생성자 아이디 */
    String cretrId;

    /** 생성 일시 */
    String cretDt;

    /** 수정자 아이디 */
    String amdrId;

    /** 수정 일시 */
    String amdDt;

    /* 검색 유무 */
    String searchConfirm;

    /** 검색어 */
    String keyword;

    /** 계약 시작 일시 */
    private String contStDt;

    /** 계약 종료 일시 */
    private String contFnsDt;

    public int getCpSeq() {
        return cpSeq;
    }

    public void setCpSeq(int cpSeq) {
        this.cpSeq = cpSeq;
    }

    public String getCpNm() {
        return cpNm;
    }

    public void setCpNm(String cpNm) {
        this.cpNm = cpNm;
    }

    public String getCpTelNo() {
        return cpTelNo;
    }

    public void setCpTelNo(String cpTelNo) {
        this.cpTelNo = cpTelNo;
    }

    public String getZipcd() {
        return zipcd;
    }

    public void setZipcd(String zipcd) {
        this.zipcd = zipcd;
    }

    public String getCpBasAddr() {
        return cpBasAddr;
    }

    public void setCpBasAddr(String cpBasAddr) {
        this.cpBasAddr = cpBasAddr;
    }

    public String getCpDtlAddr() {
        return cpDtlAddr;
    }

    public void setCpDtlAddr(String cpDtlAddr) {
        this.cpDtlAddr = cpDtlAddr;
    }

    public String getBizno() {
        return bizno;
    }

    public void setBizno(String bizno) {
        this.bizno = bizno;
    }

    public String getUseYn() {
        return useYn;
    }

    public void setUseYn(String useYn) {
        this.useYn = useYn;
    }

    public String getCretrId() {
        return cretrId;
    }

    public void setCretrId(String cretrId) {
        this.cretrId = cretrId;
    }

    public String getCretDt() {
        return cretDt;
    }

    public void setCretDt(String cretDt) {
        this.cretDt = cretDt;
    }

    public String getAmdrId() {
        return amdrId;
    }

    public void setAmdrId(String amdrId) {
        this.amdrId = amdrId;
    }

    public String getAmdDt() {
        return amdDt;
    }

    public void setAmdDt(String amdDt) {
        this.amdDt = amdDt;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getSearchConfirm() {
        return searchConfirm;
    }

    public void setSearchConfirm(String searchConfirm) {
        this.searchConfirm = searchConfirm;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getContStDt() {
        return contStDt;
    }

    public void setContStDt(String contStDt) {
        this.contStDt = contStDt;
    }

    public String getContFnsDt() {
        return contFnsDt;
    }

    public void setContFnsDt(String contFnsDt) {
        this.contFnsDt = contFnsDt;
    }

}
