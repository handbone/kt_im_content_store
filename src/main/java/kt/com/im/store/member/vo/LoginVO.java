/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */
package kt.com.im.store.member.vo;

import java.io.Serializable;

/**
 *
 * 로그인 관리 VO 클래스
 *
 * @author A2TEC
 * @since 2018.05.16
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 16.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public class LoginVO extends MemberVO implements Serializable {

    /** 시리얼 버전 ID */
    private static final long serialVersionUID = -4362492758590922719L;

    /** 멤버 CP사 사용 여부 */
    String cpUseYn;

    /** 멤버 계약 종료 여부 */
    String cpExpireYn;

    /** 멤버 서비스 활성화 여부 */
    String svcUseYn;

    /** 로그인 성공 여부 */
    String loginYn;

    /** 로그인 설명 */
    String loginDesc;

    /** 멤버 매장 활성화 여부 */
    String storUseYn;

    /** OTP 발급 시간 (millisecond) */
    long otpCretTime;

    public String getCpUseYn() {
        return cpUseYn;
    }

    public void setCpUseYn(String cpUseYn) {
        this.cpUseYn = cpUseYn;
    }

    public String getCpExpireYn() {
        return cpExpireYn;
    }

    public void setCpExpireYn(String cpExpireYn) {
        this.cpExpireYn = cpExpireYn;
    }

    public String getLoginYn() {
        return loginYn;
    }

    public void setLoginYn(String loginYn) {
        this.loginYn = loginYn;
    }

    public String getLoginDesc() {
        return loginDesc;
    }

    public void setLoginDesc(String loginDesc) {
        this.loginDesc = loginDesc;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getSvcUseYn() {
        return svcUseYn;
    }

    public void setSvcUseYn(String svcUseYn) {
        this.svcUseYn = svcUseYn;
    }

    public String getStorUseYn() {
        return storUseYn;
    }

    public void setStorUseYn(String storUseYn) {
        this.storUseYn = storUseYn;
    }

    public long getOtpCretTime() {
        return otpCretTime;
    }

    public void setOtpCretTime(long otpCretTime) {
        this.otpCretTime = otpCretTime;
    }

}
