/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.common.util;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import kt.com.im.store.member.service.MemberService;
import kt.com.im.store.member.vo.MemberVO;

public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Resource(name = "MemberService")
    private MemberService memberService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            if (request.getSession().getAttribute("seq") == null || request.getSession().getAttribute("id") == null
                    || request.getSession().getAttribute("name") == null) {
                String requestUri = this.removeSlash(request.getRequestURI());
                requestUri = "?uri=" + requestUri.replace(request.getContextPath(), "");
                response.sendRedirect(request.getContextPath() + "/" + requestUri);
                return false;
            }
        } catch (Exception e) {

        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //클라이언트의 요청을 처리한 뒤에 호출됨. 컨트롤러에서 예외가 발생되면 실행하지 않는다.

        // 화면처리 시마다 비밀번호 변경 기간이 된 경우 비밀번호 변경 페이지로 이동하도록 설정
        MemberVO vo = new MemberVO();
        vo.setMbrSeq(Integer.parseInt((String) request.getSession().getAttribute("seq")));
        if (memberService.shouldChangePassword(vo)) {
            modelAndView.setViewName("/views/cautionPwd");
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) throws Exception {
        //클라이언트 요청 처리뒤 클리이언트에 뷰를 통해 응답을 전송한뒤 실행 됨. 뷰를 생설항때 예외가 발생해도 실행된다
    }

    @Override
    public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //동시에 핸들링
    }

    private String removeSlash(String value) {
        if (value == null || value.isEmpty()) {
            return value;
        }

        // 슬러시 제거
        int lastSlashIndex = 0;
        String result = "";
        for (int i = 0; i < value.length(); i++) {
            char ch = value.charAt(i);
            if ('/' == ch) {
                if ((lastSlashIndex + 1) == i) {
                    lastSlashIndex++;
                    continue;
                }
                lastSlashIndex = i;
            }
            result += value.charAt(i);
        }
        char ch = result.charAt(result.length() - 1);
        if ('/' == ch) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }
}
