/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.code.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.store.code.dao.CodeDAO;
import kt.com.im.store.code.vo.CodeVO;


/**
*
* 클래스에 대한 상세 설명
*
* @author A2TEC
* @since 2018
* @version 1.0
* @see
*
* <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 2.   A2TEC      최초생성
*
*
* </pre>
*/


@Service("CodeService")
public class CodeServiceImpl implements CodeService {
    @Resource(name = "CodeDAO")
    private CodeDAO CodeDAO;

    /**
     * 검색조건에 해당되는 코드 리스트 정보
     * 
     * @param CodeVO
     * @return 조회 목록 결과
    */
    @Override
    public List<CodeVO> codeList(CodeVO item) {
        return CodeDAO.codeList(item);
    }

}
