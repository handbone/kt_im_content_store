﻿  /*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

/*
 * This is an example of how to cancel all the files queued up.  It's made somewhat generic.  Just pass your SWFUpload
 * object in to this method and it loops through cancelling the uploads.
 */
/*
 * custom setting options
 *
 */
/*
function cancelQueue(instance) {
    document.getElementById(instance.customSettings.cancelButtonId).disabled = true;
    instance.stopUpload();
    var stats;

    do {
        stats = instance.getStats();
        instance.cancelUpload();
    } while (stats.files_queued !== 0);

}
*/

/* **********************
   Event Handlers
   These are my custom event handlers to make my
   web application behave the way I went when SWFUpload
   completes different tasks.  These aren't part of the SWFUpload
   package.  They are part of my application.  Without these none
   of the actions SWFUpload makes will show up in my application.
   ********************** */
var uploadingCnt = 0;
var uploadQue = 0;
var totalQue = 0;
var videoUploadTemp;
var thumbnailUploadTemp;
var metadataUploadTemp;
var coverImgUploadTemp;
var contentsUploadTemp;
var fileTemp;
var file2Temp;
var prevUploadTemp;
var prevmetaUploadTemp;
var prevcontUploadTemp;
var webtoonUploadTemp;

var prevmetadataSeq = 0;
var metadataSeq = 0;

var uploadErrorList = new Array();

function fileDialogStart() {
    /* I don't need to do anything here */
    this.setFilePostName("fileupload");
    this.addPostParam("imgMode", this.customSettings.fileSe);
}
function fileQueued(file) {

    var pattern = /[\u3131-\u314e|\u314f-\u3163|\uac00-\ud7a3]/g;
//    if(pattern.test(file.name)){
//        alert(getMessage("common.ko.file.name.error.msg"));
//        return;
//    }

    if (file.name.indexOf("\0") != -1 || file.name.indexOf(";") != -1 || file.name.indexOf("./") != -1 || file.name.indexOf(".\\") != -1) {
        popAlertLayer(getMessage("info.upload.failed.special.word.msg"));
        this.cancelUpload(file.id);
        return;
    }

    if (file.name.length > 50) {
        popAlertLayer(getMessage("info.upload.failed.max.length.msg"));
        this.cancelUpload(file.id);
        return;
    }
    try {
        // You might include code here that prevents the form from being submitted while the upload is in
        // progress.  Then you'll want to put code in the Queue Complete handler to "unblock" the form
        //var progress = new FileProgress(file, this.customSettings.progressTarget);
        //progress.setStatus("Pending...");
        //progress.toggleCancel(true, this);
        if ( this.customSettings.fileSe == "thumbnail") {
            $("#thumbnailArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">" + getMessage("common.filename") + " : "
                    + file.name + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('thumbnailArea',"
                    + " this, '" + file.id + "')\" value=\"" + getMessage("common.delete") + "\"/><input type=\"button\" class=\"btnPrevWhite prevGalleryNone\""
                    + " onclick=\"previewThumbnail('thumbnailArea', '" + file.id + "')\"" + " value=\"" + getMessage("common.preview.thumbnail")
                    + "\"/></td></tr></table>");
            $("#thumbnailArea").show();
            $("#btnGalleryCretThumb").show();
            thumbnailUploadTemp = this;
            // 썸네일 생성을 완료한 상태에서 파일을 다시 추가할 경우 false로 셋팅하여 등록 시 썸네일 생성 여부를 다시 확인하도록 설정
            if (thumbnailUploadTemp.customSettings.isThumb == "true") {
                thumbnailUploadTemp.customSettings.isThumb = "false";
            }
        } else if (this.customSettings.fileSe =="prevmeta") {
            if($("#prevArea .prevForm").index() == 0) {
                popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                this.cancelUpload(file.id);
                return;
            }

            if(this.customSettings.filePath != undefined){
                this.addPostParam("filePath", this.customSettings.filePath);
            }
            $("#prevmetaName"+this.customSettings.fileSeq).html("<p>" + getMessage("common.filename") + " : " + file.name + "</p>");
            prevmetaUploadTemp.push(this);
        } else if (this.customSettings.fileSe =="metadata") {
            if(this.customSettings.filePath != undefined){
                this.addPostParam("filePath", this.customSettings.filePath);
            }
            $("#metadataName"+this.customSettings.fileSeq).html("<p>" + getMessage("common.filename") + " : " + file.name + "</p>");
            metadataUploadTemp = this;
            $("#metadataName"+this.customSettings.fileSeq).next().css({"position": "absolute","top": "-250px"})
        } else if (this.customSettings.fileSe =="coverImg") {
            if($("#coverImgArea .fileTable").index() == 0) {
                popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                this.cancelUpload(file.id);
                return;
            }

            $("#coverImgArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">" + getMessage("common.filename") + " : "
                    + file.name + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('coverImgArea', this, '" + file.id + "')\""
                    + " value=\"" + getMessage("common.delete") + "\"/><input type=\"button\" class=\"btnPrevWhite prevCoverNone\""
                    + " onclick=\"previewThumbnail('coverImgArea', '" + file.id + "')\"" + " value=\"" + getMessage("common.preview.thumbnail") + "\"/></td></tr></table>");
            $("#coverImgArea").show();
            $("#btnCoverCretThumb").show();
            coverImgUploadTemp = this;
        } else if (this.customSettings.fileSe =="file") {
            if($("#fileArea .fileTable").index() == 0) {
                popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                this.cancelUpload(file.id);
                return;
            }

            $("#fileArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">" + getMessage("common.filename") + " : "
                    + file.name + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('fileArea', this, '" + file.id + "')\""
                    + " value=\"" + getMessage("common.delete") + "\"/></td></tr></table>");
            $("#fileArea").show();
            fileTemp = this;
        } else if(this.customSettings.fileSe =="video") {
            if($(".videoForm").index() == 0) {
                popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                this.cancelUpload(file.id);
                return;
            }

            metadataSeq++;
            var videoHtml = "";
            videoHtml += "<table class=\"videoForm\" id=\"videoItem" + metadataSeq + "\">";
            videoHtml += "  <tr><td class=\"sub\">" + getMessage("common.filename") +" : " + file.name  + "</td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('videoArea', this, '" + file.id + "')\""
            + " value=\"" + getMessage("common.delete") + "\"/></td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" id=\"video" + file.id + "\">";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"metadata" + metadataSeq + "\">";
            videoHtml += "  <tr><td class=\"sub\">" + getMessage("common.metadata") + "</td><td class=\"content\" colspan=\"3\">&nbsp;&nbsp;"
                            + "<div style=\"display:inline-block; float:left;\" id=\"metadataName" + metadataSeq+"\"></div>&nbsp;&nbsp;<div id=\"metadata_btn_placeholder"
                            + metadataSeq + "\"></div></td></tr>";
            videoHtml += "</table>";
            $("#videoArea").append(videoHtml);
            $("#videoArea").show();
            metadataUploadBtn(metadataSeq);
            videoUploadTemp = this;
        } else if (this.customSettings.fileSe =="qna") {
            $("#fileArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">" + getMessage("common.filename") + " : "
                    + file.name + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete(this, '" + file.id + "')\""
                    + " value=\"" + getMessage("common.delete") + "\"/></td></tr></table>");
            $("#fileArea").show();
            fileTemp = this;
        } else if(this.customSettings.fileSe =="webtoon"){
            if($("#webtoonArea .fileTable").index() == 0) {
                popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                this.cancelUpload(file.id);
                return;
            }

            $("#webtoonArea").append("<table class=\"fileTable\"><tr><td><p class=\"orginlFileNm\">" + getMessage("common.filename") + " : "
                    + file.name + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('webtoonArea', this, '" + file.id + "')\""
                    + " value=\"" + getMessage("common.delete") + "\"/></td></tr></table>");
            $("#webtoonArea").show();
            webtoonUploadTemp = this;
        } else {
            if($("#prevArea .prevForm").index() == 0) {
                popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                this.cancelUpload(file.id);
                return;
            }
            prevmetadataSeq++;
            var videoHtml = "";
            videoHtml += "<table class=\"prevForm\" id=\"prevItem" + prevmetadataSeq + "\">";
            videoHtml += "  <tr><td class=\"sub\">" + getMessage("common.filename") +" : " + file.name  + "</td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\" onclick=\"fileQueueDelete('prevArea', this, '" + file.id + "')\""
                    + " value=\"" + getMessage("common.delete") + "\"/></td></tr>";
            videoHtml += "          <input type=\"hidden\" name=\"videoPath\" id=\"prev"+file.id+"\">";
            videoHtml += "          <input type=\"hidden\" name=\"metadataPath\" id=\"prevmeta"+prevmetadataSeq+"\">";
            videoHtml += "  <tr style='display:none'><td class=\"sub\">" + getMessage("common.metadata") + "</td><td class=\"content\" colspan=\"3\">&nbsp;&nbsp;"
                            + "<div style=\"display:inline-block;\" id=\"prevmetaName" + prevmetadataSeq + "\"></div>&nbsp;&nbsp;<div id=\"prevmeta_btn_placeholder"
                            + prevmetadataSeq + "\"></div></td></tr>";
            videoHtml += "</table>";
            $("#prevArea").append(videoHtml);
            $("#prevArea").show();
            prevmetaUploadBtn(prevmetadataSeq);
            prevUploadTemp = this;
        }
    } catch (ex) {
        this.debug(ex);
    }
}

function fileQueueError(file, errorCode, message) {
    try {
        if (this.customSettings.fileSe == "photo") {
            $("#"+this.customSettings.progressTarget).css("cursor","default");
            $(".blockUI.blockOverlay").css("cursor","default");
            $.unblockUI();
        }
        if (errorCode === SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT) {
            if ( this.customSettings.fileSe == "photo" ) {
                //popAlert(getMsg("tit_6034"),getMsgV1("err_6017", SCREENSHOT_MAX_SIZE));
                // file queue 삭제
                while ( this.getStats().files_queued != 0 ) {
                    this.cancelUpload();
                }
                return;
            }
        }
        if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
            if (message === 0) {
                popAlertLayer(getMessage("common.file.upload.limit.msg"));
            } else {
                if (message > 1) {
                    popAlertLayer(getMessage(message + "common.multi.file.upload.msg"));
                } else {
                    popAlertLayer(getMessage("common.only.one.file.upload.msg"));
                }
            }
            //alert("You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
            return;
        }

        //var progress = new FileProgress(file, this.customSettings.progressTarget);
        //progress.setError();
        //progress.toggleCancel(false);

        switch (errorCode) {
        case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
            //progress.setStatus("File is too big.");
            this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
            //progress.setStatus("Cannot upload Zero Byte files.");
            this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
            //progress.setStatus("Invalid File Type.");
            this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
            alert("You have selected too many files.  " +  (message > 1 ? "You may only add " +  message + " more files" : "You cannot add any more files."));
            break;
        default:
            if (file !== null) {
                //progress.setStatus("Unhandled Error");
            }
            this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        }
    } catch (ex) {
        this.debug(ex);
    }
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
    try {
        //this.startUpload();
    } catch (ex)  {
        this.debug(ex);
    }
}

function uploadStart(file) {
    if (this.customSettings.isThumb === "false") {
        if (this.customSettings.fileSe == "coverImg") {
            this.addPostParam("thumbSe", "coverImgArea");
        } else {
            this.addPostParam("thumbSe", "thumbnailArea");
        }
        this.addPostParam("fileId", file.id);
        this.addPostParam("thumbSizes", this.customSettings.thumbSizes);
    } else {
        $("#uploadText").html(file.name + " " + getMessage("common.file.uploading.msg"));
        $("#divCenter").css("display", "block");
        this.addPostParam("contsSeq", $("#contsSeq").val());

        if (this.customSettings.fileSe == "coverImg") {
            for (var i = 0; i < coverThumbInfo.length; i++) {
                if (coverThumbInfo[i].fileId == file.id) {
                    this.addPostParam("thumbList", coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1));
                }
            }
        } else if (this.customSettings.fileSe == "thumbnail") {
            for (var i = 0; i < galleryThumbInfo.length; i++) {
                if (galleryThumbInfo[i].fileId == file.id) {
                    this.addPostParam("thumbList", galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1));
                }
            }
        }
    }

    return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
    var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);
    $("#uploadPercent").html(percent+"%");
    $("#barWidth").attr("width", percent+"%");
}

var addPhotoSeqs = "";
function uploadSuccess(file, serverData) {
    var data  = $.parseJSON(serverData);
    if (this.customSettings.isThumb == "false") {
        var stats = this.getStats();
        if (data.resultCode == "1000") {
            if (this.customSettings.fileSe == "coverImg") {
                var findFileId = false;
                var findIndex = 0;
                for (var i = 0; i < coverThumbInfo.length; i++) {
                    if (coverThumbInfo[i].fileId == data.result.fileInfo.fileId) {
                        findFileId = true;
                        findIndex = i;
                        break;
                    }
                }

                if (findFileId) {
                    coverThumbInfo[findIndex].thumbList = data.result.fileInfo.thumbList;
                } else {
                    coverThumbInfo.push(data.result.fileInfo);
                }

                $("#coverImgArea").find(".fileTable tr").each(function(i) {
                    var name = $(this).find(".orginlFileNm").text().replace(getMessage("common.filename") + " : ","").trim();
                    if (name == file.name && $(this).find(".btnPrevWhite").hasClass("prevCoverNone")) {
                        $(this).find(".btnPrevWhite").toggleClass("prevCoverNone");
                        var splitImg_F = data.result.fileInfo.thumbList.split(",")[0].replace("[","").trim();
                        var splitImg = data.result.fileInfo.thumbList.split(",").last().replace("]","").trim();
                        if (location.href.indexOf("/edit") != -1) {
                            $(this).find(".orginlFileNm").wrap("<div class='tableBox'></div>").before("<span class='article' class='thumImgPv'><a href=\""+makeAPIUrl(splitImg_F,"img")+"\" class=\"swipebox\"><img class='prevVwImg' src="+makeAPIUrl(splitImg,"img")+" title="+file.name+" /></a></span>");
                        }
                        if (!$(this).find(".orginlFileNm").hasClass("fileName") && location.href.indexOf("/edit") != -1) {
                            $(this).find(".orginlFileNm").addClass("fileName");
                        }
                        if (!$(this).find(".orginlFileNm").hasClass("tableCell") && location.href.indexOf("/edit") != -1) {
                            $(this).find(".orginlFileNm").addClass("tableCell");
                        }
                        if (location.href.indexOf("/edit") != -1) {
                            $(".swipebox").swipebox();
                        }

                    }
                });
            } else if (this.customSettings.fileSe == "thumbnail") {
                var findFileId = false;
                var findIndex = 0;
                for (var i = 0; i < galleryThumbInfo.length; i++) {
                    if (galleryThumbInfo[i].fileId == data.result.fileInfo.fileId) {
                        findFileId = true;
                        findIndex = i;
                        break;
                    }
                }

                if (findFileId) {
                    galleryThumbInfo[findIndex].thumbList = data.result.fileInfo.thumbList;
                } else {
                    galleryThumbInfo.push(data.result.fileInfo);
                }

                $("#thumbnailArea").find(".fileTable tr").each(function(i) {
                    var name = $(this).find(".orginlFileNm").text().replace(getMessage("common.filename") + " : ","").trim();
                    if (name == file.name && $(this).find(".btnPrevWhite").hasClass("prevGalleryNone")) {

                        var splitImg_F = data.result.fileInfo.thumbList.split(",")[0].replace("[","").trim();
                        var splitImg = data.result.fileInfo.thumbList.split(",").last().replace("]","").trim();

                        if (location.href.indexOf("/edit") != -1) {
                            $(this).find(".orginlFileNm").wrap("<div class='tableBox'></div>").before("<span class='article' class='thumImgPv'><a href=\""+makeAPIUrl(splitImg_F,"img")+"\" class=\"swipebox\"><img class='prevVwImg' src="+makeAPIUrl(splitImg,"img")+" title="+file.name+" /></a></span>");
                        }
                        if (!$(this).find(".orginlFileNm").hasClass("fileName") && location.href.indexOf("/edit") != -1) {
                            $(this).find(".orginlFileNm").addClass("fileName");
                        }
                        if (!$(this).find(".orginlFileNm").hasClass("tableCell") && location.href.indexOf("/edit") != -1) {
                            $(this).find(".orginlFileNm").addClass("tableCell");
                        }
                        if (location.href.indexOf("/edit") != -1) {
                            $(".swipebox").swipebox();
                        }

                        $(this).find(".btnPrevWhite").toggleClass("prevGalleryNone");
                    }
                });
            }
        } else {
            addErrorFile({ name : file.name, message : '' });
            if (typeof window["createThumbImgFail"] === "function") {
                window["createThumbImgFail"].apply();
            }
        }
    } else {
        if (data.resultCode == "1000") {
            $("#orginlFileNm").val(data.result.fileInfo.orginlFileNm);
            $("#fileDir").val(data.result.fileInfo.fileDir);
            $("#fileSize").val(file.size);
            $("#fileExt").val(data.result.fileInfo.fileExt);
            $("#streFileNm").val(data.result.fileInfo.streFileNm);
            /*var TimeH = 3600*Number($("#hour").val());
            var TimeM = 60*Number($("#min").val());
            $("#runTime").val(TimeH+TimeM);*/
            if (this.customSettings.fileSe == "thumbnail") {
                $("#fileSe").val("img");
                var thumbListStr = data.result.fileInfo.thumbList.substring(1, data.result.fileInfo.thumbList.length - 1);
                $("#thumbList").val(thumbListStr);
            } else if (this.customSettings.fileSe == "coverImg") {
                $("#fileSe").val("cover");
                var thumbListStr = data.result.fileInfo.thumbList.substring(1, data.result.fileInfo.thumbList.length - 1);
                $("#thumbList").val(thumbListStr);
            } else if (this.customSettings.fileSe == "metadata") {
                $("#metadata"+this.customSettings.fileSeq).val(data.result.fileInfo.fileDir);
                $("#fileSe").val("metadata");
            } else if (this.customSettings.fileSe =="file") {
                $("#fileSe").val("file");
            } else if (this.customSettings.fileSe =="file2") {
                $("#fileSe").val("file");
            } else if (this.customSettings.fileSe =="video") {
                $("#video"+file.id).val(data.result.fileInfo.fileDir);
                $("#fileSe").val("video");
            } else if (this.customSettings.fileSe =="prev") {
                $("#prev"+file.id).val(data.result.fileInfo.fileDir);
                $("#fileSe").val("prev");
            } else if(this.customSettings.fileSe == "prevmeta"){
                $("#prevmeta"+this.customSettings.fileSeq).val(data.result.fileInfo.fileDir);
                $("#fileSe").val("prev");
            } else if(this.customSettings.fileSe =="qna") {
                $("#fileSe").val("qna");
            } else if(this.customSettings.fileSe =="webtoon"){
                $("#fileSe").val("webtoon");
            } else {
                $("#prev"+file.id).val(data.result.fileInfo.fileDir);
                $("#fileSe").val("prev");
            }

            if (this.customSettings.fileSe == "coverImg" || this.customSettings.fileSe == "thumbnail") {
                callByPost("/api/thumbFileProcess", "fileInsertSuccess", "fileForm");
            } else {
                callByPost("/api/fileProcess", "fileInsertSuccess", "fileForm");
            }
        } else {
            addErrorFile({ name : file.name, message : '' });
            if (typeof window["fileInsertFail"] === "function") {
                window["fileInsertFail"].apply();
            }
        }
    }
}

var requeueFileIds = []; // 썸네일 생성 시 업로드 완료된 파일들의 id 값들을 저장하고 썸네일 생성 완료 후 다시 queue에 넣어주도록 함.
function uploadComplete(file) {
    try {
        if (this.customSettings.isThumb === "false") {
            requeueFileIds.push(file.id);

            if ((queueLen - 1) == 0) {
                var stats = this.getStats();
                stats.successful_uploads = stats.successful_uploads - queueTotLen;
                this.setStats(stats);
                this.customSettings.isThumb = "true";

                if (this.customSettings.fileSe == "thumbnail") {
                    $("#btnGalleryCretThumb").val(getMessage("common.recreate.thumbnail"));
                } else if (this.customSettings.fileSe == "coverImg") {
                    $("#btnCoverCretThumb").val(getMessage("common.recreate.thumbnail"));
                }

                for (var i = requeueFileIds.length - 1; i >= 0; i--) {
                    this.requeueUpload(requeueFileIds[i]);
                    if (i == 0) {
                        requeueFileIds = [];
                    }
                }

                $("#loading").hide().css("background", "initial");
                $(".thumbCretLoading").remove();
            } else {
                this.startUpload();
                queueLen--;
            }
            file.filestatus = -1;
        } else {
            /*  I want the next upload to continue automatically so I'll call startUpload here */
            if (this.getStats().files_queued === 0) {
                //document.getElementById(this.customSettings.cancelButtonId).disabled = true;
                //$("#"+this.customSettings.uploadButtonLayer).css("display","none");
                //$("#"+this.customSettings.progressTarget).css("display",  "none");
            } else {
                this.startUpload();
            }
        }
    } catch (ex) {
        this.debug(ex);
    }
}

function uploadError(file, errorCode, message) {
    //console.log(JSON.stringify(file,null,4));
    try {
        //var progress = new FileProgress(file, this.customSettings.progressTarget);
        //progress.setError();
        //progress.toggleCancel(false);

        if ( errorCode == SWFUpload.UPLOAD_ERROR.FILE_CANCELLED) {
            if (this.customSettings.fileSe != "screenshot") {
                $("#"+this.customSettings.progressTarget).css("cursor","default");
                $(".blockUI.blockOverlay").css("cursor","default");
                $.unblockUI();
            }
            return;
        }
        //console.log(errorCode);
        //console.log(message);
        switch ( message ) {
            case "401"      : {
                //document.location.href = _context + "/auth/signin"; // 로그인 페이지로 이동
                break;
            }
            case "403"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0012"));
                break;
            }
            case "404"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0013"));
                break;
            }
            case "400"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0014"));
                break;
            }
            case "500"      : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0016"));
                break;
            }
            default     : {
                //popAlert(getMsg("tit_0002"),getMsg("err_0010")+"\nErrorMessage: "+message+", ErrorCode: "+errorCode);
                break;
            }
        }
        switch (errorCode) {
        case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
            //console.log("1");
            //progress.setStatus("Upload Error: " + message);
            this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.MISSING_UPLOAD_URL:
            //console.log("2");
            //progress.setStatus("Configuration Error");
            this.debug("Error Code: No backend file, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: No backend file, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
            //console.log("3");
            //progress.setStatus("Upload Failed.");
            this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.IO_ERROR:
            //console.log("4");
            //progress.setStatus("Server (IO) Error");
            this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
            //console.log("5");
            //progress.setStatus("Security Error");
            this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
            console.log("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
            //console.log("6");
            //progress.setStatus("Upload limit exceeded.");
            this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.SPECIFIED_FILE_ID_NOT_FOUND:
            //console.log("7");
            //progress.setStatus("File not found.");
            this.debug("Error Code: The file was not found, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: The file was not found, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
            //console.log("8");
            //progress.setStatus("Failed Validation.  Upload skipped.");
            this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
            //console.log("9");
            if (this.getStats().files_queued === 0) {
                //document.getElementById(this.customSettings.cancelButtonId).disabled = true;
            }
            //progress.setStatus("Cancelled");
            //progress.setCancelled();
            break;
        case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
            //console.log("10");
            //progress.setStatus("Stopped");
            break;
        default:
            //console.log("11");
            //progress.setStatus("Unhandled Error: " + error_code);
            this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            console.log("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
            break;
        }

        addErrorFile({ name : file.name, message : getErrorMessage(errorCode) });

        if (this.customSettings.isThumb != undefined) {
            if (this.customSettings.isThumb == "true") {
                if (typeof window["fileInsertFail"] === "function") {
                    window["fileInsertFail"].apply();
                }
            } else {
                if (typeof window["createThumbImgFail"] === "function") {
                    window["createThumbImgFail"].apply();
                }
            }
        } else {
            if (typeof window["fileInsertFail"] === "function") {
                window["fileInsertFail"].apply();
            }
        }
    } catch (ex) {
        this.debug(ex);
    }
}

addErrorFile = function(item) {
    uploadErrorList.push(item);
}

hasErrorFiles = function() {
    return uploadErrorList.length >= 1;
}

getErrorMessage = function(errorCode) {
    var message = "";
    switch (errorCode) {
    case SWFUpload.UPLOAD_ERROR.IO_ERROR:
        message = getMessage("info.upload.failed.wrong.path.msg");
        break;
    default:
        break;
    }

    if (message !== "") {
        message = "(" + message + ")";
    }
    return message;
}

getErrorMessages = function() {
    var message = "";
    if (uploadErrorList.length < 1) {
        return message;
    }

    var fileNames = "";
    $(uploadErrorList).each(function(i, item) {
        fileNames += item.name + item.message;
        if (fileNames !== "" && i < uploadErrorList.length - 1) {
            fileNames += ", ";
        }
    });

    if (fileNames !== "") {
        message += "<br><br>" +  fileNames + " " + getMessage("info.upload.failed.msg");
    }
    uploadErrorList = [];

    return message;
}