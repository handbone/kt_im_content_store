/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.qna.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.common.util.ConfigProperty;

/**
 *
 * QNA 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 3.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Component
@Controller
public class QnaController {

    @Autowired
    private ConfigProperty configProperty;

    /**
     * QNA 리스트 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/qna", method = RequestMethod.GET)
    public ModelAndView qnaList(ModelAndView mv) {
        mv.setViewName("/views/customer/qna/QnaList");
        return mv;
    }

    /**
     * QNA 상세 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/qna/{qnaSeq}", method = RequestMethod.GET)
    public ModelAndView qnaView(ModelAndView mv, @PathVariable(value="qnaSeq") String qnaSeq, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        mv.addObject("qnaSeq", qnaSeq);
        mv.setViewName("/views/customer/qna/QnaDetail");
        return mv;
    }

    /**
     * QNA 등록 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/qna/regist", method = RequestMethod.GET)
    public ModelAndView qnaRegister(ModelAndView mv) {
        mv.addObject("usesAutoSendSms", configProperty.getProperty("use.auto.send.sms"));
        mv.setViewName("/views/customer/qna/QnaRegist");
        return mv;
    }

    /**
     * QNA 수정 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/qna/{qnaSeq}/edit", method = RequestMethod.GET)
    public ModelAndView qnaEdit(ModelAndView mv, @PathVariable(value = "qnaSeq") String qnaSeq) {
        mv.addObject("qnaSeq", qnaSeq);
        mv.setViewName("/views/customer/qna/QnaEdit");
        return mv;
    }

}
