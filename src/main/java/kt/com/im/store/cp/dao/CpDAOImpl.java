/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.cp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.store.common.dao.MemberAbstractMapper;
import kt.com.im.store.cp.vo.CpVO;

/**
 *
 * 콘텐츠 제공사(CP) 관리에 관한 데이터 접근  클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.10
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Repository("CpDAO")
public class CpDAOImpl extends MemberAbstractMapper implements CpDAO {

    /**
     * 콘텐츠 제공사(CP) 목록 정보 조회
     * 
     * @param CpVO
     * @return 조회 목록 결과
     */
    @Override
    public List<CpVO> listContentProvider(CpVO vo) throws Exception {
        return selectList("CpDAO.selectContentProviderList", vo);
    }

    /**
     * 콘텐츠 제공사(CP)  정보 조회
     * 
     * @param CpVO
     * @return 조회 결과
     */
    @Override
    public CpVO getContentProvider(CpVO vo) throws Exception {
        return selectOne("CpDAO.selectContentProvider", vo);
    }

    /**
     * 콘텐츠 제공사(CP)  정보 리스트 조회
     *
     * @param CpVO
     * @return 조회 결과
     */
    @Override
    public List<CpVO> getContentProviderList(CpVO vo) {
         return selectList("CpDAO.selectContentProviders", vo);
    }

    /**
     * 콘텐츠 제공사(CP) 정보 추가
     * 
     * @param CpVO
     * @return 처리 결과
     */
    @Override
    public int addContentProvider(CpVO vo) throws Exception {
        return insert("CpDAO.insertContentProvider", vo);
    }

    /**
     * 콘텐츠 제공사(CP) 정보 수정
     * 
     * @param CpVO
     * @return 처리 결과
     */
    @Override
    public int updateContentProvider(CpVO vo) throws Exception {
        return update("CpDAO.updateContentProvider", vo);
    }

    /**
     * 콘텐츠 제공사(CP) 정보 삭제
     * 
     * @param CpVO
     * @return 처리 결과
     */
    @Override
    public int deleteContentProvider(CpVO vo) throws Exception {
        return update("CpDAO.deleteContentProvider", vo);
    }

    /**
     * 콘텐츠 제공사(CP) 목록 정보 조회
     *
     * @param CpVO
     * @return 조회 목록 결과
     */
    @Override
    public List<CpVO> cpList(CpVO vo) throws Exception {
        return selectList("CpDAO.cpList", vo);
    }
}
