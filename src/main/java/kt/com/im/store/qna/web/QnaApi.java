/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.qna.web;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.member.web.MemberApi;
import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.file.service.FileService;
import kt.com.im.store.file.vo.FileVO;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.qna.service.QnaService;
import kt.com.im.store.qna.vo.QnaVO;

/**
 *
 * 문의사항 관련 처리 API
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 10.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Controller
public class QnaApi {

    @Resource(name="QnaService")
    private QnaService qnaService;

    @Resource(name="FileService")
    private FileService fileService;

    @Inject
    private FileSystemResource fsResource;

    @Resource(name="transactionManager")
    private DataSourceTransactionManager transactionManager;

    /**
     * 문의사항 등록 및 조회 관련 API
     * @param modelView - 화면 정보를 저장하는 변수
     * @param Method - 메소드 구분(API 동작 - GET PUT DELETE POST)
     * 
     * 
     * @return modelAndView  & API 메세지값 & WebStatus 
     */
    @RequestMapping(value = "/api/qna")
    public ModelAndView qnaProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName("../resources/api/customer/qna/qnaProcess");

        // Master 관리자, CMS 관리자, CP 관리자만 접근 가능
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        boolean isAllowMember = this.isAllowMember(userVO);
        if (userVO == null || !isAllowMember) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        request.setCharacterEncoding("UTF-8");
        QnaVO item = new QnaVO();
        FileVO fileVO = new FileVO();

        String method = CommonFnc.getMethod(request);
        if (method.equals("POST")) {
            // Checking Mendatory S
            String invalidParams = CommonFnc.requiredChecking(request,"title,content");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            String title = CommonFnc.emptyCheckString("title", request);
            String content = CommonFnc.emptyCheckString("content", request);
            String type = CommonFnc.emptyCheckString("type", request);
            String category = CommonFnc.emptyCheckString("category", request);

            item.setCretrId(userVO.getMbrId());
            item.setQnaTitle(title);
            item.setQnaSbst(content);
            item.setQnaType(type);
            item.setQnaCtgCd(category);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                qnaService.insertQna(item);
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("qnaSeq", item.getQnaSeq());
                rsModel.setResultType("qnaInsert");
                transactionManager.commit(status);
            } catch (Exception e){
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_INSERT);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if(!status.isCompleted()){
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("PUT")) {
            // Checking Mendatory S
            String invalidParams = CommonFnc.requiredChecking(request,"qnaSeq,title,content");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            // Checking Mendatory E

            int qnaSeq = CommonFnc.emptyCheckInt("qnaSeq", request);
            String title = CommonFnc.emptyCheckString("title", request);
            String content = CommonFnc.emptyCheckString("content", request);
            String type = CommonFnc.emptyCheckString("type", request);
            String category = CommonFnc.emptyCheckString("category", request);

            item.setQnaSeq(qnaSeq);
            item.setQnaTitle(title);
            item.setQnaSbst(content);
            item.setQnaType(type);
            item.setQnaCtgCd(category);

            int result = 0;
            List<String> deleteFilePath = null;

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                String delFileSeqs = CommonFnc.emptyCheckString("delFileSeqs", request);
                deleteFilePath = new ArrayList<String>();

                if (delFileSeqs != "") {
                    String[] delFileSeq = delFileSeqs.split(",");

                    for (int i = 0; i < delFileSeq.length; i++) {
                        item.setFileSeq(Integer.parseInt(delFileSeq[i]));
                        QnaVO resData = qnaService.fileInfo(item);
                        String filePath = resData.getFilePath();
                        if (qnaService.attachFileDelete(item) == 1) {
                            deleteFilePath.add(filePath);
                        }
                    }
                }
                result = qnaService.updateQna(item);

                if (result == 1) {
                    for (int i = 0; i < deleteFilePath.size(); i++) {
                        File f = new File(fsResource.getPath() + deleteFilePath.get(i));
                        f.delete();
                    }

                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e){
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAILED_UPDATE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if(!status.isCompleted()){
                    transactionManager.rollback(status);
                }
            }
        } else if (method.equals("DELETE")) {
            /* Checking Mendatory S */
            String invalidParams = CommonFnc.requiredChecking(request,"qnaSeq");
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }
            /* Checking Mendatory E */

            int qnaSeq = CommonFnc.emptyCheckInt("qnaSeq", request);
            item.setQnaSeq(qnaSeq);

            DefaultTransactionDefinition def = new DefaultTransactionDefinition();
            def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            TransactionStatus status = transactionManager.getTransaction(def);

            try {
                int result = qnaService.deleteQna(item);
                if (result == 1) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    transactionManager.commit(status);
                } else {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    transactionManager.rollback(status);
                }
            } catch (Exception e) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_INVALID);
                rsModel.setResultMessage(ResultModel.MESSAGE_FAIL_DELETE);
                rsModel.setStatus(ResultModel.HTTP_STATUS_INTERNAL_SERVER_ERROR);
                transactionManager.rollback(status);
            } finally {
                if (!status.isCompleted()) {
                    transactionManager.rollback(status);
                }
            }
        } else { // GET
            int qnaSeq = CommonFnc.emptyCheckInt("qnaSeq", request);
            String reqType = CommonFnc.emptyCheckString("reqType", request);

            if (qnaSeq != 0) {
                item.setQnaSeq(qnaSeq);
                fileVO.setFileContsSeq(qnaSeq);
                // 문의사항 첨부파일
                fileVO.setFileSe("QNA");
                List<FileVO> fList = fileService.fileList(fileVO);
                // 문의사항 답변 첨부파일
                fileVO.setFileSe("QNA_ANS");
                List<FileVO> ansFList = fileService.fileList(fileVO);

                QnaVO resultItem = qnaService.qnaDetail(item);

                if (resultItem == null) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                // 문의사항 수정 시 작성자가 아닌 경우 접근 불가
                if (reqType.equals("edit") && !userVO.getMbrId().equals(resultItem.getCretrId())) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                // 문의사항 구분이 검수가 아닌 경우 접근 불가 (01: 전체, 05: 검수)
                if (!resultItem.getQnaTypeCd().equals("05")) {
                    rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                    return rsModel.getModelAndView();
                }

                // 문의사항은 등록자만 수정이 가능, 수정 가능 여부 확인
                if (userVO.getMbrId().equals(resultItem.getCretrId())) {
                    rsModel.setData("canModify", "true");
                } else {
                    rsModel.setData("canModify", "false");
                }

                // 문의사항 수정을 위해 상세 정보 요청 시에는 조회수를 업데이트 하지 않음.
                String searchType = CommonFnc.emptyCheckString("searchType", request);
                if (!searchType.equals("edit")) {
                    qnaService.updateQnaRetvNum(item);
                    resultItem.setRetvNum(resultItem.getRetvNum() + 1);
                }

                /* 개인정보 마스킹 (등록자 이름, 아이디, 답변자 이름, 아이디, 답변 수정자 아이디) */
                resultItem.setCretrNm(CommonFnc.getNameMask(resultItem.getCretrNm()));
                resultItem.setCretrId(CommonFnc.getIdMask(resultItem.getCretrId()));
                resultItem.setAnsCretrNm(CommonFnc.getNameMask(resultItem.getAnsCretrNm()));
                resultItem.setAnsCretrId(CommonFnc.getIdMask(resultItem.getAnsCretrId()));
                resultItem.setAnsAmdrId(CommonFnc.getIdMask(resultItem.getAnsAmdrId()));

                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
                rsModel.setData("fileList", fList);
                rsModel.setData("ansFileList", ansFList);
                rsModel.setResultType("qnaDetail");
            } else {
                // 0번의 게시글을 요청한 것인지 확인, 아닐 경우 목록에서 조회를 요청한 것으로 목록을 조회
                if (qnaSeq == 0 && CommonFnc.checkReqParameter(request, "qnaSeq")) {
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    return rsModel.getModelAndView();
                }

                String searchConfirm = request.getParameter("_search") == null ? "false" : request.getParameter("_search");
                item.setSearchConfirm(searchConfirm);
                // 검색 여부가 true일 경우
                if(searchConfirm.equals("true")){
                    String searchField = CommonFnc.emptyCheckString("searchField", request);
                    String searchString = CommonFnc.emptyCheckString("searchString", request);

                    if (searchField =="qnaTitle") {
                        searchField = "QNA_TITLE";
                    } else if (searchField =="cretrNm") {
                        searchField = "CRETR_NM";
                    }

                    item.setSearchTarget(searchField);
                    item.setSearchKeyword(searchString);
                }
                /*
                 * page: 현재 페이지, limit: 검색에 출력될 개수, sidx: 정렬할 필드, sord: 정렬 방법
                 */
                int currentPage = CommonFnc.emptyCheckIntRetVal("page", request, 1);
                int limit = CommonFnc.emptyCheckIntRetVal("rows", request, 10);
                String sidx = CommonFnc.emptyCheckString("sidx", request);
                String sord = CommonFnc.emptyCheckString("sord", request);

                int page = (currentPage -1) * limit+1;
                int pageEnd = page + limit - 1;

                if (sidx.equals("num")) {
                    sidx="QNA_SEQ";
                }

                item.setSidx(sidx);
                item.setSord(sord);
                item.setOffset(page);
                item.setLimit(pageEnd);

                List<QnaVO> resultItem = qnaService.qnaList(item);

                if(resultItem.isEmpty()){
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                    rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                } else {
                    /* 개인정보 마스킹 (등록자 이름, 아이디, 답변자 아이디) */
                    for (int i = 0; i < resultItem.size(); i++) {
                        resultItem.get(i).setCretrNm(CommonFnc.getNameMask(resultItem.get(i).getCretrNm()));
                        resultItem.get(i).setCretrId(CommonFnc.getIdMask(resultItem.get(i).getCretrId()));
                        resultItem.get(i).setAnsCretrId(CommonFnc.getIdMask(resultItem.get(i).getAnsCretrId()));
                    }

                    int totalCount = qnaService.qnaListTotalCount(item);
                    rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                    rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                    rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                    rsModel.setData("item", resultItem);
                    rsModel.setData("totalCount", totalCount);
                    rsModel.setData("row", limit);
                    rsModel.setData("currentPage", currentPage);
                    rsModel.setData("totalPage", totalCount / limit);
                    rsModel.setResultType("qnaList");
                }
            }
        }

        return rsModel.getModelAndView();
    }

    private boolean isAllowMember(MemberVO userVO) {
        String mbrSe = userVO.getMbrSe();
        return MemberApi.MEMBER_SECTION_MASTER.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CMS.equals(mbrSe)
            || MemberApi.MEMBER_SECTION_CP.equals(mbrSe);
    }

}
