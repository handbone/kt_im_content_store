<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/left_menu.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/contents/ContractList.js?version=201805311709"></script>

<input type="hidden" id="leftNum" value="1">
<input type="hidden" id="leftSubMenuNum" value="1">
<input type="hidden" id="offset" value="0">
<input type="hidden" id="limit" value="10">
<input type="hidden" name="mbrSe" id="mbrSe" value="${memberSec}" />
<input type="hidden" name="cpSeq" id="cpSeq" value="${cpSeq}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div id="tabGrp">
            <div class="tab tabEnd tabOn"><spring:message code="contents.contract.list.title"/></div>
        </div>
        <!-- Tab E -->

        <!-- Select Box S -->
        <div class="rsvGrp">
            <div class="rsvSelect">
                <span><spring:message code="title.cp"/></span>
                <span>
                    <select id="cpSelbox" onchange="jqGridReload()"></select>
                </span>
            </div>
        </div> 
        <!-- Select Box E -->

        <!-- content S -->
        <div class="content">
            <!-- Table Area S -->
            <div id="gridArea">
                <!-- jQGrid S -->
                <table id="jqgridData"></table>
                <div id="pageDiv"></div>
                <!-- jQGrid E -->
            </div>
            <!-- Table Area E -->

            <div class="CenterGrp">
                <!-- Search Group S -->
                <div class="searchBox nbr24">
                    <div class="searchGrp">
                        <div class="selectBox">
                            <select id="target"></select>
                        </div>
                        <div class="searchInput">
                            <input type="text" id="keyword">
                        </div>
                        <div class="searchBtn">
                            <input type="button" class="btnNormal btnSearch cursorPointer" onclick="keywordSearch()" value="<spring:message code="button.search"/>">
                        </div>
                    </div>
                </div>
                <!-- Search Group E -->
            </div>
        </div>
        <!-- content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp"%>