/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

$(document).ready(function(){
    noticeInfo();
});

function noticeInfo(){
    formData("NoneForm", "noticeSeq", $("#noticeSeq").val());
    callByGet("/api/notice", "noticeInfoSuccess", "NoneForm", "noticeInfoFail");
}

noticeInfoSuccess = function(data) {
    formDataDelete("NoneForm", "noticeSeq");
    if(data.resultCode == "1000"){
        var noticeDetail = data.result.noticeDetail;
        $("#noticeTitle").html(noticeDetail.noticeTitle + "<span class='noticeDate'>" + getMessage("common.type") + " : " + noticeDetail.noticeType + "<span class='div'>|</span>" + getMessage("common.reger") + " : " + noticeDetail.cretrNm + "<span class='div'>|</span>" + getMessage("common.regdate") + " : " + noticeDetail.regDt + "</span>" );
        $("#noticeSbst").html(xssChk(noticeDetail.noticeSbst));
        $("#noticeSbst").html($("#noticeSbst").text());

        if (data.result.fileList.length == 0) {
            $("#fileGrp").hide();
        } else {
            var fileListHtml = "<table width='100%'><tr><td class='attatch'><div class='txtFileadd'>" + getMessage("common.attachfile") + " : </div></td><td class='attatchFile'>";
            $(data.result.fileList).each(function(i,item) {
                fileListHtml += "<p><span class='float_l'>" + item.fileName + "</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.fileSeq + "' class='fontblack btnDownloadGray'>[" + getMessage("common.download") + "]</a></p>";
            });
            fileListHtml += "</td></tr></table>";
            $("#fileGrp").html(fileListHtml);
        }

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/notice");
    }
}

noticeInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/notice");
}