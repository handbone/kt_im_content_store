<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><spring:message code="common.page.not.found"/></title>
<script src="<%=request.getContextPath()%>/resources/js/libs/jquery/jquery-1.10.2.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/apiFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/baseFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/commonFunc.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/commonFunc/cookie.js"></script>
</head>

<style>
.main {margin:0 auto; width:600px; margin-top:100px;}
center {margin:20px 0 0 0;}
a {cursor:pointer;}
</style>

<body>

<input type="hidden" id="contextPath" value="<%=request.getContextPath()%>">
<div class="main">
    <img src="<c:url value='/resources/image/errorpage_img.jpg'/>" alt="error">
    <center>
        <a href="javascript:;" onclick="pageMove('/')">
            <img src="<c:url value='/resources/image/btn_main.png'/>" alt="home">
        </a>
    </center>
</div>

</body>
</html>