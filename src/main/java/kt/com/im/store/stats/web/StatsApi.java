/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.stats.web;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.common.util.FormatCheckUtil;
import kt.com.im.store.code.service.CodeService;
import kt.com.im.store.code.vo.CodeVO;
import kt.com.im.store.common.util.CommonFnc;
import kt.com.im.store.common.util.ResultModel;
import kt.com.im.store.common.util.Validator;
import kt.com.im.store.content.service.ContentService;
import kt.com.im.store.content.vo.ContentVO;
import kt.com.im.store.member.vo.MemberVO;
import kt.com.im.store.member.web.MemberApi;
import kt.com.im.store.stats.service.StatsService;
import kt.com.im.store.stats.vo.StatsVO;

@Controller
public class StatsApi {
    private String viewName = "../resources/api/stats/statsProcess";

    @Resource(name = "StatsService")
    private StatsService statsService;

    @Resource(name = "ContentService")
    private ContentService contentService;

    @Resource(name = "CodeService")
    private CodeService codeService;

    @Autowired
    MessageSource messageSource;

    /**
     * 통계 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/statistics")
    public ModelAndView processStatistics(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        request.setCharacterEncoding("UTF-8");

        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String validateParams = "type";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return rsModel.getModelAndView();
            }

            String type = request.getParameter("type");
            if ("contentUsage".equals(type)) {
                return this.getContentUsage(request, rsModel, userVO);
            }

            if ("contentUsageDetail".equals(type)) {
                return this.getContentUsageDetail(request, rsModel, userVO);
            }

            if ("contentDownload".equals(type)) {
                return this.getContentDownload(request, rsModel, userVO);
            }

            if ("contentDownloadDetail".equals(type)) {
                return this.getContentDownloadDetail(request, rsModel, userVO);
            }
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 사용 정보 목록조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentUsage(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "cpSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO vo = new StatsVO();
        vo.setCpSeq(Integer.parseInt(request.getParameter("cpSeq")));
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (userVO.getCpSeq() != vo.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));
        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");

        int totalCount = statsService.getContentUsageListTotalCount(vo);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        vo.setOffset(offset);
        vo.setLimit(rows);

        List<StatsVO> resultItem = statsService.getContentUsageList(vo);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultType("contentUsageInfo");
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 사용현황 상세보기 목록조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentUsageDetail(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "contsSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));
        ContentVO contsVO = new ContentVO();
        contsVO.setContsSeq(contsSeq);

        contsVO = contentService.getContentTitle(contsVO);
        if (contsVO == null) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        // CP 관계자의 경우 자신의 회사의 콘텐츠에 대해서만 조회 가능
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (contsVO.getCpSeq() != userVO.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        StatsVO vo = new StatsVO();
        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));
        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");
        vo.setContsSeq(contsSeq);

        rsModel.setData("contsTitle", contsVO.getContsTitle());
        rsModel.setResultType("contentUsageDetailInfo");

        int totalCount = statsService.getContentUsageDetailListTotalCount(vo);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        vo.setOffset(offset);
        vo.setLimit(rows);

        List<StatsVO> resultItem = statsService.getContentUsageDetailList(vo);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 다운로드현황 목록조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentDownload(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {
        rsModel.setViewName("jsonView");
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "cpSeq:{" + Validator.NUMBER + "};"
            + "storSeq:{" + Validator.NUMBER + "};"
            + "startDate:{" + Validator.DATE + "};"
            + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        StatsVO vo = new StatsVO();
        vo.setCpSeq(Integer.parseInt(request.getParameter("cpSeq")));
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (userVO.getCpSeq() != vo.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }

        ContentVO item = new ContentVO();
        item.setGroupingYN("Y");
        List<ContentVO> firstCtgList = contentService.firstCtgList(item);

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));
        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");
        vo.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        vo.setFirstCtgList(firstCtgList);

        // 전체와 카테고리 별 총 갯수 조회
        int totalCount = statsService.getContentDownloadListTotalCount(vo);
        vo.setLimit(totalCount);
        int categorySum[] = new int[firstCtgList.size() + 1];
        List<Map> resultTotalItem = statsService.getContentDownloadList(vo);

        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        vo.setOffset(offset);
        vo.setLimit(rows);

        List<Map> resultItem = statsService.getContentDownloadList(vo);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        for (int i = 0; i < resultTotalItem.size(); i++) {
            Map result = resultTotalItem.get(i);
            categorySum[0] += Integer.parseInt(String.valueOf(result.get("contsTotalDownCount")));
            for (int j = 0; j < firstCtgList.size(); j++) {
                ContentVO contsVO = firstCtgList.get(j);
                categorySum[j + 1] += Integer.parseInt(String.valueOf(result.get(contsVO.getFirstCtgID())));
            }
        }

        for (int i = 0; i < categorySum.length; i++) {
            if (i == 0) {
                rsModel.setData("contsTotalCnt", categorySum[0]);
            } else {
                ContentVO contsVO = firstCtgList.get(i - 1);
                rsModel.setData(contsVO.getFirstCtgID() + "TotalCnt", categorySum[i]);
            }
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setResultData("contentDownloadList", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 다운로드현황 상세보기 목록조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public ModelAndView getContentDownloadDetail(HttpServletRequest request, ResultModel rsModel, MemberVO userVO) throws Exception {

        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String validateParams = "cpSeq:{" + Validator.NUMBER + "};"
            + "storSeq:{" + Validator.NUMBER + "};"
            + "startDate:{" + Validator.DATE + "};"
            + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return rsModel.getModelAndView();
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return rsModel.getModelAndView();
        }

        int cpSeq = Integer.parseInt(request.getParameter("cpSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));

        StatsVO vo = new StatsVO();
        vo.setCpSeq(cpSeq);
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (userVO.getCpSeq() != vo.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return rsModel.getModelAndView();
            }
        }
        String storNm = this.getStoreName(storSeq);
        if (storNm.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        /*
         * page: 현재 페이지, rows: 목록에 출력될 개수, sidx: 정렬할 필드, sord: 정렬
         */
        int currentPage = this.getCurrentPage(request);
        int rows = this.getRowNumber(request);

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));
        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");
        vo.setStorSeq(storSeq);

        rsModel.setData("storNm", storNm);
        rsModel.setResultType("contentDownloadDetailInfo");

        int totalCount = statsService.getContentDownloadDetailListTotalCount(vo);
        int totalPage = (rows > 0) ? (int) Math.ceil((double) totalCount / rows) : 1;

        if (currentPage > totalPage) {
            currentPage = 1;
        }

        int offset = ((currentPage - 1) * rows);
        vo.setOffset(offset);
        vo.setLimit(rows);

        List<StatsVO> resultItem = statsService.getContentDownloadDetailList(vo);
        if (resultItem.isEmpty()) {
            rsModel.setNoData();
            return rsModel.getModelAndView();
        }

        rsModel.setData("totalCount", totalCount);
        rsModel.setData("currentPage", currentPage);
        rsModel.setData("totalPage", totalPage);
        rsModel.setData("item", resultItem);
        return rsModel.getModelAndView();
    }

    /**
     * 통계 엑셀 다운로드 관련 API
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/statistics/download")
    public void downloadStatistics(Locale locale, HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        request.setCharacterEncoding("UTF-8");

        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            String validateParams = "type";
            String invalidParams = Validator.validate(request, validateParams);
            if (!invalidParams.isEmpty()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
                return;
            }

            String type = request.getParameter("type");
            if ("contentUsage".equals(type)) {
                this.getContentUsageExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("contentUsageDetail".equals(type)) {
                this.getContentUsageDetailExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("contentDownload".equals(type)) {
                this.getContentDownloadExcelFile(locale, request, response, rsModel, userVO);
                return;
            }

            if ("contentDownloadDetail".equals(type)) {
                this.getContentDownloadDetailExcelFile(locale, request, response, rsModel, userVO);
                return;
            }
        }

        rsModel.setResponseStatus(ResultModel.HTTP_STATUS_NOT_IMPLEMENTED);
    }

    /**
     * 콘텐츠 사용 정보 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 결과 엑셀 파일
     */
    public void getContentUsageExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "cpSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        StatsVO vo = new StatsVO();
        vo.setCpSeq(Integer.parseInt(request.getParameter("cpSeq")));
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (userVO.getCpSeq() != vo.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));

        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");
        int limit = statsService.getContentUsageListTotalCount(vo);
        vo.setLimit(limit);

        List<StatsVO> resultItem = statsService.getContentUsageList(vo);

        String fileName = messageSource.getMessage("contents.usage.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = {
                             messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("table.category", null, locale),
                messageSource.getMessage("contents.table.name", null, locale),
                messageSource.getMessage("contents.table.playCount", null, locale),
                messageSource.getMessage("contents.table.playTime", null, locale),
                             messageSource.getMessage("contents.table.avgPlayTime", null, locale)
                         };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            StatsVO result = resultItem.get(i);

            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = result.getCtgNm();
            content[i][2] = result.getContsTitle();
            content[i][3] = result.getContsTotalPlayCount();
            content[i][4] = result.getContsTotalPlayTime();
            content[i][5] = result.getContsAvgPlayTime();
        }

        downloadExcel(request, response, fileName, columnTitle, content, null);
    }

    /**
     * 콘텐츠 사용현황 상세보기 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getContentUsageDetailExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "contsSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int contsSeq = Integer.parseInt(request.getParameter("contsSeq"));
        ContentVO contsVO = new ContentVO();
        contsVO.setContsSeq(contsSeq);

        contsVO = contentService.getContentTitle(contsVO);
        if (contsVO == null) {
            rsModel.setNoData();
            return;
        }

        // CP 관계자의 경우 자신의 회사의 콘텐츠에 대해서만 조회 가능
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (contsVO.getCpSeq() != userVO.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        StatsVO vo = new StatsVO();
        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));

        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");
        vo.setContsSeq(contsSeq);
        int limit = statsService.getContentUsageDetailListTotalCount(vo);
        vo.setLimit(limit);

        List<StatsVO> resultItem = statsService.getContentUsageDetailList(vo);
        String fileName = messageSource.getMessage("contents.usage.detail.title", null, locale) + "_" + contsVO.getContsTitle();
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = {
                messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("contents.table.service.name", null, locale),
                messageSource.getMessage("contents.table.store", null, locale),
                messageSource.getMessage("contents.table.playCount", null, locale),
                messageSource.getMessage("contents.table.playTime", null, locale),
                messageSource.getMessage("contents.table.avgPlayTime", null, locale)
        };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            StatsVO result = resultItem.get(i);

            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = result.getSvcNm();
            content[i][2] = result.getStorNm();
            content[i][3] = result.getContsTotalPlayCount();
            content[i][4] = result.getContsTotalPlayTime();
            content[i][5] = result.getContsAvgPlayTime();
        }

        downloadExcel(request, response, fileName, columnTitle, content, null);
    }

    /**
     * 콘텐츠 다운로드현황 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 결과 엑셀 파일
     */
    public void getContentDownloadExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "cpSeq:{" + Validator.NUMBER + "};"
                + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        StatsVO vo = new StatsVO();
        vo.setCpSeq(Integer.parseInt(request.getParameter("cpSeq")));
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (userVO.getCpSeq() != vo.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }

        ContentVO item = new ContentVO();
        item.setGroupingYN("Y");
        List<ContentVO> firstCtgList = contentService.firstCtgList(item);

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));
        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");
        vo.setStorSeq(Integer.parseInt(request.getParameter("storSeq")));
        vo.setFirstCtgList(firstCtgList);
        int limit = statsService.getContentDownloadListTotalCount(vo);
        vo.setLimit(limit);

        List<Map> resultItem = statsService.getContentDownloadList(vo);

        String fileName = messageSource.getMessage("contents.download.title", null, locale);
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        int baseTitleSize = 3;
        int dynamicTitleSize = firstCtgList.size();
        int columnTitleSize = baseTitleSize + dynamicTitleSize;

        int totalSum = 0;
        int[] categorySum = new int[dynamicTitleSize];
        String[] columnTitle = new String[columnTitleSize];
        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            Map result = resultItem.get(i);
            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = (String) result.get("storNm");
            content[i][2] = String.valueOf(result.get("contsTotalDownCount"));
            totalSum += Integer.parseInt(String.valueOf(result.get("contsTotalDownCount")));

            for (int j = 0; j < dynamicTitleSize; j++) {
                ContentVO contsVO = firstCtgList.get(j);
                content[i][baseTitleSize + j] = String.valueOf(result.get(contsVO.getFirstCtgID()));
                categorySum[j] += Integer.parseInt(String.valueOf(result.get(contsVO.getFirstCtgID())));
            }
        }

        columnTitle[0] = messageSource.getMessage("table.num", null, locale);
        columnTitle[1] = messageSource.getMessage("contents.table.store", null, locale);
        columnTitle[2] = messageSource.getMessage("select.all", null, locale) + "(" + totalSum + ")";
        for (int i = 0; i < dynamicTitleSize; i++) {
            ContentVO contsVO = firstCtgList.get(i);
            columnTitle[baseTitleSize + i] = contsVO.getFirstCtgNm() + "(" + categorySum[i] + ")";
        }

        String[][] groupTitle = {
                                     {messageSource.getMessage("table.num", null, locale), "0,1,0,0"},
                                     {messageSource.getMessage("contents.table.store", null, locale), "0,1,1,1"},
                                     {messageSource.getMessage("common.category", null, locale), "0,0,2," + (columnTitleSize - 1)}
                                };

        downloadExcel(request, response, fileName, columnTitle, content, groupTitle);
    }

    /**
     * 콘텐츠 다운로드현황 상세보기 엑셀 다운로드
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    public void getContentDownloadDetailExcelFile(Locale locale, HttpServletRequest request, HttpServletResponse response, ResultModel rsModel, MemberVO userVO) throws Exception {
        boolean prevented = MemberApi.MEMBER_SECTION_ACCEPTOR.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_SERVICE.equals(userVO.getMbrSe())
                || MemberApi.MEMBER_SECTION_STORE.equals(userVO.getMbrSe());
        if (prevented) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return;
        }

        String validateParams = "cpSeq:{" + Validator.NUMBER + "};"
                + "storSeq:{" + Validator.NUMBER + "};"
                + "startDate:{" + Validator.DATE + "};"
                + "endDate:{" + Validator.DATE + "}";
        String invalidParams = Validator.validate(request, validateParams);
        if (!invalidParams.isEmpty()) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(" + invalidParams + ")");
            return;
        }

        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        if (!FormatCheckUtil.checkPeriod(startDate, endDate)) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_BAD_REQUEST, "invalid parameter(endDate)");
            return;
        }

        int cpSeq = Integer.parseInt(request.getParameter("cpSeq"));
        int storSeq = Integer.parseInt(request.getParameter("storSeq"));

        StatsVO vo = new StatsVO();
        vo.setCpSeq(cpSeq);
        if (MemberApi.MEMBER_SECTION_CP.equals(userVO.getMbrSe())) {
            if (userVO.getCpSeq() != vo.getCpSeq()) {
                rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
                return;
            }
        }
        String storNm = this.getStoreName(storSeq);
        if (storNm.isEmpty()) {
            rsModel.setNoData();
            return;
        }

        vo.setSidx(request.getParameter("sidx"));
        vo.setSord(request.getParameter("sord"));
        vo.setTarget(request.getParameter("target"));
        vo.setKeyword(request.getParameter("keyword"));
        vo.setStartDate(startDate + " 00:00");
        vo.setEndDate(endDate + " 23:59");
        vo.setStorSeq(storSeq);
        int limit = statsService.getContentDownloadDetailListTotalCount(vo);
        vo.setLimit(limit);

        List<StatsVO> resultItem = statsService.getContentDownloadDetailList(vo);
        String fileName = messageSource.getMessage("contents.usage.detail.title", null, locale) + "_" + storNm;
        if (startDate.equals(endDate)) {
            fileName += "(" + startDate + ")";
        } else {
            fileName += "(" + startDate + "~" + endDate + ")";
        }

        String[] columnTitle = {
                             messageSource.getMessage("table.num", null, locale),
                messageSource.getMessage("common.category", null, locale),
                messageSource.getMessage("contents.table.name", null, locale),
                messageSource.getMessage("table.download.no", null, locale),
                messageSource.getMessage("table.download.date", null, locale),
                             messageSource.getMessage("table.group.name", null, locale)
                         };

        String[][] content = new String[resultItem.size()][columnTitle.length];
        for (int i = 0; i < resultItem.size(); i++) {
            StatsVO result = resultItem.get(i);

            int rowNum = i + 1;
            content[i][0] = String.valueOf(rowNum);
            content[i][1] = result.getCtgNm();
            content[i][2] = result.getContsTitle();
            content[i][3] = result.getContsDownloadCount();
            content[i][4] = result.getContsDownloadDate();
            content[i][5] = result.getFrmtnNm();
        }

        downloadExcel(request, response, fileName, columnTitle, content, null);
    }

    private void downloadExcel(HttpServletRequest request, HttpServletResponse response, String fileName, String[] columnTitle, String[][] content, String[][] groupTitle) throws Exception {
        XSSFWorkbook workBook = new XSSFWorkbook();

        XSSFFont defaultFontStyle = workBook.createFont();
        defaultFontStyle.setFontHeight(10);
        defaultFontStyle.setColor(HSSFColor.BLACK.index);
        defaultFontStyle.setFontName("맑은 고딕");

        // 기본 스타일
        CellStyle defaultCellStyle = workBook.createCellStyle();
        defaultCellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        defaultCellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        defaultCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        defaultCellStyle.setWrapText(true);// 자동줄바꿈
        defaultCellStyle.setFont(defaultFontStyle);

        // Cell 스타일 생성
        XSSFFont titelFontStyle = workBook.createFont();
        titelFontStyle.setFontHeight(12);
        titelFontStyle.setColor(HSSFColor.BLACK.index);
        titelFontStyle.setFontName("맑은 고딕");
        titelFontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); // 폰트굵게

        // 타이틀 스타일
        CellStyle titleCellStyle = workBook.createCellStyle();
        titleCellStyle.cloneStyleFrom(defaultCellStyle);
        titleCellStyle.setFont(titelFontStyle);
        titleCellStyle.setFillForegroundColor(IndexedColors.TAN.index);
        titleCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

        boolean hasGroupTitle = (groupTitle != null);
        int baseIndex = hasGroupTitle ? 1: 0;
        Sheet sheet = workBook.createSheet();
        for (int i = 0; i < content.length; i++) {
            int rowNum = i + (baseIndex + 1);
            Row row = sheet.createRow(rowNum);
            for (int j = 0; j < content[i].length; j++) {
                Cell cell = row.createCell(j);
                cell.setCellStyle(defaultCellStyle);
                cell.setCellValue(content[i][j]);
            }
        }

        Row headerRow = sheet.createRow(baseIndex);
        for (int i = 0; i < columnTitle.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellStyle(titleCellStyle);
            cell.setCellValue(columnTitle[i]);
            sheet.autoSizeColumn(i);
        }

        if (hasGroupTitle) {
            Row groupRow = sheet.createRow(0);
            for (int i = 0; i < groupTitle.length; i++) {
                String title = groupTitle[i][0];
                String[] range = groupTitle[i][1].split(",");
                if (range.length < 4) {
                    continue;
                }

                int startRowMergeIndex = Integer.parseInt(range[0]);
                int endRowMergeIndex = Integer.parseInt(range[1]);
                int startColumnMergeIndex = Integer.parseInt(range[2]);
                int endColumnMergeIndex = Integer.parseInt(range[3]);

                Cell startCell = groupRow.createCell(startColumnMergeIndex);
                startCell.setCellStyle(titleCellStyle);
                startCell.setCellValue(title);

                Cell endCell = groupRow.createCell(endColumnMergeIndex);
                endCell.setCellStyle(titleCellStyle);
                endCell.setCellValue(title);
                sheet.addMergedRegion(new CellRangeAddress(startRowMergeIndex, endRowMergeIndex, startColumnMergeIndex, endColumnMergeIndex));
            }
        }

        // 엑셀 파일 다운로드
        response.setContentType("Application/octet-stream");
        String userAgent = request.getHeader("User-Agent");
        if (userAgent.indexOf("MSIE 5.5") > -1) { // MS IE 5.5 이하
            response.setHeader("Content-Disposition", "filename=" + (URLEncoder.encode(fileName, "UTF-8")+".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("MSIE") > -1) { // MS IE (보통은 6.x 이상 가정)
            response.setHeader("Content-Disposition", "attachment; filename=" + (java.net.URLEncoder.encode(fileName, "UTF-8")+".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else if (userAgent.indexOf("Trident") > -1 || userAgent.indexOf("Edge") > -1) { // MS IE 11, Edge
            response.setHeader("Content-Disposition", "attachment; filename=" + (java.net.URLEncoder.encode(fileName, "UTF-8")+".xlsx").replaceAll("\\+", "\\ ") + ";");
        } else { // 모질라나 오페라
            response.setHeader("Content-Disposition", "attachment; filename=" + (new String(fileName.getBytes("UTF-8"), "latin1")+".xlsx").replaceAll("\\+", "\\ ") + ";");
        }

        OutputStream os = response.getOutputStream();
        workBook.write(os);
        os.flush();
        os.close();
    }

    /**
     * 콘텐츠 검수현황 통계 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contsStusStat")
    public ModelAndView contsStusProcess(HttpServletRequest request, HttpServletResponse response, HttpSession session)
            throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        StatsVO item = new StatsVO();
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            item.setCpSeq(cpSeq);

            List<StatsVO> resultItem = statsService.contsStusStat(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
            }

            rsModel.setResultType("contsStusStat");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 사용현황 (TOP 5) 통계 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contsUseStus")
    public ModelAndView contsUseStusProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        StatsVO item = new StatsVO();
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            item.setCpSeq(cpSeq);

            List<StatsVO> resultItem = statsService.contsUseStus(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
            }

            rsModel.setResultType("contsUseStus");
        }

        return rsModel.getModelAndView();
    }

    /**
     * 콘텐츠 다운현황 (TOP 5) 통계 관련 API
     *
     * @param request - 요청
     * @param response - 응답
     * @param session - 세션
     * @return modelAndView & API 메세지값 & WebStatus
     */
    @RequestMapping(value = "/api/contsDownTopStus")
    public ModelAndView contsDownTopStusProcess(HttpServletRequest request, HttpServletResponse response,
            HttpSession session) throws Exception {
        request.setCharacterEncoding("UTF-8");
        ResultModel rsModel = new ResultModel(response);
        rsModel.setViewName(viewName);

        StatsVO item = new StatsVO();
        MemberVO userVO = (MemberVO) session.getAttribute("authenticatedUser");
        if (userVO == null) {
            rsModel.setResponseStatus(ResultModel.HTTP_STATUS_UNAUTHORIZED);
            return rsModel.getModelAndView();
        }

        String method = CommonFnc.getMethod(request);
        if (method.equals("GET")) {
            int cpSeq = CommonFnc.emptyCheckInt("cpSeq", request);

            item.setCpSeq(cpSeq);

            List<StatsVO> resultItem = statsService.contsDownTopStus(item);

            if (resultItem.isEmpty()) {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NO_RESULT);
                rsModel.setResultMessage(ResultModel.MESSAGE_NO_DATA);
            } else {
                rsModel.setResultCode(ResultModel.RESULT_CODE_NORMAL);
                rsModel.setResultMessage(ResultModel.MESSAGE_SUCESS);
                rsModel.setStatus(ResultModel.HTTP_STATUS_OK);
                rsModel.setData("item", resultItem);
            }

            rsModel.setResultType("contsDownTopStus");
        }

        return rsModel.getModelAndView();
    }

    private String getStoreName(int storSeq) throws Exception {
        String storNm = "";
        CodeVO item = new CodeVO();
        item.setComnCdCtg("STORE");
        List<CodeVO> codeList = codeService.codeList(item);

        for (int i = 0; i < codeList.size(); i++) {
            CodeVO result = codeList.get(i);
            if (storSeq == Integer.parseInt(result.getComnCdValue())) {
                storNm = result.getComnCdNm();
                break;
            }
        }
        return storNm;
    }

    private int getCurrentPage(HttpServletRequest request) {
        boolean validPageNumber = FormatCheckUtil.checkNumber(request.getParameter("page"));
        int currentPage = (validPageNumber) ? Integer.parseInt(request.getParameter("page")) : 1;
        if (currentPage <= 0) {
            currentPage = 1;
        }
        return currentPage;
    }

    private int getRowNumber(HttpServletRequest request) {
        boolean validRowNumber = FormatCheckUtil.checkNumber(request.getParameter("rows"));
        int rowNumber = (validRowNumber) ? Integer.parseInt(request.getParameter("rows")) : 10;
        return rowNumber;
    }
}
