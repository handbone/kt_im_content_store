/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var cpListData = null;

var fileUploadQueue = new Array();
var curFileUpload;

var coverThumbSizes = coverThumbSizes();
var galleryThumbSizes = galleryThumbSizes();

var coverThumbInfo = [];
var galleryThumbInfo = [];

var metaData = ""; // 메타데이터 정보

var FILE_VIDEO;
var FILE_THUMBNAIL;
var FILE_METADATA;
var FILE_VIDEO;
var FILE_PREV;
var FILE_PREVMETA;
var FILE_PREVCONT;
var FILE_COVERIMG;
var FILE_CONTENTS;
var FILE_FILE1;
var FILE_WEBTOON;
var FILE_SETTINGS    = {
    // Backend Settings
    // upload_url                : makeAPIUrl("/api/fileProcess"),
    upload_url                : makeAPIUrl("/api/file"),

    // Flash Settings
    flash_url                : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

    // File Upload Settings

    // Event Handler Settings (all my handlers are in the Handler.js file)
    file_dialog_start_handler       : fileDialogStart,
    file_queued_handler                : fileQueued,
    file_queue_error_handler        : fileQueueError,
    file_dialog_complete_handler    : fileDialogComplete,
    upload_start_handler            : uploadStart,
    upload_progress_handler         : uploadProgress,
    upload_error_handler            : uploadError,
    upload_success_handler          : uploadSuccess,
    upload_complete_handler         : uploadComplete,

    // Button Settings
    //button_action                    : SWFUpload.BUTTON_ACTION.SELECT_FILE,
    button_action                   : -110,
    button_cursor                   : SWFUpload.CURSOR.HAND,
    // Debug Settings
    debug: false
    };

//swfupload - cover
coverImgUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'coverImg', isThumb : 'false', thumbSizes : coverThumbSizes};
    var file_settings ={};
    file_settings.button_placeholder_id    = "coverImg_btn_placeholder";
    file_settings.button_width = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true, file_settings, FILE_SETTINGS);
    FILE_COVERIMG = new SWFUpload(file_settings);
}

//swfupload - thumbnail
thumbnailUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'thumbnail', isThumb : 'false', thumbSizes : galleryThumbSizes};
    var file_settings    ={};
    file_settings.button_placeholder_id    = "thumbnail_btn_placeholder";
    file_settings.button_width    = 62;
    file_settings.button_height    = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.jpg;*.jpeg;*.png;",
    file_settings.file_queue_limit =  "0";
    file_settings.file_size_limit =  "20MB";
    file_settings.button_image_url    = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,    file_settings,    FILE_SETTINGS);
    FILE_THUMBNAIL    = new SWFUpload(file_settings);
}
//swfupload - video
videoUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'video'};
    var file_settings    ={};
    file_settings.button_placeholder_id    = "video_btn_placeholder";
    file_settings.button_width    = 62;
    file_settings.button_height    = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url    = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,    file_settings,    FILE_SETTINGS);
    FILE_VIDEO    = new SWFUpload(file_settings);
}

//swfupload - PREV
prevUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'prev'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prev_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.mp4;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREV  = new SWFUpload(file_settings);
}

//swfupload - metadata
prevmetaUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'prevmeta', fileSeq : seq};
    var file_settings   ={};
    file_settings.button_placeholder_id = "prevmeta_btn_placeholder"+seq;
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    FILE_PREVMETA   = new SWFUpload(file_settings);
}

//swfupload - metadata
metadataUploadBtn = function(seq){
    //File Upload screenshot
    var temp = {fileSe : 'metadata', fileSeq : seq};
    var file_settings    ={};
    file_settings.button_placeholder_id    = "metadata_btn_placeholder"+seq;
    file_settings.button_width    = 62;
    file_settings.button_height    = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.xml;",
    file_settings.file_queue_limit =  "1";
    file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url    = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,    file_settings,    FILE_SETTINGS);
    FILE_METADATA    = new SWFUpload(file_settings);
}

//swfupload - file1
fileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'file'};
    var file_settings    ={};
    file_settings.button_placeholder_id    = "file_btn_placeholder";
    file_settings.button_width    = 62;
    file_settings.button_height    = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.zip;*.exe;",
    file_settings.file_queue_limit =  "1";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url    = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,    file_settings,    FILE_SETTINGS);
    FILE_FILE1    = new SWFUpload(file_settings);
}

//swfupload - webtoon
webtoonUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'webtoon'};
    var file_settings    ={};
    file_settings.button_placeholder_id    = "webtoon_btn_placeholder";
    file_settings.button_width    = 62;
    file_settings.button_height    = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.zip;",
    file_settings.file_queue_limit =  "1";
    //file_settings.file_size_limit =  "100GB";
    file_settings.button_image_url    = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,    file_settings,    FILE_SETTINGS);
    FILE_WEBTOON = new SWFUpload(file_settings);
}

// 썸네일 미리보기 팝업이 나타나 있을 경우 window 크기에 따른 resize
$(window).resize(function() {
    clearTimeout(window.resizedFinished);
    window.resizedFinished = setTimeout(function() {
        if ($("#previewThumnail").css("display") != "none") {
            var popupX = parseFloat(document.body.clientWidth / 2) - parseFloat((preThumMaxW + 416 )/ 2);
            var popupY = parseFloat(document.body.clientHeight / 2) - parseFloat((preThumMaxH + 240.8) / 2);
            $("#previewThumnail").css({"left":popupX,"top":popupY});
        }
    }, 250);
});

$(document).ready(function(){
    var now=new Date();
    $("#popupRegistSubmetadata").draggable();

    $("#cntrctStDt").val(dateString((new Date(now.getTime()))));
    $("#cntrctFnsDt").val(nextYearDateString((new Date(now.getTime()))));

    $("#cntrctStDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
            $('#cntrctFnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $("#cntrctFnsDt").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selected) {
            $("#cntrctStDt").datepicker("option","maxDate", selected);
        }
    });

    $("#contsTitle").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$("#contsTitle").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#contsTitle").val(temp.replace(re,""));
        }
    });

    $("#contsSubTitle").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$("#contsSubTitle").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#contsSubTitle").val(temp.replace(re,""));
        }
    });

    $("#exeFilePath").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$("#exeFilePath").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#exeFilePath").val(temp.replace(re,""));
        }
    });

    setkeyup();

    $("#btnGalleryCretThumb").hide();
    $("#btnCoverCretThumb").hide();

    Array.prototype.last = function() {return this[this.length-1];}
    contsCtgList();
    coverImgUploadBtn();
    videoUploadBtn();
    thumbnailUploadBtn();
    fileUploadBtn();
    CPList();
    prevUploadBtn();
    webtoonUploadBtn();
});

contsCtgList = function(){
    formData("NoneForm", "GroupingYN", "Y");
    if($("#firstCtg option:selected").val()){
        formData("NoneForm", "firstCtgID", encodeURI($("#firstCtg option:selected").val()));
    }
    callByGet("/api/contentsCategory" , "contsCtgListSuccess", "NoneForm");
}

var _firstLoading = true;
var curYear = new Date().getFullYear(); // 현재 날짜의 년도(ContentsID에서 사용)
contsCtgListSuccess = function(data){
    formDataDelete("NoneForm", "GroupingYN");
    formDataDelete("NoneForm", "firstCtgID");
    metaData = ""; // 카테고리 변경시 메타데이터 값 리셋
    if(data.resultCode == "1000"){
        var contsCtgListHtml = "";
        var firstCtgNmListHtml= "";

        $(data.result.firstCtgList).each(function(i,item) {
            firstCtgNmListHtml += "<option value='"+item.firstCtgID+"' data-id='"+item.firstCtgID+"'>"+item.firstCtgNm+"</option>";
        });

        if(_firstLoading){
            $("#firstCtg").html(firstCtgNmListHtml);
            _firstLoading = false;
        }

        $(data.result.contsCtgList).each(function(i,item) {
            if(i == 0){
                $("#contsID").text(item.firstCtgID + item.secondCtgID + curYear);
            }
            contsCtgListHtml += "<option value='"+item.contsCtgSeq+"' name=\"" + item.firstCtgID + item.secondCtgID + item.ctgCnt + "\">"+item.secondCtgNm+"</option>";
        });

        // 첫번째 카테고리 명이 '영상'이 아닐때
        if ($("#firstCtg option:selected").text().match(getMessage("common.video")) == null) {
            $(".attrPart").show();
            $(".gamePart").show();
            $(".videoPart").hide();
            $(".exeFilePath").show();
            $(".exeFilePath th").text(getMessage("common.run.file.path"));
            $(".webtoonPart").hide();
        } else if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) { // 첫번째 카테고리 명이 '영상'일때
            $(".attrPart").hide();
            $(".gamePart").hide();
            $(".videoPart").show();
            $(".exeFilePath").hide();
            $("#exeFilePath").val("");
            if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){ // 첫번째 카테고리 명이 '영상_LiveOn'일때
                $(".exeFilePath").show();
                $(".exeFilePath th").text(getMessage("common.run.streaming.url"));
            }
        }
        $("#secondCtg").html(contsCtgListHtml);

        GenreList();
    }
}

// 카테고리 변경시 콘텐츠 ID 최신화
contentsIDRefresh = function(){
    var categoryInfo = $("#secondCtg option:selected").attr("name");
    $("#contsID").text(categoryInfo.slice(0,2) + curYear);
}

// 콘텐츠 ID 년도 뒷부분 숫자 앞 빈칸 0으로 채우기
function prependZero(num, len) {
    while(num.toString().length < len) {
        num = "0" + num;
    }
    return num;
}

contentsRegister = function(codeVal){
    if($("#contsTitle").val().trim() == ""){
        popAlertLayer(getMessage("contents.empty.contents.name.msg"));
        return;
    }

    if($("#coverImgArea").children().length <= 0){
        popAlertLayer(getMessage("contents.empty.cover.image.msg"));
        return;
    }

    if($("#thumbnailArea").children().length <= 0){
        popAlertLayer(getMessage("contents.empty.thumbnail.image.msg"));
        return;
    }

    if (coverImgUploadTemp.customSettings.isThumb == "false"
        || thumbnailUploadTemp.customSettings.isThumb == "false") {
        popAlertLayer(getMessage("contents.empty.thumbnail.msg"));
        return;
    }

    if($("#contsSubTitle").val().trim() == ""){
        popAlertLayer(getMessage("contents.empty.sub.title.msg"));
        return;
    }

    if($("#contsDesc").val().trim() == ""){
        popAlertLayer(getMessage("contents.empty.detail.description.msg"));
        return;
    }

    if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) { // 첫번째 카테고리 명이 '영상'일때
        if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){ // 첫번째 카테고리가 LiveOn일 경우
            if($("#exeFilePath").val() == ""){ // 스트리밍 URL정보가 없을 경우
                if(_isHaveComicsValue){ // 영상_LiveOn 카테고리 장르에 코믹이 있을 경우
                    if($("#webtoonArea").children().length <= 0){ // 웹툰 콘텐츠 파일이 없을 경우
                        popAlertLayer(getMessage("contents.empty.webtoon.msg"));
                        return;
                    }
                } else {
                    if($("#videoArea").children().length <= 0){ // 영상 파일이 없을 경우
                        popAlertLayer(getMessage("contents.empty.video.msg"));
                        return;
                    }
                }
            }
        } else {
            if($("#videoArea").children().length <= 0){
                popAlertLayer(getMessage("contents.empty.video.msg"));
                return;
            }
        }
    }

    var genre="";
    $("#genreList .ContentSelectBoxOn").each(function(i, item){
        genre += $(this).attr("data-seq") + ",";
    });

    var service="";
    $("#serviceList .ContentSelectBoxOn").each(function(i, item){
        service += $(this).attr("data-seq") + ",";
    });

    if(!genre){
        popAlertLayer(getMessage("contents.genre.error.msg"));
        return;
    }

    if(!service){
        popAlertLayer(getMessage("contents.service.error.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.regist.msg"), function() {
        if ($("#firstCtg option:selected").text().match(getMessage("common.video")) != null) { // 첫번째 카테고리 명이 '영상'일때
            if(_isHaveComicsValue){ // 영상_LiveOn 카테고리 장르에 코믹이 있을 경우
                formData("NoneForm" , "fileType", "ZIP");
            } else {
                formData("NoneForm" , "fileType", "VIDEO");
            }
        } else {
            formData("NoneForm" , "fileType", $("#exefileType option:selected").val());
        }
        formData("NoneForm" , "contsTitle", $("#contsTitle").val().trim());
        formData("NoneForm" , "contsCtgSeq", $("#secondCtg option:selected").val());
        formData("NoneForm" , "contsID", $("#contsID").text());
        formData("NoneForm" , "exeFilePath", $("#exeFilePath").val());
        formData("NoneForm" , "contsSubTitle", $("#contsSubTitle").val());
        formData("NoneForm" , "contsDesc", $("#contsDesc").val());
        formData("NoneForm" , "maxAvlNop", $("#maxAvlNop").val());
        formData("NoneForm" , "genre", genre.slice(0, -1));
        formData("NoneForm" , "service", service.slice(0, -1));
        formData("NoneForm" , "contsCtgSeq", $("#secondCtg").val());
        formData("NoneForm" , "cpSeq", $("#cpList option:selected").val());
        formData("NoneForm" , "cntrctStDt", $("#cntrctStDt").val() + " 00:00:00");
        formData("NoneForm" , "cntrctFnsDt", $("#cntrctFnsDt").val() + " 23:59:59");
        formData("NoneForm" , "sttus", codeVal);

        callByPost("/api/contents" , "contentsInsertSuccess", "NoneForm");
    }, null, getMessage("common.confirm"));
}

contentsInsertSuccess = function(data){
    formDataDeleteAll("NoneForm");
    if(data.resultCode == "1000"){
        $("#contsSeq").val(data.result.contsSeq);
        if (metaData != "") {
            formData("NonePopup" , "contsSeq", data.result.contsSeq);
            formData("NonePopup" , "metaData", metaData);
            callByPost("/api/metadata" , "metadataInsertSuccess", "NonePopup", "metadataInsertFail");
        } else {
            uploadS();
        }
    }
}

metadataInsertSuccess = function(data){
    formDataDeleteAll("NonePopup");
    uploadS();
}
metadataInsertFail = function(data) {
    formDataDeleteAll("NonePopup");
    // 서브메타데이터 등록이 실패하더라도 파일 등록이 되도록 uploadS() 호출
    uploadS();
}

function uploadS(){
    // 파일 업로드 초기화 - 썸네일 생성 시 동일 코드 사용으로 썸네일 생성 후 등록 시 이전 업로드 객체를 초기화
    if (curFileUpload) {
        curFileUpload = null;
    }

    if(prevUploadTemp != undefined){
        fileUploadQueue.push(prevUploadTemp);
    }

    $(prevmetaUploadTemp).each(function(){
        fileUploadQueue.push(this);
    });

    if(coverImgUploadTemp != undefined){
        coverImgUploadTemp.setUploadURL(makeAPIUrl("/api/thumbFileUpload"));
        fileUploadQueue.push(coverImgUploadTemp);
    }

    if(thumbnailUploadTemp != undefined){
        thumbnailUploadTemp.setUploadURL(makeAPIUrl("/api/thumbFileUpload"));
        fileUploadQueue.push(thumbnailUploadTemp);
    }

    if(videoUploadTemp != undefined){
        fileUploadQueue.push(videoUploadTemp);
    }

    $(metadataUploadTemp).each(function(){
        fileUploadQueue.push(this);
    });

    if(fileTemp != undefined){
        fileUploadQueue.push(fileTemp);
    }

    if(webtoonUploadTemp != undefined){
        fileUploadQueue.push(webtoonUploadTemp);
    }

    startUploadIfNeeded();
}

function startUploadIfNeeded() {
    if (curFileUpload && curFileUpload.getStats().files_queued > 0) {
        return;
    }

    if (!fileUploadQueue || fileUploadQueue.length <= 0) {
        $("#divCenter").hide();
        curFileUpload = null;
        contentsXmlCreate();
        return;
    }

    curFileUpload = fileUploadQueue.shift();
    curFileUpload.startUpload();
}

/*모든파일등록성공시 호출 (xml 등록)*/
contentsXmlCreate = function(){
    if ($("#firstCtg option:selected").text().match(getMessage("common.video")) == null) {
        fileInsertSuccessPopup();
    } else {
        if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){
            if(!_isHaveComicsValue){ // 영상_LiveOn 카테고리 장르에 코믹이 없을 경우
                var videoFormData = "";
                $("table[id ^= videoItem]").each(function(i,item){
                    videoFormData += $(this).find("input[name=videoPath]").val()+",";
                    videoFormData += $(this).find("input[name=metadataPath]").val()+"GNB";
                });

                formData("NoneForm" , "contsSeq", $("#contsSeq").val());
                formData("NoneForm" , "contsID", $("#contsID").text());
                formData("NoneForm" , "videoFormData", videoFormData.slice(0,-3));
                callByPost("/api/contentsXml", "contentsXmlSuccess", "NoneForm" );
            } else {
                fileInsertSuccessPopup();
            }
        } else {
            var videoFormData = "";
            $("table[id ^= videoItem]").each(function(i,item){
                videoFormData += $(this).find("input[name=videoPath]").val()+",";
                videoFormData += $(this).find("input[name=metadataPath]").val()+"GNB";
            });

            formData("NoneForm" , "contsSeq", $("#contsSeq").val());
            formData("NoneForm" , "contsID", $("#contsID").text());
            formData("NoneForm" , "videoFormData", videoFormData.slice(0,-3));
            callByPost("/api/contentsXml", "contentsXmlSuccess", "NoneForm" );
        }
    }
}

contentsXmlSuccess = function(data){
    if(data.resultCode == "1000"){
        fileInsertSuccessPopup();
    }
}

fileInsertSuccess = function(data){
    startUploadIfNeeded();
}

fileInsertFail = function() {
    startUploadIfNeeded();
}

fileInsertSuccessPopup = function() {
    $("#divCenter").hide();

    var msg = getMessage("success.common.insert");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }
    popAlertLayer(msg, "/contents");
}

fileQueueDelete = function(id, e, fileId){
    if (id == "thumbnailArea") {
        $(e).parents(".fileTable").remove();
        /* <br> 삭제 코드 주석 처리
        if ($("#thumbnailArea").find(".fileTable").length == 0){
            $("#thumbnailArea").next().remove();
        }
        */
    }else {
        $("#"+id).html("");
        $("#"+id).hide("");
    }

    if(id == "thumbnailArea"){
        thumbnailUploadTemp.cancelUpload(fileId);

        // 썸네일 정보 및 파일 삭제
        if (galleryThumbInfo.length > 0) {
            deleteThumb(id, fileId);
        }

        // 썸네일 생성이 완료된 모든 항목이 삭제되었을 경우 썸네일 생성 아이콘 값 초기화
        if (thumbnailUploadTemp.getStats().files_queued == 0) {
            $("#btnGalleryCretThumb").hide();
            $("#btnGalleryCretThumb").val(getMessage("common.create.thumbnail"));
            thumbnailUploadTemp.customSettings.isThumb = "false";
        }
    } else if(id == "coverImgArea"){
        coverImgUploadTemp.cancelUpload(fileId);

        // 썸네일 정보 및 파일 삭제
        if (coverThumbInfo.length > 0) {
            deleteThumb(id, fileId);
        }

        // 썸네일 생성이 완료된 모든 항목이 삭제되었을 경우 썸네일 생성 아이콘 값 초기화
        if (coverImgUploadTemp.getStats().files_queued == 0) {
            $("#btnCoverCretThumb").hide();
            $("#btnCoverCretThumb").val(getMessage("common.create.thumbnail"));
            coverImgUploadTemp.customSettings.isThumb = "false";
        }
    } else if(id == "fileArea"){
        fileTemp.cancelUpload(fileId);
    } else if(id == "prevArea"){
        prevUploadTemp.cancelUpload(fileId);
    } else if(id == "videoArea"){
        videoUploadTemp.cancelUpload(fileId);
        metadataUploadTemp.cancelUpload();
    } else if(id == "webtoonArea"){
        webtoonUploadTemp.cancelUpload(fileId);
    }
}

function GenreList(){
    callByGet("/api/contents/genre?firstCtgId="+$("#firstCtg").find(':selected').data("id"), "GenreListSuccess");
}

function GenreListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $("#genreList").html("");
        $(data.result.genreList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.genreSeq+"\" value=\""+item.genreNm+"\" class=\"ContentSelectBox\"/ onclick=\"ContentSelectBoxToggle(this);\"> ";
        });
        $("#genreList").append(menuHtml);
    } else {
        $("#genreList").html("");
    }
}

function ServiceList(){
    var searchCp = "";
    if ($("#cpList").val() != "" && $("#cpList").val() != null) {
        searchCp = "?cpSeq="+$("#cpList").val();

        // 콘텐츠 제공사(CP) 변경 시 콘텐츠 유효기간이 콘텐츠 제공사의 계약 기간을 초과할 수 없도록 종료일시 max 값 설정
        if (cpListData != undefined && cpListData != null) {
            $(cpListData).each(function(i, item) {
                if ($("#cpList").val() == item.cpSeq) {
                    console.log(">> cpNm : " + item.cpNm + ", contFnsDt : " + item.cpContFnsDt);
                    $("#cntrctStDt").datepicker("option", "minDate", item.cpContStDt);
                    $("#cntrctStDt").datepicker("option", "maxDate", item.cpContFnsDt);
                    $("#cntrctFnsDt").datepicker("option", "minDate", item.cpContStDt);
                    $("#cntrctFnsDt").datepicker("option", "maxDate", item.cpContFnsDt);
                }
            });
        }
    }

    callByGet("/api/service"+searchCp, "ServiceListSuccess");
}

function ServiceListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "";
        $(data.result.serviceList).each(function(i, item){
            menuHtml += "<input type=\"button\" data-seq=\""+item.svcSeq+"\" value=\""+item.svcNm+"\" class=\"ContentSelectBox\"/ onclick=\"ContentSelectBoxToggle(this);\"> ";
        });
        $("#serviceList").append(menuHtml);
    }
}

function CPList(){
    callByGet("/api/cp?rows=10000", "CPListSuccess");
}

function CPListSuccess(data){
    if(data.resultCode == "1000"){
        var menuHtml = "<option value=''>== " + getMessage("common.select") + " ==</option>";

        if ($("#memberSec").val() == "06") {
            menuHtml = "";
            $("#cpList").attr("disabled","disabled");
        }

        cpListData = data.result.cpList;
        $(data.result.cpList).each(function(i, item){
            menuHtml += "<option value=\""+item.cpSeq+"\">"+item.cpNm+"</option>";
        });
        $("#cpList").append(menuHtml);

        ServiceList();
    }
}

var _isHaveComicsValue = false;
function ContentSelectBoxToggle(e){
    if ($("#serviceList input.ContentSelectBoxOn").length == 0 && $("#cpList option:selected").val() == "" && $(e).parent().attr("id") == "serviceList"){
        popAlertLayer(getMessage("contents.not.select.cp.msg"));
        return;
    }

    $(e).toggleClass("ContentSelectBoxOn");
    if($("#firstCtg option:selected").text().match(getMessage("common.liveon")) != null){ // 첫번째 카테고리가 LiveOn일 경우
        _isHaveComicsValue = false;
        $("#genreList .ContentSelectBoxOn").each(function(i, item){
            if(item.value == "__COMICS__"){
                _isHaveComicsValue = true;
                return false;
            } else {
                _isHaveComicsValue = false;
            }
        });

        if(_isHaveComicsValue){ // 장르에 코믹이 있을 경우
            $(".webtoonPart").show();
            $(".videoPart").hide();
        } else {
            $(".webtoonPart").hide();
            $(".videoPart").show();
        }
    } else {
        $(".webtoonPart").hide();
    }
}

/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function showMetadataPopLayer() {
    callByGet("/api/codeInfo?comnCdCtg=CONTS_SUB_META", "showMetadataConts", "NoneForm");
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function popLayerClose() {
    $("body").css("overflow-y" , "visible");
    $("#submetadataTableArea").mCustomScrollbar('destroy');
    $.unblockUI();
}

showMetadataConts = function(data) {
    if(data.resultCode == "1000"){
        var tableHtml = "";
        $("#registSubmetadata").html("");
        $(data.result.codeList).each(function(i, item) {
            if(item.comnCdNM == getMessage("submeta.time")){ // '영상상영시간'이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthM onlynum remaining keyupEq\" max=\"200\"> "
                    + getMessage("common.minute") + "</td></tr>";
            } else if(item.comnCdNM == getMessage("submeta.rating")){ // '심의등급'이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\"></th>"
                    + "<td><select class='selectBoxUnselect'><option>" +getMessage("submeta.rating.basic")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.general")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.twelve")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.fifteen")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.RatedR")+ "</option>"
                    + "<option>" +getMessage("submeta.rating.limited")+ "</option></select></td></tr>";
            } else if(item.comnCdNM.indexOf(getMessage("submeta.rt")) != -1){ // 'RT(0'00")'이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthL onlyNumNQuotation remaining\" max=\"200\"> "
                    + "</td></tr>";
            } else if(item.comnCdNM.indexOf(getMessage("submeta.fps")) != -1 || item.comnCdNM.indexOf(getMessage("submeta.gb")) != -1
                    || item.comnCdNM.indexOf(getMessage("submeta.bitrate")) != -1){ // 'FPS' 또는 '용량(GB)' 또는 'Bitrate(mbps)' 이라는 단어가 포함되어 있을 경우
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthL onlyNumNSpot remaining keyupEq\" max=\"200\"> "
                    + "</td></tr>";
            } else {
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">"
                    + "</th><td>" + "<input type=\"text\" class=\"inputBox lengthL remaining keyupEq\" max=\"200\">"
                    + "</td></tr>";
            }
        });

        $("#registSubmetadata").append(tableHtml);

        initDatePicker();

        if (metaData != "") {
            var metaObj = JSON.parse(metaData);
            for(key in metaObj) {
                $("#registSubmetadata tr").each(function(i) {
                    if (key == $(this).find("input[type=hidden]").val()) {
                        if ($(this).find("th").text() == getMessage("submeta.rating")) {
                            $(this).find("option").each(function() {
                                if (this.value == metaObj[key]) {
                                    $(this).attr("selected",true);
                                }
                            });
                        } else if($(this).find("th").text().indexOf(getMessage("submeta.rt")) != -1){
                            // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                            if(metaObj[key].match("#a8484") != null || metaObj[key].match("#q0808") != null){
                                metaObj[key] = metaObj[key].replace("#a8484", "\'");
                                metaObj[key] = metaObj[key].replace("#q0808", "\"");
                            }
                            $(this).find("input[type=text]").val(metaObj[key]);
                        } else {
                            $(this).find("input[type=text]").val(metaObj[key]);
                        }
                    }
                });
            }
        }
    } else {
        $("#registSubmetadata").html("");
    }

    keyup();

    $(".keyupEq").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$(this).val();
        if (re.test(temp)) { // 특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $(this).val(temp.replace(re,""));
        }
    });

    $(".onlyNumNQuotation").bind("keyup",function(){ // 숫자, ', " 만 입력가능
        re = /[^\d\'\"]/gi;
        var temp=$(this).val();
        if (re.test(temp)) { // 특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $(this).val(temp.replace(re,""));
        }
    });

    $(".onlyNumNSpot").bind("keyup",function(){ // 숫자, . 만 입력가능
        re = /[^\d\.]/gi; //숫자와 온점 말고는 다 삭제
        var temp=$(this).val();
        if (re.test(temp)) { // 특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $(this).val(temp.replace(re,""));
        }
    });

    setkeyup();

    $("#popupRegistSubmetadata").css({"left":"-1000px","top":"-1000px"}).show();
    var popupHeight = $("#popupRegistSubmetadata").height();
    var popupWidth = $("#popupRegistSubmetadata").width();
    $("#popupRegistSubmetadata").hide();
    popLayerDiv("popupRegistSubmetadata", popupWidth, popupHeight, true);
    $("#submetadataTableArea").mCustomScrollbar({ axis : "y", theme:"inset-3" });
}

//메타데이터 등록
registMetadata = function() {
    var metaStr = "";
    $("#registSubmetadata tr").each(function(i){
        metaStr += "\"";
        metaStr += $(this).find("input[value]").val();
        metaStr += "\":\"";
        if ($(this).find("select").length == 1) {
            var optionVal = $(this).find("select option:selected").val();
            if(optionVal.indexOf(getMessage("submeta.rating.basic")) == -1){
                metaStr += $(this).find("select option:selected").val();
            }
        } else if($(this).find("th").text().indexOf(getMessage("submeta.rt")) != -1){
            var rtValue = $(".onlyNumNQuotation").val();
            if(rtValue.match("\'") != null || rtValue.match("\"") != null){
                // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                rtValue = rtValue.replace("\'", "#a8484"); // 작은 따옴표 입력시 #a8484로 변환하여 DB 저장
                rtValue = rtValue.replace("\"", "#q0808"); // 큰 따옴표 입력시 #q0808로 변환하여 DB 저장
            }
            $(".onlyNumNQuotation").val(rtValue);
            metaStr += $(this).find("input[type=text]").val();
        } else {
            metaStr += $(this).find("input[type=text]").val();
        }
        metaStr += "\"";
        var trLength = $("#registSubmetadata tr").length-1;
        if (trLength != i) {
            metaStr += ",";
        }
    });
    metaData = "{" + metaStr + "}";
    popLayerClose();
}

initDatePicker = function() {
    $(".scheduledatepicker").datepicker({
        dateFormat:'yy-mm-dd',
        monthNamesShort:[
            '1'+getMessage("datePciker.day.mon"),
            '2'+getMessage("datePciker.day.mon"),
            '3'+getMessage("datePciker.day.mon"),
            '4'+getMessage("datePciker.day.mon"),
            '5'+getMessage("datePciker.day.mon"),
            '6'+getMessage("datePciker.day.mon"),
            '7'+getMessage("datePciker.day.mon"),
            '8'+getMessage("datePciker.day.mon"),
            '9'+getMessage("datePciker.day.mon"),
            '10'+getMessage("datePciker.day.mon"),
            '11'+getMessage("datePciker.day.mon"),
            '12'+getMessage("datePciker.day.mon")
        ],
        dayNamesMin:[
            getMessage("datePicker.day.sun"),
            getMessage("datePciker.day.mon"),
            getMessage("datePicker.day.tue"),
            getMessage("datePicker.day.wed"),
            getMessage("datePicker.day.thu"),
            getMessage("datePicker.day.fri"),
            getMessage("datePicker.day.sat")
        ],
        changeMonth:true,
        changeYear:true,
        showMonthAfterYear:true,
        // timepicker 설정
        controlType:'select',
        oneLine:true,
        onSelect: function(selectedDate) {
        //    $('#cntrctFnsDt').datepicker('option', 'minDate', selectedDate);
        }
    });

    $(".scheduledatepicker").datepicker('setDate', 'today');
    $('#ui-datepicker-div').addClass('blockMsg');
}

var queueLen = 0;
var queueTotLen = 0;
function createThumbImg(thumbSe) {
    $("#loading").show().css("background", "#f9f9f9");
    $("#loading").append("<p class='thumbCretLoading'>" + getMessage("common.creating.thumbnail") + "</p>");

    if (curFileUpload) {
        curFileUpload = null;
    }

    if (thumbSe == "coverImgArea") {
        // 썸네일 재생성 시 썸네일 생성 flag인 isThumb를 초기화
        if (coverImgUploadTemp.customSettings.isThumb == "true") {
            deleteThumb(thumbSe, "");
            return;
        } else {
            queueLen = coverImgUploadTemp.getStats().files_queued;
            queueTotLen = coverImgUploadTemp.getStats().files_queued;
            coverImgUploadTemp.setUploadURL(makeAPIUrl("/api/cretThumb"));
            fileUploadQueue.push(coverImgUploadTemp);
        }
    } else if (thumbSe == "thumbnailArea") {
        // 썸네일 재생성 시 썸네일 생성 flag인 isThumb를 초기화
        if (thumbnailUploadTemp.customSettings.isThumb == "true") {
            deleteThumb(thumbSe, "");
            return;
        } else {
            queueLen = thumbnailUploadTemp.getStats().files_queued;
            queueTotLen = thumbnailUploadTemp.getStats().files_queued;
            thumbnailUploadTemp.setUploadURL(makeAPIUrl("/api/cretThumb"));
            fileUploadQueue.push(thumbnailUploadTemp);
        }
    }

    startUploadIfNeeded();
}

createThumbImgFail = function(data) {
    $("#loading").hide().css("background", "initial");
    $(".thumbCretLoading").remove();

    popAlertLayer(getMessage("common.fail.creating.thumbnail"));
}

deleteThumbList = function(thumbSe, fileId, thumbInfo) {
    var delThumbList = "";

    if (fileId == "") {
        for (var i = 0; i < thumbInfo.length; i++) {
            delThumbList += thumbInfo[i].thumbList.substring(1, thumbInfo[i].thumbList.length - 1);
            delThumbList += ", ";
        }
    } else {
        for (var i = 0; i < thumbInfo.length; i++) {
            if (thumbInfo[i].fileId == fileId) {
                delThumbList += thumbInfo[i].thumbList.substring(1, thumbInfo[i].thumbList.length - 1);
            }
        }
    }

    return delThumbList;
}

deleteThumb = function(thumbSe, fileId) {
    var delThumbList = "";

    if (thumbSe == "coverImgArea") {
        delThumbList = deleteThumbList(thumbSe, fileId, coverThumbInfo);
    } else if (thumbSe == "thumbnailArea") {
        delThumbList = deleteThumbList(thumbSe, fileId, galleryThumbInfo);
    }

    if (fileId != "") {
        $("#loading").show().css("background", "#f9f9f9");
        $("#loading").append("<p class='thumbDelLoading'>" + getMessage("common.delete.prev.thumbnail") + "</p>");
        formData("NoneForm", "fileId", fileId);
        formData("NoneForm", "delThumbList", delThumbList);
    } else {
        formData("NoneForm", "delThumbList", delThumbList.slice(0, -2));
    }
    formData("NoneForm", "thumbSe", thumbSe);
    callByPost("/api/delThumb", "delThumbSuccess", "NoneForm", "delThumbFail");
}

deleteThumbInfo = function(thumbSe, fileId, thumbInfo) {
    var delIndexStr = "";

    for (var i = 0; i < thumbInfo.length; i++) {
        if (thumbInfo[i].fileId == fileId) {
            delIndexStr = String(i);
            break;
        }
    }

    if (delIndexStr != "") {
        thumbInfo.splice(parseInt(delIndexStr), 1);
    }
}

delThumbSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        if (data.result.delThumbInfo.fileId == undefined) {
            if (data.result.delThumbInfo.thumbSe == "coverImgArea") {
                coverImgUploadTemp.customSettings.isThumb = "false";
                coverThumbInfo = [];
            } else if (data.result.delThumbInfo.thumbSe == "thumbnailArea") {
                thumbnailUploadTemp.customSettings.isThumb = "false";
                galleryThumbInfo = [];
            }

            createThumbImg(data.result.delThumbInfo.thumbSe);
        } else {
            if (data.result.delThumbInfo.thumbSe == "coverImgArea") {
                deleteThumbInfo("coverImgArea", data.result.delThumbInfo.fileId, coverThumbInfo);
            } else if (data.result.delThumbInfo.thumbSe == "thumbnailArea") {
                deleteThumbInfo("thumbnailArea", data.result.delThumbInfo.fileId, galleryThumbInfo);
            }
            $("#loading").hide().css("background", "initial");
            $(".thumbDelLoading").remove();
        }
    } else {
        $("#loading").hide().css("background", "initial");
        $(".thumbDelLoading").remove();
        popAlertLayer(getMessage("common.fail.delete.thumbnail"));
    }
}

delThumbFail = function(data) {
    $("#loading").hide().css("background", "initial");
    $(".thumbDelLoading").remove();
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("common.fail.delete.thumbnail"));
}

var preThumMaxW = 0;
var preThumMaxH = 0;
function previewThumbnail(thumbSe, fileId) {
    $("#thumbView").html("");
    var tableHtml = "<table class='thumGrp'><tr>";
    if (thumbSe == "coverImgArea") {
        for (var i = 0; i < coverThumbInfo.length; i++) {
            if (coverThumbInfo[i].fileId == fileId) {
                $("#originFile").html(getMessage("common.origin.file.name") + " : " + coverThumbInfo[i].coverImgNm);
                var thumbList = coverThumbInfo[i].thumbList.substring(1, coverThumbInfo[i].thumbList.length - 1).split(", ");
                for (var j = thumbList.length - 1; j > 0; j--) { // 0번은 원본이미지 이므로 제외
                    var fileNmArr = thumbList[j].split(".");
                    var fileInfoArr = fileNmArr[0].split("_");
                    var fileSizeArr = fileInfoArr[fileInfoArr.length - 1].split("x");

                    tableHtml += "<td><img src='" + makeAPIUrl(thumbList[j], "img") + "'><br>" + fileSizeArr[0] + " x " + fileSizeArr[1] + " (px)" + "</td>";

                    if(preThumMaxW <= parseInt(fileSizeArr[0])) {
                        preThumMaxW = parseInt(fileSizeArr[0]);
                    }
                    if(preThumMaxH <= parseInt(fileSizeArr[1])) {
                        preThumMaxH = parseInt(fileSizeArr[1]);
                    }
                }
            }
        }
    } else {
        for (var i = 0; i < galleryThumbInfo.length; i++) {
            if (galleryThumbInfo[i].fileId == fileId) {
                $("#originFile").html(getMessage("common.origin.file.name") + " : " + galleryThumbInfo[i].thumbnailNm);

                var thumbList = galleryThumbInfo[i].thumbList.substring(1, galleryThumbInfo[i].thumbList.length - 1).split(", ");
                for (var j = thumbList.length - 1; j > 0; j--) { // 0번은 원본이미지 이므로 제외
                    var fileNmArr = thumbList[j].split(".");
                    var fileInfoArr = fileNmArr[0].split("_");
                    var fileSizeArr = fileInfoArr[fileInfoArr.length - 1].split("x");

                    tableHtml += "<td><img src='" + makeAPIUrl(thumbList[j], "img") + "'><br>" + fileSizeArr[0] + " x " + fileSizeArr[1] + " (px)" + "</td>";

                    if(preThumMaxW <= parseInt(fileSizeArr[0])) {
                        preThumMaxW = parseInt(fileSizeArr[0]);
                    }
                    if(preThumMaxH <= parseInt(fileSizeArr[1])) {
                        preThumMaxH = parseInt(fileSizeArr[1]);
                    }
                }
            }
        }
    }

    tableHtml += "</tr></table>";
    $("#thumbView").append(tableHtml);
    $("#thumbView").show();
    popLayerDiv("previewThumnail", 500, 300, true);
}

function closePreviewThumbnail() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}
