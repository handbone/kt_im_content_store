/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.genre.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import kt.com.im.store.common.dao.mysqlAbstractMapper;
import kt.com.im.store.genre.vo.GenreVO;

/**
 *
 * 회원 관리에 관한 데이터 접근 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.30
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 30.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Repository("GenreDAO")
public class GenreDAOImpl extends mysqlAbstractMapper implements GenreDAO {

    /**
     * 장르 목록 정보 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public List<GenreVO> getGenreList(GenreVO vo) throws Exception {
        return selectList("GenreDAO.selectGenreList", vo);
    }

    /**
     * 장르 목록 수 조회
     *
     * @param GenreVO
     * @return 조회 목록 결과
     */
    @Override
    public int getGenreListTotalCount(GenreVO vo) throws Exception {
        return selectOne("GenreDAO.selectGenreListTotalCount", vo);
    }
}
