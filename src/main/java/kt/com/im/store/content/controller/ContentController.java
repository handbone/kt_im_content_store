/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.content.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import kt.com.im.store.member.web.MemberApi;

/**
 *
 * 콘텐츠 관련 페이지 컨트롤러
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 *      <pre>
* << 개정이력(Modification Information) >>
* 수정일      수정자           수정내용
* -------    -------------    ----------------------
* 2018. 5. 2.   A2TEC      최초생성
* 2018. 5. 14.  A2TEC      콘텐츠 검수결과 확인 페이지 이동 메소드 추가 (/content/sttus)
 *
 *
 *      </pre>
 */
@Component
@Controller
public class ContentController {
    /**
     * 콘텐츠 리스트 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents", method = RequestMethod.GET)
    public ModelAndView getContentsList(ModelAndView mv) {
        mv.setViewName("/views/contents/ContentList");
        return mv;
    }

    /**
     * 콘텐츠 상세 정보 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/{contsSeq}", method = RequestMethod.GET)
    public ModelAndView contentsView(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/contents/ContentDetail");
        return mv;
    }

    /**
     * 콘텐츠 수정 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @param contsSeq - 콘텐츠 번호값 저장, JSP 변수값으로 전달
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/{contsSeq}/edit", method = RequestMethod.GET)
    public ModelAndView contentsEdit(ModelAndView mv, @PathVariable(value = "contsSeq") String contsSeq) {
        mv.addObject("contsSeq", contsSeq);
        mv.setViewName("/views/contents/ContentEdit");
        return mv;
    }

    /**
     * 콘텐츠 등록 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/regist", method = RequestMethod.GET)
    public ModelAndView contentsRegist(ModelAndView mv) {
        mv.setViewName("/views/contents/ContentRegist");
        return mv;
    }

    /**
     * 콘텐츠 검수 결과 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     *
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/sttus", method = RequestMethod.GET)
    public ModelAndView contentsSttusList(ModelAndView mv) {
        mv.setViewName("/views/contents/ContentSttusList");
        return mv;
    }

    /**
     * 카테고리 리스트 화면
     * 
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/category", method = RequestMethod.GET)
    public ModelAndView contentsCategoryList(ModelAndView mv) {
        mv.setViewName("/views/contents/CategoryList");
        return mv;
    }

    /**
     * 블록 체인 통계 화면
     *
     * @param mv - 화면 정보를 저장하는 변수
     * @return 모델 정보와 뷰 정보를 반환
     */
    @RequestMapping(value = "/contents/blockStats", method = RequestMethod.GET)
    public ModelAndView blockStats(ModelAndView mv,HttpSession session) {
        if (session != null && session.getAttribute("id") != null) {
            if (MemberApi.MEMBER_SECTION_CMS.equals(session.getAttribute("memberSec")) || MemberApi.MEMBER_SECTION_MASTER.equals(session.getAttribute("memberSec"))) {
                mv.setViewName("/views/contents/blockStats");
            } else {
                mv.setViewName("redirect:/home");
            }
            return mv;
        }
        return mv;
    }

}
