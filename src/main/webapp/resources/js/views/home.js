/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */


var chart = []; //차트정보 객체
$(document).ready(function() {

    cpList();

});

cpList = function(){
    callByGet("/api/cp?rows=10000","cpListSuccess","NoneForm");
}

cpListSuccess = function(data){
    if (data.resultCode == "1000") {
        var cpListHtml = "<option value=''>"+getMessage("select.all")+"</option>";
        $(data.result.cpList).each(function(i, item) {
            cpListHtml += "<option value=\""+item.cpSeq+"\">"+item.cpNm+"</option>";
        });
        $("#cpSelbox").html(cpListHtml);
    }

    if ($("#mbrSe").val() != "01" && $("#mbrSe").val() != "02" ) {
        $("#cpSelbox option").not("[value="+$("#cpSeq").val()+"]").remove();
        $("#cpSelbox").attr('disabled', 'true');
    }


    contentStateList();
    contentUseStateList();
    contentDownStateList();
}

cpStatsReload = function(){
    $(".first-chart-container").html("");
    $(".second-chart-container").html("");
    $('.third-chart-container').html("");

    for(var j=1; j<chart.length; j++) {
        chart[j].destroy();
    }

    ary_donut_count = 0;
    ary_useBar_count = 0;
    ary_useBar_second = 0;
    ary_donut = new Array();
    ary_useBar = new Array();

    storList = new Array();
    ctgList = new Array();
    dataList = new Array();
    chartDate = new Array();
    totalAry = new Array();

    type1_cnt = 0;
    type2_cnt = 0;
    maxCount = 0;

    contentStateList();
    contentUseStateList();
    contentDownStateList();
}

var ary_donut_count = 0;
var ary_useBar_count = 0;
var ary_useBar_second = 0;

//STTUS, STTUS_NM, COUNT, BACKGROUND
var ary_donut = new Array();
var ary_useBar = new Array();


contentStateList = function(){
    callByGet("/api/contsStusStat?cpSeq="+$("#cpSelbox").val(),"contentStateListSuccess","NoneForm");
}

contentUseStateList = function(){
    callByGet("/api/contsUseStus?cpSeq="+$("#cpSelbox").val(),"contsUseStusListSuccess","NoneForm");
}

contentDownStateList = function(){
    callByGet("/api/contsDownTopStus?cpSeq="+$("#cpSelbox").val(),"contsDownTopStusListSuccess","NoneForm");
}

contentStateListSuccess = function(data){
    if (data.resultCode == "1000") {
        var randomR = Math.floor((Math.random() * 130) + 100);
        var randomG = Math.floor((Math.random() * 130) + 100);
        var randomB = Math.floor((Math.random() * 130) + 100);
        var graphBackground = "rgb("
                + randomR + ", "
                + randomG + ", "
                + randomB + ")";

        $(data.result.contsStusStat).each(function(i, item) {
            ary_donut.push({"STTUS":item.sttus,"STTUS_NM":item.sttusNm,"COUNT":item.count,"COLOR":graphBackground});
            ary_donut_count += item.count;
        });

        graphDounutCreate();
    }
}
var storList = new Array();
var ctgList = new Array();
var dataList = new Array();
var chartDate = new Array();
var totalAry = new Array();
var maxCount = 0;
contsDownTopStusListSuccess = function(data){
    if (data.resultCode == "1000") {
        $(data.result.contsDownTopStus).each(function(i, item) {
            if (jQuery.inArray(item.firstCtgNm, ctgList ) == -1){
                ctgList.push(item.firstCtgNm);
            }

            if (jQuery.inArray(item.storNm, storList ) == -1){
                storList.push(item.storNm);
            }

            dataList.push({"STOR_SEQ":item.storSeq,"STOR_NM":item.storNm,"CTG_NM":item.firstCtgNm,"COUNT":item.count});
        });

        var graphBackground = new Array();


        for(var j=0; j<ctgList.length+1; j++){
            var randomR = Math.floor((Math.random() * 130) + 100);
            var randomG = Math.floor((Math.random() * 130) + 100);
            var randomB = Math.floor((Math.random() * 130) + 100);

            var chkColor = "rgb("
                + randomR + ", "
                + randomG + ", "
                + randomB + ")";


            while(1){
                if (graphBackground.indexOf(chkColor) == -1) {
                    graphBackground.push(chkColor);
                    break;
                } else {
                    randomR = Math.floor((Math.random() * 130) + 100);
                    randomG = Math.floor((Math.random() * 130) + 100);
                    randomB = Math.floor((Math.random() * 130) + 100);

                    chkColor = "rgb("
                        + randomR + ", "
                        + randomG + ", "
                        + randomB + ")";
                }
            }


        }
        for(var i=0; i<ctgList.length; i++) {
            var countList = new Array();
            for(var j=0; j<storList.length; j++){
                countList.push(dataList.filter(function(x){return x.STOR_NM === storList[j];}).filter(function(x){ return x.CTG_NM === ctgList[i]})[0].COUNT);
            }



            chartDate.push({
                backgroundColor:graphBackground[i],
                label:ctgList[i],
                data:countList,
                type:"bar",
                borderColor:graphBackground
            })
        }

        for(var j=0; j<storList.length; j++){
            var sum =0;
            dataList.filter(function(x) { return x.STOR_NM === storList[j];}).forEach(function(num){
                sum+=num.COUNT
            });
            totalAry.push(sum);

            if (maxCount <= sum) {
                maxCount = sum;
            }
        }


        chartDate.unshift({backgroundColor:graphBackground[ctgList.length],
            label:getMessage("select.all"),
            data:totalAry,
            type:"bar",
            borderColor:graphBackground})

        var titleName = getMessage("home.stat.download.top5.title");
        options = {
            responsive: true,
            maintainAspectRatio: false,
            type: 'bar',
            data: {
                labels: storList,
                datasets: chartDate
            },
            options: {
                maintainAspectRatio: false,
                hover: {
                    animationDuration: 0
                },
                animation: {
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                        ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                },
               tooltips: {
                   intersect: false,
                   enabled: false,
                   mode: 'label'
                  },
                title: {
                    text: titleName,
                    display: false,
                    fontSize: 18
                },
                legend: {
                    position:'bottom'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        ticks:{
                            fontSize: 14
                        }
                    }],
                    yAxes: [
                            {
                                scaleLabel: {
                                    display: true,
                                    labelString: "",
                                    fontFamily: "Montserrat",
                                    fontColor: "black",
                                },
                                ticks: {
                                    fontFamily: "Montserrat",
                                    min: 0,
                                    max : Math.ceil(maxCount+(maxCount/5)),
                                    callback: function(val) {
                                        if(!is_number(val)){
                                            return "";
                                        }
                                        return val;
                                    }
                                },

                            }
                        ],
                },
            }
        };

        $('#chart_bar').remove(); // this is my <canvas> element
        $('.third-chart-container').append('<canvas id="chart_bar" height="300"></canvas>');
        var ctx = document.getElementById("chart_bar").getContext("2d");
        ctx.clearRect(0, 0, 300, 300);
         // 컨텍스트 리셋
        ctx.beginPath();
        ctx.height = 300;

        type1_cnt++;

        chart[type1_cnt] = new Chart(ctx, options);
    }
}

contsUseStusListSuccess = function(data){
    if (data.resultCode == "1000") {
        var randomR = Math.floor((Math.random() * 130) + 100);
        var randomG = Math.floor((Math.random() * 130) + 100);
        var randomB = Math.floor((Math.random() * 130) + 100);
        var graphBackground = "rgb("
                + randomR + ", "
                + randomG + ", "
                + randomB + ")";

            $(data.result.contsUseStus).each(function(i, item) {
                ary_useBar.push({"SECOND":item.second,"TITLE":item.contsTitle,"COUNT":item.count,"COLOR":graphBackground});
                ary_useBar_second += item.second;
                if(ary_useBar_count <= item.count) {
                    ary_useBar_count = item.count;
                }

            });

            graphUseBarCreate();

    }
}

var type1_cnt = 0;
var type2_cnt = 0;
var canvasType_1 = "<canvas class='chart_donut'></canvas>";
var canvasType_2 = "<div class='chartBox'><div class='contsBox'><div class='percentBox'></div></div></div>";


var options;
graphDounutCreate = function(){
    ary_donut.unshift({"STTUS":"00","STTUS_NM":getMessage("select.all"),"COUNT":ary_donut_count,"COLOR":ary_donut[type1_cnt].COLOR});
    for(var i=0; i<ary_donut.length; i++) {
        var e = $(canvasType_1);
        $(e).attr('id',"doutCht_"+type1_cnt);
        $('.first-chart-container').append(e);
        $("#doutCht_"+type1_cnt).wrap("<div style='display: inline-block;' id=\"douChtDiv_"+type1_cnt+"\" class='douChtDiv'></div>");
        $("#doutCht_"+type1_cnt).before("<p class='douChtTitle'>"+ary_donut[type1_cnt].STTUS_NM+"</p>");
        var avg = (ary_donut[type1_cnt].COUNT/ary_donut_count) * 100;
        if (isNaN(avg)){
            avg = 0;
        }
        var ctx = document.getElementById("doutCht_"+type1_cnt).getContext("2d");
        var data = {labels: [ary_donut[type1_cnt].STTUS_NM,"OTHER"],
                datasets: [
                  {
                    data: [avg,100-avg],
                    backgroundColor: [ary_donut[type1_cnt].COLOR],
                    hoverBackgroundColor: [ary_donut[type1_cnt].COLOR]
                  }]
              };
        type1_cnt++;

        options = {
        type: 'doughnut',
        animation: {
            animateRotate: true,
            animateScale: true
         },
        data: data,
        options: {
          cutoutPercentage: 85,
          responsive: true,
          legend: {
            display: false
          },
          tooltips: {
                        callbacks: {
                          label: function(tooltipItem, data) {
                              var dataset = data.datasets[tooltipItem.datasetIndex];
                            var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                              return previousValue + currentValue;
                            });
                            var currentValue = dataset.data[tooltipItem.index];
                            var precentage = (((currentValue/total) * 100)).toFixed(1);
                            return data.labels[tooltipItem.index]+" : " + precentage + "%";
                          }
                        },
                        titleFontSize: 16,
                        bodyFontSize: 16,
                        enabled: false
                      }
        },
        plugins: [{
            beforeDraw: function(chart) {
                var str = chart.chart.canvas.id;
                var width = chart.chart.width,
                    height = chart.chart.height,
                    ctx = chart.chart.ctx;

                ctx.restore();
                var fontSize = (height / 150).toFixed(2);
                ctx.font = fontSize + "em sans-serif";
                ctx.textBaseline = "middle";

                var text = ary_donut[str.charAt(str.length-1)].COUNT.toString(),
                    textX = Math.round((width - ctx.measureText(text).width) / 2),
                    textY = height / 2;

                ctx.fillText(text, textX, textY);
                text = "("+((ary_donut[str.charAt(str.length-1)].COUNT/ary_donut_count)*100).toFixed(1)+"%)";

                if (((ary_donut[str.charAt(str.length-1)].COUNT/ary_donut_count)*100).toFixed(1) == "NaN") {
                    text = "(0.0%)";
                }

                textX = Math.round((width - ctx.measureText(text).width) / 2);
                ctx.fillText(text, textX, textY+30);

                ctx.save();

              }
        }]

        }

        Chart.defaults.global.tooltips.custom = function(tooltip) {
            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-tooltip');

            // Hide if no tooltip
            if (tooltip.opacity === 0) {
                tooltipEl.style.opacity = 0;
                return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltip.yAlign) {
                tooltipEl.classList.add(tooltip.yAlign);
            } else {
                tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
                return bodyItem.lines;
            }

            // Set Text
            if (tooltip.body) {
                var titleLines = tooltip.title || [];
                var bodyLines = tooltip.body.map(getBody);

                var innerHtml = '<thead>';

                titleLines.forEach(function(title) {
                    innerHtml += '<tr><th>' + title + '</th></tr>';
                });
                innerHtml += '</thead><tbody>';
                bodyLines.forEach(function(body, i) {
                    var colors = tooltip.labelColors[i];
                    var style = 'background:' + colors.backgroundColor;
                    style += '; border-color:' + colors.borderColor;
                    style += '; border-width: 2px';
                    var span = '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
                    innerHtml += '<tr><td>' + span + body + '</td></tr>';
                });
                innerHtml += '</tbody>';

                var tableRoot = tooltipEl.querySelector('table');
                tableRoot.innerHTML = innerHtml;
            }

            var positionY = this._chart.canvas.offsetTop;
            var positionX = this._chart.canvas.offsetLeft;
            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            tooltipEl.style.left = positionX + tooltip.caretX + 'px';
            tooltipEl.style.top = positionY + tooltip.caretY - 50 + 'px';
            tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
            //tooltipEl.style.fontSize = 12;
            tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
            //tooltipEl.style.padding = 4 + 'px ' + 6 + 'px';
        };

        ctx.clearRect(0, 0, 200, 200);
        ctx.beginPath();
        chart[type1_cnt] = new Chart(ctx, options);
    }
}

graphUseBarCreate = function(){
    for(var i=0; i<ary_useBar.length; i++) {
        $('.second-chart-container').append(canvasType_2);
        var hour = Math.floor(ary_useBar[i].SECOND/3600);
        var minute = Math.floor((ary_useBar[i].SECOND%3600)/60);
        var stx = "";
        if (hour > 0){
            stx += hour+"h";
        }

        if(minute > 0) {
            stx += minute+"m";
        }

        var totalMinute = (hour*60)+minute;

        var perPxHeight = (((ary_useBar[i].COUNT/ary_useBar_count) * 100)/100) * 151;


        $(".percentBox").eq(i).animate({opacity: 1,height: ((ary_useBar[i].COUNT/ary_useBar_count) * 100)+"%"}, 1000, function() {}).append("<p class='useStatTxt'>"+ary_useBar[i].COUNT+getMessage("common.cnt")+"<br> ("+stx+")</p>");
        if (perPxHeight <= 30 || isNaN(perPxHeight)) {
            $(".percentBox").eq(i).find(".useStatTxt").css("top",perPxHeight - 30);
        }


        $(".chartBox").eq(i).append("<p class='useStatBottmTxt'>"+ary_useBar[i].TITLE+"<br />("+getMessage("common.average")+" "+(totalMinute/ary_useBar[i].COUNT).toFixed(2)+getMessage("common.minute")+")</p>");
    }
}

