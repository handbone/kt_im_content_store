/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var showModifyPopup = false;
var showVerifyPopup = false;
var sttusValue = "";

var metaData = ""; // 서브메타데이터 정보

$(document).ready(function(){
    contentInfo();
    $("#popupDetailSubmetadata").draggable();
    // 카테고리, 장르 목록에서 콘텐츠 상세정보 화면 진입 시에 대한 예외처리
    if (sessionStorage.getItem("state") == "view") {
        if (sessionStorage.getItem("last-url").indexOf("/contents/category") != -1
         || sessionStorage.getItem("last-url").indexOf("/contents/genre") != -1) {
            $(".btnModify").hide();
            $(".btnDelete").hide();
            $("#sttusBtn").hide();
            if (sessionStorage.getItem("last-url").indexOf("/contents/category") != -1) {
                $("#leftSubMenuNum").val("4");
            } else if (sessionStorage.getItem("last-url").indexOf("/contents/genre") != -1) {
                $("#leftSubMenuNum").val("5");
            }
        }
    }
});

contentInfo = function(){
    callByGet("/api/contents?contsSeq="+$("#contsSeq").val(), "contentsInfoSuccess", "NoneForm", "contentsInfoFail");
}

var buttonHtml = "";
contentsInfoSuccess = function(data) {
    if (data.resultCode == "1000") {
        var item = data.result.contentsInfo;
        var coverImg = data.result.contentsInfo.coverImg;

        $("#contsID").val(item.contsID); // 콘텐츠 수정 이력 시 사용하기 위해 데이터 추가
        $("#contsTitle, #headTitle").html(item.contsTitle);
        $("#contsSubTitle").html(item.contsSubTitle);
        $("#contsDesc").html(item.contsDesc);
        $("#contsId").html(item.contsID);
        $("#contCtgNm").html(item.contCtgNm);
        $("#cretrID").html(item.cretrID);
        $("#mbrNm").html(item.mbrNm);
        $("#contsVer").html(item.contsVer);
        $("#maxAvlNop").html(item.maxAvlNop);
        var str = item.rcessWhySbst;
        var changeNbsp = replaceAll(str,"\u0020", "&nbsp;");
        changeNbsp = replaceAll(changeNbsp," ", "&nbsp;");
        var rcessWhySbst = "";
        sttusValue = item.sttusVal;
        if (item.sttusVal == "04") {
            rcessWhySbst = 'popAlertLayer("'+changeNbsp+"\",'','" + getMessage("common.refuse") + "&nbsp;" + getMessage("common.reason") + "',true)";
            $("#sttusView").after("<span id='refuseConfirm' onclick="+rcessWhySbst+"  >("+getMessage("contents.table.refuse.verify")+")</span>");
            $("#sttusView").text(item.sttus);
            $("#sttusView").addClass("boxStateRed").removeClass("boxStateBlack");
        } else {
            rcessWhySbst = "";
            if (item.sttusVal != "01") {
                if (item.sttusVal == "06") {
                    $("#sttusView").text(getMessage("contents.table.state.verify.success"));
                } else {
                    $("#sttusView").text(item.sttus);
                }

                $("#sttusBtn").hide();
            } else {
                $("#sttusView").text(item.sttus);
                $("#sttusView").addClass("boxStateBlue").removeClass("boxStateBlack");
            }
        }

        $("#cp").html(item.cp);
        $("#cntrctDt").html(item.cntrctStDt+" ~ "+item.cntrctFnsDt);
        $("#cretDt").html(item.cretDt);
        $("#amdDt").html(item.amdDt);
        $("#amdrID").html(item.amdrID);

        var genreHtml = "";
        $(data.result.contentsInfo.genreList).each(function(i,item) {
            genreHtml += item.genreNm+"/";
        });
        $("#genreList").html(genreHtml.slice(0,-1));

        if (item.metaData != "") { //null일때도 공백이 아니라서 show로 들어감 != "" (X) != null
            metaData = item.metaData.replace(/&#034;/gi, "\"");
            buttonHtml = "<input type=\"button\" class=\"btnDefaultWhiteSub btnMetadataSub\" style=\"width:180px;\" value=\"" + getMessage("submeta.confirm") + "\" onclick=\"showMetadataPopLayer()\">";
            $("#contCtgNm").append("&nbsp;&nbsp;" + buttonHtml);
        } else {
            $(".btnMetadataSub").remove();
        }

        var serviceHtml = "";
        $(data.result.contentsInfo.serviceList).each(function(i,item) {
            serviceHtml += item.svcNm+"/";
        });
        $("#serviceList").html(serviceHtml.slice(0,-1));

        $("#fileType").html(item.fileType);
        if (item.fileType == "VIDEO") {
            if(item.firstCtgNm.match(getMessage("common.liveon")) != null){ // 첫번째 카테고리 명이 '영상_LiveOn'일때
                $("#streamingUrl").text(item.exeFilePath);
                $("#streamingUrl").parents("tr").show();
            }

            $(".exeFilePath").hide();
            $(".contentInfo").show();
        }
        $("#exeFilePath").html(item.exeFilePath);
//        $("#videoName").html(item.videoName); 없음
//        if(item.contentsXMLName){
//            $("#contentsXmlName").html(""+item.contentsXMLName+"<a href='/api/fileDownload/"+item.contentsXMLSeq+"' class='fontblack'>다운로드</a>");
//        }
        $("#coverImg").html("<a href=\""+makeAPIUrl(coverImg.coverImgPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(coverImg.coverImgPath,"img")+"' title=\""+coverImg.fileSeq+"\"></a>");
        $("#coverImg").justifiedGallery({
            rowHeight : 220,
            lastRow : 'nojustify',
            margins : 3
        });

        var videoHtml = "";
        var metadataHtml = "";
        var contsHtml = "";

        $(data.result.contentsInfo.videoList).each(function(i,item){
            if (item.metadataInfo.fileNm == "") {
                $("#metadata").hide();
            }
            videoHtml += "<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.fileSeq+"' class='fontblack btnDownloadGray'>"+getMessage("common.download")+"</a></p>";
            metadataHtml += "<p><span class='float_l'>"+item.metadataInfo.fileNm+"</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.metadataInfo.fileSeq+"' class='fontblack btnDownloadGray'>"+getMessage("common.download")+"</a></p>";
            if(item.metadataInfo.fileNm == ""){
                metadataHtml = "";
            }
//            contsHtml += "<p><span class='float_l'>"+item.contsXmlInfo.fileNm+"</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.contsXmlInfo.fileSeq+"' class='fontblack btnDownloadGray'>"+getMessage("common.download")+"</a></p>";
            contsHtml = "";
        });

        $(data.result.contentsInfo.prevList).each(function(i,item){
            $("#prevList").html("<p><span class='float_l'>"+(i+1)+". "+item.fileNm+"("+item.fileSize+"Byte)</span><a href='"+makeAPIUrl("/api/fileDownload/")+item.fileSeq+"' class='fontblack btnDownloadGray'>"+getMessage("common.download")+"</a></p>");
        });


        // $("#pakageDown").attr("onclick","pageMove('/api/package/"+item.seq+"')");
        var thumbnailHtml = "";
        $(data.result.contentsInfo.thumbnailList).each(function(i,item){
            thumbnailHtml += "<a href=\""+makeAPIUrl(item.thumbnailPath,"img")+"\" class=\"swipebox\"><img src='"+makeAPIUrl(item.thumbnailPath,"img")+"' title=\""+item.fileSeq+"\"></a>";
        });
        $("#thumbnailList").html(thumbnailHtml);

        $("#thumbnailList").justifiedGallery({
            rowHeight : 120,
            lastRow : 'nojustify',
            margins : 3
        });
        $(".swipebox").swipebox();
        $("#widthSetting").css("width", "10%");

        var fileHtml = "";

        if (videoHtml == "") {
            $("#videoList").parents("tr").hide();
            $("#metadataName").parents("tr").hide();
            $(data.result.contentsInfo.fileList).each(function(i,item){
                fileHtml = "<p><span class='float_l'>"+item.fileNm+"&nbsp;</span><a href=\""+makeAPIUrl("/api/fileDownload/")+item.fileSeq+"\" class=\"fontblack btnDownloadGray\">["+getMessage("common.download")+"]</a></p>";
                $("#file").html(fileHtml);
            });
        } else {
            $("#videoList").html(videoHtml);
            $("#metadataName").html(metadataHtml);
            if(metadataHtml == "") {
                $("#metadataName").parents("tr").hide();
            }
            $("#contsInfo").html(contsHtml);
            $(".contsInfo").hide();

            $("#file").parents("tr").hide();
        }

        // 현재 페이지의 모든 이미지에 대해 img 표시 사이즈로 썸네일 이미지를 불러와 교체 (공통함수)
        imgToThumbImg();

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
    }
}

contentsInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), sessionStorage.getItem("last-url"));
}

function contentsSttusChange() {
    popConfirmLayer(getMessage("msg.contents.verify.confirm"), function(){
        formData("NoneForm" , "contsSeq", $("#contsSeq").val());
        formData("NoneForm" , "sttus", "02");
        callByPut("/api/contents" , "contentsSttusChangeSuccess", "NoneForm");
    });
}

contentsSttusChangeSuccess = function(data) {
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("msg.common.complte.response"),"/contents/"+$("#contsSeq").val());
    }else if(data.resultCode == "1011"){
        formDataDeleteAll("NoneForm");
        popAlertLayer(getMessage("msg.common.update.fail"));
    }
}

contentsDelete = function() {
    popConfirmLayer(getMessage("msg.contents.confirm.delete")+"<br />"+$("#contsTitle").text() +getMessage("msg.contents.confirm.delete2"), function(){
        formData("NoneForm" , "contsSeq", $("#contsSeq").val());
        formData("NoneForm" , "delYn", 'Y');
        callByDelete("/api/contents" , "contentsDeleteSuccess", "NoneForm");
    });

}

function contentsDeleteSuccess(data) {
    formDataDeleteAll("NoneForm");
    if(data.resultCode == "1000"){
        popAlertLayer(getMessage("msg.common.delete.success"), '/contents');
    } else if (data.resultCode == "1010") {
        popAlertLayer(getMessage("msg.common.nopermission"),'/contents');
    } else {
        popAlertLayer(getMessage("msg.common.delete.fail"));
    }
}

/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function historyShow(name) {
    popLayerDiv(name, 500, 300, true);

    if (name == "popupModifyHst") {
        if (showModifyPopup) {
            $("#jqgridModifyHstData").jqGrid("GridUnload");
            showModifyPopup = false;
        }
        modifyHistory()
    } else {
        if (showVerifyPopup) {
            $("#jqgridVerifyHstData").jqGrid("GridUnload");
            showVerifyPopup = false;
        }
        verifyHistory();
    }
}

/* blockUI 닫기
 * parameter: x
 * return: x
 */
function popLayerClose() {
    $("body").css("overflow-y" , "visible");
    $.unblockUI();

    $("#popupModifyHstTitle").html("");
    $("#popupModifyHstContsId").html("");

    $("#popupVerifyHstTitle").html("");
    $("#popupVerifyHstContsId").html("");
}

modifyHistory = function() {
    if (!showModifyPopup) {
        showModifyPopup = true;
    }

    $("#popupModifyHstTitle").html("<center>"+getMessage("contents.table.updateHis")+"</center>");
    $("#popupModifyHstContsId").html(getMessage("contents.table.id")+" : " + $("#contsID").val());

    var columnNames = [
        getMessage("table.modifycol"),
        getMessage("table.modifybefore"),
        getMessage("table.modifyafter"),
        getMessage("table.modifyid"),
        getMessage("table.modifydate"),
        "contsHstSeq"
    ];

    $("#jqgridModifyHstData").jqGrid({
        url:makeAPIUrl("/api/modifyHistory?contsSeq=" + $("#contsSeq").val()),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.modifyHstList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:columnNames,
        colModel:[
            {name:"contsChgInfo", index: "contsChgInfo", align:"center"},
            {name:"legacyValue", index: "legacyValue", align:"center"},
            {name:"changeValue", index:"changeValue", align:"center"},
            {name:"amdrID", index:"amdrID", align:"center"},
            {name:"amdDt", index:"amdDt", align:"center"},
            {name:"contsHstSeq", index:"contsHstSeq", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageModifyHstDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "contsHstSeq",
            sortorder: "desc",
            height: "auto",
            loadComplete: function () {
                //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridModifyHstData").getGridParam("lastpage")) this.value = $("#jqgridModifyHstData").getGridParam("lastpage");
                });
            }
    });
    jQuery("#jqgridModifyHstData").jqGrid('navGrid','#pageModifyHstDiv',{del:false,add:false,edit:false,search:false});
}

verifyHistory = function() {
    if (!showVerifyPopup) {
        showVerifyPopup = true;
    }

    $("#popupVerifyHstTitle").html("<center>"+getMessage("contents.table.verifyHis")+"</center>");
    $("#popupVerifyHstContsId").html(getMessage("contents.table.name")+" : " + $("#contsTitle").text());

    var columnNames = [
        getMessage("table.reger"),
        getMessage("table.status"),
        getMessage("table.updatedate"),
        "actcSeq"
    ];

    $("#jqgridVerifyHstData").jqGrid({
        url:makeAPIUrl("/api/verifyHistory?contsSeq=" + $("#contsSeq").val()),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.verifyHstList",
            records: "result.totalCount",
            repeatitems: false,
            id: "seq_user"
        },
        colNames:columnNames,
        colModel:[
            {name:"verifyCretrNm", index: "verifyCretrNm", align:"center"},
            {name:"verifySttus", index: "verifySttus", align:"center"},
            {name:"verifyRegDt", index:"verifyRegDt", align:"center"},
            {name:"actcSeq", index:"actcSeq", hidden:true}
            ],
            autowidth: true, // width 자동 지정 여부
            rowNum: $("#limit").val(), // 페이지에 출력될 칼럼 개수
            //rowList:[10,20,30],
            pager: "#pageVerifyHstDiv", // 페이징 할 부분 지정
            viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
            sortname: "actcSeq",
            sortorder: "desc",
            height: "auto",
            loadComplete: function () {
                //pageMove Max
                $('.ui-pg-input').on('keyup', function() {
                    this.value = this.value.replace(/\D/g, '');
                    if (this.value > $("#jqgridVerifyHstData").getGridParam("lastpage")) this.value = $("#jqgridVerifyHstData").getGridParam("lastpage");
                });
            }
    });
    jQuery("#jqgridVerifyHstData").jqGrid('navGrid','#pageVerifyHstDiv',{del:false,add:false,edit:false,search:false});
}

function moveToEditView() {
    if (sttusValue == '03') {
        popAlertLayer(getMessage("contents.verify.not.modify.msg"));
        return;
    }

    pageMove("/contents/" + $("#contsSeq").val() + "/edit");
}


/* blockUI 닫기
 * parameter: x
 * return: x
 */
function popLayerDetailClose() {
    $("#submetadataTableArea").mCustomScrollbar('destroy');
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}


/* blockUI 열기
 * parameter: name(blockUI를 설정할 ID값)
 * return: x
 */
function showMetadataPopLayer() {
    callByGet("/api/codeInfo?comnCdCtg=CONTS_SUB_META", "showMetadataConts", "NoneForm");
}

showMetadataConts = function(data) {
    if(data.resultCode == "1000"){
        var tableHtml = "";
        var metaVal = "";
        $("#detailSubmetadata").html("");
        $(data.result.codeList).each(function(i, item){
            var metaObj = JSON.parse(metaData);
            for (key in metaObj) {
                if (key == item.comnCdValue) {
                    if (item.comnCdNM.indexOf(getMessage("submeta.time")) != -1 && metaObj[key] != "") {
                        metaVal = metaObj[key] + " " + getMessage("common.minute");
                    } else if(item.comnCdNM.indexOf(getMessage("submeta.rt")) != -1){
                        // #a8484는 작은 따옴표(') #q0808은 큰 따옴표(")로 DB 저장
                        if(metaObj[key].match("#a8484") != null || metaObj[key].match("#q0808") != null){
                            metaObj[key] = metaObj[key].replace("#a8484", "\'");
                            metaObj[key] = metaObj[key].replace("#q0808", "\"");
                        }
                        metaVal = metaObj[key];
                    } else {
                        metaVal = metaObj[key];
                    }
                }
            }
            if (metaVal != "") { //메타데이터가 없으면 조회 리스트에 추가하지 않음
                tableHtml += "<tr><th>" + item.comnCdNM + "<input type=\"hidden\" value=\"" + item.comnCdValue + "\">" + "</th><td>" + metaVal + "</td>";
                metaVal = "";
            }
            tableHtml += "</tr>";
        });
        $("#detailSubmetadata").append(tableHtml);
    } else {
        $("#detailSubmetadata").html("");
    }
    $("#popupDetailSubmetadata").css({"left":"-1000px","top":"-1000px"}).show();
    var popupHeight = $("#popupDetailSubmetadata").height();
    var popupWidth = $("#popupDetailSubmetadata").width();
    $("#popupDetailSubmetadata").hide();
    popLayerDiv("popupDetailSubmetadata", popupWidth, popupHeight, true);
    $("#submetadataTableArea").mCustomScrollbar({ axis : "y", theme:"inset-3" });
}
