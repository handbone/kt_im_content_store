/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.member.dao;

import kt.com.im.store.member.vo.LoginVO;
import kt.com.im.store.member.vo.MemberVO;

/**
 *
 * 회원 관리에 관한 데이터 접근 인터페이스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.08
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 8.   A2TEC      최초생성
 *
 *
 * </pre>
 */

public interface MemberDAO {

    LoginVO getLoginInfo(LoginVO vo) throws Exception;

    int insertLoginHistory(LoginVO vo) throws Exception;

    int increaseLoginFailCount(MemberVO vo) throws Exception;

    int resetLoginFailCount(MemberVO vo) throws Exception;

    MemberVO getMember(MemberVO vo) throws Exception;

    int addMember(MemberVO vo) throws Exception;

    int updateMember(MemberVO vo) throws Exception;

    int deleteMember(MemberVO vo) throws Exception;

    String getPassword(MemberVO vo) throws Exception;

    int updatePassword(MemberVO vo) throws Exception;

    String getSalt(MemberVO vo) throws Exception;

    int updateMemberToken(MemberVO vo) throws Exception;

    int insertDelayToChangePassword(MemberVO vo) throws Exception;

    boolean shouldChangePassword(MemberVO vo) throws Exception;

    long getOtpCreateTime(LoginVO vo) throws Exception;

    int updateOtpCreateTime(LoginVO vo) throws Exception;
}