<%--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="json" uri="http://www.atg.com/taglibs/json"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<json:object>
    <c:set var="count" value="0"></c:set>
    <json:property name="resultCode" value="${ resultCode }" />
    <json:property name="resultMsg" value="${ resultMsg }" />
    <c:if test="${resultCode eq '1010' and setResetPage == true}">
        <json:object name="result">
            <json:property name="currentPage" value="${1}" />
        </json:object>
    </c:if>

    <c:if test="${ resultType eq 'contentsInsert' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="contsSeq" value="${ contsSeq }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contentsList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsTitle" value="${ i.contsTitle }" escapeXml="false"/>
                        <json:property name="cretrID" value="${ i.mbrID }"/>
                        <json:property name="cretrNm" value="${ i.mbrNm }(${ i.mbrID })"/>
                        <json:property name="contCtgNm" value="${ i.contCtgNm}"/>
                        <json:property name="sttus" value="${ i.sttusNm }"/>
                        <json:property name="sttusVal" value="${ i.sttusVal }"/>
                        <json:property name="contsID" value="${ i.contsID }"/>
                        <json:property name="contsHdn" value="${i.contsHdn }"/>
                        <json:property name="cretDt" value="${ i.cretDt }"/>
                        <c:if test="${ i.genreNm != null and i.genreNm != ''}">
                            <json:property name="genreNm" value="${ i.genreNm }"/>
                        </c:if>
                        <c:set var="count" value="${count + 1 }"></c:set>
                </json:object>
            </json:array>
            <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="contentsInfo">
                    <json:property name="contsSeq" value="${ item.contsSeq }"/>
                    <json:property name="contsID" value="${ item.contsID }"/>
                    <json:property name="contsTitle" value="${ item.contsTitle }" escapeXml="false"/>
                    <json:property name="contsSubTitle" value="${ item.contsSubTitle }" escapeXml="false"/>
                    <json:property name="contsDesc" value="${ item.contsDesc }" escapeXml="false"/>
                    <json:property name="contCtgNm" value="${ item.contCtgNm }"/>
                    <json:property name="firstCtgNm" value="${ item.firstCtgNm }"/>
                    <json:property name="firstCtgID" value="${ item.firstCtgID }"/>
                    <json:property name="secondCtgNm" value="${ item.secondCtgNm }"/>
                    <json:property name="secondCtgID" value="${ item.secondCtgID }"/>
                    <json:property name="rcessWhySbst" value="${ item.rcessWhySbst }"/>
                    <json:property name="cretrID" value="${ item.cretrID }"/>
                    <json:property name="fileType" value="${ item.fileType }"/>
                    <json:property name="maxAvlNop" value="${ item.maxAvlNop }"/>
                    <json:property name="exeFilePath" value="${ item.exeFilePath }"/>
                    <json:property name="mbrNm" value="${ item.mbrNm }"/>
                    <json:property name="contsVer" value="${ item.contsVer }"/>
                    <json:property name="contsCtgSeq" value="${ item.contsCtgSeq }"/>
                    <json:property name="sttusVal" value="${ item.sttusVal }"/>
                    <json:property name="sttus" value="${ item.sttusNm }"/>
                    <json:property name="cp" value="${ item.cpNm }"/>
                    <json:property name="cpSeq" value="${ item.cpSeq }"/>
                    <json:property name="cntrctStDt" value="${ item.cntrctStDt }"/>
                    <json:property name="cntrctFnsDt" value="${ item.cntrctFnsDt }"/>
                    <json:property name="cretDt" value="${ item.cretDt }"/>
                    <json:property name="amdDt" value="${ item.amdDt }"/>
                    <json:property name="amdrID" value="${ item.amdrID }"/>
                    <json:property name="amdDtDetail" value="${ item.amdDtDetail }"/>
                    <json:property name="contsSubMetadataSeq" value="${ item.contsSubMetadataSeq }"/>
                    <json:property name="metaData" value="${ item.metaData }"/>
                    <json:object name="coverImg">
                        <json:property name="fileSeq" value="${ coverImg.fileSeq }"/>
                        <json:property name="coverImgNm" value="${ coverImg.orginlFileNm }"/>
                        <json:property name="coverImgPath" value="${ coverImg.filePath }"/>
                        <json:property name="fileId" value="${ coverImg.fileId }"/>
                        <json:property name="thumbList" value="${ coverImg.thumbList }"/>
                    </json:object>
                    <json:array name="thumbnailList" items="${thumbnailList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="thumbnailNm" value="${ i.orginlFileNm }"/>
                            <json:property name="thumbnailPath" value="${ i.filePath }"/>
                            <json:property name="fileId" value="${ i.fileId }"/>
                            <json:property name="thumbList" value="${ i.thumbList }"/>
                        </json:object>
                    </json:array>
                    <c:if test="${ item.fileType eq 'VIDEO'}">
                    <json:array name="videoList" items="${videoList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                            <json:object name="metadataInfo">
                                <json:property name="fileSeq" value="${ i.metadataXmlInfo.fileSeq}"/>
                                <json:property name="fileNm" value="${ i.metadataXmlInfo.orginlFileNm }"/>
                                <json:property name="filePath" value="${ i.metadataXmlInfo.filePath }"/>
                            </json:object>
                            <json:object name="contsXmlInfo">
                                <json:property name="fileSeq" value="${ i.contsXmlInfo.fileSeq }"/>
                                <json:property name="fileNm" value="${ i.contsXmlInfo.orginlFileNm }"/>
                                <json:property name="filePath" value="${ i.contsXmlInfo.filePath }"/>
                            </json:object>
                        </json:object>
                    </json:array>
                    </c:if>
                    <json:array name="prevList" items="${prevList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                        </json:object>
                    </json:array>
                    <c:if test="${ item.fileType ne 'VIDEO'}">
                    <json:array name="fileList" items="${fileList}" var="i">
                        <json:object>
                            <json:property name="fileSeq" value="${ i.fileSeq }"/>
                            <json:property name="filePath" value="${ i.filePath }"/>
                            <json:property name="fileSize" value="${ i.fileSize }"/>
                            <json:property name="fileNm" value="${ i.orginlFileNm }"/>
                        </json:object>
                    </json:array>
                    </c:if>
                    <json:array name="genreList" items="${genreList}" var="i">
                        <json:object>
                            <json:property name="genreSeq" value="${ i.genreSeq }"/>
                            <json:property name="genreNm" value="${ i.genreNm }"/>
                        </json:object>
                    </json:array>
                    <json:array name="serviceList" items="${serviceList}" var="i">
                        <json:object>
                            <json:property name="svcSeq" value="${ i.svcSeq }"/>
                            <json:property name="svcNm" value="${ i.svcNm }"/>
                        </json:object>
                    </json:array>
                </json:object>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'validateContsInfo' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:property name="isValid" value="${ isValid }"/>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsCtgList' }">
      <c:if test="${ resultCode eq '1000' }">
          <json:object name="result">
              <c:if test="${ !empty firstCtgList}">
                  <json:array name="firstCtgList" items="${firstCtgList}" var="i">
                      <json:object>
                          <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                          <json:property name="firstCtgID" value="${ i.firstCtgID }"/>
                      </json:object>
                  </json:array>
              </c:if>
              <json:array name="contsCtgList" items="${item}" var="i">
                  <json:object>
                      <json:property name="contsCtgSeq" value="${ i.contsCtgSeq }"/>
                      <json:property name="firstCtgNm" value="${ i.firstCtgNm }"/>
                      <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                      <json:property name="firstCtgID" value="${ i.firstCtgID }"/>
                      <json:property name="secondCtgID" value="${ i.secondCtgID }"/>
                      <json:property name="ctgCnt" value="${ i.ctgCnt+1 }"/>
                      <json:property name="cretDt" value="${ i.cretDt }"/>
                  </json:object>
              </json:array>
          </json:object>
      </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contsCtgListNm' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="contsCtgList" items="${item}" var="i">
                    <json:object>
                        <json:property name="secondCtgNm" value="${ i.secondCtgNm }"/>
                        <json:property name="secondCtgID" value="${ i.secondCtgID }"/>
                    </json:object>
                </json:array>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'contentsUpdateHistoryList' }">
      <c:if test="${ resultCode eq '1000' }">
          <json:object name="result">
              <json:array name="contentsUpdateHistoryList" items="${item}" var="i" >
                  <json:object>
                      <json:property name="num" value=" ${totalCount - ((currentPage-1)*10+count) }" />
                      <json:property name="seq" value="${ i.fileSeq }"/>
                      <json:property name="contentsSeq" value="${ i.contentsSeq }"/>
                      <c:choose>
                          <c:when test="${ i.columnName eq 'TITLE' }"><json:property name="columnName" value="콘텐츠명"/></c:when>
                          <c:when test="${ i.columnName eq 'SUBTITLE' }"><json:property name="columnName" value="설명"/></c:when>
                          <c:when test="${ i.columnName eq 'CONTENTS_CATEGORY_SEQ' }"><json:property name="columnName" value="카테고리"/></c:when>
                          <c:when test="${ i.columnName eq 'CONTENTS_ID' }"><json:property name="columnName" value="콘텐츠 ID"/></c:when>
                      </c:choose>
                      <json:property name="oldValue" value="${ i.oldValue }"/>
                        <json:property name="newValue" value="${ i.newValue }"/>
                        <json:property name="updateId" value="${ i.updaterId }"/>
                        <json:property name="regDate" value="${ i.regDate }"/>
                        <c:set var="count" value="${count + 1 }"></c:set>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <json:property name="totalPage" value="${ totalPage+1 }" />
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'verifyHstList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="verifyHstList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="actcSeq" value="${ i.actcSeq }"/>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="verifyCretrNm" value="${ i.verifyCretrNm }"/>
                        <json:property name="verifySttus" value="${ i.verifySttus }"/>
                        <json:property name="verifyRegDt" value="${ i.verifyRegDt }"/>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 5 == 0}">
                        <json:property name="totalPage"
                            value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage"
                            value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'modifyHstList' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:array name="modifyHstList" items="${item}" var="i" >
                    <json:object>
                        <json:property name="contsHstSeq" value="${ i.contsHstSeq }"/>
                        <json:property name="contsSeq" value="${ i.contsSeq }"/>
                        <json:property name="contsChgInfo" value="${ i.contsChgInfo }"/>
                        <json:property name="legacyValue" value="${ i.legacyValue }"/>
                        <json:property name="changeValue" value="${ i.changeValue }"/>
                        <json:property name="amdrID" value="${ i.amdrID }"/>
                        <json:property name="amdDt" value="${ i.amdDt }"/>
                    </json:object>
                </json:array>
                <json:property name="totalCount" value="${ totalCount }" />
                <json:property name="currentPage" value="${ currentPage }" />
                <c:choose>
                    <c:when test="${totalCount == 0 }">
                        <json:property name="totalPage" value="${ 0 }" />
                    </c:when>
                    <c:when test="${totalCount % 5 == 0}">
                        <json:property name="totalPage"
                            value="${ totalPage }" />
                    </c:when>
                    <c:otherwise>
                        <json:property name="totalPage"
                            value="${ totalPage+1 }" />
                    </c:otherwise>
                </c:choose>
            </json:object>
        </c:if>
    </c:if>

    <c:if test="${ resultType eq 'resultSubMetadata' }">
        <c:if test="${ resultCode eq '1000' }">
            <json:object name="result">
                <json:object name="subMetadataInfo">
                    <json:property name="contsSubMetadataSeq" value="${ item.contsSubMetadataSeq }"/>
                    <json:property name="metaData" value="${ item.metaData }"/>
                </json:object>
            </json:object>
        </c:if>
    </c:if>
</json:object>