/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var hasChangedHash = false;
var shouldPreventHashChangeEvent = false;
var didFirstLayout = false;
var shouldUpdateStorList = false;

var columnNames = new Array();
var colModel = new Array();

$(document).ready(function() {
    $("#cpList").change(function() {
        shouldUpdateStorList = true;
        updateContentsList();
    });

    $("#storeList").change(function() {
        updateContentsList();
    });

    $("#yearSelector").change(function() {
        updateDateSelectorLayout();
        didChangeDateSelector();
        updateContentsList();
    });

    $("#monthSelector").change(function() {
        didChangeDateSelector();
        updateContentsList();
    });

    $("#btnPrveDate").click(function() {
        clickPrevDateButton();
        updateContentsList();
    });

    $("#btnNextDate").click(function() {
        clickNextDateButton();
        updateContentsList();
    });

    $("#btnDay").click(function() {
        setDateType("day");
        updateContentsList();
    });

    $("#btnWeek").click(function() {
        setDateType("week");
        updateContentsList();
    });

    $("#btnMonth").click(function() {
        setDateType("month");
        updateContentsList();
    });

    $("#btnCustom").click(function() {
        setDateType("custom");
        updateContentsList();
    });

    $(".btnXls").click(function() {
        downloadExcel();
    });

    $(".btnSearch").click(function() {
        updateContentsList();
    });

    $("#btnContentUsage").click(function() {
        pageMove("/contents/usage");
    });

    $("#keyword").keydown(function(event) {
        if (event.keyCode == 13) {
            updateContentsList();
        }
    });

    resizeJqGridWidth("jqgridData", "gridArea");

    setDateType();

    $(window).on('hashchange', function () {
        if (!document.location.hash) {
            history.back();
            return;
        }

        if (shouldPreventHashChangeEvent) {
            shouldPreventHashChangeEvent = false;
            return;
        }

        hasChangedHash = true;
        $("#jqgridData").trigger("reloadGrid");
    });

    if (document.location.hash) {
        hasChangedHash = true;
    }

    shouldUpdateStorList = true;

    initJqGridColumnInfo();

    getCategoryList();
});

getCategoryList = function() {
    formData("NoneForm", "GroupingYN", "Y");
    callByGet("/api/contentsCategory" , "didReceiveCategoryList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didReceiveCategoryList = function(data) {
    if (data.resultCode == "1000") {
        appendJqGridColumnInfo(data);
    }

    cpList();
}

initJqGridColumnInfo = function() {
    columnNames.push(getMessage("table.num"));
    columnNames.push(getMessage("contents.table.store"));
    columnNames.push(getMessage("select.all"));

    colModel.push({name:"num", index: "num", align:"center", width:40, hidden:true});
    colModel.push({name:"storNm", index: "storNm", align:"center", classes:"pointer", formatter:pointercursor});
    colModel.push({name:"contsTotalDownCount", index:"contsTotalDownCount", align:"center"});
}

appendJqGridColumnInfo = function(data) {
    var firstCategoryListHtml = "";
    $(data.result.firstCtgList).each(function(i,item) {
        columnNames.push(item.firstCtgNm);
        colModel.push({name:item.firstCtgID, index:item.firstCtgID, align:"center"});
    });
    columnNames.push("seq");
    colModel.push({name:"storSeq", index:"storSeq", hidden:true});
}

cpList = function() {
    callByGet("/api/codeInfo?comnCdCtg=CP","cpListSuccess","NoneForm");
    formDataDeleteAll("NoneForm");
}

cpListSuccess = function(data) {
    if (data.resultCode != "1000") {
        didNotReceiveData("contents.download.select.noCp");
        $("#cpList").html("");
        $("#storeList").html("");
        return
    }

    setPreventDownloadExcelFile(false);

    var cpListHtml = "<option value='0'>" + getMessage("select.all") + "</option>";
    $(data.result.codeList).each(function(i, item) {
        cpListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNM + "</option>";
    });

    $("#cpList").html(cpListHtml);

    var cpSeq = $("#cpSeq").val();
    if (cpSeq != "" && cpSeq != 0) {
        $("#cpList > option[value=" + cpSeq + "]").attr("selected", true);
    } else {
        $("#cpListGrp").show();
    }

    getStoreList();
}

getStoreList = function() {
    callByGet("/api/codeInfo?comnCdCtg=STORE", "didReceiveStoreList", "NoneForm");
    formDataDeleteAll("NoneForm");
}

didReceiveStoreList = function(data) {
    var storeListHtml = "<option value='0'>" + getMessage("select.all") + "</option>";
    if (data.resultCode != "1000") {
        didNotReceiveData("contents.download.select.noStore");
        $("#storeList").html(storeListHtml);

        if (!didFirstLayout) {
            didFirstLayout = true;
            initContentDownloadList();
            return;
        }
        updateContentsList();
        return
    }

    $(data.result.codeList).each(function(i, item) {
        storeListHtml += "<option value=\"" + item.comnCdValue + "\">" + item.comnCdNM + "</option>";
    });
    $("#storeList").html(storeListHtml);

    if (!didFirstLayout) {
        didFirstLayout = true;
        initContentDownloadList();
        return;
    }
    updateContentsList();
}

didNotReceiveData = function(msgId) {
    popAlertLayer(getMessage(msgId));
    $("#jqgridData").clearGridData();
    $("#sp_1_pageDiv").text(1);
    setPreventDownloadExcelFile(true);
}

setPreventDownloadExcelFile = function(value) {
    $("#btnXls").off("click");
    if (value) {
        $("#btnXls").on("click", function() {
            popAlertLayer(getMessage("common.noDownload.msg"));
        });
        return;
    }
    $("#btnXls").on("click", function() {
        downloadExcel();
    });
}

initContentDownloadList = function() {
    $("#jqgridData").jqGrid({
        url:makeAPIUrl("/api/statistics"),
        datatype: "json", // 데이터 타입 지정
        jsonReader : {
            page: "result.currentPage",
            total: "result.totalPage",
            root: "result.contentDownloadList",
            records: "result.totalCount",
            repeatitems: false,
            id: "contsSeq"
        },
        colNames: columnNames,
        colModel:colModel,
        autowidth: true, // width 자동 지정 여부
        rowNum: 10, // 페이지에 출력될 칼럼 개수
        pager: "#pageDiv", // 페이징 할 부분 지정
        viewrecords: true, // 페이징 바에서 총 레코드 수 표시 여부
        sortname: "storNm",
        sortorder: "asc",
        height: "auto",
        multiselect: false,
        postData: {
            type : "contentDownload",
            cpSeq: $("#cpList option:selected").val(),
            storSeq: $("#storeList option:selected").val(),
            startDate: $("#startDate").val(),
            endDate: $("#endDate").val()
        },

        beforeRequest:function() {
            var myPostData = $('#jqgridData').jqGrid("getGridParam", "postData");
            if (hasChangedHash) {
                var strHash = document.location.hash.replace("#", "");
                var page = parseInt(findGetParameter(strHash, "page"));
                var dateType = findGetParameter(strHash, "dateType");
                var cpSeq = findGetParameter(strHash, "cpSeq");
                var storSeq = findGetParameter(strHash, "storSeq");
                var startDate = findGetParameter(strHash, "startDate");
                var endDate = findGetParameter(strHash, "endDate");
                var target = findGetParameter(strHash, "target");
                var keyword = findGetParameter(strHash, "keyword");

                $("#dateType").val(dateType);
                $("#startDate").val(startDate);
                $("#endDate").val(endDate);
                restoreDateType(dateType, startDate, endDate);

                $("#cpList").val(cpSeq);
                $("#storeList").val(storSeq);
                $("#target").val(target);
                $("#keyword").val(keyword);

                myPostData.page = page;
                myPostData.cpSeq = $("#cpList option:selected").val();
                myPostData.storSeq = $("#storeList option:selected").val();
                myPostData.startDate = startDate;
                myPostData.endDate = endDate;
                if (keyword != null) {
                    myPostData.target = target;
                    myPostData.keyword = keyword;
                } else {
                    delete myPostData.target;
                    delete myPostData.keyword;
                }
            }

            $('#jqgridData').jqGrid('setGridParam', { postData: myPostData });
        },
        /*
         * parameter: x
         * jqGrid 그리기가 끝나면 함수 실행
         */
        loadComplete: function(data) {
            updateHeaderTitle(data);
            didCompleteLoad(this);
        },
        loadError: function (jqXHR, textStatus, errorThrown) {
            updateHeaderTitle(null);
            didCompleteLoad(this);
            $("#jqgridData").clearGridData();
            $("#sp_1_pageDiv").text(1);
        },
        /*
         * rowId: 선택한 셀의 행 번호, columnId: 선택한 셀의 열 번호, cellValue: 선택한 셀의 값, event: event object
         * 해당 로우의 각 셀마다 이벤트 생성
         */
        onCellSelect: function(rowId, columnId, cellValue, event) {
            // 콘텐츠명 셀 클릭시
            if (columnId == 1) {
                sessionStorage.setItem("last-url", location);
                sessionStorage.setItem("state", "view");

                var list = $("#jqgridData").jqGrid('getRowData', rowId);
                var cpSeq = $("#cpList option:selected").val();
                pageMove("/contents/download/" + list.storSeq + "?cpSeq=" + cpSeq + "&dateType=" + dateType + "&startDate=" + $("#startDate").val() + "&endDate=" + $("#endDate").val());
            }
        }
    });
    $("#jqgridData").jqGrid('navGrid','#pageDiv',{del:false,add:false,edit:false,search:false});
    $("#jqgridData").jqGrid('setGroupHeaders', {
        useColSpanStyle: true,
        groupHeaders:[
          {startColumnName: 'contsTotalDownCount', numberOfColumns: (columnNames.length - 3), titleText: '<em>카테고리</em>'},
        ]
    });

    //pageMove Max
    $('.ui-pg-input').on('keyup', function() {
        this.value = this.value.replace(/\D/g, '');
        if (this.value > $("#jqgridData").getGridParam("lastpage")) {
            this.value = $("#jqgridData").getGridParam("lastpage");
        }
    });
}

function updateContentsList() {
    var storSeq = $("#storeList option:selected").val();
    $("#jqgridData").jqGrid("setGridParam",
        {
            search: true,
            postData: {
                type : "contentDownload",
                cpSeq: $("#cpList option:selected").val(),
                storSeq: storSeq,
                startDate: $("#startDate").val(),
                endDate: $("#endDate").val(),
                target: $("#target").val(),
                keyword: $("#keyword").val()
            },
            page: 1
        }
    );

    $("#jqgridData").trigger("reloadGrid");
}

function setSearchField() {
    if ($("#target > option").length == 0) {
        var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
        var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

        var searchHtml = "";

        for (var i = 0; i < colModel.length; i++) {
            var name = colModel[i].name;
            if (name == "storNm") {
                searchHtml += "<option value=\"" + name + "\">" + colNames[i] + "</option>";
            }
        }
        $("#target").html(searchHtml);
        if ($("#target option").index() == 0) {
            $("#target").before("<div class='target_bak'>"+$("#target").text()+"</div>");
            $(".target_bak").parent(".selectBox").css("display","table");
        } else {
            $(".target_bak").remove();
        }
    }
}

updateHeaderTitle = function(data) {
    var colModel = $("#jqgridData").jqGrid('getGridParam', 'colModel');
    var colNames = $("#jqgridData").jqGrid('getGridParam', 'colNames');

    for (var i = 2; i < (colModel.length - 1); i++) {
        var name = colModel[i].name;
        var titleId = "#jqgh_jqgridData_" + name;
        var title = colNames[i];
        if (i == 2) {
            var cnt = (data == undefined || data["contsTotalCnt"] == undefined) ? 0 : data["contsTotalCnt"];
            title += "(" + cnt + ")";
        } else {
            var cnt = (data == undefined || data[name + "TotalCnt"] == undefined) ? 0 : data[name + "TotalCnt"];
            title += "(" + cnt + ")";
        }
        $(titleId).text(title);
    }
}

function downloadExcel() {
    var gridParam = $(jqgridData).jqGrid("getGridParam");
    var params = {
            "type" : "contentDownload",
            "sidx" : gridParam.sortname,
            "sord" : gridParam.sortorder,
            "cpSeq" : $("#cpList option:selected").val(),
            "storSeq" : $("#storeList option:selected").val(),
            "startDate" : $("#startDate").val(),
            "endDate" : $("#endDate").val(),
            "target" : $("#target").val(),
            "keyword" : $("#keyword").val()
    };

    downloadExcelFile("/api/statistics/download", params);
}

didCompleteLoad = function(obj) {
    setSearchField();

    //session
    if (sessionStorage.getItem("last-url") != null) {
        sessionStorage.removeItem('state');
        sessionStorage.removeItem('last-url');
    }

    if (hasChangedHash) {
        hasChangedHash = false;
    } else {
        shouldPreventHashChangeEvent = true;

        // 뒤로가기 동작을 위한 설정
        var hashlocation = "page=" + $(obj).context.p.page;
        hashlocation += "&dateType=" + dateType;
        var cpSeqValue = $("#cpList option:selected").val();
        if (cpSeqValue) {
            hashlocation += "&cpSeq=" + cpSeqValue;
        }
        var storSeqValue = $("#storeList option:selected").val();
        if (storSeqValue) {
            hashlocation += "&storSeq=" + storSeqValue;
        }
        var startDate = $("#startDate").val();
        if (startDate) {
            hashlocation += "&startDate=" + startDate;
        }
        var endDate = $("#endDate").val();
        if (endDate) {
            hashlocation += "&endDate=" + endDate;
        }

        var searchTarget = $("#target").val();
        var searchKeyword = $("#keyword").val();
        if ($(obj).context.p.search && searchKeyword) {
            hashlocation += "&target=" + searchTarget + "&keyword=" + searchKeyword;
        }
        document.location.hash = hashlocation;
    }
}