/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var qnaTypeCode;
var qnaCtgCode;
var ATTCH_FILE;
var FILE_SETTINGS   = {
        // Backend Settings
//      upload_url                      : makeAPIUrl("/api/fileProcess"),
        upload_url                      : makeAPIUrl("/api/file"),

        // Flash Settings
        flash_url                       : makeAPIUrl("/resources/image/swfupload/swfupload.swf"),

        // File Upload Settings

        // Event Handler Settings (all my handlers are in the Handler.js file)
        file_dialog_start_handler       : fileDialogStart,
        file_queued_handler             : fileQueued,
        file_queue_error_handler        : fileQueueError,
        file_dialog_complete_handler    : fileDialogComplete,
        upload_start_handler            : uploadStart,
        upload_progress_handler         : uploadProgress,
        upload_error_handler            : uploadError,
        upload_success_handler          : uploadSuccess,
        upload_complete_handler         : uploadComplete,

        // Button Settings
        //button_action                 : SWFUpload.BUTTON_ACTION.SELECT_FILE,
        button_action                   : -110,
        button_cursor                   : SWFUpload.CURSOR.HAND,
        // Debug Settings
        debug: false
};

$(document).ready(function() {
    $("#title").bind("keyup",function(){
        re = /[\<>&\"']/gi;
        var temp=$("#title").val();
        if (re.test(temp)) { //특수문자가 포함되면 삭제하여 값으로 다시셋팅
            $("#title").val(temp.replace(re,""));
        }
    });

    setkeyup();
    qnaInfo();
    fileUploadBtn();
});

function qnaInfo(){
    formData("NoneForm", "qnaSeq", $("#qnaSeq").val());
    formData("NoneForm", "searchType", "edit");
    callByGet("/api/qna?reqType=edit", "qnaInfoSuccess", "NoneForm", "qnaInfoFail");
}

qnaInfoSuccess = function(data) {
    formDataDelete("NoneForm", "qnaSeq");
    if(data.resultCode == "1000"){
        var qnaDetail = data.result.qnaDetail;
        $("#title").val(qnaDetail.qnaTitle);
        qnaTypeCode = qnaDetail.qnaTypeCode;
        qnaCtgCode = qnaDetail.qnaCtgCode;

        var strQnaSbst = strConv(qnaDetail.qnaSbst);
        strQnaSbst = strConv(strQnaSbst);
        CKEDITOR.instances.sbst.setData(strQnaSbst);

        var fileHtml = "";
        $(data.result.fileList).each(function(i,item){
            fileHtml += "<table class=\"fileTable\"><tr><td><p class=\"fileName\" title=\"" + item.fileName
                        + "\">" + getMessage("common.filename") + " : "+ item.fileName + "</p></td><td><input type=\"button\" class=\"btnNormal btnDelete btnDownSmall\""
                        + " onclick=\"savedfileDelete(this, " + item.fileSeq + ")\" value=\"" + getMessage("button.delete") + "\"></td></tr></table>";
        });
        $("#fileArea").html(fileHtml);

        qnaTypeList();
        qnaCtgList();

        listBack($(".btnList"));
    } else {
        $(".content").hide();
        popAlertLayer(getMessage("common.bad.request.msg"), "/qna");
    }
}

qnaInfoFail = function(data) {
    $(".content").hide();
    popAlertLayer(getMessage("common.bad.request.msg"), "/qna");
}

qnaTypeList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_SE", "qnaTypeListSuccess","NoneForm");
}

qnaTypeListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var qnaTypeListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            qnaTypeListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNM+"</option>";
        });
        $("#typeSelbox").html(qnaTypeListHtml);
        $("#typeSelbox > option[value=05]").attr("selected","true");
        $("#typeSelbox").attr('disabled', 'true');
    }
}

qnaCtgList = function() {
    callByGet("/api/codeInfo?comnCdCtg=POST_CTG", "qnaCategoryListSuccess","NoneForm");
}

qnaCategoryListSuccess = function(data) {
    if (data.resultCode == "1000") {
        var qnaCtgListHtml = "";
        $(data.result.codeList).each(function(i, item) {
            qnaCtgListHtml += "<option value=\""+item.comnCdValue+"\">"+item.comnCdNM+"</option>";
        });
        $("#ctgSelbox").html(qnaCtgListHtml);
        $("#ctgSelbox > option[value=" + qnaCtgCode + "]").attr("selected","true");
    }
}

//swfupload - attch_file
fileUploadBtn = function(){
    //File Upload screenshot
    var temp = {fileSe : 'qna'};
    var file_settings   ={};
    file_settings.button_placeholder_id = "file_btn_placeholder";
    file_settings.button_width  = 62;
    file_settings.button_height = 22;
    file_settings.custom_settings = temp;
    file_settings.file_types =  "*.pdf;*.hwp;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.png;*.jpg;*.jpeg;*.bmp;*.gif",
    file_settings.file_queue_limit =  "0";
    //file_settings.file_size_limit =  "10MB";
    file_settings.button_image_url  = makeAPIUrl("/resources/image/swfupload/XPButtonUploadText_61x22.png");
    $.extend(true,  file_settings,  FILE_SETTINGS);
    ATTCH_FILE  = new SWFUpload(file_settings);
}

var delFileSeq="";
savedfileDelete = function(e, fileSeq){
    $(e).parents("table.fileTable").remove();
    delFileSeq += fileSeq + ",";
}

fileQueueDelete = function(e, fileId){
    $(e).parents("table.fileTable").remove();
    fileTemp.cancelUpload(fileId);
}

function updateQna() {
    if ($("#title").val() == "") {
        popAlertLayer(getMessage("common.title.empty.msg"));
        return;
    }

    if (CKEDITOR.instances.sbst.getData().length < 1) {
        popAlertLayer(getMessage("common.content.empty.msg"));
        return;
    }

    popConfirmLayer(getMessage("common.update.msg"), function() {
        CKEDITOR.instances.sbst.updateElement();
        $("#sbst").val(CKEDITOR.instances.sbst.getData());

        formData("NoneForm", "qnaSeq", $("#qnaSeq").val());
        formData("NoneForm", "title", $("#title").val());
        formData("NoneForm", "type", $("#typeSelbox").val());
        formData("NoneForm", "category", $("#ctgSelbox").val());
        formData("NoneForm", "content", $("#sbst").val());
        formData("NoneForm", "delFileSeqs", delFileSeq.slice(0,-1));

        callByPut("/api/qna", "updateQnaSuccess", "NoneForm", "updateFail");
    }, null, getMessage("common.confirm"));
}

updateFail = function(data){
    formDataDeleteAll("NoneForm");
    popAlertLayer(getMessage("fail.common.update"));
}

updateQnaSuccess = function(data) {
    formDataDeleteAll("NoneForm");
    if (data.resultCode == "1000") {
        $("#contsSeq").val($("#qnaSeq").val());
        uploadAttachFiles();
    } else {
        popAlertLayer(getMessage("fail.common.update"));
    }
}

function uploadAttachFiles(){
    if(fileTemp != undefined){
        fileTemp.startUpload();
    } else {
        popAlertLayer(getMessage("success.common.update"), "/qna");
    }
}

fileInsertSuccess = function(data){
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }

    fileInsertSuccessPopup();
}

fileInsertFail = function() {
    if (fileTemp.getStats().files_queued > 0) {
        return;
    }
    fileInsertSuccessPopup();
}

fileInsertSuccessPopup = function() {
    $("#divCenter").hide();

    var msg = getMessage("success.common.update");
    if (hasErrorFiles()) {
        msg += getErrorMessages();
    }

    popAlertLayer(msg, "/qna");
}
