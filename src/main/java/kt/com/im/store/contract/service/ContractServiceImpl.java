/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.contract.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.store.contract.dao.ContractDAO;
import kt.com.im.store.contract.vo.ContractVO;

/**
 *
 * 과금관리 관련 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 10. 18.   A2TEC      최초생성
 *
 *
 * </pre>
 */

@Service("ContractService")
public class ContractServiceImpl implements ContractService {

    @Resource(name = "ContractDAO")
    private ContractDAO contractDAO;

    /**
     * 과금관리 리스트 조회
     * @param ContractVO
     * @return 조회 목록
     */
    @Override
    public List<ContractVO> contractList(ContractVO vo) {
        return contractDAO.contractList(vo);
    }

    /**
     * 과금관리 리스트 합계 조회
     * @param ContractVO
     * @return 목록 합계
     */
    public int contractListTotalCount(ContractVO vo) {
         return contractDAO.contractListTotalCount(vo);
    }

    /**
     * 과금 계약 상세 정보 조회
     * @param ContractVO
     * @return 상세 정보
     */
    @Override
    public ContractVO contractDetail(ContractVO vo) {
        return contractDAO.contractDetail(vo);
    }

}
