/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

var reloadFlag = true;

$(document).ready(function(){
    if (isFunction("getMessage")) {
        $(".searchInput > input").attr("placeholder", getMessage("input.placeholer.research"));
    }
});

var pageUrl;
var ContextPath;
var glo_url;
init = function(pageUrlTemp,CntxtPath,url){
    pageUrl = pageUrlTemp;
    ContextPath = CntxtPath;
    glo_url = url;
    var chkUrl = url.replace(ContextPath,"");

    if (chkUrl != "/mypage") {
        sessionStorage.removeItem('cofirm-Pwd');
        sessionStorage.setItem("cofirm-last-url", location.href);
    } else {
        formData("NoneForm" , "type", "comfirmEdit");
        formData("NoneForm" , "pwd", sessionStorage.getItem("cofirm-Pwd"));
        callByGet("/api/member/find" , "initFindUserInfo", "NoneForm", "initNotFindUserInfo");
        formDataDeleteAll("NoneForm");
    }
}

function initFindUserInfo(data) {
    $("#loading").hide();
    if (data.resultCode != "1000") {
        $(".inputBox").val("");
        popAlertLayer(getMessage("common.bad.request.msg"), "/home");

//        back
//        var str = sessionStorage.getItem("cofirm-last-url");
//        if(typeof str != "undefined"){
//            location.href = str;
//        }
    }
}

function initNotFindUserInfo(data) {
    $("#loading").hide();
    $(".inputBox").val("");
    popAlertLayer(getMessage("common.bad.request.msg"), "/home");
//    back
}

/**
 * 페이지 이동 함수
 * @param {Object} url URL 주소 값
 */
function pageMove(url){
    // full domain 주소가 아닌 경우에만 contextPath 추가
    if (url.indexOf("http") == -1) {
        url = makeAPIUrl(url);
    }
    //defer 가져오기
    location.href = url;
}

function pageMove_reload(url){
    // full domain 주소가 아닌 경우에만 contextPath 추가
    if (url.indexOf("http") == -1) {
        url = makeAPIUrl(url);
    }
    //defer 가져오기
    location.href = url;
    if(reloadFlag){
        location.reload();
        reloadFlag = false
    }
}


/**
 * Window Resize Event 발생 시에 Popup Layer 위치 변경
 */
function popupResize(layerId) {
    var wHeight = $(window).height();
    var wWidth = $(window).width();
    var sTop = document.body.scrollTop == 0 ? document.documentElement.scrollTop : document.body.scrollTop;
    var sLeft = document.body.scrollLeft == 0 ? document.documentElement.scrollLeft : document.body.scrollLeft;

    /*console.log(sTop+"/"+sLeft);
    console.log(wHeight+"/"+wWidth);*/
    var offsetTop = (wHeight - $("#" + layerId).height()) /2 + sTop;
    var offsetLeft = (wWidth - $("#" + layerId).width()) /2 + sLeft;
    offsetTop = offsetTop > 0 ? offsetTop : 0;
    offsetLeft = offsetLeft > 0 ? offsetLeft : 0;

    $("#" + layerId).offset({
        top: offsetTop,
        left: offsetLeft
    });

}

/**
 * Vertical Middle, Horizontal Center Popup Layer Load
 * (페이지 내 DIV 영역을 popup으로 띄움)
 * @param layerId : DIV id
 * @param width : Popup Layer width
 * @param height : Popup Layer height
 * @param changePosition : Window Resize Event 시에 위치 변경 여부
 */

var x, y, top, left, down, mouseEventOn = false;
function popLayerDiv(layerId, width, height, changePosition) {
    var offsetTop = ($(window).height() - height) /2;
    var offsetLeft = ($(window).width() - width) /2;
    offsetTop = (offsetTop > 0)? offsetTop:0;
    offsetLeft = (offsetLeft > 0)? offsetLeft:0;
    var cssTo = {
            border: 'none',
            top:  offsetTop +300+'px',
            left: offsetLeft+150+ 'px',
            width: '0px',
            height: '0px',
            textAlign   : "",
            cursor:null
        };

    if(layerId == "previewThumnail") {
        changePosition = false;
        cssTo = {
                border: 'none',
                top:  0,
                left: "0%",
                width: '0px',
                height: '0px',
                textAlign   : "",
                cursor:null
        };
        $("#previewThumnail").find(".content").css("padding-bottom","50px");
        $("#previewThumnail").find(".btnGrp").css({position:"absolute",left:"calc(50% - 60px)"});
        $("#thumbView").on("mousedown",function(e) {
            e.preventDefault();
            down=true;
            x=e.pageX;
            y=e.pageY;
            top=$(this).scrollTop();
            left=$(this).scrollLeft();
        });
        mouseEventOn = true;
        $("body").mousemove(function(e) {
            if(down){
                var newX=e.pageX;
                var newY=e.pageY;

                $("#thumbView").scrollTop(top-newY+y);
                $("#thumbView").scrollLeft(left-newX+x);
            }
        });

        $("body").mouseup(function(e){down=false;});
        var popupX = parseFloat(document.body.clientWidth / 2) - parseFloat((preThumMaxW + 416 )/ 2);
        var popupY = parseFloat(document.body.clientHeight / 2) - parseFloat((preThumMaxH + 240.8) / 2);
        $("#previewThumnail").css({"left":popupX,"top":popupY});
    } else {
        if(mouseEventOn) {
            $("body").off("mousemove");
            mouseEventOn = false;
        }
    }

    $.blockUI({
        message: $("#" + layerId),
        css: cssTo
    });

    $("body").css("overflow-y" , "hidden");
    $("body").children().filter(".blockUI").css("cursor", "default");
    if(changePosition) {
        popupResize(layerId);
        $(window).resize(function(){
            if($("#" + layerId).length == 1 && $("#" + layerId).css("display")=="block") {
                popupResize(layerId);
            }
        });
    }
}

/**
 * Popup Layer 닫기
 */
function popLayerClose()
{
    $("body").css("overflow-y" , "visible");
    $.unblockUI();
}


/**
 * Page Navigation 그리기
 *
 * @param totCnt 총 건수
 */
var _blockSize = 10;
function drawPaging(totCnt,offset,limit,searchFunc,pagingDiv) {
    pageNo = toInt(offset) / toInt(limit) + 1;
    pageSize = toInt(limit);
    var totPageCnt = toInt(totCnt / pageSize) + (totCnt % pageSize > 0 ? 1 : 0);
    var totBlockCnt = toInt(totPageCnt / _blockSize) + (totPageCnt % _blockSize > 0 ? 1 : 0);
    var blockNo = toInt(pageNo / _blockSize) + (pageNo % _blockSize > 0 ? 1 : 0);
    var startPageNo = (blockNo - 1) * _blockSize + 1;
    var endPageNo = blockNo * _blockSize;

    if (endPageNo > totPageCnt) {
        endPageNo = totPageCnt;
    }
    var prevBlockPageNo = (blockNo - 1) * _blockSize;
    var nextBlockPageNo = blockNo * _blockSize + 1;

    var strHTML = "<ul>";
    if (totPageCnt > 1 && pageNo != 1) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(1);\"><img src=\"/resources/image/btn_fast_rewind.gif\" width=\"15\" height=\"15\"></a></li>";

    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\"><img src=\"/resources/image/btn_fast_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    if (pageNo > 1) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + (pageNo-1) + ");\" ><img src=\"/resources/image/btn_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_rewind.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    var numberStyle = "", numberClass = "";
    for (var i = startPageNo; i <= endPageNo; i++) {
        numberStyle = (i == pageNo) ? "font-weight:bold;  letter-spacing:-1px;" : "";
        strHTML += "<li><a href=\"javascript:"+searchFunc+"(" + i + ");\" style='color:#3677b2;" + numberStyle + "'>" + i + "</a></li>";

    }
    if (totCnt == 0) {
        strHTML += "<li><a href=\"javascript:search(1);\">1</a></li>";

    }
    if (pageNo < totPageCnt) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + (pageNo+1) + ");\" ><img src=\"/resources/image/btn_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    }
    if (totPageCnt > 1 && pageNo != totPageCnt) {
        strHTML += "<li class=\"btn\"><a href=\"javascript:"+searchFunc+"(" + totPageCnt + ");\" ><img src=\"/resources/image/btn_fast_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    } else {
        strHTML += "<li class=\"btn\"><a href=\"javascript:;\" ><img src=\"/resources/image/btn_fast_forward.gif\" width=\"15\" height=\"15\"></a></li>";
    }

    strHTML += "</ul>";
    $('#'+pagingDiv).html(strHTML);

}
/**
 * 검색 API offset parameter 값 계산
 *
 * @param pageNo 페이지번호
 * @return offset
*/
function getSearchOffset(pageNo) {
    return (pageNo - 1) * toInt($("#limit").val());
}


function toInt(str) {
    var n = null;
    try {
        n = parseInt(str, 10);
    } catch (e) {}
    return n;
}


var CALL_COUNT            = 0;
var CALL_SUCCESS_COUNT    = 0;
var CALL_TOT_COUNT        = 0;
var alertFlag = true;
function jsCallComplete() {
    if ( CALL_COUNT == CALL_TOT_COUNT ) {
        if ( CALL_SUCCESS_COUNT == CALL_COUNT ) {
            if(alertFlag){
                alertFlag = false;
                contentsXmlCreate();
            }
        }else{
            setTimeout("jsCallComplete()", 100);
        }
    }
    else {
        setTimeout("jsCallComplete()", 100);
    }
}
function jsCallComplete1() {
    if ( CALL_COUNT == CALL_TOT_COUNT ) {
        if ( CALL_SUCCESS_COUNT == CALL_COUNT ) {
            if(alertFlag){
                alertFlag = false;
                contentsXmlCreate();
            }
        }else{
            setTimeout("jsCallComplete1()", 100);
        }
    }
    else {
        setTimeout("jsCallComplete1()", 100);
    }
}


function strConv(str){
    str = str.replace(/&lt;/gi,"<");
    str = str.replace(/&gt;/gi,">");
    str = str.replace(/&quot;/gi,"\"");
    //str = str.replace(/&nbsp;/gi," ");
    str = str.replace(/&amp;/gi,"&");
    str = str.replace(/&amp;#034;/gi,"\"");
    str = str.replace(/&#034;/gi, "\"");
    return str;
}



//jqgrid 포인터
function pointercursor(cellvalue, options, rowObject)
{
    var new_formatted_cellvalue = '<span class="pointer">' + cellvalue + '</span>';

    return new_formatted_cellvalue;
}

function userInfoValidateCheck(type, memberType){
    // 등록일때
    if(type == "r"){
        if($("#memberId").val() == ""){
            popAlertLayer("아이디를 입력하세요");
            return false;
        } else if(!duplicationFlag){
            popAlertLayer("아이디를 중복체크해주세요.");
            return false;
        }

        if($("#memberPwd").val() == ""){
            popAlertLayer("비밀번호를 입력해주세요.");
            return false;
        } else if($("#memberPwd").val() !=$("#memberPwdRe").val()){
            popAlertLayer("비밀번호가 일치하지 않습니다.");
            return false;
        } else {
            var pwdRegular = /[a-z|0-9]{6,15}$/gi;
            if(!pwdRegular.test($("#memberPwd").val())){
                popAlertLayer("비밀번호 입력 양식을 확인하여 주십시오.");
                $("#memberPwd").val("");
                $("#pwdValueChk").css("display", "block");
                return false;
            } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
                $("#pwdValueChk").css("display", "block");
                return false;
            }
        }
    }
    // 수정일 때
    else if(type == "e"){
        if($("#memberPwd").val() != ""){
            if($("#memberPwd").val() !=$("#memberPwdRe").val()){
                popAlertLayer("비밀번호가 일치하지 않습니다.");
                return false;
            } else {
                var pwdRegular = /[a-z|0-9]{6,15}$/gi;
                if(!pwdRegular.test($("#memberPwd").val())){
                    popAlertLayer("비밀번호 입력 양식을 확인하여 주십시오.");
                    $("#pwdValueChk").css("display", "block");
                    $("#memberPwd").val("");
                    return false;
                } else if($("#memberPwd").val().search(/[0-9]/g) < 0 || $("#memberPwd").val().search(/[a-z]/g) < 0 ){
                    $("#pwdValueChk").css("display", "block");
                    return false;
                }
            }
        }
    }
    $("#pwdValueChk").css("display", "none");


    if($("#memberName").val() == ""){
        popAlertLayer("고객명을 입력해주세요.");
        return false;
    } else {
        var nameRegular = /^[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]{2,10}$/gi;
        if(!nameRegular.test($("#memberName").val())){
            popAlertLayer("고객명 입력 양식을 확인하여 주십시오.");
            $("#memberName").val("");
            return false;
        }
    }

    // 관리자일때
    if(memberType == "admin"){
        var phoneRegular1 = /[0-9]{2,3}/;
        var phoneRegular2 = /[0-9]{3,4}/;
        var phoneRegular3 = /[0-9]{4}/;
        if(!phoneRegular1.test($("#memberPhone1").val()) || !phoneRegular2.test($("#memberPhone2").val())
            || !phoneRegular3.test($("#memberPhone3").val())){
            popAlertLayer("전화번호를 정확히 입력해 주십시오.");
            return false;
        }
    }

    var mobilePhoneRegular1 = /[0-9]{3}/;
    var mobilePhoneRegular2 = /[0-9]{3,4}/;
    var mobilePhoneRegular3 = /[0-9]{4}/;
    if(!mobilePhoneRegular1.test($("#mobilePhone1").val()) || !mobilePhoneRegular2.test($("#mobilePhone2").val())
        || !mobilePhoneRegular3.test($("#mobilePhone3").val())){
        popAlertLayer("휴대폰 번호를 정확히 입력해 주십시오.");
        return false;
    }

    var email1Regular = /[a-z|0-9]$/gi;
    if(!email1Regular.test($("#memberEmail1").val())){
        popAlertLayer("이메일 입력 양식을 확인하여 주십시오.");
        $("#memberEmail1").val("");
        return false;
    }
    var email2Regular = /([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+)$/;
    if(!email2Regular.test($("#memberEmail2").val())){
        popAlertLayer("이메일 입력 양식을 확인하여 주십시오.");
        $("#memberEmail2").val("");
        return false;
    }
    return true;
}

function cutString(object, maxLength){
    var strLength = 0;
    var newStr = '';
    var str = "";
    if(typeof object == "object"){
        str = object.value;
    } else {
        str = object;
    }

    for (var i=0;i<str.length; i++) {
        var n = str.charCodeAt(i);
        var nv = str.charAt(i); // charAt : string 개체로부터 지정한 위치에 있는 문자를 꺼낸다.
        if ((n>= 0)&&(n<256)) {
            strLength ++; // ASCII 문자코드 set.
        } else {
            strLength += 2; // 한글이면 2byte로 계산한다.
        }

        if (strLength>maxLength) {
            break; // 제한 문자수를 넘길경우.
        } else {
            newStr = newStr + nv;
        }
    }

    if(typeof object == "object"){
        object.value = newStr;
    } else {
        return newStr;
    }
}

function valueFormatter(value, length){
    if(value.length <= length){
        return value;
    } else {
        return value.substring(0, length) + "...";
    }
}

/*str에서 해당 parameter 값 가져오기 ex) page=5&val=5 */
function findGetParameter(str,parameterName) {
    var result = null,
        tmp = [];
    var items = str.split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}
/*뒤로가기*/

/*목록(뒤로가기)*/
function listBack(obj,str){
    if(typeof str != "undefined"){
        location.href = str;
    }

    if(sessionStorage.getItem("state") == "view"){
        $(obj).attr("onclick","listBack(this,'"+sessionStorage.getItem("last-url")+"')");
    }
}

/* 목록(뒤로가기) - 수정버전*/
function setListButton(options) {
    var obj = options.obj; // Jquery 오브젝트
    var defaultUri = options.defaultUri; // 기본 uri
    var extraAllowedUriList = options.extraAllowedUriList; // 추가적으로 세션을 허용하는 uri
    var useConfirmPopup = options.useConfirmPopup || false; // Confirm 팝업 사용 여부
    var confirmPopupMsg = options.useConfirmPopupMsg || "common.cancel.msg"; // Confirm 팝업 표시 메세지 아이디
    if (defaultUri == "" || defaultUri == null || defaultUri == undefined) {
        console.log("defaultUri is required");
        return;
    }

    var clickFunction;
    var isAllowedSessonUrl = true;
    var sessionUrl = sessionStorage.getItem("last-url");
    if (sessionStorage.getItem("state") != "view" || sessionUrl == null || sessionUrl == undefined || sessionUrl == "") {
        isAllowedSessonUrl = false;
    } else {
        var isAllowedSessonUrl = (sessionUrl.indexOf(defaultUri) != -1);
        if (!isAllowedSessonUrl && extraAllowedUriList) {
            $(extraAllowedUriList).each(function(i, item) {
                if (sessionUrl.indexOf(item) != -1) {
                    isAllowedSessonUrl = true;
                    return false;
                }
            });
        }
    }

    if (useConfirmPopup) {
        if (isAllowedSessonUrl) {
            clickFunction = function() {
                popConfirmLayer(getMessage(confirmPopupMsg), function() {
                    location.href = sessionUrl;
                }, null, getMessage("button.confirm"));
            };
        } else {
            clickFunction = function() {
                popConfirmLayer(getMessage(confirmPopupMsg), function() {
                    location.href = makeAPIUrl(defaultUri);
                }, null, getMessage("button.confirm"));
            };
        }
    } else {
        if (isAllowedSessonUrl) {
            clickFunction = function() { location.href = sessionUrl; };
        } else {
            clickFunction = function() { location.href = makeAPIUrl(defaultUri); };
        }
    }

    $(obj).removeAttr("onclick");

    $(obj).click(clickFunction);
}

/*글자 조회*/
function setkeyup(){
    $('.remaining').each(function() {
        var $maximumCount = $(this).attr("max") * 1;
        var $input = $(this);
        var update = function() {
            var now = $maximumCount - $input.val().length;
            // 사용자가 입력한 값이 제한 값을 초과하는지를 검사한다.
            if (now < 0) {
                var str = $input.val();
                popAlertLayer(getMessage("common.input.max.limit.first.msg") + ' ' + $maximumCount + getMessage("common.input.max.limit.second.msg"));
                $input.val(str.substr(0, $maximumCount));
                $input.val($input.val().replace(/[\<>&\"']/gi, ''));
                now = 0;
            }
        }
        $input.bind('input keyup paste', function() {
            setTimeout(update, 0)
        });
        update();
    });
}

/* ul header*/
function setHeaderUse(){
    var firstName = $(".leftMenuOn").text().trim();
    var secondName = $(".leftSubMenuOn").text().trim();
    var thirdName = $("#tabGrp .tabOn").text().trim() || $(".conTitle.tabOn").text().trim() || $(".conTitle").text().trim();

    var firstUrl = $(".leftMenuOn").next(".leftSubMenu").children("a").attr("onclick");
    var secondUrl = $(".leftSubMenuOn a").attr("onclick");
    var thirdUrl = "window.location.reload()";

    var sVal = $("#leftSubMenuNum").val();

    $("div.locate > ul").html("");
    $("div.locate > ul").append("<li class='iconHome'>&nbsp;</li>");
    var hasPrevious = false;
    if (firstName) {
        hasPrevious = true;
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + firstUrl + "\">" + firstName + "</li>");
    }
    if (sVal) {
        if (hasPrevious) {
            $("div.locate > ul").append("<li><img src='" + makeAPIUrl("/resources/image/gts.gif") + "'>&nbsp;</li>");
        }
        hasPrevious = true;
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + secondUrl + "\">" + secondName + "</li>");
    }

    if (thirdName) {
        if (hasPrevious) {
            $("div.locate > ul").append("<li><img src='" + makeAPIUrl("/resources/image/gts.gif") + "'>&nbsp;</li>");
        }
        $("div.locate > ul").append("<li style='cursor:pointer' onclick=\"" + thirdUrl + "\">" + thirdName + "</li>");
    }
}

function resizeJqGridWidth(grid_id, div_id){
    $(window).bind("resize", function() { // 그리드의 width 초기화
        var resizeWidth = $("#gridArea").width();

        $("#" + grid_id).setGridWidth(resizeWidth, true);
    }).trigger("resize");
}


function keyup(){
    $(".onlynum").keyup(function(e) {
        $(this).val($(this).val().replace(/[^\d]+/g, ''));
    });
}

function dateString(dayVal){
    var str = dayVal.getFullYear()+"-";
    var month = dayVal.getMonth()+1;
    var day = dayVal.getDate();
    if(month <10){        str += "0";        }
    str += (dayVal.getMonth()+1)+"-";
    if(day <10){        str += "0";        }
    str += dayVal.getDate();

    return str;
}

// 1년 뒤 날짜 리턴
function nextYearDateString(dayVal){
    var str = (dayVal.getFullYear()+1)+"-";
    var month = dayVal.getMonth()+1;
    var day = dayVal.getDate();
    if(month <10){        str += "0";        }
    str += (dayVal.getMonth()+1)+"-";
    if(day <10){        str += "0";        }
    str += dayVal.getDate();

    return str;
}

function dateTimeString(dayVal){
    var str = dayVal.getFullYear()+"-";
    var month = dayVal.getMonth()+1;
    var day = dayVal.getDate();
    if(month <10){        str += "0";        }
    str += (dayVal.getMonth()+1)+"-";
    if(day <10){        str += "0";        }
    str += dayVal.getDate();

    var hour = dayVal.getHours();
    str += " ";
    if(hour <10){        str += "0";        }
    str += hour+":";

    var minute = dayVal.getMinutes();
    if(minute <10){        str += "0";        }
    str += minute;

    return str;
}


function is_number(x)

{

    var reg = /^\d+$/;

    return reg.test(x);

}

function validatePassword(value, id) {
    var pwdRegular = /^(?=.*[0-9])(?=.*[a-zA-Z])[0-9a-zA-Z]{10,16}$/g;
    if (!pwdRegular.test(value)) {
        return "invalidSyntax"
    }

    var continuousCharRegular = /(\w)\1\1\1/;
    if (continuousCharRegular.test(value)) {
        return "continuousChar";
    }

    if (id && id !== undefined && id.length >= 4) {
        for (var i = 0; i + 3 < id.length; i++) {
            var idPart = id.substring(i, i + 4);
            if (value.indexOf(idPart) != -1) {
                return "containsIdPart";
            }
        }
    }

    return "valid";
}

function validateId(str) {
    if (!str || str.length <=0) {
        return false;
    }
    var regStr = /^[a-z]+[a-z|0-9]{5,14}$/g;
    return regStr.test(str);
}

function validateName(str) {
    if (!str || str.length <= 0) {
        return false;
    }
    var regStr = /^[가-힣a-zA-Z]{2,10}$/gi;
    return regStr.test(str);
}

function validateTelNumber(str) {
    if (!str) {
        return false;
    }

    str = str.trim();
    var temp = str.split('-').join('');
    if (temp.length <= 0 || temp.length < 9 || temp.length > 11) {
        return false;
    }
    var regStr = /^(02)-?([1-9]{1}[0-9]{2,3})-?[0-9]{4}$|(0505|0[3-8]{1}[0-5]{1})-?([1-9]{1}[0-9]{2,3})-?[0-9]{4}$/;
    return regStr.test(str);
}

function validatePhoneNumber(str) {
    if (!str) {
        return false;
    }

    str = str.trim();
    var temp = str.split('-').join('');
    if (temp.length <= 0 || temp.length < 10 || temp.length > 11) {
        return false;
    }
    var regStr = /^(01[16789]{1})-?([1-9]{1}[0-9]{2,3})-?[0-9]{4}$|(010)-?([1-9]{1}[0-9]{3})-?[0-9]{4}$/;
    return regStr.test(str);
}

function phoneNumberFormatter(num) {
    if (!num) {
        return num;
    }
    num = num.trim();
    return num.replace(/-/g, "").replace(/(^02.{0}|^0505.{0}|^01.{1}|[0-9]{3})([0-9]+)([0-9]{4})/,"$1-$2-$3");
}

function validateBizNo(bizID) {
    if (!bizID) {
        return false;
    }

    // bizID는 숫자만 10자리로 해서 문자열로 넘긴다.
    var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
    var tmpBizID;
    var i;
    var chkSum = 0;
    var c2;
    var remander;
    bizID = bizID.replace(/-/gi,'');

    for (i=0; i<=7; i++)
        chkSum += checkID[i] * bizID.charAt(i);

    c2 = "0" + (checkID[8] * bizID.charAt(8));
    c2 = c2.substring(c2.length - 2, c2.length);
    chkSum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
    remander = (10 - (chkSum % 10)) % 10 ;

    if (Math.floor(bizID.charAt(9)) == remander)
        return true ; // OK!

    return false;
}

function bizNoFormatter(num) {
    if (!num) {
        return num;
    }
    return num.replace(/-/g, "").replace(/(\d{3})(\d{2})(\d{5})/g, "$1-$2-$3");
}

function validateEmail(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var regStr = /^([0-9a-zA-Z+_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;
    return regStr.test(str);
}

function validateEmailId(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var regStr = /^([0-9a-zA-Z+_\.-]+)$/;
    return regStr.test(str);
}

function validateEmailDomain(str) {
    if (!str || str.length <= 0) {
        return false;
    }

    var regStr = /^([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;
    return regStr.test(str);
}

function checkUndefined(str){
    if (typeof str == "undefined") {
        return "";
    } else {
        return str;
    }

}

function checkNull(str) {
    if( str == "" || str == null || typeof str == "undefined" || !str || 0 === str.length){
        return "";
    } else {
        return str;
    }
}

function integerInRange(value, min, max, name) {

    if(value < min || value > max)
    {
        alert("Write here your message");
    }
}

function addEmailDomainList(obj) {
    if (obj == "undefined" || (obj.tagName != "SELECT")) {
        return;
    }

    var msgFuntion = window["getLoginMessage"] || window["getMessage"];
    var listHtml = "<option value=''>" + msgFuntion('login.select.direct') + "</option>";
    listHtml += "<option value='naver.com'>naver.com</option>";
    listHtml += "<option value='gmail.com'>gmail.com</option>";
    listHtml += "<option value='nate.com'>nate.com</option>";
    listHtml += "<option value='yahoo.co.kr'>yahoo.co.kr</option>";
    listHtml += "<option value='hanmail.net'>hanmail.net</option>";
    listHtml += "<option value='daum.net'>daum.net</option>";
    listHtml += "<option value='dreamwiz.com'>dreamwiz.com</option>";
    listHtml += "<option value='lycos.co.kr'>lycos.co.kr</option>";
    listHtml += "<option value='empal.com'>empal.com</option>";
    listHtml += "<option value='korea.com'>korea.com</option>";
    listHtml += "<option value='paran.com'>paran.com</option>";
    listHtml += "<option value='freechal.com'>freechal.com</option>";
    listHtml += "<option value='hitel.net'>hitel.net</option>";
    listHtml += "<option value='hanmir.com'>hanmir.com</option>";
    listHtml += "<option value='hotmail.com'>hotmail.com</option>";

    $(obj).html(listHtml);
}

downloadExcelFile = function(uri, params) {
    var gridId = params.gridId || "jqgridData";
    if ($("#" + gridId).length && parseInt($("#" + gridId).getGridParam("records")) <= 0) {
        popAlertLayer(getMessage("info.nodata.excel.msg"));
        return;
    }

    var form = document.forms[0];
    var prevTarget = form.target;
    var prevAction = form.action;

    $("#excelForm").remove();
    $("body").append("<iframe id='excelForm' src='' width='0' height='0' frameborder='0' scrolling='no'></iframe>");

    form.target = "excelForm";
    form.action = makeAPIUrl(uri);

    Object.keys(params).forEach(function(key) {
        formData("NoneForm" , key, params[key]);
    })

    form.submit();

    formDataDeleteAll("NoneForm");

    form.target = prevTarget;
    form.action = prevAction;
}

/* XSS 값 replace*/
function xssChk(str){
    var text = str;
    var decoded = $('<div/>').html(str).text();

    return decoded;
}

function registNo(use,url) {
    if (use == "N") {
        $(".btnWrite").attr("onclick","popAlertLayer('"+getMessage("info.nodata.msg")+"')");
    } else {
        $(".btnWrite").attr("onclick","pageMove('"+url+"')");
    }
}

function getTermInfo() {
    var termStr = "";
    var msgFunction = window["getLoginMessage"] || window["getMessage"];
    if (typeof msgFunction != "function") {
        return termStr;
    }
    termStr = msgFunction("info.term.1st") + msgFunction("info.term.2nd") + msgFunction("info.term.3th") + msgFunction("info.term.4th")
                    + msgFunction("info.term.5th") + msgFunction("info.term.6th") + msgFunction("info.term.7th") + msgFunction("info.term.8th")
                    + msgFunction("info.term.9th") + msgFunction("info.term.10th") + msgFunction("info.term.11th") + msgFunction("info.term.12th")
                    + msgFunction("info.term.13th") + msgFunction("info.term.14th") + msgFunction("info.term.15th") + msgFunction("info.term.16th")
                    + msgFunction("info.term.17th") + msgFunction("info.term.18th") + msgFunction("info.term.19th") + msgFunction("info.term.20th")
                    + msgFunction("info.term.sub");
    return termStr;
}

function getPolicyInfo() {
    var policyStr = "";
    var msgFunction = window["getLoginMessage"] || window["getMessage"];
    if (typeof msgFunction != "function") {
        return policyStr;
    }
    policyStr = msgFunction("info.policy.1st") + msgFunction("info.policy.2nd") + msgFunction("info.policy.3th") + msgFunction("info.policy.4th")
                    + msgFunction("info.policy.5th") + msgFunction("info.policy.6th") + msgFunction("info.policy.7th") + msgFunction("info.policy.8th")
                    + msgFunction("info.policy.9th") + msgFunction("info.policy.10th") + msgFunction("info.policy.11th");
    return policyStr;
}

function replaceAll(str, searchStr, replaceStr) {
    return str.split(searchStr).join(replaceStr);
}

//IE 10 버전 이하에서 SweetAlert Layer에 의해 이벤트 차단되는 문제 해결
function setVisibilitySweetAlert(visible) {
    if (!$(".swal-overlay").length) {
        return;
    }

    if (navigator.appName != 'Microsoft Internet Explorer') {
        return;
    }

    var agent = navigator.userAgent.toLowerCase();
    if (agent.indexOf("msie") == -1 || (agent.indexOf("9.0") == -1 && agent.indexOf("10.0") == -1)) {
        return;
    }

    if (visible) {
        $(".swal-overlay").show();
    } else {
        $(".swal-overlay").hide();
    }
}

/**
 * Client에서 개인정보 마스킹 처리 코드
 * Server 단 처리로 현재 사용하지 않는 코드이지만 소스 내 호출 코드가 있어,
 * 원형만 유지하고 전달 받은 값을 그대로 return 하도록 함.
 * 또한 Client 단에서 처리 필요 시 사용토록 주석으로 코드 유지함.
 * @param name
 * @returns
 */
//회원 이름 숨김 처리 - 뒤 1자리 (이름 길이의 1/3 영역 숨김)
function maskName(name) {
    /*
    if (name == undefined || name === '') {
        return '';
    }

    var nameLen = name.length;
    var maskLen = Math.round(nameLen / 3);
    var nonMaskStr = name.substr(0, nameLen - maskLen);
    var maskStr = name.substr(nameLen - maskLen, nameLen);

    for (var i = 0; i < maskStr.length; i++) {
        maskStr = maskStr.replace(maskStr.substring(i, i+ 1), "*");
    }

    return nonMaskStr + maskStr;
    */
    return name;
}

// 회원 아이디 숨김 처리 - 뒤 3자리
function maskId(id) {
    /*
    if (id == undefined || id === '') {
        return '';
    }

    var pattern = /.{3}$/; // 뒤에 3자리 마스킹
    return id.replace(pattern, "***");
    */
    return id;
}

// 회원 email 숨김 처리 - email id 뒤 3자리
function maskEmail(email) {
    /*
    if (email == undefined || email === '') {
        return '';
    }

    var emailArray = email.split("@");
    var pattern;

    if (emailArray[0].length < 4) {
        pattern = /.$/;
        emailArray[0] = emailArray[0].replace(pattern, "*");
    } else {
        pattern = /.{3}$/;
        emailArray[0] = emailArray[0].replace(pattern, "***");
    }

    var retEmail = "";
    for (var i = 0; i < emailArray.length; i++) {
        retEmail += emailArray[i];
        if (i == 0) {
            retEmail += "@";
        }
    }

    return retEmail;
    */
    return email;
}

// 회원 전화번호, 휴대전화번호 숨김 처리 - 국번 뒤 2자리, 번호 앞 1자리
function maskTelNum(telno) {
    /*
    if (telno == undefined || telno === '') {
        return '';
    }

    var telnoArray = telno.split("-");
    var pattern;

    var patten;
    pattern = /.{2}$/;
    telnoArray[1] = telnoArray[1].replace(pattern, "**");

    pattern = /.{1}/;
    telnoArray[2] = telnoArray[2].replace(pattern, "*");

    return telnoArray[0] + "-" + telnoArray[1] + "-" + telnoArray[2];
    */
    return telno;
}

// IP Address 숨김 처리 - ex) ***.123.***.123
function maskIpAddr(ipAddr) {
    /*
    if (ipAddr == undefined || ipAddr === '') {
        return '';
    }

    var ipAddrArray = ipAddr.split(".");
    ipAddrArray[0] = ipAddrArray[0].replace(ipAddrArray[0], "***");
    ipAddrArray[2] = ipAddrArray[2].replace(ipAddrArray[2], "***");

    return ipAddrArray[0] + "." + ipAddrArray[1] + "." + ipAddrArray[2] + "." + ipAddrArray[3];
    */
    return ipAddr;
}

// 이름(아이디) 숨김 처리
function maskNamePlusId(str) {
    /*
    if (str == undefined || str === '') {
        return '';
    }

    if ( str.indexOf("(") == -1) {
        return str;
    }

    var strArray = str.split("(");
    strArray[1] = strArray[1].slice(0, -1);

    return maskName(strArray[0]) + "(" + maskId(strArray[1]) + ")";
    */
    return str;
}

function replaceAll(str, searchStr, replaceStr) {
    return str.split(searchStr).join(replaceStr);
}

function coverThumbSizes() {
    return '640x480,320x240,300x225,240x180,100x75';
}

function galleryThumbSizes() {
    return '584x438,368x276,240x180,160x120,80x60';
}

function imgToThumbImg(str) {
    // 현재 페이지의 모든 이미지에 대해 img 표시 사이즈로 썸네일 이미지를 불러와 교체
    $('img[src*="/api/imgUrl"]:not([class^="' + str + '"])').each(function(){
        console.log($(this).css("width"));
        console.log($(this).css("height"));
        $(this).attr('src',$(this).attr("src").replace("/imgUrl?", "/imgUrl?width=" + $(this).css("width").replace("px","") + "&height=" + $(this).css("height").replace("px","") + "&"));
    });
}