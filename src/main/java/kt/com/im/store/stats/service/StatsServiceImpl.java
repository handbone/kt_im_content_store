/*
 * IM Platform version 1.0
 *
 *  Copyright ⓒ 2018 kt corp. All rights reserved.
 *
 *  This is a proprietary software of kt corp, and you may not use this file except in
 *  compliance with license agreement with kt corp. Any redistribution or use of this
 *  software, with or without modification shall be strictly prohibited without prior written
 *  approval of kt corp, and the copyright notice above does not evidence any actual or
 *  intended publication of such software.
 */

package kt.com.im.store.stats.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import kt.com.im.store.stats.dao.StatsDAO;
import kt.com.im.store.stats.vo.StatsVO;

/**
 *
 * 회원 관리에 관한 서비스 클래스를 정의한다
 *
 * @author A2TEC
 * @since 2018.05.21
 * @version 1.0
 * @see
 *
 *      <pre>
 * << 개정이력(Modification Information) >>
 * 수정일      수정자           수정내용
 * -------    -------------    ----------------------
 * 2018. 5. 21.   A2TEC      최초생성
 *
 *
 *      </pre>
 */

@Service("StatsService")
public class StatsServiceImpl implements StatsService {

    @Resource(name = "StatsDAO")
    private StatsDAO statsDAO;

    /**
     * 콘텐츠 사용 정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<StatsVO> getContentUsageList(StatsVO vo) throws Exception {
        return statsDAO.getContentUsageList(vo);
    }

    /**
     * 콘텐츠 사용 정보 목록 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentUsageListTotalCount(StatsVO vo) throws Exception {
        return statsDAO.getContentUsageListTotalCount(vo);
    }

    /**
     * 콘텐츠 사용 상세정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getContentUsageDetailList(StatsVO vo) throws Exception {
        return statsDAO.getContentUsageDetailList(vo);
    }

    /**
     * 콘텐츠 사용 정보 상세목록 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentUsageDetailListTotalCount(StatsVO vo) throws Exception {
        return statsDAO.getContentUsageDetailListTotalCount(vo);
    }

    /**
     * 콘텐츠 다운로드 정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<Map> getContentDownloadList(StatsVO vo) throws Exception {
        return statsDAO.getContentDownloadList(vo);
    }

    /**
     * 콘텐츠 다운로드 정보 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentDownloadListTotalCount(StatsVO vo) throws Exception {
        return statsDAO.getContentDownloadListTotalCount(vo);
    }

    /**
     * 콘텐츠 다운로드 상세정보 목록 조회
     *
     * @param StatsVO
     * @return 조회 목록 결과
     */
    @Override
    public List<StatsVO> getContentDownloadDetailList(StatsVO vo) throws Exception {
        return statsDAO.getContentDownloadDetailList(vo);
    }

    /**
     * 콘텐츠 다운로드 정보 상세목록 총 개수 조회
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public int getContentDownloadDetailListTotalCount(StatsVO vo) throws Exception {
        return statsDAO.getContentDownloadDetailListTotalCount(vo);
    }

    /**
     * 콘텐츠 검수현황 통계
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<StatsVO> contsStusStat(StatsVO vo) {
        return statsDAO.contsStusStat(vo);
    }

    /**
     * 콘텐츠 사용현황 (TOP 5) 통계
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<StatsVO> contsUseStus(StatsVO vo) {
        return statsDAO.contsUseStus(vo);
    }

    /**
     * 콘텐츠 다운현황 (TOP 5) 통계
     *
     * @param StatsVO
     * @return 조회 결과
     */
    @Override
    public List<StatsVO> contsDownTopStus(StatsVO vo) {
        return statsDAO.contsDownTopStus(vo);
    }
}
