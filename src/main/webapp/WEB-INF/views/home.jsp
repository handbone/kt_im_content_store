<!--
   IM Platform version 1.0

   Copyright ⓒ 2018 kt corp. All rights reserved.

   This is a proprietary software of kt corp, and you may not use this file except in
   compliance with license agreement with kt corp. Any redistribution or use of this
   software, with or without modification shall be strictly prohibited without prior written
   approval of kt corp, and the copyright notice above does not evidence any actual or
   intended publication of such software.
 -->

<%@ page contentType="text/html;charset=UTF-8"%>
<%@include file="/WEB-INF/views/frame/left_menu.jsp" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/views/home.js"></script>
<input type="hidden" id="leftNum" value="0">
<input type="hidden" id="leftSubMenuNum" value="0">
<input type="hidden" name="mbrSe" id="mbrSe" value="${memberSec}" />
<input type="hidden" name="cpSeq" id="cpSeq" value="${cpSeq}" />

<!-- right contents S -->
<div id="contents">
    <div class="boxRound">
        <!-- locate S -->
        <div id="locateGrp">
            <div class="locate">
                <ul>
                </ul>
            </div>
        </div>
        <!-- locate E -->

        <!-- Tab S -->
        <div id="tabGrp">
            <div class="tab tabOn tabEnd"><spring:message code="home.title"/></div>
            <!-- Select box S -->
            <div class="rsvGrp">
                <div class="rsvSelect">
                    <span style='font-size:14px; vertical-align: middle;'><spring:message code="title.cp"/> &nbsp;</span>
                    <span>
                        <select id="cpSelbox" onchange="cpStatsReload();"></select>
                    </span>
                </div>
            </div>
            <!-- Select box E -->
        </div>
        <!-- Tab E -->

        <!-- right content S -->
        <div class="content">
            <div class="conTitleGrp">
                <h2>
                    <a href="javascript:pageMove('/contents');"><spring:message code="home.stat.verify.title"/></a>
                </h2>
            </div>
            <div id="chartjs-tooltip">
            <table></table>
        </div>
            <div class="graphGrp first-chart-container">

            </div>

            <div class="conTitleGrp">
                <h2>
                    <a href="javascript:pageMove('/contents/usage');">
                        <spring:message code="home.stat.use.top5.title"/>
                    </a>
                </h2>
            </div>
            <div class="graphGrp second-chart-container">

            </div>

            <div class="conTitleGrp">
                <h2>
                    <a href="javascript:pageMove('/contents/download');">
                        <spring:message code="home.stat.download.top5.title"/>
                    </a>
                </h2>
            </div>
            <div class="graphGrp third-chart-container">

            </div>
        </div>
        <!-- right content E -->
    </div>
</div>
<!-- right contents E -->

<%@include file="/WEB-INF/views/frame/footer.jsp" %>
